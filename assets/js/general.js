function gotoHome(){
	window.location.href = $('#baseUrl').val();
}
;(function($, window, document, undefined) {
	$(document).ready(function() {
		var baseUrl = $('#baseUrl').val();
		var now = moment();

		var d = new Date();
		var n = d.getTimezoneOffset();
		var ans = new Date(d.getTime() + n * 60 * 1000);
		var f = ans.getHours()+':'+ans.getMinutes()+':'+ans.getSeconds()
		
		var dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'numeric', day: '2-digit' });
		[{ value: mo },,{ value: da },,{ value: ye }] = dtf.formatToParts(d) ;
		var bb = `${ye}-${mo}-${da}`;
		var newnow =  bb+' '+f;

		$('.displayMomentDate').each(function(i, e) {
			var time = moment($(e).attr('datetime'));
			// console.log(now.diff(time, 'days'));
			//if(now.diff(time, 'days') <= 1) {
				$(e).html('<span>Published: ' + time.from(newnow) + '</span>');
			//}
		});
		$('.displayMomentDateOnly').each(function(i, e) {
			var time = moment($(e).attr('datetime'));
			// console.log(now.diff(time, 'days'));
			//if(now.diff(time, 'days') <= 1) {
				$(e).html('<span>' + time.from(newnow) + '</span>');
			//}
		});
	
		var max_fields = 20; //maximum input boxes allowed
		var wrapper = $(".input_fields_wrap"); //Fields wrapper
		var add_button = $(".add_field_button"); //Add button ID
		
		var x = 1; //initlal text box count
		$(add_button).click(function(e) { //on add input button click
			e.preventDefault();
			if (x < max_fields) { //max input box allowed
				x++; //text box increment
				$(wrapper).append('<div><input type="text" class="form-control mt10" name="option[]" placeholder="Enter option" /><a href="#" class="remove_field text-danger btn btn-sm">Remove</a></div>'); //add input box
			}
		});
		
		$(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
			e.preventDefault();
			$(this).parent('div').remove();
			x--;
		});
		
		$(document).delegate(".btnFollowUnfollow", "click", function (event) {
			event.preventDefault();
			var _ele = $(this);
			var followerID = $(this).attr('data-id');
			var status = $(this).attr('data-status');
			if(followerID){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/followUnfollowUser',
					data: 'followerID=' + followerID + '&status=' + status,
					success: function(response) {
						var result = JSON.parse(response);
						if(result.status==1){
							if(result.followStatus==1){
								
								$('#userFollow'+followerID).hide();
								$('#userUnfollow'+followerID).show();
							}else if(result.followStatus==4){
								$('#userfollowRequest'+followerID).hide();
								$('#userFollow'+followerID).hide();
								$('#userUnfollow'+followerID).show();
							}else{
								$('#userUnfollow'+followerID).hide();
								$('#userFollow'+followerID).show();
							}
						}else{
							alert(result.error);
						}
					}
				});
			}
		});

		$(document).delegate(".getPollAnswer", "click", function (event) {
			event.preventDefault();
			var _ele = $(this);
			var postID = $(this).attr('data-postid');
			var optionID = $(this).attr('data-id');
			if(postID){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/savePollAnswer',
					data: 'postID=' + postID + '&optionID=' + optionID,
					success: function(response) {
						var result = JSON.parse(response);
						if(result.status==1){
							$('.pollOptionDiv'+postID).hide();
							$('.pollAnswerDiv'+postID).html(result.content);
							$('.pollAnswerDiv'+postID).show();
						}else{
							alert(result.error);
						}
					}
				});
			}
		});

		// for update user online status
		setInterval(function() {
			$.ajax({
				type: "post",
				url: baseUrl+'user/updateUserLiveStatus',
				dataType: 'JSON',
				success: function(response) {
				}
			});
		}, 60000);

		$(document).delegate(".delete_post", "click", function (event) {
			var postID = $(this).attr('data-id');
			if(postID){
				$.ajax({
					type: "post",
					url: baseUrl+'delete-this-post',
					data: {postID: postID},
					success: function(response){
						$('#divPost_'+postID).hide();
					}
				});
			}
		});

		$(document).delegate(".unquote_post", "click", function (event) {
			var postID = $(this).attr('data-id');
			if(postID){
				$.ajax({
					type: "post",
					url: baseUrl+'home/unquotePost',
					data: {postID: postID},
					success: function(response){
						$('#divSharePost_'+postID).hide();
					}
				});
			}
		});

		$(document).delegate(".hide_post", "click", function (event) {
			var postID = $(this).attr('data-id');
			if(postID){
				$.ajax({
					type: "post",
					url: baseUrl+'delete-this-post',
					data: {postID: postID},
					success: function(response){
						$('#divPost_'+postID).hide();
					}
				});
			}
		});
		
		$(document).delegate(".edit_post", "click", function (event) {
			var postID = $(this).attr('data-id');
			var posttype = $(this).attr('data-posttype');
			if(postID){
				$.ajax({
					type: "post",
					url: baseUrl+'home/edit_post_data',
					data: {postID: postID,posttype:posttype},
					success: function(response){
						$('#Modeleditpost .modal-body').html(response);
						$('#Modeleditpost').modal('show');
					}
				});
			}
		});
		
		$(document).delegate(".report_post", "click", function (event) {
			var postID = $(this).attr('data-id');
			var reporttype = $(this).attr('data-reporttype');
			if(postID){
				$.ajax({
					type: "post",
					url: baseUrl+'delete-this-post',
					data: {postID: postID,reporttype:reporttype},
					success: function(response){
						
					}
				});
			}
		});

		$(document).delegate(".loadCommentSection", "click", function (event) {
            event.preventDefault();
			var mainPostDiv = $(this).parents('.singlePostView');
			var postID = $(this).attr('data-id');
			var userid = $(this).attr('data-userid');
			var postShareID = $(this).attr('data-postshareid');
			if(postID){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/loadPostComment',
					data: 'postID=' + postID + '&userid=' + userid + '&postShareID=' + postShareID,
					success: function(response) {
						// $('#viewCommentList_'+postID).html(response);
						// $('#viewCommentList_'+postID).parent('.coment-area').fadeToggle();
						mainPostDiv.find('#viewCommentList_'+postID).html(response);
						mainPostDiv.find('#viewCommentList_'+postID).parent('.coment-area').fadeToggle();
					}
				});
			}
		});
		
		$(document).delegate(".commentForm", "submit", function (event){
			event.preventDefault();
			// var postID = $(this).attr('data-id');
			var mainPostDiv = $(this).parents('.singlePostView');
			var userid = "";
			var postID = $(this).find('.postID').val();
			var formData = new FormData(this);
			if(formData){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/savePostComment',
					data: formData,
					contentType: false,
					cache: false,
					processData:false,
					success: function(response) {
						var result = JSON.parse(response);
						if(result.status==1){
							mainPostDiv.find('#postCommentCount'+postID).text(result.totalCommentCount);
							if(postID){
								$.ajax({
									type: 'POST',
									url: baseUrl+'home/loadPostComment',
									data: 'postID=' + postID + '&userid=' + userid,
									success: function(response) {
										mainPostDiv.find('#viewCommentList_'+postID).html(response);
										mainPostDiv.find('#viewCommentList_'+postID).parent('.coment-area').fadeToggle();
									}
								});
							}
						}else{
							alert(result.error);
						}
					}
				});
			}
		});

		$(document).delegate(".postLike", "click", function (event){
			event.preventDefault();
			var _ele = $(this);
			var postID = $(this).attr('data-id');
			var userid = $(this).attr('data-userid');
			var postShareID = $(this).attr('data-postshareid');
			if(postID){ 
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/likePost',
					data: 'postID=' + postID + '&userid=' + userid + '&postShareID=' + postShareID,
					success: function(response) {
						var result = JSON.parse(response);
						if(result.status==1){
							$('#postLikeCount'+postID).text(result.totalLikeCount);
							if(result.likestatus==1){
								_ele.addClass('happy');
							}else{
								_ele.removeClass('happy');
							}
						}else{
							alert(result.error);
						}
					}
				});
			}
		});

		$(document).delegate(".postDislike", "click", function (event){
			event.preventDefault();
			var _ele = $(this);
			var postID = $(this).attr('data-id');
			var userid = $(this).attr('data-userid');
			var postShareID = $(this).attr('data-postshareid');
			if(postID){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/disLikePost',
					data: 'postID=' + postID + '&userid=' + userid + '&postShareID=' + postShareID,
					success: function(response) {
						var result = JSON.parse(response);
						if(result.status==1){
							$('#postDislikeCount'+postID).text(result.totalDislikeCount);
							if(result.dislikestatus==1){
								_ele.addClass('happy');
							}else{
								_ele.removeClass('happy');
							}
						}else{
							alert(result.error);
						}
					}
				});
			}
		});

		$(document).delegate(".likePostComment", "click", function (event){
			event.preventDefault();
			var _ele = $(this);
			var commentID = $(this).attr('data-id');
			var postID = $(this).attr('data-postid');
			var postShareID = $(this).attr('data-postshareid');
			// console.log(commentID); return false;
			// var userid = $(this).attr('data-userid');
			var userid = '';
			if(postID){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/likePostComment',
					data: 'postID=' + postID + '&postCommentID=' + commentID + '&userid=' + userid + '&postShareID=' + postShareID,
					success: function(response) {
						var result = JSON.parse(response);
						if(result.status==1){
							$('#postCommentlikeCount'+commentID).text(result.totalLikeCount);
							if(result.likestatus==1){
								_ele.addClass('active');
							}else{
								_ele.removeClass('active');
							}
						}else{
							alert(result.error);
						}
					}
				});
			}
		});

		$(document).delegate(".dislikePostComment", "click", function (event){
			event.preventDefault();
			var _ele = $(this);
			var commentID = $(this).attr('data-id');
			var postID = $(this).attr('data-postid');
			var postShareID = $(this).attr('data-postshareid');
			var userid = '';
			if(postID){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/dislikePostComment',
					data: 'postID=' + postID + '&postCommentID=' + commentID + '&userid=' + userid + '&postShareID=' + postShareID,
					success: function(response) {
						var result = JSON.parse(response);
						if(result.status==1){
							$('#postCommentDislikeCount'+commentID).text(result.totalDislikeCount);
							if(result.dislikestatus==1){
								_ele.addClass('active');
							}else{
								_ele.removeClass('active');
							}
						}else{
							alert(result.error);
						}
					}
				});
			}
		});

		$(document).delegate(".sharePost", "click", function (event){
			event.preventDefault();
			var postID = $(this).attr('data-id');
			// var userid = $(this).attr('data-userid');
			$('#sharepid').val(postID);
		});
		
		$(document).delegate(".formsharePost", "submit", function (event){
			event.preventDefault();
			// var postID = $(this).find('#sharepid').val();
			var formData = new FormData(this);
			if(formData){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/sharePost',
					data: formData,
					contentType: false,
					cache: false,
					processData:false,
					success: function(response) {
						location.reload();
					}
				});
			}
		});

		$(document).delegate(".editpostsave", "submit", function (event){
			event.preventDefault();
			// var postID = $(this).find('#sharepid').val();
			var formData = new FormData(this);
			if(formData){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/editPost',
					data: formData,
					contentType: false,
					cache: false,
					processData:false,
					success: function(response) {
						location.reload();
					}
				});
			}
		});

		// for update user online status
		$(document).delegate("#changeOnlineStatus", "click", function (event){
			event.preventDefault();
			$('#changeuserOnlineStatus').trigger('click');
			// $('#changeuserOnlineStatus').val();
			var status = 0;
			if ($('#changeuserOnlineStatus').is(":checked")){
				status = 1;
			}
			$.ajax({
				type: 'POST',
				url: baseUrl+'user/updateUserOnlineStatus',
				data: 'status=' + status,
				success: function(response) {
					var result = JSON.parse(response);
					if(result.status==1){
						$("#userStatusIndi").removeClass().addClass('status '+result.statusLabel);
					}
				}
			});
		});

		// for load post count users
		$(document).delegate(".loadPostCountUsers", "click", function (event){
			var postID = $(this).attr('data-id');
			var type = $(this).attr('data-type');
			if(postID){
				$.ajax({
					type: "post",
					url: baseUrl+'home/loadPostCountUsers',
					data: {postID: postID,type:type},
					success: function(response){
						$('#modalPostCountUsers .modal-body').html(response);
						$('#modalPostCountUsers').modal('show');
					}
				});
			}
		});
		
		$(document).delegate("#postcontrol", "click", function (event){
			$('.emoji-wysiwyg-editor').click();
		});

		$(document).delegate(".postLoader", "click", function (event){
			var type = $(this).attr('data-type');
			var nextPage = parseInt($('#pageno').val())+1;
			var otherUserID = $(this).attr('data-user');
			var other = $(this).attr('data-other');
			console.log('nextPage:-'+nextPage);
			if(type && nextPage){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/loadMorePost',
					data: {
						pageno: nextPage,
						type:type,
						otherUserID:otherUserID,
						other:other
					},
					success: function(data){
						if(data != ''){		
							if(nextPage != $('#pageno').val()){
								$('.loadMorePosts').append(data);
								$('#pageno').val(nextPage);
							}
						} else {
							$(".postLoader").hide();
						}
					}
				});
			}
		});

		$(document).delegate(".savePollPostForm", "submit", function (event){
			event.preventDefault();
			var formData = new FormData(this);
			if(formData){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/savePollPost',
					data: formData,
					contentType: false,
					cache: false,
					processData:false,
					success: function(response) {
						location.reload();
					}
				});
			}
		});

		//for plus account js start
		
		$(document).delegate("#customFrame", "change", function (event){
			var jscolor = $(this).val();
			$.ajax({
				type: 'POST',
				url: baseUrl+'user/profileFrame',
				data: 'customFrame=' + jscolor,
				success: function(data) {
					document.getElementById("editProfilePic").style.border = "6px solid #"+jscolor;
				}
			});
		});

		$(document).delegate("#highlightPost", "change", function (event){
			var jscolor = $(this).val();
			$.ajax({
				type: 'POST',
				url: baseUrl+'user/postHighlighted',
				data: 'highlightPost=' + jscolor,
				success: function(data) {
					document.getElementById("myDiv").style.boxShadow = "inset 0 0 " + 30 +"px #"+jscolor;
				}
			});
		});

		$(document).delegate("input#highlightStatus", "change", function (event){
			var status = 0;
			if ($('#highlightStatus').is(":checked")){
				status = 1;
			}
			$.ajax({
				type: 'POST',
				url: baseUrl+"user/postHighlightStatus",
				data: {
					status:status,
				},
				success: function(data){
				}
			});
		}); 

		$(document).delegate(".aditional", "change", function (event){
			//var input = $(this).val();
			var types = $(this).data("types");
			adtionalImage(this,types);
			var aditionalID = $(this).data("id");
			//console.log('aditionalID : - '+aditionalID);
			if (aditionalID =='') {
				// Retrieve
				if(types=='img1' && localStorage.getItem("aditionalID2") !=''){
					var aditionalID = localStorage.getItem("aditionalID1")??'';
				}
				if(types=='img2' && localStorage.getItem("aditionalID2") !=''){
					var aditionalID = localStorage.getItem("aditionalID2")??'';
				}
			}
			
			var file_data = $(this).prop('files')[0];
			var form_data = new FormData();
			form_data.append('picture', file_data);
			form_data.append('aditionalID', aditionalID);
			// alert('ok');
			$.ajax({
				url: baseUrl+'user/uploadAditionalimg',
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function(response) {
					//console.log('aditionalID Res:-'+response);
					// Store
					if(types=='img1'){
						localStorage.setItem("aditionalID1", response);
					}else if(types=='img2'){
						localStorage.setItem("aditionalID2", response);
					}
				}
			});
		});
		
		function adtionalImage(input, type) {
			counter = input.files.length;
			for (x = 0; x < counter; x++) {
				if (input.files && input.files[x]) {
					var reader = new FileReader();
					reader.onload = function(e) {
						if(type=='img1'){
							$("#img1").attr('src',e.target.result);
						}else if(type=='img2'){
							$("#img2").attr('src',e.target.result);
						}
					};
					reader.readAsDataURL(input.files[x]);
				}
			}
		}

		// End for plus account js
		
		$(document.body).on('touchmove', onScroll); // for mobile
		$(window).on('scroll', onScroll); 

		// callback
		function onScroll(){ 
			if( $(window).scrollTop() + window.innerHeight >= document.body.scrollHeight ) { 
				$('.postLoader').click();
			}
		}

		//let firedEvents = [];
		$(window).scroll(function() {
			if ($(window).scrollTop() == $(document).height() - $(window).height()){
				$('.postLoader').click();
			}
		});

		// for update user online status
		$(document).delegate("#changeThemeDarkModeLabel", "click", function (event){
			event.preventDefault();
			$('#changeThemeDarkMode').trigger('click');
			var status = 0;
			if ($('#changeThemeDarkMode').is(":checked")){
				status = 1;
			}
			$.ajax({
				type: 'POST',
				url: baseUrl+'user/updateThemeSetting',
				data: 'isDarkmode=' + status,
				success: function(response) {
					var result = JSON.parse(response);
					if(result.status==1){
						location.reload();
					}
				}
			});
		});
		
		$(document).delegate(".btnClearNotification", "click", function (event) {
			$.ajax({
				type: "post",
				url: baseUrl+'user/clearNotification',
				data: {isClear: 'clear'},
				success: function(response){
					location.reload();
				}
			});
		});
		
		$(document).delegate("#postform", "submit", function (event){
			event.preventDefault();
			// console.log(encodeURIComponent($(".emoji-wysiwyg-editor").html()));
			// return false;
			var formData = new FormData(this);
			formData.append('postContent', $(".emoji-wysiwyg-editor").html());
			if(formData){
				$.ajax({
					type: 'POST',
					url: baseUrl+'home/createPost',
					data: formData,
					contentType: false,
					cache: false,
					processData:false,
					success: function(response) {
						location.reload();
					}
				});
			}
		});

	});
})(jQuery, window, document);