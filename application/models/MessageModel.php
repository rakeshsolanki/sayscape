<?php

    class MessageModel extends CI_Model{

        public function getUserMessages($userID,$toUserID,$type,$limit='',$offset='0',$orderby='')
        {
            $tempQuery = "";
            $checkClearData = $this->CommonModel->get_row(TBL_MESSAGE_CLEAR, ['*'], ['isActive'=>1,'userID' => $userID, 'toUserID'=>$toUserID]);

            $this->db->select("tm.messageID,tm.text,tm.gif,IFNULL(tm.gif, '') as gif,tm.createdDateTime,tu.username,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profile,tm.fromUserID as userID,tm.toUserID,IF(tm1.filename is null,'',CONCAT('".base_url()."upload/messagemedia/',tm1.filename)) as media,IF(tm1.type is null,'',tm1.type) as media_type")
                ->from(TBL_MESSAGE. ' as tm')
                ->join(TBL_MEDIA. ' as tm1','tm1.comID = tm.messageID AND tm1.mediaRef = 1','LEFT')
                ->join(TBL_MESSAGE_CLEAR. ' as mc','mc.userID = tm.fromUserID and mc.isActive = 1 and mc.isDelete = 0','LEFT')
                ->join(TBL_USER. ' as tu','tu.userID = tm.fromUserID and tu.isActive = 1 and tu.isDelete = 0','LEFT');
                if($type == 0){
                    $tempQuery = "((tm.fromUserID = ".$userID." and tm.toUserID = ".$toUserID.") OR (tm.fromUserID = ".$toUserID." and tm.toUserID = ".$userID."))";
                }else{
                    $tempQuery = "(tm.toUserID = ".$toUserID.")";
                }
                if(!empty($checkClearData)){
                    $lastMsgID = $checkClearData['messageID'];
                    $tempQuery .= " AND (tm.messageID > ".$lastMsgID.")";
                }
                $this->db->where($tempQuery);
                if($limit!=''){
                    $this->db->limit($limit , $offset);
                }
                if($orderby!=''){
                    $this->db->order_by("tm.createdDateTime",$orderby);
                }
                return $this->db->where('tm.type', $type)
                ->where('tm.isDelete', 0)
                ->where('tm.isActive', 1)
                ->order_by("tm.createdDateTime","ASC")
                ->group_by('tm.messageID')
                ->get()
                //->get_compiled_select();
                ->result_array();
        }

        public function getUserNewMessages($userID,$toUserID,$type,$limit='',$offset='0',$orderby='',$lastmsgid='')
        {
            $tempQuery = "";
            $checkClearData = $this->CommonModel->get_row(TBL_MESSAGE_CLEAR, ['*'], ['isActive'=>1,'userID' => $userID, 'toUserID'=>$toUserID]);
            
            $this->db->select("tm.messageID,tm.text,tm.gif,IFNULL(tm.gif, '') as gif,tm.createdDateTime,tu.username,tm.fromUserID as userID,tm.toUserID,IF(tm1.filename is null,'',CONCAT('".base_url()."upload/messagemedia/',tm1.filename)) as media,IF(tm1.type is null,'',tm1.type) as media_type")
                ->from(TBL_MESSAGE. ' as tm')
                ->join(TBL_MEDIA. ' as tm1','tm1.comID = tm.messageID AND tm1.mediaRef = 1','LEFT')
                ->join(TBL_MESSAGE_CLEAR. ' as mc','mc.userID = tm.fromUserID and mc.isActive = 1 and mc.isDelete = 0','LEFT')
                ->join(TBL_USER. ' as tu','tu.userID = tm.fromUserID','LEFT');
            if($type == 0){
                $tempQuery = "((tm.fromUserID = ".$userID." and tm.toUserID = ".$toUserID.") OR (tm.fromUserID = ".$toUserID." and tm.toUserID = ".$userID."))";
            }else{
                $tempQuery = "(tm.toUserID = ".$toUserID.")";
            }
            if(!empty($checkClearData)){
                $lastMsgID = $checkClearData['messageID'];
                $tempQuery .= " AND (tm.messageID > ".$lastMsgID.")";
            }
            $this->db->where($tempQuery);
            if($lastmsgid!=''){
                $this->db->where('tm.messageID >' , $lastmsgid);
            }
            if($limit!=''){
                $this->db->limit($limit , $offset);
            }

            if($orderby!=''){
                $this->db->order_by("tm.createdDateTime",$orderby);
            }
            return $this->db->where('tm.type', $type)
                ->where('tm.isDelete', 0)
                ->where('tm.isActive', 1)
                ->order_by("tm.createdDateTime","ASC")
                ->group_by('tm.messageID')
                ->get()
                //->get_compiled_select();
                ->result_array();
        }

        public function insertUserMessage($data)
        {
            if($this->db->insert(TBL_MESSAGE,$data))
                return $this->db->insert_id();
            return FALSE;
        }

        public function insertMedia($data)
        {
            return $this->db->insert(TBL_MEDIA,$data);
        }

        public function getRecentChatList1($userID)
        {
            return $this->db->query("
            SELECT
                IF(tm1.toUserID = $userID,tm1.fromUserID,tm1.toUserID) as toUserID,
                tu.name,
                tu.username,
                tm1.createdDateTime,
                IF(
                    (
                        LEFT(tu.profileImage, 5) = 'https'
                    ) OR(
                        LEFT(tu.profileImage, 4) = 'http'
                    ),
                    tu.profileImage,
                    CONCAT(
                        '".base_url()."upload/userprofile/',
                        tu.profileImage 
                    )
                ) AS profileImage,
                tm1.text
            FROM
                tblmessage tm1
            LEFT JOIN tbluser tu ON
                tu.userID = IF(tm1.toUserID = $userID,tm1.fromUserID,tm1.toUserID) and tu.isActive = 1 and tu.isDelete = 0
            WHERE
            tm1.toUserID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )
            AND tm1.messageID = (
                SELECT
                    tm2.messageID
                FROM
                    tblmessage tm2
                WHERE
                    tm2.fromUserID = tm1.fromUserID AND tm2.toUserID = tm1.toUserID 
                UNION
                SELECT
                    tm3.messageID
                FROM
                    tblmessage tm3
                WHERE
                    tm3.fromUserID = tm1.toUserID AND tm3.toUserID = tm1.fromUserID
                
                ORDER BY
                    messageID
                   DESC
                LIMIT 1
            )
            AND ( tm1.fromUserID = $userID OR tm1.toUserID = $userID) AND
                tm1.isActive = 1 AND
                tm1.isDelete = 0
                ORDER BY tm1.createdDateTime DESC
            ")->result_array();
        }

        public function getRecentChatList($userID)
        {
            return $this->db->query("
            SELECT  IF(tm.toUserID = ".$userID.",tm.fromUserID,tm.toUserID) as toUserID,
                tu.name,
                tu.username,
                tm.messageID,
                tm.createdDateTime,
                IF(
                    (
                        LEFT(tu.profileImage, 5) = 'https'
                    ) OR(
                        LEFT(tu.profileImage, 4) = 'http'
                    ),
                    tu.profileImage,
                    CONCAT(
                        '".base_url()."/upload/userprofile/',
                        tu.profileImage 
                    )
                ) AS profileImage,
                tm.text FROM  (SELECT MAX(messageID) AS messageID 
                                FROM tblmessage 
                                WHERE ".$userID." IN (fromUserID,toUserID)
                                GROUP BY IF (".$userID." = fromUserID,toUserID,fromUserID)
                               ) AS latest
                LEFT JOIN tblmessage as tm USING(messageID)
                LEFT JOIN tbluser as tu ON tu.userID = IF(tm.toUserID = ".$userID.",tm.fromUserID,tm.toUserID) and tu.isActive = 1 and tu.isDelete = 0 WHERE tm.type = 0  ORDER BY tm.createdDateTime DESC")->result_array();
        }

        // for get last message
        public function getUserLastMessage($userID,$toUserID,$type){
            if($type == 0){
                $tempQuery = "((tm.fromUserID = ".$userID." and tm.toUserID = ".$toUserID.") OR (tm.fromUserID = ".$toUserID." and tm.toUserID = ".$userID."))";
            }else{
                $tempQuery = "(tm.toUserID = ".$toUserID.")";
            }
            $this->db->select("tm.*")
                ->from(TBL_MESSAGE.' as tm')
                ->where($tempQuery)
                ->where('tm.type', $type)
                ->where('tm.isActive', 1)
                ->where('tm.isDelete', 0)
                ->order_by('tm.messageID','DESC');
            $result = $this->db->get()->row_array();
            return $result;
        }

    }
?>