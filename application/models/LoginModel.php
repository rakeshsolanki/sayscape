<?php

class LoginModel extends CI_Model {

    public function getUser($password,$username)
    {
            $query =  $this->db->select("*")
                ->from("tbluser u")
                ->where("u.password = '$password' and  (u.email = '$username' or u.username = '$username')  ")
                ->where('u.isDelete', 0)
                ->where('u.isActive', 1)
                ->limit(1)
                ->get()
                ->result_array();
                return $query;
                
    }

 
    //new function
    public function admin_login($data) {
        $result = array();
        $query = $this->db->get_where('tbluser', array('email' => $data['email'],'isActive' => 1,'isDelete' => 0));
        if ($query->num_rows() != 0) {
            $userData = $query->row_array();
            if(md5($data['password']) == $userData['password']){
                $result = $userData;
            }
        }
        return $result;
    }

    //Insert Data
    public function insert($tbl,$data){
        $this->db->insert($tbl,$data);
        return $this->db->insert_id();
    }

    //Update Data
    public function update($editID,$tbl,$data,$id){
        $this->db->where($editID,$id);
        $this->db->update($tbl,$data);
        return $id; 
    } 
    
    //Delete Data
    public function delete($delID,$tbl,$id){
        $this->db->where($delID,$id);
        $this->db->delete($tbl);
    }


}

?>