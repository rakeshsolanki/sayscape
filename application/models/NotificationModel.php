<?php

class NotificationModel extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    public function getNotificationList_old($userID,$limit =''){
        return $this->db->select("tn.n_id,tn.title,tn.text,CONCAT('".base_url()."upload/notification/',tn.image) as image,tn.createdDateTime,tn.targetID as redirectID,tn.type as type,tun.userID")
            ->from("tblusernotification tun")
            ->join("tblnotification tn","tn.n_id = tun.n_id AND tn.isActive = 1 AND tn.isDelete = 0")
            ->join("tbluser tu","tun.userID = tu.userID")
            ->where("tun.userID",$userID)
            ->where("tun.isActive",1)
            ->where("tun.isDelete",0)
            ->where("tun.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            // ->group_by("tn.targetID")
            ->order_by("tun.createdDateTime","DESC")
            ->get()->result_array();
            if($limit!=''){
                $this->db->limit($limit,0);
            }
    }

    public function getuneradNotificationList($userID){
        return $this->db->select("tn.n_id,tn.title,tn.text,CONCAT('".base_url()."upload/notification/',tn.image) as image,tn.createdDateTime,tn.targetID as redirectID,tn.type as type,tun.userID")
            ->from("tblusernotification tun")
            ->join("tblnotification tn","tn.n_id = tun.n_id AND tn.isActive = 1 AND tn.isDelete = 0")
            ->join("tbluser tu","tun.userID = tu.userID")
            ->where("tun.userID",$userID)
            ->where("tun.isActive",1)
            ->where("tun.isRead",0)
            ->where("tun.isDelete",0)
            ->where("tun.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->order_by("tun.createdDateTime","DESC")
            ->get()->result_array();
            
    }

    public function getNotificationList($userID,$limit =''){
        return $this->db->select("tun.un_id,tn.n_id,tn.title,tn.text,tn.image,tn.createdDateTime,tn.targetID as redirectID,tn.type as type,tun.userID,tun.otherID,tun.senderID,tun.notifyType, count(`tun`.`otherID`) as totalCount")
            ->from("tblusernotification tun")
            ->join("tblnotification tn","tn.n_id = tun.n_id AND tn.isActive = 1 AND tn.isDelete = 0")
            ->join("tbluser tu","tun.userID = tu.userID")
            ->where("tun.userID",$userID)
            ->where("tun.isActive",1)
            ->where("tun.isDelete",0)
            ->where("tun.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->group_by(array("tun.otherID","tun.userID","tun.notifyType","IFNULL(tun.otherID,UUID())"))
            ->order_by("tun.createdDateTime","DESC")
            ->get()->result_array();
            // echo $this->db->last_query();
            if($limit!=''){
                $this->db->limit($limit,0);
            }
    }

    public function getRelatedNotificationList($data){
        $this->db->select("tn.*,tun.*,tu.profileImage,tu.username,tu.name")
            ->from("tblusernotification tun")
            ->join("tblnotification tn","tn.n_id = tun.n_id AND tn.isActive = 1 AND tn.isDelete = 0")
            ->join("tbluser tu","tun.userID = tu.userID")
            ->where("tun.isActive",1)
            ->where("tun.isDelete",0);
            if(isset($data['userID']) && $data['userID']!=""){
                $this->db->where("tun.userID",$data['userID']);
                $this->db->where("tun.userID NOT IN (
                    SELECT
                        tbu.blockUserID
                    FROM
                        tblblockuser tbu
                    WHERE
                        tbu.userID = ".$data['userID']." AND tbu.isActive = 1 AND tbu.isDelete = 0
                    UNION
                    SELECT
                        tbu1.userID
                    FROM
                        tblblockuser tbu1
                    WHERE
                        tbu1.blockUserID = ".$data['userID']." AND tbu1.isActive = 1 AND tbu1.isDelete = 0
                )");
            }
            if(isset($data['otherID']) && $data['otherID']!=""){
                $this->db->where("tun.otherID",$data['otherID']);
            }
            if(isset($data['notifyType']) && $data['notifyType']!=""){
                $this->db->where("tun.notifyType",$data['notifyType']);
            }
            $this->db->group_by("tun.senderID");
            $this->db->order_by("tun.createdDateTime","DESC");
            // echo $this->db->last_query();
            if(isset($data['limit']) && $data['limit']!=""){
                $this->db->limit($data['limit']);
            }
            $result = $this->db->get()->result_array();
            return $result;
    }
}

?>