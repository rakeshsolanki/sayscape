<?php

class UserModel extends CI_Model {



    /**

     * Get single record for a users table

     *

     * @param string $columns

     * @param string $where

     * @return array

     */

    public function getUser($columns = "*", $where = '1 = 1') {

        $this->db->select($columns)

            ->from(TBL_USER. ' as tu')

            ->join(TBL_USER_ACCESS_TOKEN. ' as tuat', 'tuat.userID = tu.userID', 'LEFT')

            ->where($where)

            ->where('tu.isDelete', 0)

            ->where('tu.isActive', 1)

            ->limit(1);

        return $this->db->get()->row_array();

    }



    public function getAllUser($columns = "*", $where = '1 = 1'){

        return $this->db->select($columns)

            ->from(TBL_USER)

            ->where($where)

            ->order_by('userID','DESC')

            ->get()->result_array();

    }



    public function getUserName($userID){

        return $this->db->select('*')

            ->from(TBL_USER)

            ->where('userID',$userID)

            ->order_by('userID','DESC')

            ->get()->result_array();

    }

    public function getUserNamerow($userID){

        return $this->db->select('username')
            ->from(TBL_USER)
            ->where('userID',$userID)
            ->get()->row_array();

    }



    public function getAllRportUser($columns = "*", $where = '1 = 1'){

        return $this->db->select($columns)

            ->from("tblreport r")

            ->join("tbluser u","u.userID = r.comID AND u.isActive = 1 AND u.isDelete = 0")
            ->join("tbluser ru","ru.userID = r.userID AND ru.isActive = 1 AND ru.isDelete = 0")

            ->where($where)

            ->where('u.isDelete', 0)

            ->where('u.isActive', 1)
            ->where('r.isDelete', 0)
            ->where('r.isActive', 1)

            ->order_by('u.userID','DESC')

            ->get()->result_array();

    }



    //check user authentication by user id and access accessToken

    public function checkUserAuth($userID, $accessToken){

        $response = array();

        $result = array();

        if($userID!="" && $accessToken!=""){

            $checkUserAvailable = $this->user->getUser(['tu.userID'],['tu.userID'=>$userID]);

            if (empty($checkUserAvailable)) {

                $response['code'] = 101;

                $response['message'] = getMessage('userNotFound');

                $this->response($response);

            }

            $checkUserVerify = $this->user->getUser(['tu.userID'],['tu.userID'=>$userID,'tu.isVerified'=>1]);

            if (empty($checkUserVerify)) {

                $response['message'] = getMessage('verifyAccount');

                $this->response($response);

            }

            $checkLogin = $this->user->getUser(['tu.userID','tu.name'],['tu.userID'=>$userID,'tuat.accessToken' => $accessToken]);



            if (empty($checkLogin)) {

                $response['code'] = 102;

                $response['message'] = getMessage('tokenExpire');

                $this->response($response);

            }else{

                $result = $checkLogin;

            }

        }

        return $result;

    }





    public function response($response) {

        if(empty($response['data'])){

            $response['data'] = new stdClass();

        }

        if(empty($response['code'])){

            $response['code'] = 101;

        }

        if(empty($response['message'])){

            $response['message'] = "No Message found";

        }

        header('Content-Type:application/json');

        echo json_encode($response);



        $this->generateLog($response);

        exit();

    }



    public function generateLog($response=""){

        $requestParameter = file_get_contents('php://input');

        $requestUrl = base_url(uri_string());

        //echo "<pre>"; print_r($requestParameter); die();



        $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL;

        $log .= "URL: ".$requestUrl.PHP_EOL;

        $log .= "Parameter: ".$requestParameter.PHP_EOL;

        if(!empty($response)){

            //$log .= "Response: ".json_encode($response).PHP_EOL;

        }

        $log .= "--------------------------------------------------".PHP_EOL.PHP_EOL;

        file_put_contents('application/logs/log_'.date("d_M_Y").'.log', $log, FILE_APPEND);

    }

    //for get user response

    public function getUserResponse($userID){

        $response = array();

        if(isset($userID) && $userID!=""){

            $response['userDetails'] = $this->user->getUser(['tu.userID','tu.username','tu.name','tu.email','tu.phone','tu.location','tu.dob','tu.loginType','tu.profileImage','tu.coverImage','tu.website','tu.about'],['tu.userID' => $userID]);



            $response['userDetails']['profileImage'] =base_url()."upload/userprofile/".$response['userDetails']['profileImage'];

            $response['userDetails']['coverImage'] =base_url()."upload/usercover/".isset($response['userDetails']['coverImage']) ?  $response['userDetails']['coverImage']:'';

        }

        return $response;

    }

    //for get user response

    public function getUserGroupMembers($columns = '*', $where = '1 = 1', $order_by = null, $sort_by = 'DESC') {

        $this->db->select($columns)

            ->from(TBL_GROUP. ' as tg')

            ->join(TBL_USER_GROUP. ' as tug', 'tg.groupID = tug.groupID', 'LEFT')

            ->join(TBL_USER. ' as tu', 'tu.userID = tug.userID')

            ->where($where)

            ->where('tg.isDelete', 0)

            ->where('tg.isActive', 1);

        if($order_by != null) {

            $this->db->order_by($order_by, $sort_by);

        }

        return $this->db->get()->result_array();

    }



    // THIS FUNCTION REMOVE

    public function getUserFollowingList($columns = '*', $where = '1 = 1', $order_by = null, $sort_by = 'DESC') {

        $this->db->select($columns)

            ->from(TBL_USER_FOLLOWER. ' as tuf')

            ->join(TBL_USER. ' as tu', 'tu.userID = tuf.followerID')

            ->where($where)

            ->where('tuf.isDelete', 0)

            ->where('tuf.isActive', 1);

        if($order_by != null) {

            $this->db->order_by($order_by, $sort_by);

        }

        return $this->db->get()->result_array();

    }



    public function searchuser($userID,$search_keyword)
    {
        // return $this->db->query("SELECT tu.userID, tu.name, tu.username, tu.email,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('".base_url()."upload/userprofile/',tu.profileImage)) as profileImage, IFNULL( ( SELECT tuf.status FROM tbluserfollower AS tuf WHERE tuf.followerID = tu.userID AND tuf.userID = $userID AND tuf.status = 1 AND tuf.isActive = 1 AND tuf.isDelete = 0 ), '0' ) AS isfollow FROM tbluser AS tu WHERE (tu.username LIKE '$search_keyword%' OR tu.name LIKE '$search_keyword%' OR tu.location LIKE '$search_keyword%' ) AND tu.userID <>$userID and tu.userID NOT IN( SELECT blockUserID FROM tblblockuser WHERE userID = $userID AND blockUserID AND isActive = 1 AND isDelete = 0 UNION SELECT userID FROM tblblockuser WHERE blockUserID = $userID AND isActive = 1 AND isDelete = 0 )  and tu.isActive = 1 and tu.isDelete = 0")
        //     ->result_array();
        return $this->db->query("SELECT tu.userID, tu.name, tu.username, tu.email,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('".base_url()."upload/userprofile/',tu.profileImage)) as profileImage, IFNULL( ( SELECT tuf.status FROM tbluserfollower AS tuf WHERE tuf.followerID = tu.userID AND tuf.userID = $userID AND tuf.isActive = 1 AND tuf.isDelete = 0 ), '0' ) AS isfollow FROM tbluser AS tu WHERE (tu.username LIKE '$search_keyword%' OR tu.name LIKE '$search_keyword%' OR tu.location LIKE '$search_keyword%' ) AND tu.userID <>$userID and tu.userID NOT IN( SELECT blockUserID FROM tblblockuser WHERE userID = $userID AND blockUserID AND isActive = 1 AND isDelete = 0 UNION SELECT userID FROM tblblockuser WHERE blockUserID = $userID AND isActive = 1 AND isDelete = 0 )  and tu.isActive = 1 and tu.isDelete = 0")
            ->result_array();
    }



    public function getUserdetails($watchID,$userID="")
    {
        $finalQuery = "u.userID,u.name,u.username,u.email,u.phone,u.createdDateTime,u.location,u.dob,u.isVerified,u.googleId,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,CONCAT('".base_url()."upload/usercover/',u.coverImage) as coverImage,u.website,u.about,u.isVeteran,u.allowVeteran,u.isResponder,u.isOffical,(select COUNT(tuf.userFollowerID) from tbluserfollower as tuf where tuf.userID = u.userID and tuf.isActive = 1 and tuf.isDelete = 0 and tuf.status = 1) as totalFollowing,(select COUNT(tuf.userFollowerID) from tbluserfollower as tuf where tuf.followerID = u.userID  and tuf.isActive = 1 and tuf.isDelete = 0 and tuf.status = 1) as totalFollowers,(SELECT count(*)  FROM `tblgroup` WHERE `userID` = ".$watchID." AND `type` = 0 AND `isActive` = 1 AND `isDelete` = 0) as userGroupCount";
        $finalQuery .= (empty($userID) ? "" : ",if(uf.status is null,'0',uf.status) as isfollow");
        $finalQuery .= (empty($userID) ? "" : ",if(mu.isActive is null,'0',mu.isActive) as isMute");
        $finalQuery .= (empty($userID) ? "" : ",if(us.privateProfileStatus is null,'0',us.privateProfileStatus) as privateProfileStatus");
        $this->db->select($finalQuery)
            ->from("tbluser u");
        if(!empty($userID)){ 
            $this->db->join("tbluserfollower uf","uf.followerID = u.userID and uf.userID = $userID and uf.isActive = 1 and uf.isDelete = 0","LEFT");
            $this->db->join("tblmuteuser mu","mu.muteUserID = u.userID and mu.userID = $userID and mu.isActive = 1 and mu.isDelete = 0","LEFT");
        }
        $this->db->join("tblsettings us","us.userID = u.userID and us.userID = $watchID and us.isActive = 1 and us.isDelete = 0","LEFT");
        $this->db->where("u.isDelete = 0 and u.isActive = 1 and u.userID = $watchID")->limit(1);
        $result = $this->db->get()->row_array();
        // echo $this->db->last_query(); die();
        return $result;
    }



    public function getPostByUser($userID,$watchID = NULL, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {
        $watchID=$watchID == NULL?$userID:$watchID;
        $this->db->select("p.createdDateTime as datetime,p.postID,p.userID,p.isTop,IFNULL(p.gif, '') as gif,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,p.text,p.isPermanent,p.isexpireOn,p.expiryDateTime,p.createdDateTime,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,'' as reshareName,0 as postShareID")

            ->from("tblpost p")

            ->join("tbluser u","u.userID = p.userID AND u.isActive = 1 and u.isDelete=0","LEFT")

            ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")

            ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")

            ->join("tblpostshare tps","tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0","LEFT")

            ->where("p.isActive = 1 and p.isDelete= 0 and p.userID = $watchID and p.postType = 0")

            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime) ) ) )");

        // ->order_by("p.isTop","DESC")
        // ->order_by("p.createdDateTime","DESC");
        $query1 = $this->db->get_compiled_select();
        //Add Reshare Post by unioun
        $columns="p.createdDateTime as datetime,p.postID,p.userID,p.isTop,IFNULL(p.gif, '') as gif,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,p.text,p.isPermanent,p.isexpireOn,p.expiryDateTime,p.createdDateTime,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if((SELECT postID  FROM `tblpostshare` WHERE postShareID=tprs.postShareID AND `postID` = p.postID AND `userID` = $watchID GROUP BY postID) is null,'0',1) as isReshare,1 as isResharePost,urs.username as reshareName,tprs.postShareID as postShareID";
        $this->db->select($columns)

            ->from("tblpostshare tprs")

                ->join("tbluser urs","urs.userID = tprs.userID and urs.isActive = 1 and urs.isDelete = 0")

            ->join("tblpost p","p.postID = tprs.postID AND p.isActive = 1 and p.isDelete=0")

            ->join("tbluser u","u.userID = p.userID AND u.isActive = 1 and u.isDelete=0")

            ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")

            ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")

            ->where("tprs.isActive = 1 and tprs.isDelete= 0")

            ->where("p.userID NOT IN (

                    SELECT

                        tbu.blockUserID

                    FROM

                        tblblockuser tbu

                    WHERE

                        tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0

                    UNION

                    SELECT

                        tbu1.userID

                    FROM

                        tblblockuser tbu1

                    WHERE

                        tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0

                )")

            ->where("p.postID NOT IN (

                    SELECT

                        thp.postID

                    FROM

                        tblhidepost thp

                    WHERE

                        thp.userID = $watchID and thp.isActive = 1 AND thp.isDelete = 0

                )")

            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");

        $this->db->where("p.postType",0);

        $this->db->where("tprs.userID",$watchID);

        $query2 = $this->db->get_compiled_select();
        if (!empty($pageNumber)) {
            $page = $pageNumber;
            $offset = $limit * ($page - 1);
            $query = $this->db->query($query1." UNION ".$query2." order by isTop desc,datetime desc LIMIT " . $offset . "," . $limit);
        }else{
            $query = $this->db->query($query1." UNION ".$query2." order by isTop desc,datetime desc");
        }
        
        /*if($totalPages != FALSE){

            $query = $this->db->query($query1." UNION ".$query2." order by isTop desc,datetime desc");

            $totalNumberPost = $query->num_rows();

            // echo "<pre>"; print_r($query->result_array()); die();

            return round($totalNumberPost/ONE_PAGE_LIMIT);

        }
        if(!empty($pageNumber)  && $pageNumber != 'ALL') {

            $page = $pageNumber;

            $offset = ONE_PAGE_LIMIT * ($page-1);

         }else {

            $page = 0;

            $offset = 0;

        }
        if(!empty($pageNumber) && $pageNumber != 'ALL')

            $query = $this->db->query($query1." UNION ".$query2." order by isTop desc,datetime desc LIMIT ".$offset .",". ONE_PAGE_LIMIT);

        else
            $query = $this->db->query($query1." UNION ".$query2." order by isTop desc,datetime desc");
        */
        return $query->result_array();
    }


    public function getMediaPostByUser($userID, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,if(tps.status is null,'0',tps.status) as isReshare,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,u.name")

            ->from("tblpost p")
            ->join("tbluser u","u.userID = p.userID",'LEFT')
            
            ->join("tblmedia m","m.comID = p.postID")

            ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")

            ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")

            ->join("tblpostshare tps","tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0","LEFT")

            ->where("m.isActive = 1 and m.isDelete= 0 and p.isActive = 1 and p.isDelete=0 and p.userID = $userID and mediaRef = 0")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))")
            ->where("p.postType",0)
            ->where("u.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->group_by('p.postID')
            ->order_by("p.createdDateTime","DESC");
            if (!empty($pageNumber)) {
                $page = $pageNumber;
                $offset = $limit * ($page - 1);
                $this->db->limit($limit,$offset);
            }
            $query = $this->db->get()->result_array();
        return $query;

    }

    public function getCommentPostByUser($userID, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,u.name,if(tps.status is null,'0',tps.status) as isReshare,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,IF(c.postShareID != 0,1,0) as isResharePost,IF(c.postShareID != 0,(SELECT `username` FROM `tbluser` WHERE `userID` = (SELECT userID  FROM `tblpostshare` WHERE `postShareID` = c.postShareID)),'') as reshareName,IF(c.postShareID != 0,c.postShareID,0) as postShareID")

            ->from("tblpost p")

        ->join("tbluser u","u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")

            ->join("tblsettings ts1","ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0","LEFT")

            ->join("tblpostcomment c","c.postID = p.postID")

            ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")

            ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")

            ->join("tblpostshare tps","tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0","LEFT")

            ->where("c.isActive = 1 and c.isDelete=0 and p.isActive = 1 and p.isDelete=0 and c.userID = $userID ")

            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))")

            ->where("u.userID NOT IN (

                SELECT

                    tbu.blockUserID

                FROM

                    tblblockuser tbu

                WHERE

                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0

                UNION

                SELECT

                    tbu1.userID

                FROM

                    tblblockuser tbu1

                WHERE

                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0

            )")

            ->where("p.postID NOT IN (

                SELECT

                    thp.postID

                FROM

                    tblhidepost thp

                WHERE

                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0

            )")

            ->where("IF(ts1.blacklistKeyword != '', p.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")

            ->where("p.postType",0)

            ->group_by('p.postID')

            ->order_by("p.createdDateTime","DESC");
            if (!empty($pageNumber)) {
                $page = $pageNumber;
                $offset = $limit * ($page - 1);
                $this->db->limit($limit,$offset);
            }
            $query = $this->db->get()->result_array();

        return $query;

    }



    public function getLikedPostByUser($userID, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {

        $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,u.name,if(tpl1.isLiked is null,'0',tpl1.isLiked) as isLiked,if(tpl2.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,IF(tpl.postShareID != 0,1,0) as isResharePost,IF(tpl.postShareID != 0,(SELECT `username` FROM `tbluser` WHERE `userID` = (SELECT userID  FROM `tblpostshare` WHERE `postShareID` = tpl.postShareID)),'') as reshareName,IF(tpl.postShareID != 0,tpl.postShareID,0) as postShareID")

            ->from("tblpostlike tpl")

                ->join("tblpost tp","tp.postID = tpl.postID and tp.isActive = 1 and tp.isDelete = 0")
                ->join("tbluser u","u.userID = tp.userID and u.isActive = 1 and u.isDelete = 0")

            ->join("tblsettings ts1","ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0","LEFT")

            ->join("tblpostlike tpl1","tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 1 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")

            ->join("tblpostlike tpl2","tpl2.postID = tp.postID and tpl2.userID = $userID and tpl2.isLiked = 0 and tpl2.isActive = 1 and tpl2.isDelete = 0","LEFT")

            ->join("tblpostshare tps","tps.userID = $userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0","LEFT")

            ->where("tpl.userID",$userID)

            ->where("u.userID NOT IN (

                    SELECT

                        tbu.blockUserID

                    FROM

                        tblblockuser tbu

                    WHERE

                        tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0

                    UNION

                    SELECT

                        tbu1.userID

                    FROM

                        tblblockuser tbu1

                    WHERE

                        tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0

                )")

            ->where("tp.postID NOT IN (

                    SELECT

                        thp.postID

                    FROM

                        tblhidepost thp

                    WHERE

                        thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0

                )")

            ->where("tpl.isActive",1)

            ->where("tpl.isDelete",0)

            ->where("tp.postType",0)

            ->where("IF(ts1.blacklistKeyword != '', tp.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")

            ->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))")

            ->order_by("tp.createdDateTime","DESC")

            ->group_by("tp.postID");
            if (!empty($pageNumber)) {
                $page = $pageNumber;
                $offset = $limit * ($page - 1);
                $this->db->limit($limit,$offset);
            }
            $query = $this->db->get()->result_array();
        return $query;
    }

    public function changePassword($oldPassword,$newPassword,$userID){

        $oldPassword = md5($oldPassword);

        if($this->db->select("*")

                ->from("tbluser tu")

                ->where("tu.password",$oldPassword)

                ->get()

                ->num_rows() > 0){

            $this->CommonModel->update('tbluser', ['password'=>md5($newPassword)], ['userID' => $userID]);

            return true;

        }

        return false;

    }

    public function getFollowingList($userID,$wid){

        return $this->db->select("tuf.followerID,tu.username,tu.name,tu.email,tu.phone,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('".base_url()."upload/userprofile/',tu.profileImage)) as profileImage,if(uf.status is null,'0',uf.status) as isfollow")

            ->from("tbluserfollower tuf")

            ->join("tbluser tu","tu.userID = tuf.followerID and tu.isActive = 1 and tu.isDelete = 0")

            ->join("tbluserfollower uf" ,"uf.followerID = tu.userID and uf.userID = $userID and uf.isActive = 1 and uf.isDelete = 0","LEFT")

            ->where("tuf.followerID NOT IN (

                    SELECT

                        tbu.blockUserID

                    FROM

                        tblblockuser tbu

                    WHERE

                        tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0

                    UNION

                    SELECT

                        tbu1.userID

                    FROM

                        tblblockuser tbu1

                    WHERE

                        tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0

                )")

            ->where("tuf.userID",$wid)

            ->where("tuf.status",1)

            ->where("tuf.isActive",1)

            ->where("tuf.isDelete",0)

            ->order_by('tuf.userFollowerID','DESC')
            ->get()->result_array();

    }

    public function getFollowersList($userID,$wid){

        return $this->db->select("tuf.userID as followerID,tu.username,tu.name,tu.email,tu.phone,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('".base_url()."upload/userprofile/',tu.profileImage)) as profileImage,if(uf.status is null,'0',uf.status) as isfollow")

            ->from("tbluserfollower tuf")

            ->join("tbluser tu","tuf.userID = tu.userID and tu.isActive = 1 and tu.isDelete = 0")

            ->join("tbluserfollower uf" ,"uf.followerID = tu.userID and uf.userID = $userID and uf.isActive = 1 and uf.isDelete = 0","LEFT")
            
            ->where("tuf.followerID",$wid)

            ->where("tuf.userID NOT IN (

                SELECT

                    tbu.blockUserID

                FROM

                    tblblockuser tbu

                WHERE

                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0

                UNION

                SELECT

                    tbu1.userID

                FROM

                    tblblockuser tbu1

                WHERE

                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0

            )")

            ->where("tuf.status",1)

            ->where("tuf.isActive",1)

            ->where("tuf.isDelete",0)

            ->order_by('tuf.userFollowerID','DESC')
            ->get()->result_array();

    }

    public function getUserBlockedList($userID){

        return $this->db->select("tu.userID,tu.username,tu.name,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('".base_url()."upload/userprofile/',tu.profileImage)) as profileImage")

            ->from("tblblockuser tbu")

                ->join("tbluser tu","tbu.blockUserID = tu.userID and tu.isActive = 1 and tu.isDelete = 0","LEFT")

            ->where("tbu.userID",$userID)

            ->where("tbu.isActive",1)

            ->where("tbu.isDelete",0)

            ->get()->result_array();

    }



    public function getFollowRequestList($columns = '*', $where = '1 = 1', $order_by = null, $sort_by = 'DESC') {

        $this->db->select($columns)

            ->from(TBL_USER_FOLLOWER. ' as tuf')

            ->join(TBL_USER. ' as tu', 'tu.userID = tuf.userID')

            ->where($where)

            ->where('tuf.isDelete', 0)

            ->where('tuf.isActive', 1);

        if($order_by != null) {

            $this->db->order_by($order_by, $sort_by);

        }

        return $this->db->get()->result_array();

    }

    // for get user online status
    public function getUserOnlineStatus($userID){
        $result = "away";
        $userOnlineStatus = $this->user->userOnlineStatus($userID);
        if($userOnlineStatus==1){
            // if(onlineTime is null,'0', if(TIMESTAMPDIFF(MINUTE, onlineTime, NOW()) > 20,'online','away')) as liveTime
            $this->db->select("userID, IFNULL(onlineTime, 0) as onlineTime");
            $this->db->from(TBL_USER);
            $this->db->where('userID', $userID);
            $userdata = $this->db->get()->row_array();
            if(!empty($userdata) && !empty($userdata['onlineTime'])){
                $currentTime = time();
                $onlinetime = $userdata['onlineTime'];
                $difference = $currentTime - $onlinetime;
                if ($difference < 300) {
                    $result = "online";
                }
            }
        }
        return $result;
    }

    // for get username json
    public function getUsernameJson(){
        $result = array();
        $this->db->select("userID,username,name");
        $this->db->from(TBL_USER);
        $this->db->where('isActive', 1);
        $this->db->where('isDelete', 0);
        $userList = $this->db->get()->result_array();
        if (!empty($userList)) {
            foreach($userList as $user){
                $result[] = $user['username'];
            }
        }
        return json_encode($result);
    }

    public function getUserId($username="")
    {
        $result = "";
        $this->db->select("userID,username");
        $this->db->from(TBL_USER);
        $this->db->where('username', $username);
        $this->db->where('isActive', 1);
        $this->db->where('isDelete', 0);
        $userdata = $this->db->get()->row_array();
        if(!empty($userdata)){
            $result = $userdata['userID'];
        }
        return $result;
    }

    // for get user online status
    public function userOnlineStatus($userID=""){
        $result = "0";
        $this->db->select("userID,username,onlineStatus");
        $this->db->from(TBL_USER);
        $this->db->where('userID', $userID);
        $this->db->where('isActive', 1);
        $this->db->where('isDelete', 0);
        $userdata = $this->db->get()->row_array();
        if(!empty($userdata)){
            $result = $userdata['onlineStatus'];
        }
        return $result;
    }

    public function getRecommendUser($userID){
        $userdata = $this->user->getUserResponse($userID);
        $useridArray = $this->getRecomUsers($userID,$userdata['userDetails']['location']);
        $recommendUser = array();
        foreach($useridArray as $userid){
            if($userid !=  $userID){
                $u = $this->getUserResponse($userid);
                if(!empty($u['userDetails'])){
                    $recommendUser[] = $u['userDetails'];
                }
            }
        }
        return $recommendUser;
    }

    public function getRecomUsers($userID,$location='surat'){
        $tagfrend=array();
        $postsdata = $this->post->getPosts($userID,0);
        if (!empty($postsdata)) {
            $hashtagArray = array();
            $userArray = array();
            foreach ($postsdata as &$value) {
                preg_match_all('/#(\w+)/', $value['text'], $hashtagMatches);
                if (!empty($hashtagMatches[0])) {
                    foreach ($hashtagMatches[0] as $match) {
                        $hashtagArray[] = $match;
                    }
                    $userArray[] = $value['userID'];
                }
            }
            $usersId = array_unique($userArray);
            shuffle($usersId);
            array_push($tagfrend,$usersId['0']);
        }
        
        $this->db->select("tbluser.userID");
        $this->db->from("tbluser");
        $this->db->like('tbluser.location',$location);
        $this->db->where('tbluser.isActive',1);
        $this->db->where('tbluser.isDelete',0);
        $this->db->order_by('rand()');
        $query = $this->db->get();
        $areaFriend = $query->row()->userID;
        array_push($tagfrend,$areaFriend);

        $this->db->select("userID,followerID");
        $this->db->where('followerID',$userID);
        $this->db->or_where('userID',$userID);
        $this->db->where('status',1);
        $this->db->order_by('rand()');
        $this->db->limit(5);
        $query = $this->db->get('tbluserfollower');
        $follower = $query->result_array();
        //array_push($tagfrend,$follower);

        $friendOfFriend = array();
        foreach($follower as $friend){
            $followerID =$friend['userID'];
            if($friend['userID']==$userID){
                $followerID = $friend['followerID'];
            }
            $this->db->select("userID");
            $this->db->where('followerID',$followerID);
            $this->db->or_where('userID',$followerID);
            // $this->db->where('followerID!=',$userID);
            $this->db->where('status',1);
            $this->db->order_by('rand()');
            $this->db->limit(1);
            $query = $this->db->get('tbluserfollower');
            $friends = $query->row_array();
            if(!in_array($friends['userID'], $friendOfFriend)){
                $friendOfFriend[] =  $friends['userID'];
            }
            
        }
        $allResult = array_unique(array_merge($tagfrend,$friendOfFriend));

        //alll follwer following
        $this->db->select("userID");
        $this->db->where('followerID',$userID);
        $this->db->where('status',1);
        $query = $this->db->get('tbluserfollower');
        $follower = $query->result_array();
        $arr1 = array();
        foreach($follower as $folw){
            $arr1[] = $folw['userID'];
        }

        $this->db->select("followerID");
        $this->db->or_where('userID',$userID);
        $this->db->where('status',1);
        $query = $this->db->get('tbluserfollower');
        $following = $query->result_array();
        $arr2 = array($userID);
        foreach($following as $folwi){
            $arr2[] = $folwi['followerID'];
        }
        $myFriends = array_unique(array_merge($arr1,$arr2));
        $respond= array();
        foreach($allResult as $all){
            if(!in_array($all, $myFriends)){
                $respond[] =  $all;
            }
        }
        return $respond;
        
    }

    public function checkUserPlusAccount($userID){
        $result = array();
        $today = date('Y-m-d');
        $userTransData = $this->CommonModel->get_row(TBL_USER_TRANSACTION, '*', ['userID' => $userID, 'isActive' => 1],'transID','DESC');
        if(!empty($userTransData)){
            if(isset($userTransData['expiry']) && $userTransData['expiry'] >= $today){
                $result = $userTransData;
            }
        }
        return $result;
    }

}

?>