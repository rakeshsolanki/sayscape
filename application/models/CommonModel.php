<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class CommonModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    /*Function: varifySession :Hetal Patel*/
    public function varifySession()
    {
        if (!$this->session->userdata('admin_login')) {
            redirect('admin/auth');
        }
    }
    /**
     * inserts new record into table
     *
     * @param string $table
     * @param array $data
     * @return int last inserted ID
     */
    public function insert($table, $data)
    {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }

    public function insertdata($table, $data)
    {
        return $this->db->insert_batch($table, $data);
    }

    /**
     * Get records of table
     *
     * @param string $table
     * @param string $columns
     * @param mixed $where
     * @param string $order_by
     * @param string $sort_by
     * @return array
     */
    public function get_all($table, $columns = '*', $where = '1 = 1', $order_by = null, $sort_by = 'DESC')
    {
        $this->db->select($columns)
            ->from($table)
            ->where($where)
            ->where('isDelete', 0)
            ->where('isActive', 1);
        if ($order_by != null) {
            $this->db->order_by($order_by, $sort_by);
        }
        return $this->db->get()->result_array();
    }

    /**
     * Set records as deleted
     *
     * @param string $table
     * @param mixed $where
     * @return int affected rows
     */
    public function set_delete($table, $where)
    {
        $this->db->where($where)
            ->update($table, ['isDelete' => 1]);
        return $this->db->affected_rows();
    }

    /**
     * update records
     *
     * @param string $table
     * @param mixed $data
     * @param mixed $where
     * @return boolean
     */
    public function update($table, $data, $where)
    {
        $this->db->where($where);
        return $this->db->update($table, $data);
    }

    /**
     * Get single record for a table
     *
     * @param string $table
     * @param string $columns
     * @param string $where
     * @return array
     */
    public function get_row($table, $columns = "*", $where = '1 = 1', $order_by = null, $sort_by = 'DESC')
    {
        $this->db->select($columns)
            ->from($table)
            ->where($where)
            ->where('isDelete', 0)
            ->where('isActive', 1)
            ->limit(1);
        if ($order_by != null) {
            $this->db->order_by($order_by, $sort_by);
        }
        return $this->db->get()->row_array();
    }

    /**
     * Get single record for a table with deleted
     *
     * @param string $table
     * @param string $columns
     * @param string $where
     * @return array
     */
    public function get_row_deleted($table, $columns = "*", $where = '1 = 1')
    {
        $this->db->select($columns)
            ->from($table)
            ->where($where)
            ->where('isDelete', 1)
            ->where('isActive', 1)
            ->limit(1);
        return $this->db->get()->row_array();
    }

    public function get_row_com($table, $columns = "*", $where = '1 = 1')
    {
        $this->db->select($columns)
            ->from($table)
            ->where($where)
            ->limit(1);
        return $this->db->get()->row_array();
    }

    /**
     * Get number of rows  of table
     *
     * @param string $table
     * @param string $columns
     * @param mixed $where
     * @param string $order_by
     * @param string $sort_by
     * @return array
     */
    public function get_num_rows($table, $where = '1 = 1')
    {
        $this->db->select('*')
            ->from($table)
            ->where($where)
            ->where('isDelete', 0)
            ->where('isActive', 1);
        return $this->db->get()->num_rows();
    }
    public function checkExists($table, $where = '1=1')
    {
        if ($this->db->select("*")->from($table)->where($where)->get()->num_rows() > 0) {
            return true;
        }
        return false;
    }
    //for get site setting value
    public function getSiteSetting($key = "")
    {
        $result = "";
        if (!empty($key)) {
            $query = $this->db->get_where(TBL_SITE_SETTING, array('isActive' => 1, 'isDelete' => 0, 'settingKey' => $key));
            $resultData = $query->row_array();
            if (!empty($resultData)) {
                $result = $resultData['settingValue'];
            }
        }
        return $result;
    }
    public function send_mail($to, $subject, $message, $from = NULL)
    {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'sayscapeotp@gmail.com';
            $mail->Password = 'S@yscap3$';
            $mail->SMTPSecure = 'tls';
            $mail->Port = '587';

            $mail->setFrom('sayscapeotp@gmail.com');
            $mail->addAddress($to);
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body = $message;

            $mail->send();
            return true;
        } catch (Exception $e) {
            return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

        return false;
        //   $from = empty($from) ? FROM_EMAIL : $from;
        //   $config['protocol'] = 'smtp';
        //   $config['smtp_host'] = 'ssl://smtp.gmail.com';
        //   $config['smtp_port'] = '465';
        //   //$config['smtp_host'] = 'mail.aashitaindia.com';
        //   //$config['smtp_port'] = '587';
        //   // $config['smtp_timeout'] = '7';
        //   //$config['smtp_user'] = 'noreply@aashitaindia.com';
        //   $config['smtp_user'] = 'rahul.credencetech@gmail.com';
        //   $config['smtp_pass'] = 'Rphd@9725';
        //   //$config['smtp_pass'] = 'Aashita@123';
        //   $config['charset'] = 'utf-8';
        //   $config['newline'] = "\r\n";
        //   $config['mailtype'] = 'html'; // or html
        //   //$config['validation'] = TRUE; // bool whether to validate email or not

        //   $this->email->initialize($config);
        //   $this->email->from($from);
        //   $this->email->to($to);
        //   $this->email->subject($subject);
        //   $this->email->message($message);
        //   //$this->email->send();
        //   if(!$this->email->send()) {
        //       $this->email->print_debugger();
        //       return false;
        //   }
        //   return true;
        // //   echo $this->email->print_debugger();
        // //   die();
    }

    public function delete($tableName, $arrWhere)
    {
        $this->db->where($arrWhere);
        return $this->db->delete($tableName);
    }
    public function sendAndroidNotification($registatoin_ids, $mess, $code, $title, $notification_id, $notification_type, $redirectID, $postShareID)
    {
        //echo "</br> PUSH ::: ".$registatoin_ids;
        $response = array();
        $response['notification_title'] = $title;
        $response['notification_message'] = $mess;
        $response['notification_redirection_id'] = $redirectID;
        $response['notification_type'] = (string)$notification_type;
        $response['notification_id'] = (string)$notification_id;
        $response['post_share_id'] = (string)$postShareID;
        $response['code'] = (string)$code;
        $message1 = json_encode($response);
        // pr($message1);die;
        $message = array("Message" => $message1);

        $url = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );
        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_FAILONERROR, true);

        // Execute post
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        return $result;
    }

    public function checkAppleErrorResponse($fp)
    {

        //byte1=always 8, byte2=StatusCode, bytes3,4,5,6=identifier(rowID). Should return nothing if OK.
        $apple_error_response = fread($fp, 6);
        //NOTE: Make sure you set stream_set_blocking($fp, 0) or else fread will pause your script and wait forever when there is no response to be sent.

        if ($apple_error_response) {
            //unpack the error response (first byte 'command" should always be 8)
            $error_response = unpack('Ccommand/Cstatus_code/Nidentifier', $apple_error_response);

            if ($error_response['status_code'] == '0') {
                $error_response['status_code'] = '0-No errors encountered';
            } else if ($error_response['status_code'] == '1') {
                $error_response['status_code'] = '1-Processing error';
            } else if ($error_response['status_code'] == '2') {
                $error_response['status_code'] = '2-Missing device token';
            } else if ($error_response['status_code'] == '3') {
                $error_response['status_code'] = '3-Missing topic';
            } else if ($error_response['status_code'] == '4') {
                $error_response['status_code'] = '4-Missing payload';
            } else if ($error_response['status_code'] == '5') {
                $error_response['status_code'] = '5-Invalid token size';
            } else if ($error_response['status_code'] == '6') {
                $error_response['status_code'] = '6-Invalid topic size';
            } else if ($error_response['status_code'] == '7') {
                $error_response['status_code'] = '7-Invalid payload size';
            } else if ($error_response['status_code'] == '8') {
                $error_response['status_code'] = '8-Invalid token';
            } else if ($error_response['status_code'] == '255') {
                $error_response['status_code'] = '255-None (unknown)';
            } else {
                $error_response['status_code'] = $error_response['status_code'] . '-Not listed';
            }

            echo '<br><b>+ + + + + + ERROR</b> Response Command:<b>' . $error_response['command'] . '</b>&nbsp;&nbsp;&nbsp;Identifier:<b>' . $error_response['identifier'] . '</b>&nbsp;&nbsp;&nbsp;Status:<b>' . $error_response['status_code'] . '</b><br>';
            echo 'Identifier is the rowID (index) in the database that caused the problem, and Apple will disconnect you from server. To continue sending Push Notifications, just start at the next rowID after this Identifier.<br>';

            return true;
        }
        return false;
    }

    public function sendAppleNotification($registatoin_ids, $message, $code, $type, $title, $notification_id, $redirectID, $postShareID)
    {
        // Put your private key's passphrase here:
        $passphrase = '';

        // Put your alert message here:


        $message = $message;
        if (APPLE_API_STATUS == 1) {
            if (!defined("PRODUCTION_MODE")) define("PRODUCTION_MODE", true);
        } else {
            if (!defined("PRODUCTION_MODE")) define("PRODUCTION_MODE", false);
        }
        ////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();

        if (APPLE_API_STATUS == 1)
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'SayScapePushNotificationsProductionCertificates.pem');
        else
            stream_context_set_option($ctx, 'ssl', 'local_cert', 'SayScapePushNotificationsDevelopmentCertificates.pem');

        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);


        // Open a connection to the APNS server
        if (APPLE_API_STATUS == 1) {
            $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err,
                $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        } else {
            $fp = stream_socket_client(
                'ssl://gateway.sandbox.push.apple.com:2195', $err,
                $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        }

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array(
            'alert' => array("title" => $title, "body" => $message),
            'sound' => 'default'
        );
        $body['aps']['notification_type'] = $type;
        $body['aps']['code'] = $code;
        $body['aps']['notification_redirection_id'] = $redirectID;
        $body['aps']['notification_title'] = $title;
        $body['aps']['notification_message'] = $message;
        $body['aps']['post_share_id'] = $postShareID;

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        //$msg .= chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $registatoin_ids)) . pack        ("n",strlen($payload)) . $payload;
        foreach ($registatoin_ids as $deviceToken) {
            //echo $deviceToken . " ";
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            //$this->checkAppleErrorResponse($fp);
            //echo "<br/>";
        }

        // Close the connection to the server
        fclose($fp);
        return $result;
    }

    // for get user profile pic
    public function getUserProfilePic($userID){
        $result = base_url().'/upload/userprofile/defaultProfile.png';

        $userDetail = $this->user->getUserdetails($userID, $userID);
        // echo "<pre>"; print_r($userDetail); die();
        if(!empty($userDetail)){
            $result = (isset($userDetail['profileImage']))?$userDetail['profileImage']:$result;
        }
        return $result;
    }

    public function isfollowing($table, $arrWhere)
    {
        return $this->db->where($arrWhere)
            ->or_where(['userID' => $arrWhere['followerID'], 'followerID' => $arrWhere['userID']])
            ->get($table)->row();
    }

    public function comment_delete($table, $where)
    {
        $this->db->or_where(['postCommentID' => $where['postCommentID'], 'parentCommentID' => $where['postCommentID']])
            ->update($table, ['isDelete' => 1]);
        return $this->db->affected_rows();
    }
    
    public function getMuteUser($table, $columns = '*', $where = '1 = 1', $order_by = null, $sort_by = 'DESC')
    {
        $this->db->select($columns)
            ->from($table)
            ->where($where);
        if ($order_by != null) {
            $this->db->order_by($order_by, $sort_by);
        }
        return $this->db->get()->row_array();
    }
}

