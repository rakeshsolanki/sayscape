<?php

class GroupModel extends CI_Model {

 public function insertdata($data)
 {
    $res = $this->db->insert_batch('tblusergroup',$data);
    if($res)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
            
 }
 public function getMembers($userID,$groupID)
 {
    $query = $this->db->select("ug.* , u.userID , u.name , u.username , u.email , u.isVerified , if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,CONCAT('".base_url()."upload/usercover/',u.coverImage) as coverImage")
        ->from("tblusergroup ug")
        ->join("tbluser u","u.userID = ug.userID and u.isActive = 1 and u.isDelete = 0") 
        ->where("u.isActive = 1 and ug.isActive = 1 and u.isDelete= 0 and ug.isDelete= 0 and ug.groupID = $groupID ")
        ->where("u.userID NOT IN (
            SELECT
                tbu.blockUserID
            FROM
                tblblockuser tbu
            WHERE
                tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
            UNION
            SELECT
                tbu1.userID
            FROM
                tblblockuser tbu1
            WHERE
                tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
        )")
        ->get()->result_array();
        return $query;
 }

//  public function getGroupPosts($groupID)
//  {
//     $query = $this->db->select("gp.* , p.* , GROUP_CONCAT(sm.filename SEPARATOR ',') as filename")
//         ->from("tblgrouppost gp")
//         ->join("tblpost p","gp.postID = p.postID") 
//         ->join("(select * from tblmedia m where (m.isActive =1 or m.isActive IS NULL) and (m.isDelete = 0 or m.isDelete IS NULL)) as sm" ,"sm.comID = p.postID" ,"left")
//         ->where("gp.isActive = 1 and p.isActive = 1 and gp.isDelete= 0 and p.isDelete= 0  and gp.groupID = $groupID ")
//         ->group_by("p.postID")
//         ->get()->result_array();
//         return $query;
//  }
public function getGroupPosts($userID,$groupID){
         $this->db->select("tp.*,tp.updatedDateTime as datetime,u.userID,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,'' as reshareName,0 as postShareID")
        ->from("tblusergroup ug")
        ->join("tblgroup tg","tg.groupID = ug.groupID AND tg.type = 0 AND tg.isActive = 1 AND tg.isDelete = 0")
        ->join("tbluser u","u.userID = ug.userID and u.isActive = 1 and u.isDelete = 0","LEFT")
        ->join("tblpost tp","tp.userID = u.userID","LEFT")
        ->join("tblsettings ts1","ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0","LEFT")
        ->join("tblpostlike tpl","tpl.postID = tp.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")
        ->join("tblpostlike tpl1","tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")
        ->join("tblpostshare tps","tps.userID = $userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0","LEFT")
        ->where("ug.isActive",1)
        ->where("ug.isDelete",0)
        ->where("tp.isActive",1)
        ->where("tp.isDelete",0)
        ->where("tp.postType",0)
        ->where("ug.groupID",$groupID)
        ->where("u.userID NOT IN (
            SELECT
                tbu.blockUserID
            FROM
                tblblockuser tbu
            WHERE
                tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
            UNION
            SELECT
                tbu1.userID
            FROM
                tblblockuser tbu1
            WHERE
                tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
        )")
        ->where("tp.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
        )")
        ->where("IF(ts1.blacklistKeyword != '', tp.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1)");

        $query1 = $this->db->get_compiled_select();
       // Add Reshare Post by unioun
        $this->db->select("p.*,tprs.updatedDateTime as datetime,u.userID,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,,if((SELECT postID  FROM `tblpostshare` WHERE postShareID=tprs.postShareID AND `postID` = p.postID AND `userID` = $userID GROUP BY postID) is null,'0',1) as isReshare,1 as isResharePost,urs.username as reshareName,tprs.postShareID as postShareID")
        ->from("tblpostshare tprs")
        ->join("tbluser urs","urs.userID = tprs.userID and urs.isActive = 1 and urs.isDelete = 0")
        ->join("tblpost p","p.postID = tprs.postID AND p.isActive = 1 and p.isDelete= 0")
        ->join("tbluser u","u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
        ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")
        ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")
        ->where("tprs.isActive = 1 and tprs.isDelete= 0")
        ->where("p.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
        )")
        ->where("(tprs.userID IN(SELECT userID  FROM `tblusergroup` WHERE `groupID` = $groupID) OR tprs.userID = $userID)")
        ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post	
        $this->db->where("p.postType",0);
        $query2 = $this->db->get_compiled_select();

        $query = $this->db->query($query1." UNION ".$query2." order by datetime desc");
        return $query->result_array();

 }
//  public function getGroups($userID)
//  {
//     $query = $this->db->select("g.*")
//         ->from("tblgroup g")
//         ->where("g.isActive = 1 and g.isDelete= 0  and g.userID = $userID and g.type = 0 ")
//         ->get()->result_array();
//         return $query;
//  }

 public function getGroupList($userID,$type = 0)
 { 
     if($type == 1){
        $query = $this->db->select("tg.groupID,tg.groupName,IF(tg.groupProfile != '',CONCAT('".base_url()."upload/group/',tg.groupProfile),'') as groupProfile ,GROUP_CONCAT(DISTINCT(tu.userName)) as membersName,count(DISTINCT(tug.userID)) as totalmembers,IFNULL((SELECT text FROM `tblmessage` WHERE `toUserID` = tg.groupID AND `type` = 1 ORDER BY createdDateTime DESC LIMIT 0, 1),'') as lastMessage,IFNULL((SELECT createdDateTime  FROM `tblmessage` WHERE `toUserID` = tg.groupID AND `type` = 1 ORDER BY createdDateTime DESC LIMIT 0, 1),'') as date")
        ->from(TBL_GROUP." tg")
        ->join(TBL_USER_GROUP." tug","tg.groupID = tug.groupID AND tug.isActive = 1 AND tug.isDelete = 0","LEFT")
        ->join(TBL_USER." tu","((`tu`.`userID` = `tug`.`userID`) OR (`tu`.`userID` = `tg`.`userID`)) AND `tu`.`userID` != ".$userID." and tu.isActive = 1 and tu.isDelete = 0","LEFT")
        ->join(TBL_MESSAGE." tm","tm.toUserID = tg.groupID AND tm.type = 1","LEFT")
        ->where('(tg.`userID` = '.$userID.' OR tg.groupID IN(SELECT groupID FROM tblusergroup WHERE userID = '.$userID.' ) )')->where('tg.`type`',1)->where('tg.`isActive`',1)->where('tg.`isDelete`',0)
        ->group_by('tg.groupID')->order_by('date','DESC')
        ->get()->result_array();
     }else{
        $query = $this->db->select("tg.groupID,tg.type,tg.groupName,IF(tg.groupProfile != '',CONCAT('".base_url()."upload/group/',tg.groupProfile),'') as groupProfile ,GROUP_CONCAT(tu.userName) as membersName,count(tug.userID) as totalmembers,'' as lastMessage, tg.createdDateTime as date")
        ->from(TBL_GROUP." tg")
        ->join(TBL_USER_GROUP." tug","tg.groupID = tug.groupID AND tug.isActive = 1 AND tug.isDelete = 0","LEFT")
        ->join(TBL_USER." tu","tu.userID = tug.userID and tu.isActive = 1 and tu.isDelete = 0","LEFT")
        ->where('(tg.`userID` = '.$userID.')')->where('tg.`type`',0)->where('tg.`isActive`',1)->where('tg.`isDelete`',0)
        ->group_by('tg.groupID')->order_by('date','DESC')
        ->get()->result_array();
     }
       
        return $query;    
 }

 public function getTotalMembers($groupID)
 {
    $query = $this->db->query("SELECT * FROM " . TBL_USER_GROUP ." WHERE groupID = $groupID");    
        return $query->num_rows();
 }

 public function getPostMedia($userID,$groupID)
 {
    $query = $this->db->select("m.mediaID,m.comID,m.userID,m.type,m.filename,u.userID,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,if(tps.status is null,'0',tps.status) as isReshare,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked")
    ->from("tblmedia m")
    ->join("tbluser u","u.userID = m.userID and u.isActive = 1 and u.isDelete = 0")
    ->join("tblpost p","p.postID = m.comID") 
    ->join("tblsettings ts1","ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0","LEFT")
    ->join("tblusergroup ug","ug.userID = p.userID")
    ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")
    ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")
    ->join("tblpostshare tps","tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0","LEFT")
    ->where("m.isActive = 1 and m.isDelete= 0 and p.isActive = 1 and p.isDelete= 0 and ug.groupID = $groupID and mediaRef = 0")
    ->where("u.userID NOT IN (
        SELECT
            tbu.blockUserID
        FROM
            tblblockuser tbu
        WHERE
            tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
        UNION
        SELECT
            tbu1.userID
        FROM
            tblblockuser tbu1
        WHERE
            tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
    )")
    ->where("p.postID NOT IN (
        SELECT
            thp.postID
        FROM
            tblhidepost thp
        WHERE
            thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
    )")
    ->where("IF(ts1.blacklistKeyword != '', p.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
    ->get()->result_array();
    return $query;
 }

 public function set_delete_group_member($groupID, $userIDs) {
    $this->db->where('groupID',$groupID)->where_in('userID',$userIDs)
    ->update(TBL_USER_GROUP, ['isDelete' => 1]);
    return $this->db->affected_rows();
}


 public function groupName($groupID){
     $this->db->where('groupID',$groupID);
     $q = $this->db->get('tblgroup');
     return $q->row_array();
 }

}

?>