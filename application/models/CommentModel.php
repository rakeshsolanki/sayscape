<?php


class CommentModel extends CI_Model {

    //comment on admin
    public function getAllComments()
    {
        $this->db->select("p.*,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isOffical")
            ->from("tblpostcomment p")
            ->join("tbluser u","u.userID = p.userID")
            ->join("tblsettings ts1","ts1.userID = u.userID and ts1.isActive = 1 and ts1.isDelete = 0","LEFT")
            ->where('p.isDelete', 0)
            ->where('p.isActive', 1);

        $this->db->order_by('datetime','DESC');
        $query = $this->db->get();

        return $query->result_array();
    }

    //Reported post on admin
    public function getAllReportComments($postType = NULL)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,r.*,ru.username as reportby,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isOffical")
            ->from("tblreport r")
            ->join("tblpostcomment p","p.postCommentID = r.comID")
            ->join("tbluser u","u.userID = p.userID")
            ->join("tbluser ru","ru.userID = r.userID")
            ->join("tblsettings ts1","ts1.userID = u.userID and ts1.isActive = 1 and ts1.isDelete = 0","LEFT")
            ->where('r.reportType', 2)
            ->where('p.isDelete', 0)
            ->where('p.isActive', 1);
        $this->db->order_by('datetime','DESC');
        $query = $this->db->get();

        return $query->result_array();
    }
}