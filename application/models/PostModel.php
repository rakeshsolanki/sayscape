<?php

class PostModel extends CI_Model
{

    public function insertdata($data)
    {
        $res = $this->db->insert_batch(TBL_MEDIA, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }

    }

    /*public function getPosts()
    {
        $query = $this->db->select('p.* ,GROUP_CONCAT(sm.filename SEPARATOR ",") as filename')
        ->from('tblpost p')
        ->join('tbluserfollower uf','uf.userID = p.userID')
        ->join('(select * from tblmedia m where (m.isActive =1 or m.isActive IS NULL) and (m.isDelete = 0 or m.isDelete IS NULL)) as sm' ,'sm.postID = p.postID' ,'left')
        ->where('p.isActive = 1 and p.isDelete= 0')
        ->group_by('p.postID')
        ->get()->result_array();
        return $query;
    }*/

   /*public function getPosts($userID, $postType = NULL, $totalPages = FALSE, $pageNumber = 0)
   {
       $this->db->select("p.postID, p.userID, p.text, p.gif, p.isPermanent, p.isexpireOn, p.expiryDateTime, p.image, p.postType, p.parentPostID, p.isPollPost, p.isTop, p.isActive, p.isDelete, p.createdBy, p.createdDateTime, p.updatedDateTime,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare ,0 as isResharePost,''as reshareName,0 as postShareID")
           ->from("tblpost p")
           ->join("tbluserfollower uf", "uf.followerID = p.userID")
           ->join("tbluser u", "u.userID = uf.followerID and u.isActive = 1 and u.isDelete = 0")
           ->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
           ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
           ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
           ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
           ->where("p.isActive = 1 and p.isDelete= 0 and p.userID NOT IN (
               SELECT
                   tbu.blockUserID
               FROM
                   tblblockuser tbu
               WHERE
                   tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
               UNION
               SELECT
                   tbu1.userID
               FROM
                   tblblockuser tbu1
               WHERE
                   tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
           )")
           ->where("p.postID NOT IN (
               SELECT
                   thp.postID
               FROM
                   tblhidepost thp
               WHERE
                   thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
           )")
           ->where("IF(ts1.blacklistKeyword != '', p.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
           ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
       //0->Normal Post , 2->Veteran Post
       if ($postType == 0 || $postType == 2)
           $this->db->where("p.postType", $postType);
       $query1 = $this->db->get_compiled_select();

       $this->db->select("p.postID, p.userID, p.text, p.gif, p.isPermanent, p.isexpireOn, p.expiryDateTime, p.image, p.postType, p.parentPostID, p.isPollPost, p.isTop, p.isActive, p.isDelete, p.createdBy, p.createdDateTime, p.updatedDateTime,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,''as reshareName,0 as postShareID")
           ->from("tblpost p")
           ->join("tbluser u", "u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
           ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
           ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
           ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
           ->where("p.isActive = 1 and p.isDelete= 0 and p.userID NOT IN (
               SELECT
                   tbu.blockUserID
               FROM
                   tblblockuser tbu
               WHERE
                   tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
               UNION
               SELECT
                   tbu1.userID
               FROM
                   tblblockuser tbu1
               WHERE
                   tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
           )")
           ->where("p.postID NOT IN (
               SELECT
                   thp.postID
               FROM
                   tblhidepost thp
               WHERE
                   thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
           )")
           ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
       //0->Normal Post , 2->Veteran Post
       if ($postType == 0 || $postType == 2)
           $this->db->where("p.postType", $postType);
       $query2 = $this->db->get_compiled_select();

       //Add Reshare Post by unioun
       //$this->db->select("p.*,IFNULL(p.gif, '') as gif,tprs.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,,if((SELECT postID  FROM `tblpostshare` WHERE postShareID=tprs.postShareID AND `postID` = p.postID AND `userID` = $userID GROUP BY postID) is null,'0',1) as isReshare,1 as isResharePost,urs.username as reshareName,tprs.postShareID as postShareID")
       $this->db->select("p.postID, p.userID, p.text, p.gif, p.isPermanent, p.isexpireOn, p.expiryDateTime, p.image, p.postType, p.parentPostID, p.isPollPost, p.isTop, p.isActive, p.isDelete, p.createdBy, p.createdDateTime, p.updatedDateTime,IFNULL(p.gif, '') as gif,tprs.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,,if((SELECT postID  FROM `tblpostshare` WHERE postShareID=tprs.postShareID AND `postID` = p.postID AND `userID` = $userID GROUP BY postID) is null,'0',1) as isReshare,1 as isResharePost,urs.username as reshareName,tprs.postShareID as postShareID")
           ->from("tblpostshare tprs")
           ->join("tbluser urs", "urs.userID = tprs.userID")
           ->join("tblpost p", "p.postID = tprs.postID AND p.isActive = 1 and p.isDelete= 0")
           ->join("tbluser u", "u.userID = p.userID")
           ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
           ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
           ->where("tprs.isActive = 1 and tprs.isDelete= 0")
           ->where("tprs.userID NOT IN (
               SELECT
                   tbu.blockUserID
               FROM
                   tblblockuser tbu
               WHERE
                   tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
               UNION
               SELECT
                   tbu1.userID
               FROM
                   tblblockuser tbu1
               WHERE
                   tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
           )")
           ->where("p.userID NOT IN (
               SELECT
                   tbu.blockUserID
               FROM
                   tblblockuser tbu
               WHERE
                   tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
               UNION
               SELECT
                   tbu1.userID
               FROM
                   tblblockuser tbu1
               WHERE
                   tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
           )")
           ->where("p.postID NOT IN (
               SELECT
                   thp.postID
               FROM
                   tblhidepost thp
               WHERE
                   thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
           )")
           ->where("(tprs.userID IN(SELECT followerID  FROM `tbluserfollower` WHERE `userID` = $userID AND isActive = 1 AND isDelete=0) OR tprs.userID = $userID)")
           ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
       //0->Normal Post , 2->Veteran Post
       if ($postType == 0 || $postType == 2)
           $this->db->where("p.postType", $postType);
       $query3 = $this->db->get_compiled_select();
       if ($totalPages != FALSE) {
           $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by postID desc,datetime desc");
           $totalNumberPost = $query->num_rows();
           return $totalPages = round($totalNumberPost / ONE_PAGE_LIMIT);
       }
       if (isset($pageNumber) && $pageNumber != 'ALL') {
           $page = $pageNumber;
           $offset = ONE_PAGE_LIMIT * ($page - 1);

       } else {
           $page = 0;
           $offset = 0;
       }
       if ($pageNumber != 'ALL')
           $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by postID desc, datetime desc LIMIT " . $offset . "," . ONE_PAGE_LIMIT);
       else
           $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by datetime desc, datetime desc");

       return $query->result_array();
   }*/

    public function getPosts($userID, $postType = NULL, $totalPages = FALSE, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare ,0 as isResharePost,''as reshareName,0 as postShareID")
            ->from("tblpost p")
            ->join("tbluserfollower uf", "uf.followerID = p.userID")
            ->join("tbluser u", "u.userID = uf.followerID and u.isActive = 1 and u.isDelete = 0")
            ->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("p.isActive = 1 and p.isDelete= 0 and uf.userID = $userID and uf.status = 1 and uf.isActive = 1 and uf.isDelete = 0 AND u.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
                UNION --
                SELECT
                    tmu.muteUserID
                FROM
                    tblmuteuser tmu
                WHERE
                    tmu.userID = $userID AND tmu.isActive = 1 AND tmu.isDelete = 0
                UNION
                SELECT
                    tmu1.userID
                FROM
                    tblmuteuser tmu1
                WHERE
                    tmu1.muteUserID = $userID AND tmu1.isActive = 1 AND tmu1.isDelete = 0
            )")
            ->where("p.postID NOT IN (
                SELECT
                    thp.postID
                FROM
                    tblhidepost thp
                WHERE
                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
            )")
            ->where("IF(ts1.blacklistKeyword != '', p.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post, 4=>Responder Post
        if ($postType == 0 || $postType == 2 || $postType == 4)
            $this->db->where("p.postType", $postType);
        $query1 = $this->db->get_compiled_select();

        $this->db->select("p.*,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,''as reshareName,0 as postShareID")
            ->from("tblpost p")
            ->join("tbluser u", "u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("p.isActive = 1 and p.isDelete= 0 and p.userID = $userID")
            ->where("p.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
        )")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post, 4=>Responder Post
        if ($postType == 0 || $postType == 2 || $postType == 4)
            $this->db->where("p.postType", $postType);
        $query2 = $this->db->get_compiled_select();

        //Add Reshare Post by unioun
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,tprs.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,,if((SELECT postID  FROM `tblpostshare` WHERE postShareID=tprs.postShareID AND `postID` = p.postID AND `userID` = $userID GROUP BY postID) is null,'0',1) as isReshare,1 as isResharePost,urs.username as reshareName,tprs.postShareID as postShareID")
            ->from("tblpostshare tprs")
            ->join("tbluser urs", "urs.userID = tprs.userID and urs.isActive = 1 and urs.isDelete = 0")
            ->join("tblpost p", "p.postID = tprs.postID AND p.isActive = 1 and p.isDelete= 0")
            ->join("tbluser u", "u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->where("tprs.isActive = 1 and tprs.isDelete= 0")
            ->where("p.postID NOT IN (
                SELECT
                    thp.postID
                FROM
                    tblhidepost thp
                WHERE
                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
            )")
            ->where("(tprs.userID IN(SELECT followerID  FROM `tbluserfollower` WHERE `userID` = $userID AND isActive = 1 AND isDelete=0) OR tprs.userID = $userID)")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post, 4=>Responder Post
        if ($postType == 0 || $postType == 2 || $postType == 4)
            $this->db->where("p.postType", $postType);
        $query3 = $this->db->get_compiled_select();
        
        if ($totalPages != FALSE) {
            $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by datetime desc");
            $totalNumberPost = $query->num_rows();
            return $totalPages = round($totalNumberPost / $limit);
        }

        if (isset($pageNumber) && $pageNumber != 'ALL') {
            $page = $pageNumber;
            $offset = $limit * ($page - 1);

        } else {
            $page = 0;
            $offset = 0;
        }

        if ($pageNumber != 'ALL')
            $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by datetime desc LIMIT " . $offset . "," . $limit);
        else
            $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by datetime desc");

        return $query->result_array();
    }

    public function getPostRow($userID, $postType = NULL, $totalPages = FALSE, $pageNumber = 0,$perpage=false)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare ,0 as isResharePost,''as reshareName,0 as postShareID")
            ->from("tblpost p")
            ->join("tbluserfollower uf", "uf.followerID = p.userID")
            ->join("tbluser u", "u.userID = uf.followerID and u.isActive = 1 and u.isDelete = 0")
            ->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("p.isActive = 1 and p.isDelete= 0 and uf.userID = $userID and uf.status = 1 and uf.isActive = 1 and uf.isDelete = 0 AND u.userID NOT IN (
            SELECT
                tbu.blockUserID
            FROM
                tblblockuser tbu
            WHERE
                tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
            UNION
            SELECT
                tbu1.userID
            FROM
                tblblockuser tbu1
            WHERE
                tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
        )")
            ->where("p.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
        )")
            ->where("IF(ts1.blacklistKeyword != '', p.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post
        if ($postType == 0 || $postType == 2)
            $this->db->where("p.postType", $postType);
        $query1 = $this->db->get_compiled_select();

        $this->db->select("p.*,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,''as reshareName,0 as postShareID")
            ->from("tblpost p")
            ->join("tbluser u", "u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("p.isActive = 1 and p.isDelete= 0 and p.userID = $userID")
            ->where("p.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
        )")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post
        if ($postType == 0 || $postType == 2)
            $this->db->where("p.postType", $postType);
        $query2 = $this->db->get_compiled_select();

        //Add Reshare Post by unioun
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,tprs.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,,if((SELECT postID  FROM `tblpostshare` WHERE postShareID=tprs.postShareID AND `postID` = p.postID AND `userID` = $userID GROUP BY postID) is null,'0',1) as isReshare,1 as isResharePost,urs.username as reshareName,tprs.postShareID as postShareID")
            ->from("tblpostshare tprs")
            ->join("tbluser urs", "urs.userID = tprs.userID and urs.isActive = 1 and urs.isDelete = 0")
            ->join("tblpost p", "p.postID = tprs.postID AND p.isActive = 1 and p.isDelete= 0")
            ->join("tbluser u", "u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->where("tprs.isActive = 1 and tprs.isDelete= 0")
            ->where("p.postID NOT IN (
             SELECT
                 thp.postID
             FROM
                 tblhidepost thp
             WHERE
                 thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
         )")
            ->where("(tprs.userID IN(SELECT followerID  FROM `tbluserfollower` WHERE `userID` = $userID AND isActive = 1 AND isDelete=0) OR tprs.userID = $userID)")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post
        if ($postType == 0 || $postType == 2)
            $this->db->where("p.postType", $postType);
        $query3 = $this->db->get_compiled_select();

        if ($totalPages != FALSE) {
            $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by datetime desc");
            $totalNumberPost = $query->num_rows();
            return $totalPages = round($totalNumberPost / $perpage);
        }

        if (isset($pageNumber) && $pageNumber != 'ALL') {
            $page = $pageNumber;
            $offset = $perpage * ($page - 1);

        } else {
            $page = 0;
            $offset = 0;
        }

        if ($pageNumber != 'ALL')
            $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by datetime desc LIMIT " . $offset . "," . $perpage);
        else
            $query = $this->db->query($query1 . " UNION " . $query2 . " UNION " . $query3 . " order by datetime desc");

        return $query->result_array();
    }

    public function getSearchPosts($userID, $postType = NULL, $search_keyword = NULL, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,''as reshareName,0 as postShareID")
            ->from("tblpost p")
            ->join("tbluser u", "u.userID = p.userID")
            ->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("p.isActive = 1 and p.isDelete= 0 AND u.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->where("p.postID NOT IN (
                SELECT
                    thp.postID
                FROM
                    tblhidepost thp
                WHERE
                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
            )")
            ->where("IF(ts1.blacklistKeyword != '', p.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
            ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post
        if ($postType == 0 || $postType == 2)
            $this->db->where("p.postType", $postType);
        $this->db->like('p.text', $search_keyword, 'both')->order_by('datetime', 'DESC');
        if (!empty($pageNumber)) {
            $page = $pageNumber;
            $offset = $limit * ($page - 1);
            $this->db->limit($limit,$offset);
        }
        $query = $this->db->get();

        return $query->result_array();
    }

    //post on admin
    public function getAllPosts($postType = NULL)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,''as reshareName,0 as postShareID")
            ->from("tblpost p")
            ->join("tbluser u", "u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
            ->join("tblsettings ts1", "ts1.userID = u.userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = u.userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = u.userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = u.userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where('p.isDelete', 0)
            ->where('p.isActive', 1);
        //0->Normal Post , 2->Veteran Post
        if ($postType == 0 || $postType == 2)
            $this->db->where("p.postType", $postType);
        $this->db->order_by('datetime', 'DESC');
        $query = $this->db->get();

        return $query->result_array();
    }

    //Reported post on admin
    public function getAllReportPosts($postType = NULL)
    {
        $this->db->select("p.*,IFNULL(p.gif, '') as gif,ru.username as reportby,p.updatedDateTime as datetime,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,0 as isResharePost,''as reshareName,0 as postShareID")
            ->from("tblreport r")
            ->join("tblpost p", "p.postID = r.comID")
            ->join("tbluser u", "u.userID = p.userID and u.isActive = 1 and u.isDelete = 0")
            ->join("tbluser ru", "ru.userID = r.userID and ru.isActive = 1 and ru.isDelete = 0")
            ->join("tblsettings ts1", "ts1.userID = u.userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = p.postID and tpl.userID = u.userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = p.postID and tpl1.userID = u.userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = u.userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where('r.reportType', 0)
            ->where('p.isDelete', 0)
            ->where('p.isActive', 1);
        //0->Normal Post , 2->Veteran Post
        if ($postType == 0 || $postType == 2)
            $this->db->where("p.postType", $postType);
        $this->db->order_by('datetime', 'DESC');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function getParticularPost($userID, $postID, $postShareID=0)
    {
        return $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,tu.isVeteran,tu.isResponder,tu.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,IF(tps1.postShareID is null,0,1) as isResharePost,IF(tps1.postShareID is null,'',(SELECT username  FROM `tbluser` WHERE `userID` = tps1.userID)) as reshareName,IF(tps1.postShareID is null,0,tps1.postShareID) as postShareID")
            ->from("tblpost tp")
            ->join("tbluser tu", "tp.userID = tu.userID and tu.isActive = 1 and tu.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = tp.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->join("tblpostshare tps1", "tps1.postShareID = $postShareID and tps1.postID = tp.postID and tps1.isActive = 1 and tps1.isDelete = 0", "LEFT")
            ->where("tp.postID", $postID)
            ->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->where("tp.postID NOT IN (
                SELECT
                    thp.postID
                FROM
                    tblhidepost thp
                WHERE
                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
            )")
            ->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))")
            ->where("tp.isActive", 1)
            ->where("tp.isDelete", 0)
            ->get()->result_array();
    }

    public function getParticularPostBYPostID($userID, $postID, $postShareID=0)
    {
        return $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,tu.isVeteran,tu.isResponder,tu.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,IF(tps1.postShareID is null,0,1) as isResharePost,IF(tps1.postShareID is null,'',(SELECT username  FROM `tbluser` WHERE `userID` = tps1.userID)) as reshareName,IF(tps1.postShareID is null,0,tps1.postShareID) as postShareID")
            ->from("tblpost tp")
            ->join("tbluser tu", "tp.userID = tu.userID", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = tp.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->join("tblpostshare tps1", "tps1.postShareID = $postShareID and tps1.postID = tp.postID and tps1.isActive = 1 and tps1.isDelete = 0", "LEFT")
            ->where("tp.postID", $postID)
            ->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->where("tp.postID NOT IN (
                SELECT
                    thp.postID
                FROM
                    tblhidepost thp
                WHERE
                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
            )")
            ->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))")
            ->where("tp.isActive", 1)
            ->where("tp.isDelete", 0)
            ->get()->row_array();
    }

    public function totalComments($postID, $userID = "", $postShareID="")
    {
        // $query = $this->db->query("SELECT * FROM " . TBL_POST_COMMENT . " WHERE postID = $postID and isActive = 1 and isDelete = 0");
        // return $query->num_rows();
        $this->db->select("pc.*")
            ->from(TBL_POST_COMMENT . " pc")
            ->join("tbluser u", "u.userID = pc.userID")
            ->where("pc.postID", $postID)
            ->where("pc.isActive = 1 and pc.isDelete = 0");
        if ($userID != "") {
            $this->db->where("u.userID NOT IN (
                SELECT 
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )");
        }
        if(!empty($postShareID)){
            $this->db->where("pc.postShareID",$postShareID);
        }
        $resultData = $this->db->get()->result_array();
        return count($resultData);
    }

    public function totalShares($postID, $userID = "",$postShareID="")
    {
        // $query = $this->db->query("SELECT * FROM " . TBL_POST_SHARE . " WHERE postID = $postID and isActive = 1 and isDelete = 0");
        // return $query->num_rows();
        $this->db->select("pc.*")
            ->from(TBL_POST_SHARE . " pc")
            ->join("tbluser u", "u.userID = pc.userID")
            ->where("pc.postID", $postID)
            ->where("pc.isActive = 1 and pc.isDelete = 0");
        if ($userID != "") {
            $this->db->where("u.userID NOT IN (
                SELECT 
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )");
        }
        if(!empty($postShareID)){
            $this->db->where("pc.postShareID",$postShareID);
        }
        $resultData = $this->db->get()->result_array();
        return count($resultData);
    }

    public function totalLikes($postID, $userID = "",$postShareID="")
    {
        // $query = $this->db->query("SELECT * FROM " . TBL_POST_LIKE ." WHERE isLiked = 1 AND postID = $postID and isActive = 1 and isDelete = 0");    
        // return $query->num_rows();
        $this->db->select("pc.*")
            ->from(TBL_POST_LIKE . " pc")
            ->join("tbluser u", "u.userID = pc.userID")
            ->where("pc.isLiked", 1)
            ->where("pc.postID", $postID)
            ->where("pc.isActive = 1 and pc.isDelete = 0");
        if ($userID != "") {
            $this->db->where("u.userID NOT IN (
                SELECT 
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )");
        }
        if(!empty($postShareID)){
            $this->db->where("pc.postShareID",$postShareID);
        }
        $resultData = $this->db->get()->result_array();
        return count($resultData);
    }

    public function totalDisLikes($postID, $userID = "", $postShareID="")
    {
        // $query = $this->db->query("SELECT * FROM " . TBL_POST_LIKE ." WHERE isLiked = 0 AND postID = $postID and isActive = 1 and isDelete = 0");    
        // return $query->num_rows();
        $this->db->select("pc.*")
            ->from(TBL_POST_LIKE . " pc")
            ->join("tbluser u", "u.userID = pc.userID")
            ->where("pc.isLiked", 0)
            ->where("pc.postID", $postID)
            ->where("pc.isActive = 1 and pc.isDelete = 0");
        if ($userID != "") {
            $this->db->where("u.userID NOT IN (
                SELECT 
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )");
        }
        if(!empty($postShareID)){
            $this->db->where("pc.postShareID",$postShareID);
        }
        $resultData = $this->db->get()->result_array();
        return count($resultData);
    }

    public function getCommentsLikeName($postCommentID)
    {
        return $this->db->select("*")
            ->from(TBL_POST_COMMENT_LIKE)
            ->where("postCommentID", $postCommentID)
            //->where("isActive",1)
            // ->where("isDelete",0)
            //  ->where("isLiked",1)
            ->get()->result_array();
    }

    public function getComments($postID, $parentCommentID = "", $loginUserID = "",$postShareID="")
    {
        $result = array();
        $this->db->select("c.parentCommentID,c.text,c.gif,c.commentImage,IFNULL(c.gif, '') as gif,c.postCommentID,c.createdDateTime,u.userID,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('" . base_url() . "upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name")
            ->from("tblpostcomment c")
            ->join("tbluser u", "u.userID = c.userID and u.isActive = 1 and u.isDelete = 0")
            ->where("c.postID = $postID and c.isActive = 1 and c.isDelete = 0");
        if ($parentCommentID != "") {
            $this->db->where("c.parentCommentID = " . $parentCommentID);
        } else {
            $this->db->where("c.parentCommentID = 0");
        }
        if(!empty($postShareID)){
            $this->db->where("c.postShareID",$postShareID);
        }
        if ($loginUserID != "") {
            $this->db->where("u.userID NOT IN (
                SELECT 
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $loginUserID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $loginUserID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )");
        }
        $getPostComments = $this->db->get()->result_array();
        if (!empty($getPostComments)) {
            foreach ($getPostComments as $comment) {
                $postCommentID = $comment['postCommentID'];
                $res = $comment;
                $res['postID'] = $postID . "";
                $res['gif'] = trim($comment['gif']);
                if (isset($res['gif']) && trim($res['gif']) != "" && $res['gif'] == null || is_null($res['gif'])) {
                    $res['gif'] = "";
                }
                $res['commentImage'] = (isset($comment['commentImage']) && $comment['commentImage'] != "") ? base_url() . "upload/postmedia/" . $comment['commentImage'] : "";
                $res['isLiked'] = "0";
                $res['isdisLiked'] = "0";
                $res['totalLikes'] = 0;
                $res['totaldisLikes'] = 0;
                if (!empty($loginUserID)) {
                    $getAllPostCommentLikes = $this->CommonModel->get_all(TBL_POST_COMMENT_LIKE, ['*'], ['postID' => $postID, 'postCommentID' => $postCommentID, 'isLiked' => 1, 'isActive' => 1, 'isDelete' => 0]);
                    if (!empty($getAllPostCommentLikes)) {
                        $totalLikes = count($getAllPostCommentLikes);
                        $res['totalLikes'] = (int)$totalLikes;
                    }
                    $getAllPostCommentdisLikes = $this->CommonModel->get_all(TBL_POST_COMMENT_LIKE, ['*'], ['postID' => $postID, 'postCommentID' => $postCommentID, 'isLiked' => 0, 'isActive' => 1, 'isDelete' => 0]);
                    if (!empty($getAllPostCommentdisLikes)) {
                        $res['totaldisLikes'] = (int)count($getAllPostCommentdisLikes);
                    }
                    $checkPostCommentLike = $this->CommonModel->get_row(TBL_POST_COMMENT_LIKE, ['*'], ['userID' => $loginUserID, 'postID' => $postID, 'postCommentID' => $postCommentID, 'isLiked' => 1, 'isActive' => 1, 'isDelete' => 0]);
                    if (!empty($checkPostCommentLike)) {
                        $res['isLiked'] = $checkPostCommentLike['isLiked'] . "";
                    }
                    $checkPostCommentLike = $this->CommonModel->get_row(TBL_POST_COMMENT_LIKE, ['*'], ['userID' => $loginUserID, 'postID' => $postID, 'postCommentID' => $postCommentID, 'isLiked' => 0, 'isActive' => 1, 'isDelete' => 0]);
                    if (!empty($checkPostCommentLike)) {
                        $res['isdisLiked'] = "1";
                    }
                }
                $checkSubComment = $this->post->getComments($postID, $postCommentID, $loginUserID);
                if (!empty($checkSubComment)) {
                    $res['comments'] = $checkSubComment;
                } else {
                    $res['comments'] = null;
                }
                $result[] = $res;
            }
        }
        return $result;
    }

    public function getVeteranPost($userID)
    {
        if (
            $this->db->select("userID")
                ->from("tbluser tu")
                ->where("tu.userID", $userID)
                ->where("tu.isVeteran", 1)
                ->where("tu.isActive", 1)
                ->where("tu.isDelete", 0)
                ->get()->num_rows() > 0
        ) {
            return $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,tp.updatedDateTime as datetime,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,tu.isVeteran,u.isResponder,tu.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,IF(tps.postShareID is null,0,1) as isResharePost,IF(tps.postShareID is null,'',(SELECT username  FROM `tbluser` WHERE `userID` = tps.userID)) as reshareName,IF(tps.postShareID is null,0,tps.postShareID) as postShareID")
                ->from("tblpost tp")
                ->join("tbluser tu", "tu.userID = tp.userID AND tu.isActive = 1 AND tu.isDelete = 0", "LEFT")
                ->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
                ->join("tblpostlike tpl", "tpl.postID = tp.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
                ->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
                ->join("tblpostshare tps", "tps.userID = tu.userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
                ->where("tp.postType", 2)
                ->where("tp.isActive", 1)
                ->where("tp.isDelete", 0)
                ->where("tp.postID NOT IN (
                SELECT
                    thp.postID
                FROM
                    tblhidepost thp
                WHERE
                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
            )")
                ->where("tu.userID NOT IN ( 
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
                ->where("IF(ts1.blacklistKeyword != '', tp.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
                ->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))")
                ->order_by("tp.createdDateTime", "DESC")
                ->get()->result_array();
        } else {
            return false;
        }
    }

    public function admin_getVeteranPost($adminID)
    {
        return $this->db->select("tp.*,IFNULL(tp.gif, '') as gif")
            ->from("tblpost tp")
            ->where("tp.userID", $adminID)
            ->where("tp.postType", 2)
            ->where("tp.isActive", 1)
            ->where("tp.isDelete", 0)
            ->get()->result_array();
    }

    public function getUserBookmarkPost($userID, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {
        $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,tu.isVeteran,tu.isResponder,tu.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare")
            ->from("tblpostbookmark tpb")
            ->join("tblpost tp", "tpb.postID = tp.postID", "LEFT")
            ->join("tbluser tu", "tu.userID = tp.userID and tu.isActive = 1 and tu.isDelete = 0", "LEFT")
            ->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = tp.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("tpb.userID", $userID)
            ->where("tpb.isActive", 1)
            ->where("tpb.isDelete", 0)
            ->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))")
            ->where("tu.userID NOT IN (
                    SELECT
                        tbu.blockUserID
                    FROM
                        tblblockuser tbu
                    WHERE
                        tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                    UNION
                    SELECT
                        tbu1.userID
                    FROM
                        tblblockuser tbu1
                    WHERE
                        tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
                )")
            ->where("tp.postID NOT IN (
                    SELECT
                        thp.postID
                    FROM
                        tblhidepost thp
                    WHERE
                        thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
                )")
            ->where("IF(ts1.blacklistKeyword != '', tp.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
            ->order_by("tp.createdDateTime", "DESC");
            if (!empty($pageNumber)) {
                $page = $pageNumber;
                $offset = $limit * ($page - 1);
                $this->db->limit($limit,$offset);
            }
        $query = $this->db->get()->result_array();
        return $query;
    }

    public function insertBookmark($data)
    {
        return $this->db->insert(TBL_POST_BOOKMARK, $data);
    }

    public function updateBookmark($userID, $postID)
    {
        if ($this->CommonModel->update(TBL_POST_BOOKMARK, ['isActive' => 0], ['userID' => $userID, 'postID' => $postID]) > 0) {
            return true;
        }
        return false;
    }

    public function getTrendingPost($userID, $trendingTime, $totalPages = FALSE, $pageNumber = 0, $limit = ONE_PAGE_LIMIT)
    {

        $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,isVeteran,tu.isResponder,isOffical,(COUNT(tpc.postCommentID)+COUNT(tps.postShareID)+COUNT(tpl.postLikeID)) as TrendingPoints,if(tpl1.isLiked is null,'0',tpl1.isLiked) as isLiked,if(tpl2.isLiked is null,'0',1) as isDisLiked,if(tps1.status is null,'0',tps1.status) as isReshare");
        $this->db->from("tblpost as tp");
        $this->db->join("tbluser tu", "tu.userID = tp.userID", "LEFT");
        $this->db->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT");
        $this->db->join("tblpostcomment as tpc", "tpc.postID = tp.postID", "LEFT");
        $this->db->join("tblpostshare as tps", "tps.postID = tp.postID", "LEFT");
        $this->db->join("tblpostlike as tpl", "tpl.postID = tp.postID and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT");
        $this->db->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 1 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT");
        $this->db->join("tblpostlike tpl2", "tpl2.postID = tp.postID and tpl2.userID = $userID and tpl2.isLiked = 0 and tpl2.isActive = 1 and tpl2.isDelete = 0", "LEFT");
        $this->db->join("tblpostshare tps1", "tps1.userID = $userID and tps1.postID = tp.postID and tps1.isActive = 1 and tps1.isDelete = 0", "LEFT");
        $this->db->where("tp.isActive", 1);
        $this->db->where("tp.isDelete", 0);
        $this->db->where("tp.postType", 0);
        $this->db->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))");
        $this->db->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )");
        $this->db->where("tp.postID NOT IN (
                SELECT
                    thp.postID
                FROM
                    tblhidepost thp
                WHERE
                    thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
            )");
        switch ($trendingTime) {
            case 0:
                $this->db->where("tp.createdDateTime > DATE_SUB(NOW(), INTERVAL 1 DAY)");
                break;
            case 1:
                $this->db->where("tp.createdDateTime > DATE_SUB(NOW(), INTERVAL 1 WEEK)");
                break;
            case 2:
                $this->db->where("MONTH(tp.createdDateTime) = MONTH(CURRENT_DATE())");
                break;
            case 3:
                $this->db->where("YEAR(tp.createdDateTime) = YEAR(CURRENT_DATE())");
                break;
        }
        $this->db->where("IF(ts1.blacklistKeyword != '', tp.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ");
        $this->db->group_by("tp.postID");
        $this->db->order_by("TrendingPoints", "desc");
        if ($totalPages != FALSE) {
            $totalNumberPost = $this->db->get()->num_rows();
            return $totalPages = round($totalNumberPost / $limit);
        }
        if (isset($pageNumber) && $pageNumber != 'ALL') {
            $page = $pageNumber;
            $offset = $limit * ($page - 1);
        } else {
            $page = 0;
            $offset = 0;
        }
        if ($pageNumber != 'ALL')
            $this->db->limit($limit,$offset);

        return $this->db->get()->result_array();
    }

    public function getPostLikeUserList($userID, $postID)
    {
        return $this->db->select("tu.userID,tu.name,tu.username,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,if(uf.status is null,'0',uf.status) as isfollow")
            ->from("tblpostlike tpl")
            ->join("tbluser tu", "tpl.userID = tu.userID and tu.isActive=1 and tu.isDelete=0")
            ->join("tbluserfollower uf", "uf.followerID = tu.userID and uf.userID = $userID and uf.isActive = 1 and uf.isDelete = 0", "LEFT")
            ->where("tpl.postID", $postID)
            ->where("tpl.isActive", 1)
            ->where("tpl.isDelete", 0)
            ->where("tpl.isLiked", 1)
            ->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->order_by("tpl.postLikeID", "desc")
            ->get()->result_array();
    }

    public function getPostDisLikeUserList($userID, $postID)
    {
        return $this->db->select("tu.userID,tu.name,tu.username,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,if(uf.status is null,'0',uf.status) as isfollow")
            ->from("tblpostlike tpl")
            ->join("tbluser tu", "tpl.userID = tu.userID and tu.isActive = 1 and tu.isDelete=0")
            ->join("tbluserfollower uf", "uf.followerID = tu.userID and uf.userID = $userID", "LEFT")
            ->where("tpl.postID", $postID)
            ->where("tpl.isActive", 1)
            ->where("tpl.isDelete", 0)
            ->where("tpl.isLiked", 0)
            ->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->order_by('tpl.postLikeID','DESC')
            ->get()->result_array();
    }

    public function getPostReshareUserList($userID, $postID)
    {
        return $this->db->select("tu.userID,tu.name,tu.username,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,if(uf.status is null,'0',uf.status) as isfollow")
            ->from("tblpostshare tps")
            ->join("tbluser tu", "tu.userID = tps.userID and tu.isActive = 1 and tu.isDelete = 0", "LEFT")
            ->join("tbluserfollower uf", "uf.followerID = tu.userID and uf.userID = $userID", "LEFT")
            ->where("tps.postID", $postID)
            ->where("tps.isActive", 1)
            ->where("tps.isDelete", 0)
            ->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->order_by('tps.postShareID','DESC')
            ->get()->result_array();
    }

    public function getHidePost($userID)
    {
        return $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,tu.isVeteran,u.isResponder,tu.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare")
            ->from("tblhidepost thp")
            ->join("tblpost tp", "tp.postID = thp.postID", "LEFT")
            ->join("tbluser tu", "tu.userID = tp.userID and tu.isActive = 1 and tu.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = tp.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = $userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("thp.userID", $userID)
            ->where("thp.isActive", 1)
            ->where("thp.isDelete", 0)
            ->get()->result_array();
    }

    /*public function getHashTagPost($hashTag,$userID)
    {
        return $this->db->select("p.*,if((LEFT(u.profileImage , 5) = 'https') OR (LEFT(u.profileImage , 4) = 'http'),u.profileImage,CONCAT('".base_url()."upload/userprofile/',u.profileImage)) as profileImage,u.username,u.name,u.isVerified,u.isVeteran,u.isResponder,u.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare")
        ->from("tblpost p")
        ->join("tbluser u","u.userID = p.userID")
        ->join("tblsettings ts1","ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0","LEFT")
        ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")
        ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")
        ->join("tblpostshare tps","tps.userID = $userID and tps.postID = p.postID and tps.isActive = 1 and tps.isDelete = 0","LEFT")
        ->where("p.isActive = 1 and p.isDelete= 0 and p.postType = 0 AND u.userID NOT IN (
            SELECT
                tbu.blockUserID
            FROM
                tblblockuser tbu
            WHERE
                tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
            UNION
            SELECT
                tbu1.userID
            FROM
                tblblockuser tbu1
            WHERE
                tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
        )")
        ->where("p.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
        )")
        ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime) ) ) )")
        ->where("p.text REGEXP ('$hashTag') ")
        ->where("IF(ts1.blacklistKeyword != '', p.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
        ->get()->result_array();
    }*/

    public function getTrendingHashtag($userID)
    {
        $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,isVeteran,u.isResponder,isOffical,(COUNT(tpc.postCommentID)+COUNT(tps.postShareID)+COUNT(tpl.postLikeID)) as TrendingPoints,if(tpl1.isLiked is null,'0',tpl1.isLiked) as isLiked,if(tpl2.isLiked is null,'0',1) as isDisLiked,if(tps1.status is null,'0',tps1.status) as isReshare");
        $this->db->from("tblpost as tp");
        $this->db->join("tbluser tu", "tu.userID = tp.userID and tu.isActive = 1 and tu.isDelete = 0", "LEFT");
        $this->db->join("tblsettings ts1", "ts1.userID = $userID and ts1.isActive = 1 and ts1.isDelete = 0", "LEFT");
        $this->db->join("tblpostcomment as tpc", "tpc.postID = tp.postID", "LEFT");
        $this->db->join("tblpostshare as tps", "tps.postID = tp.postID", "LEFT");
        $this->db->join("tblpostlike as tpl", "tpl.postID = tp.postID and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT");
        $this->db->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.userID = $userID and tpl1.isLiked = 1 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT");
        $this->db->join("tblpostlike tpl2", "tpl2.postID = tp.postID and tpl2.userID = $userID and tpl2.isLiked = 0 and tpl2.isActive = 1 and tpl2.isDelete = 0", "LEFT");
        $this->db->join("tblpostshare tps1", "tps1.userID = $userID and tps1.postID = tp.postID and tps1.isActive = 1 and tps1.isDelete = 0", "LEFT");
        $this->db->where("tp.isActive", 1);
        $this->db->where("tp.isDelete", 0);
        $this->db->where("tp.postType", 0);
        $this->db->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))");
        $this->db->where("tu.userID NOT IN (
            SELECT
                tbu.blockUserID
            FROM
                tblblockuser tbu
            WHERE
                tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
            UNION
            SELECT
                tbu1.userID
            FROM
                tblblockuser tbu1
            WHERE
                tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
        )");
        $this->db->where("tp.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $userID and thp.isActive = 1 AND thp.isDelete = 0
        )");
        $this->db->where("IF(ts1.blacklistKeyword != '', tp.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ");
        $this->db->group_by("tp.postID");
        $this->db->order_by("TrendingPoints", "desc");
        return $this->db->get()->result_array();
    }

    public function getUserSharePost($postShareID)
    {
        $query = $this->db->query("SELECT * FROM " . TBL_POST_SHARE . " WHERE postShareID = $postShareID and isActive = 1 and status = 1 and isDelete = 0");
        return $query->row_array();
    }

    //for get poll post answers avg by option
    public function getPollOptionAvgAns($postID, $optionID = "")
    {
        $totalPostCount = 0;
        $postAvg = 0;
        $query = $this->db->query("SELECT * FROM " . TBL_POST_POLL_ANSWER . " WHERE postID = $postID and isActive = 1 and isDelete = 0");
        $totalPostAns = $query->result_array();
        if (!empty($totalPostAns)) {
            $totalPostCount = count($totalPostAns);
            $totalOptionCount = 0;
            $query = $this->db->query("SELECT * FROM " . TBL_POST_POLL_ANSWER . " WHERE postID = $postID and optionID = $optionID and isActive = 1 and isDelete = 0");
            $totalOptionData = $query->result_array();
            if (!empty($totalOptionData)) {
                $totalOptionCount = count($totalOptionData);
            }
            $postAvg = round((((int)$totalOptionCount * 100) / (int)$totalPostCount));
        }
        return $postAvg . "";
    }

    //for check poll post anwer by user not
    public function checkPollPostHasUserAnswer($userID, $postID = "")
    {
        $result = "0";
        $checkAnswerExist = $this->CommonModel->get_row(TBL_POST_POLL_ANSWER, ['*'], ['userID' => $userID, 'postID' => $postID, 'isDelete' => 0]);
        if (!empty($checkAnswerExist)) {
            $result = "1";
        }
        return $result;
    }

    public function getPostCommentLikeUserList($userID, $postID, $postCommentID)
    {
        return $this->db->select("tu.userID,tu.name,tu.username,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,if(uf.status is null,'0',uf.status) as isfollow")
            ->from(TBL_POST_COMMENT_LIKE . " tpl")
            ->join("tbluser tu", "tpl.userID = tu.userID and tu.isActive=1 and tu.isDelete=0")
            ->join("tblpost tp", "tp.postID = tpl.postID and tpl.isActive=1 and tpl.isDelete=0")
            ->join("tbluserfollower uf", "uf.followerID = tu.userID and uf.userID = $userID and uf.isActive = 1 and uf.isDelete = 0", "LEFT")
            ->where("tpl.postID", $postID)
            ->where("tpl.postCommentID", $postCommentID)
            ->where("tpl.isActive", 1)
            ->where("tpl.isDelete", 0)
            ->where("tpl.isLiked", 1)
            ->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->order_by('tpl.postcommentLikeID','DESC')
            ->get()->result_array();
    }

    public function getPostCommentDisLikeUserList($userID, $postID, $postCommentID)
    {
        return $this->db->select("tu.userID,tu.name,tu.username,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,if(uf.status is null,'0',uf.status) as isfollow")
            ->from(TBL_POST_COMMENT_LIKE . " tpl")
            ->join("tbluser tu", "tpl.userID = tu.userID and tu.isActive = 1 and tu.isDelete=0")
            ->join("tblpost tp", "tp.postID = tpl.postID and tpl.isActive=1 and tpl.isDelete=0")
            ->join("tbluserfollower uf", "uf.followerID = tu.userID and uf.userID = $userID", "LEFT")
            ->where("tpl.postID", $postID)
            ->where("tpl.postCommentID", $postCommentID)
            ->where("tpl.isActive", 1)
            ->where("tpl.isDelete", 0)
            ->where("tpl.isLiked", 0)
            ->where("tu.userID NOT IN (
                SELECT
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )")
            ->order_by('tpl.postcommentLikeID','DESC')
            ->get()->result_array();
    }

    //postData
    public function postData($data = array(),$row = false,$no_of_row = false) {
        $result = array();
        $this->db->select('tblpost'.'.*');
        $this->db->from('tblpost');
        if(!empty($data)){
            if(!empty($data['postID'])){
                $this->db->where('tblpost'.'.postID', $data['postID']);
            }
            if(!empty($data['userID'])){
                $this->db->where('tblpost'.'.userID', $data['userID']);
            }
            if(!empty($data['isActive'])){
                $this->db->where('tblpost'.'.isActive', $data['isActive']);
            }
            if(!empty($data['isDelete'])){
                $this->db->where('tblpost'.'.isDelete', $data['isDelete']);
            }else{
                $this->db->where('tblpost'.'.isDelete', 0);
            }
            if(!empty($data['limit']) && !empty($data['offset'])){
                $this->db->limit($data['limit'], $data['offset']);
            }elseif(!empty($data['limit'])){
                $this->db->limit($data['limit']);
            }
            if(!empty($data['order'])){
                $this->db->order_by($data['order'], 'ASC');
            }
            if(!empty($data['order_high'])){
                $this->db->order_by($data['order_high'], 'DESC');
            }
        }
        $query = $this->db->get();
        if($row){
            $result = $query->row_array();
        }else{
            $result = $query->result_array();
        }
        if($no_of_row){
            $result = count($result);
        }
        return $result;
    }

    //userData
    public function userData($data = array(),$row = false,$no_of_row = false) {
        $result = array();
        $this->db->select('tbluser'.'.*');
        $this->db->from('tbluser');
        if(!empty($data)){
            if(!empty($data['userID'])){
                $this->db->where('tbluser'.'.userID', $data['userID']);
            }
            if(!empty($data['name'])){
                $this->db->where('tbluser'.'.name', $data['name']);
            }
            if(!empty($data['isActive'])){
                $this->db->where('tbluser'.'.isActive', $data['isActive']);
            }
            if(!empty($data['isDelete'])){
                $this->db->where('tbluser'.'.isDelete', $data['isDelete']);
            }else{
                $this->db->where('tbluser'.'.isDelete', 0);
            }
            if(!empty($data['limit']) && !empty($data['offset'])){
                $this->db->limit($data['limit'], $data['offset']);
            }elseif(!empty($data['limit'])){
                $this->db->limit($data['limit']);
            }
            if(!empty($data['order'])){
                $this->db->order_by($data['order'], 'ASC');
            }
            if(!empty($data['order_high'])){
                $this->db->order_by($data['order_high'], 'DESC');
            }
        }
        $query = $this->db->get();
        if($row){
            $result = $query->row_array();
        }else{
            $result = $query->result_array();
        }
        if($no_of_row){
            $result = count($result);
        }
        return $result;
    }

    //get Username
    public function getUsername($id) {
        $result = "";
        $this->db->where('userID', $id);
        $rowData = $this->db->get('tbluser')->row_array();
        if(!empty($rowData)){
            $result = $rowData['name'];
        }
        return $result;
    }

    // for get post response
    public function getPostResponseData($value, $userID="",$type='',$other=''){
        if($userID==""){
            $userID = $this->session->userdata('userID');
        }
        $media = array();
        $result = $this->CommonModel->get_all('tblmedia', ["mediaID", "filename", "type","video_thumb"], ["comID" => $value['postID'], "isActive" => 1, "isDelete" => 0, "mediaRef" => 0]);
        foreach ($result as $key => $res) {
            $media[$key]['mediaID'] = $res['mediaID'];
            $media[$key]['type'] = $res['type'];
            $media[$key]['video_thumb'] = (!empty($res['video_thumb'])) ? base_url() . "upload/postmedia/" . $res['video_thumb']:"";
            $media[$key]['filename'] = (!empty($res['filename'])) ? base_url() . "upload/postmedia/" . $res['filename']:"";
        }
        if (isset($value['text']) && $value['text'] == null || is_null($value['text'])) {
            $value['text'] = "";
        }
        if (isset($value['gif']) && $value['gif'] == null || is_null($value['gif'])) {
            $value['gif'] = "";
        }
        $value['ResharePostContent'] = "";
        $value['ResharePostEmoji'] = "";
        $value['ReshareUserImage'] = "";
        $value['reshareUserData'] = null;
        if (isset($value['postShareID']) && $value['postShareID'] > 0) {
            $getSharePost = $this->post->getUserSharePost($value['postShareID']);
            if (!empty($getSharePost)) {
                $value['isLiked'] = "0";
                $value['isdisLiked'] = "0";
                $likeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $value['postID'], 'postShareID' => $value['postShareID'], "isLiked" => 1, "isDelete" => 0]);
                if (!empty($likeResult)) {
                    $value['isLiked'] = $likeResult['isActive'];
                }
                $dislikeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $value['postID'], 'postShareID' => $value['postShareID'], "isLiked" => 0, "isDelete" => 0]);
                if (!empty($dislikeResult)) {
                    $value['isdisLiked'] = $dislikeResult['isActive'];
                }
                $value['ResharePostUserid'] = (isset($getSharePost['userID'])) ? $getSharePost['userID'] : "";
                $value['ResharePostDate'] = (isset($getSharePost['createdDateTime'])) ? $getSharePost['createdDateTime'] : "";
                $value['ResharePostContent'] = (isset($getSharePost['post'])) ? $getSharePost['post'] : "";
                $value['ResharePostEmoji'] = (isset($getSharePost['emoji'])) ? $getSharePost['emoji'] : "";
                $reshareUserdata = $this->user->getUserdetails($getSharePost['userID']);
                if (!empty($reshareUserdata)) {
                    $value['ReshareUserImage'] = (isset($reshareUserdata['profileImage'])) ? $reshareUserdata['profileImage'] : "";
                    $shareuserData = array();
                    $shareuserData['isVeteran'] = (isset($reshareUserdata['isVeteran'])) ? $reshareUserdata['isVeteran'] : "";
                    $shareuserData['isOffical'] = (isset($reshareUserdata['isOffical'])) ? $reshareUserdata['isOffical'] : "";
                    $shareuserData['isResponder'] = (isset($reshareUserdata['isResponder'])) ? $reshareUserdata['isResponder'] : "";
                    $value['reshareUserData'] = $shareuserData;
                }
            }
        }

        $pollPostOptions = array();
        $isPollPost = (isset($value['isPollPost'])) ? $value['isPollPost'] : "";
        $getSinglePostData = $this->CommonModel->get_row(TBL_POST, ['*'], ['postID' => $value['postID'], 'isActive' => 1, 'isDelete' => 0]);
        $value['isPollPost'] = (isset($isPollPost) && $isPollPost != "") ? $isPollPost : (isset($getSinglePostData['isPollPost'])) ? $getSinglePostData['isPollPost'] : "0";
        if (isset($value['isPollPost']) && trim($value['isPollPost']) != "" && $value['isPollPost'] == null || is_null($value['isPollPost'])) {
            $value['isPollPost'] = "";
        }

        if (isset($value['isPollPost']) && $value['isPollPost'] == 1) {
            $value['hasPollPostAnswer'] = $this->post->checkPollPostHasUserAnswer($userID, $value['postID']);
            $pollOptions = $this->CommonModel->get_all(TBL_POST_POLL_OPTIONS, ['*'], ["postID" => $value['postID'], "isActive" => 1, "isDelete" => 0]);
            if (!empty($pollOptions)) {
                foreach ($pollOptions as $opt) {
                    $avgAnswer = $this->post->getPollOptionAvgAns($value['postID'], $opt['optionID']);
                    $res = array();
                    $res['optionID'] = $opt['optionID'];
                    $res['option'] = $opt['text'];
                    $res['avgAnswer'] = $avgAnswer;
                    $pollPostOptions[] = $res;
                }
            }
        }
        $value['pollPostOptions'] = (!empty($pollPostOptions)) ? $pollPostOptions : array();
        $totalPollAnswer = $this->CommonModel->get_all(TBL_POST_POLL_ANSWER, ['*'], ["postID" => $value['postID'], "isActive" => 1, "isDelete" => 0]);
        $value['totalPollAnswer'] = (!empty($totalPollAnswer)) ? count($totalPollAnswer) . "" : "0";
        $totalComments = $this->post->totalComments($value['postID'], $userID,$value['postShareID']);
        $totalShares = $this->post->totalShares($value['postID'], $userID,$value['postShareID']);
        $totalLikes = $this->post->totalLikes($value['postID'], $userID,$value['postShareID']);
        $totalDisLikes = $this->post->totalDisLikes($value['postID'], $userID,$value['postShareID']);

        $value['media'] = $media;
        $value['commentCount'] = $totalComments."";
        $value['reShareCount'] = $totalShares;
        $value['likesCount'] = $totalLikes."";
        $value['disLikeCount'] = $totalDisLikes."";

        if($other!=""){
            if (count($media) == 0) {
                $value['text_post_list'] = $value;
            } else {
                $value['media_post_list'] = $value;
            }
        }
        unset($media);
        return $value;
    }
    
    public function loadSubComments($comment){
        if(!empty($comment['comments'])){ $loginUserID = $this->session->userdata('userID'); ?>
            <ul class="we-comet">
                <?php foreach($comment['comments'] as $comment){
                    $postID = $comment['postID'];
                    $totalLikeCount = $this->post->totalPostCommentLikes($postID, $comment['postCommentID'], $loginUserID);
                    $totaldisLikeCount = $this->post->totalPostCommentLikes($postID, $comment['postCommentID'], $loginUserID, 0);
                    ?>
                    <li>
                        <div class="comet-avatar">
                            <img src="<?php echo (!empty($comment['profileImage'])) ? $comment['profileImage'] : base_url('/assets/images/resources/admin.png') ?>" alt="" class="w35">
                        </div>
                        <div class="we-comment">
                            <div class="coment-head">
                                <h6><a href="<?= base_url('time-line/'.$comment['username']) ?>" title=""><?php echo ucfirst($comment['username']); ?></a></h6>
                                <p><?php echo $this->emoji->Decode($comment['text']); ?></p>
                                <?php
                                if (isset($comment['commentImage']) && $comment['commentImage']!="" ) { ?>
                                    <img src="<?php echo $comment['commentImage'] ?>" alt="" class="commentImg postpicture" />
                                <?php } ?>
                                <div class="inline-itms">
                                    <span class="displayMomentDateOnly" datetime="<?php echo (isset($comment['createdDateTime'])) ? $comment['createdDateTime'] : ""; ?>"></span>
                                    <a href="javascript:void(0)" title="" class="likePostComment <?php echo (!empty($checkLikeStatus))?'active':'' ?>" data-id="<?php echo $comment['postCommentID']; ?>" data-postid="<?php echo $postID; ?>"><i class="fa fa-heart"></i>
                                        <span id="postCommentlikeCount<?php echo $comment['postCommentID']; ?>"><?php echo $totalLikeCount; ?></span></a>
                                    <a href="javascript:void(0)" title="" class="dislikePostComment <?php echo (!empty($checkDisLikeStatus))?'active':'' ?>" data-id="<?php echo $comment['postCommentID']; ?>" data-postid="<?php echo $postID; ?>"><i class="fa fa-heartbeat"></i>
                                        <span id="postCommentDislikeCount<?php echo $comment['postCommentID']; ?>"><?php echo $totaldisLikeCount; ?></span></a>
                                    <a class="we-reply" href="javascript:void(0)" title="Reply" data-toggle="collapse" data-target="#post_replay<?php echo $comment['postCommentID']; ?>"><i class="fa fa-reply"></i></a>
                                </div>
                                <div id="post_replay<?php echo $comment['postCommentID']; ?>" class="row collapse in" aria-expanded="true">
                                    <div class="col-md-12">
                                        <form method="post" class="commentForm" enctype="multipart/form-data">
                                        <p class="emoji-picker-container" id="emoji-picker-container<?php echo $comment['postCommentID']; ?>">
                                            <textarea class="form-control mantionUsers" name="comment" id="replay_post_comment<?php echo $comment['postCommentID']; ?>" data-id="<?php echo $postID; ?>" data-parent="<?php echo $comment['postCommentID']; ?>" placeholder="Write a comment..." data-emojiable="true" data-emoji-input="unicode"><?php echo (isset($comment['username']))?'@'.$comment['username']:""; ?> </textarea>
                                        </p>
                                        <input type="hidden" name="commentID" value="<?php echo $comment['postCommentID']; ?>" />
                                        <input type="hidden" id="gif<?php echo $comment['postCommentID']; ?>" name="gif" value="" />
                                        <input type="hidden" class="postID" name="postID" value="<?php echo $postID; ?>" />
                                        <div class="attachments text-right">
                                            <ul class="cursor-point">
                                                <li class="emojiPicker" data-id="<?php echo $comment['postCommentID']; ?>" id="emoji<?php echo $comment['postCommentID']; ?>">
                                                    <i class="fa fa-smile-o"></i>
                                                    <label class="fileContainer"> </label>
                                                </li>
                                                <li id="image<?php echo $comment['postCommentID']; ?>">
                                                    <i class="fa fa-camera"></i>
                                                    <label class="fileContainer">
                                                        <input type="file" name="commentImage" id="files<?php echo $comment['postCommentID']; ?>" multiple accept="image/*" onchange="readFile(this);">
                                                    </label>
                                                </li>
                                                <li>
                                                    <button class="btn btn-sm webbgcolor text-white submit_post_replay mb15" type="submit" name="post" value="POST" data-id="<?php echo $postID; ?>" data-parent="<?php echo $comment['postCommentID']; ?>" id="btnSaveCommentReplay<?php echo $comment['postCommentID']; ?>">Send</button>
                                                </li>
                                            </ul>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <?php $this->post->loadSubComments($comment); ?>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        <?php }
    }

    public function totalPostCommentLikes($postID, $postCommentID, $userID = "", $type=1) {
        $this->db->select("pc.*")
            ->from(TBL_POST_COMMENT_LIKE . " pc")
            ->join("tbluser u", "u.userID = pc.userID")
            // ->where("pc.isLiked", 1)
            ->where("pc.postCommentID", $postCommentID)
            ->where("pc.postID", $postID)
            ->where("pc.isActive = 1 and pc.isDelete = 0");
        if ($type!="" || $type==0) {
            $this->db->where("pc.isLiked", $type);
        }
        if ($userID != "") {
            $this->db->where("u.userID NOT IN (
                SELECT 
                    tbu.blockUserID
                FROM
                    tblblockuser tbu
                WHERE
                    tbu.userID = $userID AND tbu.isActive = 1 AND tbu.isDelete = 0
                UNION
                SELECT
                    tbu1.userID
                FROM
                    tblblockuser tbu1
                WHERE
                    tbu1.blockUserID = $userID AND tbu1.isActive = 1 AND tbu1.isDelete = 0
            )");
        }
        $resultData = $this->db->get()->result_array();
        return count($resultData);
    }
    
    
    public function getAllDeleteVeteranPost()
    {
        return $this->db->select("tp.*,IFNULL(tp.gif, '') as gif,tp.updatedDateTime as datetime,if((LEFT(tu.profileImage , 5) = 'https') OR (LEFT(tu.profileImage , 4) = 'http'),tu.profileImage,CONCAT('" . base_url() . "upload/userprofile/',tu.profileImage)) as profileImage,tu.username,tu.name,tu.isVerified,tu.isVeteran,tu.isResponder,tu.isOffical,if(tpl.isLiked is null,'0',tpl.isLiked) as isLiked,if(tpl1.isLiked is null,'0',1) as isDisLiked,if(tps.status is null,'0',tps.status) as isReshare,IF(tps.postShareID is null,0,1) as isResharePost,IF(tps.postShareID is null,'',(SELECT username  FROM `tbluser` WHERE `userID` = tps.userID)) as reshareName,IF(tps.postShareID is null,0,tps.postShareID) as postShareID")
            ->from("tblpost tp")
            ->join("tbluser tu", "tu.userID = tp.userID AND tu.isActive = 1 AND tu.isDelete = 0", "LEFT")
            ->join("tblsettings ts1", " ts1.isActive = 1 and ts1.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl", "tpl.postID = tp.postID  and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0", "LEFT")
            ->join("tblpostlike tpl1", "tpl1.postID = tp.postID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0", "LEFT")
            ->join("tblpostshare tps", "tps.userID = tu.userID and tps.postID = tp.postID and tps.isActive = 1 and tps.isDelete = 0", "LEFT")
            ->where("tp.postType", 2)
            ->where("tp.isActive", 1)
            ->where("tp.isDelete", 1)
            ->group_by("tp.postID")
            ->where("IF(ts1.blacklistKeyword != '', tp.text NOT REGEXP (REPLACE(ts1.blacklistKeyword,',','|')), 1 = 1) ")
            ->where("(tp.isexpireOn = 0 OR ( tp.isexpireOn = 1 and (CURDATE() <= DATE(tp.expiryDateTime))))")
            ->order_by("tp.createdDateTime", "DESC")
            ->get()->result_array();
        
    }

    public function getAdminVeteranPost(){
        return $this->db->select("tp.*,IFNULL(tp.gif, '') as gif")
            ->from("tblpost tp")
            ->where("tp.postType", 2)
            ->where("tp.isActive", 1)
            ->where("tp.isDelete", 0)
            ->get()->result_array();
    }


}
?>