<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('GOOGLE_API_KEY')      OR define('GOOGLE_API_KEY', 'AAAAQhONsDU:APA91bGDJGyZ3IeCqfriDE8IzYM8V_FGUo7NaFrSCi7BKyxJDF3G6tcCjaj-L6Kso8grXw3tsnf-3rVHfaqK1WKqsyuxdR7y9Hu-dA9M3eR2eHnlkMzRQvHEw7bgkIWdvYYrcKQzGepW');
defined('APPLE_API_STATUS')      OR define('APPLE_API_STATUS', 1);
defined('POST_IMAGE_PATH') OR define('POST_IMAGE_PATH', 'upload/postmedia/');

defined('SITE_NAME') OR define('SITE_NAME', 'Sayscape');
//define table name

$scheme = (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? "https" : "http";
$base_url = "";

/*if($_SERVER['HTTP_HOST'] == 'localhost') {
   $base_url = $scheme."://".$_SERVER['HTTP_HOST'].'/sayscape/';
} else if($_SERVER['HTTP_HOST'] == 'credencetech.in') {
   $base_url = $scheme."://".$_SERVER['HTTP_HOST'].'/sayscape/';
} else if($_SERVER['HTTP_HOST'] == 'sayscape.credencetech.in'){
   $base_url = $scheme."://".$_SERVER['HTTP_HOST'].'/';
}*/
$base_url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$base_url .= "://". @$_SERVER['HTTP_HOST'];
$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);

defined('BASE_URL')                    OR define('BASE_URL', $base_url);

define('DB_PREFIX' , 'tbl');
define('TBL_USER' , DB_PREFIX . 'user');
define('TBL_USER_ACCESS_TOKEN' , DB_PREFIX . 'useraccesstoken');
define('TBL_GROUP' , DB_PREFIX . 'group');
define('TBL_USER_GROUP' , DB_PREFIX . 'usergroup');
define('TBL_USER_FOLLOWER' , DB_PREFIX . 'userfollower');
define('TBL_POST' , DB_PREFIX . 'post');
define('TBL_MEDIA' , DB_PREFIX . 'media');
define('TBL_POST_LIKE' , DB_PREFIX . 'postlike');
define('TBL_POST_SHARE' , DB_PREFIX . 'postshare');
define('TBL_POST_COMMENT' , DB_PREFIX . 'postcomment');
define('TBL_POST_BOOKMARK' , DB_PREFIX . 'postbookmark');
define('TBL_MESSAGE', DB_PREFIX . 'message');
define('TBL_REPORT', DB_PREFIX . 'report');
define('TBL_BLOCKUSER',DB_PREFIX . 'blockuser');
define("TBL_SETTINGS",DB_PREFIX . 'settings');
define("TBL_HIDEPOST",DB_PREFIX . 'hidepost');
define("TBL_ADMIN_USER",DB_PREFIX . 'adminuser');
define("TBL_MODERATOR_USER",DB_PREFIX . 'moderatoruser');
define("TBL_NOTIFICATION",DB_PREFIX . 'notification');
define("TBL_USER_NOTIFICATION",DB_PREFIX . 'usernotification');
define("TBL_REJECTED_USERNAME",DB_PREFIX . 'rejectusername');
define("TBL_POST_SUBSCRIBE",DB_PREFIX . 'postsubscribe');
define("TBL_POST_POLL_OPTIONS",DB_PREFIX . 'postpolloptions');
define("TBL_POST_POLL_ANSWER",DB_PREFIX . 'postpollanswer');
define("TBL_MUTEUSER",DB_PREFIX . 'muteuser');
define('TBL_POST_COMMENT_LIKE' , DB_PREFIX . 'postcommentlike');
define('TBL_MESSAGE_CLEAR', DB_PREFIX . 'message_clear');
define('TBL_TOPFIVE_FOLLLOWERS', DB_PREFIX . 'topfivefollowers');
define('TBL_ADITIONAL_IMG', DB_PREFIX . 'aditionalimage');
define('TBL_PACKAGE', DB_PREFIX . 'package');
define('TBL_USER_TRANSACTION', DB_PREFIX . 'usertransaction');
define('TBL_CUSTOM_EMOJI', DB_PREFIX . 'custom_emoji');

defined('CURRENT_DATE') OR define('CURRENT_DATE', date('Y-m-d'));
defined('CURRENT_DATE_TIME') OR define('CURRENT_DATE_TIME', date('Y-m-d H:i:s'));
defined('FROM_EMAIL') OR define('FROM_EMAIL', 'rahul.credencetech@gmail.com');
defined('GROUP_PROFILE_UPLOAD_PATH') OR define('GROUP_PROFILE_UPLOAD_PATH', 'upload/group/');

defined('ONE_PAGE_LIMIT') OR define('ONE_PAGE_LIMIT', 10);

//define folder name
define('UPLOAD', 'upload/');
define('ADITIONAL_IMG', UPLOAD . 'aditionalimage/');