<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
// $route['default_controller'] = 'login/index';
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// admin routes
$route['admin'] = "admin/dashboard";
$route['admin/login'] = "admin/auth";
$route['admin/logout'] = "admin/auth/logout";

// moderator user
$route['moderator'] = "moderator/dashboard";
$route['moderator/login'] = "moderator/auth";
$route['moderator/logout'] = "moderator/auth/logout";

// users routes
$route['login'] = 'user/login';
$route['register'] = 'user/register';
$route['home/login'] = 'user/login';
$route['logout'] = 'user/logout';
$route['edit-profile'] = 'user/editProfile';
$route['changepassword'] = 'user/changePassword';
$route['about'] = 'user/user_about';
$route['notification'] = 'user/notification';
$route['usergroup'] = 'user/userGroup';
// $route['groupmember'] = 'user/groupMembers';
$route['groupmember/(:any)'] = 'user/groupMembers/$1';
$route['message-box'] = 'user/message_box';
$route['message-box/(:any)'] = 'user/message_box/$1';
$route['group-message-box'] = 'user/group_message';
$route['group-message/(:any)'] = 'user/group_message/$1';
$route['pin-this-post/(:any)'] = 'user/pin_this_post/$1';
$route['unpin-this-post/(:any)'] = 'user/unpin_this_post/$1';
$route['delete-this-post'] = 'user/delete_this_post';
$route['hide-this-post'] = 'user/hide_this_post';
$route['groupDetails/(:any)'] = 'user/group_details/$1';


// posts and timelines
$route['time-line'] = 'home/timeline';
$route['time-line/(:any)'] = 'home/timeline/$1';
$route['trending'] = 'home/trending';
$route['search'] = 'home/search';
$route['search/user'] = 'home/searchUser';
$route['trending/(:any)'] = 'home/trending/$1';
$route['hashtag-trending'] = 'home/hashtag_trending';
$route['hashtag-post'] = 'home/hashtagTrending_post';
$route['hashtag-post/(:any)'] = 'home/hashtagPost/$1';
$route['veteran-feed'] = 'home/veteran_feed';
$route['responder-feed'] = 'home/responder_feed';
$route['bookmark'] = 'home/bookmark';
$route['friends'] = 'home/timeline_friends';
$route['friends-request'] = 'home/friends_request';
$route['post-details/(:any)'] = 'home/post_details/$1';

$route['testinemogi'] = 'user/testinemogi';
$route['privacy-policy'] = 'home/privacy_policy';

$route['groups'] = 'user/groups';
$route['following'] = 'user/following_list';
$route['following/(:any)'] = 'user/following_list/$1';
$route['followers'] = 'user/followers_list';
$route['followers/(:any)'] = 'user/followers_list/$1';

$route['upgrade-account'] = 'user/upgradeAccount';
$route['plus-account'] = 'user/plusAccount';
$route['payment-process'] = 'user/paymentProcess';
$route['payment-success/(:any)'] = 'user/paymentSuccess/$1';
