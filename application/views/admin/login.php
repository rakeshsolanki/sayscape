<!-- Content area -->
<div class="content d-flex justify-content-center align-items-center">
	<!-- Login form -->
	<form id="loginForm" class="login-form" action="" method="post">
		<div class="text-center mb-3">
			<div id="formMessage"></div>
			<img src="<?= BASE_URL."admin_assets/assets/images/sayscape_logo.png" ?>" width="51%" />
		</div>
		<div class="card">
			<div class="card-body">				
				<div class="form-group form-group-feedback form-group-feedback-left">
					<input type="text" class="form-control" placeholder="Email ID" id="emailAddress" name="emailAddress" autofocus/>
					<div class="form-control-feedback">
						<i class="icon-user text-muted"></i>
					</div>
				</div>
				<div class="form-group form-group-feedback form-group-feedback-left">
					<input type="password" class="form-control" placeholder="Password" id="password" name="password" />
					<div class="form-control-feedback">
						<i class="icon-lock2 text-muted"></i>
					</div>
				</div>
				<div class="form-group">
					<button id="btn-login" type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
				</div>
			</div>
		</div>
	</form>
	<!-- /login form -->
</div>