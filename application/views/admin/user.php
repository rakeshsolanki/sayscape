<div class="card" style="zoom: 1;">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Users List</h6>
    </div>

    <div class="card-body" style="">
        <ul class="nav nav-tabs nav-tabs-highlight mb-0">
            <li class="nav-item"><a href="#pill-tab1-active" class="nav-link legitRipple active show" data-toggle="tab"><span class="badge badge-success badge-pill mr-2" id="ActiveDTCount"></span> Active</a></li>
            <li class="nav-item"><a href="#pill-tab2-block" class="nav-link legitRipple" data-toggle="tab"><span class="badge badge-pill bg-warning mr-2" id="ReportUserDTCount">0</span> Report</a></li>
            <li class="nav-item"><a href="#pill-tab3-block" class="nav-link legitRipple" data-toggle="tab"><span class="badge badge-danger badge-pill mr-2" id="BlockDTCount">0</span> Block</a></li>
        </ul>

        <div class="tab-content card card-body border border-top-0 rounded-top-0 shadow-0 mb-0">
            <div class="tab-pane fade active show" id="pill-tab1-active">
                <table class="table table-bordered table-hover table-striped table-xs" id="userList_table"></table>
            </div>

            <div class="tab-pane fade" id="pill-tab2-block">
                <table class="table table-bordered table-hover table-striped table-xs" id="userReportList_table"></table>
            </div>
            <div class="tab-pane fade" id="pill-tab3-block">
                <table class="table table-bordered table-hover table-striped table-xs" id="userBlockList_table"></table>
            </div>
        </div>
    </div>
</div>

<script src="<?=BASE_URL?>admin_assets/assets/scripts/user.js"></script>