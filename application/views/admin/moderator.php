<div class="card" style="zoom: 1;">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Manage Moderator</h6>
    </div>
    <div class="card-body" style="">
    <a href="<?=base_url();?>admin/moderator/add" class="btn btn-primary bg-teal-400 btn-labeled btn-labeled-left legitRipple"><b><i class="icon-reading"></i></b> Add Moderator</a>
      
    <table class="table table-bordered table-hover table-striped table-xs" id="superAdminTable"></table>
    </div>
</div>

<div id="superAdminTableModal" class="modal fade" role="dialog" style="z-index: 999999 !important">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Moderator Detail</h4>
            </div>
            <div class="modal-body">
                <div>
                    <table class="table table-condensed table-hover table-responsive">
                        <tbody class="DataBody"></tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?=BASE_URL?>admin_assets/assets/scripts/moderator.js"></script>