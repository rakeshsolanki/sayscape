<div class="card" style="zoom: 1;">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">User Name</h6>
    </div>

    <div class="card-body" style="">
        <ul class="nav nav-tabs nav-tabs-highlight mb-0">
            <li class="nav-item"><a href="#pill-tab1-active" class="nav-link legitRipple active show" data-toggle="tab"><span class="badge badge-success badge-pill mr-2" id="ActiveDTCount"></span> View Rejected Username</a></li>
            <li class="nav-item"><a href="#pill-tab2-block" class="nav-link legitRipple" data-toggle="tab">Add To Reject</a></li>
        </ul>
        <div class="tab-content card card-body border border-top-0 rounded-top-0 shadow-0 mb-0">
            <div class="tab-pane fade active show" id="pill-tab1-active">
                <table class="table table-bordered table-hover table-striped table-xs" id="rejectedUsername_table"></table>
            </div>

            <div class="tab-pane fade" id="pill-tab2-block">
                <div class="col-lg-6 col-md-10 col-sm-12">
                    
                    <form method="post" id="usernameForm" enctype='multipart/form-data'>
                    <div id="FormMessage"></div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Username<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" name="rejectUsername" required class="form-control"
                                value="" placeholder="Enter Name">
                        </div>
                    </div>
                        <hr/>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary legitRipple">Add To Reject <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=BASE_URL?>admin_assets/assets/scripts/user.js"></script>