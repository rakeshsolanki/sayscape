<div class="card" style="zoom: 1;">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Edit Users</h6>
    </div>

    <div class="card-body" style="">

        <form class="" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="username" class="control-label">Username</label>
                        <input type="text"
                               name="username" <?= (isset($userdata['isOffical']) && $userdata['isOffical'] == 1) ? 'readonly' : '' ?>
                               value="<?php echo (isset($userdata['username'])) ? $userdata['username'] : ""; ?>"
                               class="form-control form-control-line" id="username" placeholder="Username">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="email" class="control-label">Email</label>
                        <input type="email" name="email"
                               value="<?php echo (isset($userdata['email'])) ? $userdata['email'] : ""; ?>"
                               class="form-control" id="email" placeholder="Email">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="password" class="control-label">Display Name</label>
                        <input type="text" name="name" class="form-control" id="name" placeholder="Display Name"
                               value="<?php echo (isset($userdata['name'])) ? $userdata['name'] : ""; ?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="phone" class="control-label">Phone</label>
                        <input type="text" name="phone"
                               value="<?php echo (isset($userdata['phone'])) ? $userdata['phone'] : ""; ?>"
                               class="form-control" id="phone" placeholder="Phone">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="location" class="control-label">Location</label>
                        <input type="text" name="location"
                               value="<?php echo (isset($userdata['location'])) ? $userdata['location'] : ""; ?>"
                               class="form-control" id="location" placeholder="Location"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="isActive" class="control-label">Status</label>
                        <select id="isActive" name="isActive" class="form-control">
                            <option value="">Select Status</option>
                            <option value="1" <?php echo (isset($userdata['isActive']) && $userdata['isActive'] == 1) ? "selected" : ""; ?>>
                                Active
                            </option>
                            <option value="0" <?php echo (isset($userdata['isActive']) && $userdata['isActive'] == 0) ? "selected" : ""; ?>>
                                Inactive
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <!-- <input type="hidden" name="" value="">-->
                        <input type="submit" name="btnSubmit" value="Save" class="btn btn-info">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>