<?php $formId = $this->uri->segment(3) == "add" ? "add" : "edit"; ?>
<!-- Page header -->
<div class="page-header page-header-light">
    <div class="page-header-content header-elements-md-inline">
        <div class="page-title d-flex">
            <h4>Moderator Add</h4>
        </div>
    </div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">

    <!-- Form validation -->
    <div class="card">
        <div class="card-body">

            <form class="<?= $formId ?>_superadmin_form" action="#" method="post" id="<?= $formId ?>_superadmin_form">
                <div id="FormMessage"></div>
                <input type="hidden" name="userID" value="<?= getKeyValue($admin_row, 'userID') ?>" />
                <fieldset class="mb-3">

                    <!-- Basic text input -->
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Name </label>
                        <div class="col-lg-4">
                            <input type="text" name="name" class="form-control" value="<?= getKeyValue($admin_row, 'name') ?>" placeholder="Enter Name">
                        </div>
                        <label class="col-form-label col-lg-2">Email <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" name="email" required class="form-control" value="<?= getKeyValue($admin_row, 'email') ?>" placeholder="Enter Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2">Password<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="password" class="form-control" required id="password" name="password" placeholder="Enter Password" />
                        </div>
                        <label class="col-form-label col-lg-2">Confirm Password<span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="password" class="form-control" required id="confirm_password" name="confirm_password" placeholder="Enter Confirm Password" />
                        </div>
                    </div>
                </fieldset>
                <div class="d-flex justify-content-end align-items-center">
                    <input type="submit" class="btn btn-primary ml-3" value="Save">
                </div>
            </form>
        </div>
    </div>
    <!-- /form validation -->
</div>
<!-- /content area -->
<script src="<?= BASE_URL ?>admin_assets/assets/scripts/moderator.js"></script>