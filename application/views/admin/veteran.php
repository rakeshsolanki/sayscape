<div class="card" style="zoom: 1;">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Veteran</h6>
    </div>

    <div class="card-body" style="">
        <ul class="nav nav-tabs nav-tabs-highlight mb-0">
            <li class="nav-item"><a href="#pill-tab1-active" class="nav-link legitRipple active show" data-toggle="tab"><span class="badge badge-success badge-pill mr-2" id="ActiveDTCount"></span> View Post</a></li>
            <li class="nav-item"><a href="#pill-tab2-block" class="nav-link legitRipple" data-toggle="tab">Add Post</a></li>
            <li class="nav-item"><a href="#delete-post-tab" class="nav-link legitRipple" data-toggle="tab">Delete Post</a></li>
            <li class="nav-item"><a href="#reported-post-tab" class="nav-link legitRipple" data-toggle="tab">Reported Post</a></li>
        </ul>
        <div class="tab-content card card-body border border-top-0 rounded-top-0 shadow-0 mb-0">
            <div class="tab-pane fade active show" id="pill-tab1-active">
                <table class="table table-bordered table-hover table-striped table-xs" id="veteranPost_table"></table>
            </div>

            <div class="tab-pane fade" id="pill-tab2-block">
                <div class="col-lg-6 col-md-10 col-sm-12">
                    <form method="post" onSubmit="return(submitVeteranPost(event))" id="veteranForm" enctype='multipart/form-data'>
                        <div class="form-group">
                            <label style="background-color:#f9f9f9;padding:5px 10px;width:100%;">Post Content : </label>
                            <textarea rows="5" cols="5" class="form-control" placeholder="Enter your content here." name="veteranPostContent" required autofocus></textarea>
                        </div>
                        <div class="form-group">
                            <label style="background-color:#f9f9f9;padding:5px 10px;width:100%;">Post Media : </label>
                            <ul class="nav nav-pills nav-pills-bordered nav-pills-toolbar">
                                <li class="nav-item"><a href="#toolbar-pill1" class="nav-link active" data-id="2" data-toggle="tab">Video</a></li>
                                <li class="nav-item"><a href="#toolbar-pill2" class="nav-link" data-id="1" data-toggle="tab">Photo</a></li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="toolbar-pill1">
                                    <div class="form-group">
                                        <input type="file" name="veteranPostVideo" accept="video/*" id="veteranPostVideo">
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="toolbar-pill2">
                                    <div class="form-group veteranImageInput">
                                        <input type="file" name="veteranPostImage[]" id="veteranPostImage" accept="image/x-png,image/gif,image/jpeg" multiple>
                                        <pre id="filelist" style="display:none;"></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary legitRipple">Create Post <i class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="tab-pane fade" id="delete-post-tab">
                <table class="table table-bordered table-hover table-striped table-xs" id="deletePost_table"></table>
            </div>

            <div class="tab-pane fade" id="reported-post-tab">
                <table class="table table-bordered table-hover table-striped table-xs" id="reportedPost_table"></table>
            </div>

        </div>
    </div>
</div>

<script src="<?=BASE_URL?>admin_assets/global_assets/js/plugins/notifications/bootbox.min.js"></script>
<script src="<?=BASE_URL?>admin_assets/global_assets/js/plugins/uploaders/dropzone.min.js"></script>
<script src="<?=BASE_URL?>admin_assets/assets/scripts/veteran.js"></script>