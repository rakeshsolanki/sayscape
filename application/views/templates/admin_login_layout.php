<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/material/full/login_simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 07:49:38 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="<?= BASE_URL ?>images/ico/favicon.png">
	<title><?= $title ?></title>

	<!-- Global stylesheets -->
    <link rel="shortcut icon" href="<?= BASE_URL ?>images/ico/favicon.png">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?= BASE_URL ?>admin_assets/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?= BASE_URL ?>admin_assets/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?= BASE_URL ?>admin_assets/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?= BASE_URL ?>admin_assets/assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?= BASE_URL ?>admin_assets/assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?= BASE_URL ?>admin_assets/assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?= BASE_URL ?>admin_assets/global_assets/js/main/jquery.min.js"></script>
	<script src="<?= BASE_URL ?>admin_assets/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/ui/ripple.min.js"></script>
	<script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/forms/validation/validate.min.js"></script>
	<!-- /core JS files -->

	<script>
		var site_url = "<?= site_url() ?>";
		var base_url = "<?= base_url() ?>";
	</script>

	<!-- Theme JS files -->
	<script src="<?= BASE_URL ?>admin_assets/assets/js/app.js"></script>
	<!-- /theme JS files -->
</head>
<body>
	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
		<div class="navbar-brand">
			<a href="<?= site_url('admin') ?>" class="d-inline-block">
				<img src="<?= BASE_URL."admin_assets/assets/images/sayscape_logo1.png" ?>" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
		</div>

	</div>
	<!-- /main navbar -->

	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

            <?php echo $body; ?>

			<!-- Footer -->
			<div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; <?= date('Y') ?>. <?= SITE_NAME ?>
					</span>
				</div>
			</div>
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

<script src="<?= BASE_URL ?>admin_assets/assets/js/login.js"></script>
</body>
<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/material/full/login_simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 07:49:38 GMT -->
</html>