<!DOCTYPE html>
<html lang="en">
<!-- Mirrored from demo.interface.club/limitless/demo/bs4/Template/layout_1/LTR/material/full/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 10 Oct 2018 07:18:05 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="<?= BASE_URL ?>images/ico/favicon.png">
    <title><?= $title ?></title> <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="<?= BASE_URL ?>admin_assets/global_assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css"> <!-- <link href="<?= BASE_URL ?>admin_assets/global_assets/css/icons/material/icons.css" rel="stylesheet" type="text/css">	<link href="<?= BASE_URL ?>admin_assets/global_assets/css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css"> -->
    <link href="<?= BASE_URL ?>admin_assets/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= BASE_URL ?>admin_assets/assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="<?= BASE_URL ?>admin_assets/assets/css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="<?= BASE_URL ?>admin_assets/assets/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="<?= BASE_URL ?>admin_assets/assets/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="<?= BASE_URL ?>admin_assets/assets/css/custom.css" rel="stylesheet" type="text/css">
    <!-- <link href="<?= BASE_URL ?>admin_assets/assets/css/style.css" rel="stylesheet" type="text/css"> -->
    <!-- <link href="<?= BASE_URL ?>css/select2.min.css" rel="stylesheet" type="text/css"> -->
    <!-- /global stylesheets -->
    <!-- Core JS files -->
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/main/jquery.min.js"></script>
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/main/bootstrap.bundle.min.js"></script>
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/loaders/blockui.min.js"></script>
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/ui/ripple.min.js"></script> <!-- /core JS files -->
    <!-- Theme JS files -->
    <!-- <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/visualization/d3/d3.min.js"></script>	<script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/visualization/d3/d3_tooltip.js"></script> -->
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/forms/styling/switchery.min.js"></script> <!-- <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script> -->
    <!-- <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/ui/moment/moment.min.js"></script> -->
    <!-- <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/pickers/daterangepicker.js"></script> -->
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/notifications/sweet_alert.min.js"></script> <!-- <script src="<?= BASE_URL ?>admin_assets/global_assets/js/demo_pages/extra_sweetalert.js"></script> -->
    <!-- <script src="<?= BASE_URL ?>assets/js/bootstrap-notify.min.js"></script> -->
    <!-- <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/forms/selects/select2.min.js"></script> -->
    <!-- Theme JS files -->
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/forms/validation/validate.min.js"></script>
    <script src="<?= BASE_URL ?>admin_assets/assets/js/app.js"></script>
    <script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/forms/styling/switchery.min.js"></script> <!-- <script src="<?= BASE_URL ?>global_assets/js/demo_pages/dashboard.js"></sc/ript> -->
    <!-- /theme JS files -->
    <script>
        var base_url = "<?= base_url() ?>";
        var site_url = "<?= site_url() ?>";
    </script>
    <style>
        tr {
            cursor: pointer;
        }
    </style>
</head>

<body class="sidebar-xs">
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
        <div class="navbar-brand"> <a href="<?= site_url('admin') ?>" class="d-inline-block"> <img src="<?= BASE_URL . "admin_assets/assets/images/sayscape_logo1.png" ?>" alt=""> </a> </div>
        <div class="d-md-none"> <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile"> <i class="icon-tree5"></i> </button> <button class="navbar-toggler sidebar-mobile-main-toggle" type="button"> <i class="icon-paragraph-justify3"></i> </button> </div>
        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item"> <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block"> <i class="icon-paragraph-justify3"></i> </a> </li>
            </ul>
            <ul class="navbar-nav ml-md-auto">
                <li class="nav-item"> <a href="<?= site_url('admin/logout') ?>" class="navbar-nav-link"> <i class="icon-switch2"></i> <span class="d-md-none ml-2">Logout</span> </a> </li>
            </ul>
        </div>
    </div> <!-- /main navbar -->
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-light sidebar-main sidebar-expand-md">
            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center"> <a href="#" class="sidebar-mobile-main-toggle"> <i class="icon-arrow-left8"></i> </a> <span class="font-weight-semibold">Navigation</span> <a href="#" class="sidebar-mobile-expand"> <i class="icon-screen-full"></i> <i class="icon-screen-normal"></i> </a> </div> <!-- /sidebar mobile toggler -->
            <!-- Sidebar content -->
            <div class="sidebar-content">
                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">
                    <ul class="nav nav-sidebar" data-nav-type="accordion">
                        <!-- Main -->
                        <li class="nav-item-header">
                        <div class="text-uppercase font-size-xs line-height-xs">Main</div>
                        <i class="icon-menu" title="Main"></i></li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/user') ?>" class="nav-link" title="Users">
                            <i class="icon-users4"></i> <span>Users</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/veteran') ?>" class="nav-link" title="Veteran">
                            <i class="icon-pencil7"></i> <span>Veteran</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/post') ?>" class="nav-link" title="Post">
                            <i class="icon-color-sampler"></i> <span>Post</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/comment') ?>" class="nav-link" title="Comment">
                            <i class="icon-comment"></i> <span>Comment</span>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="<?= site_url('admin/superadmin') ?>" class="nav-link" title="Manage Admin">
                            <i class="icon-people"></i> <span>Manage Admin</span>
                        </a>
                        </li>
                        <li class="nav-item">
                        <a href="<?= site_url('admin/moderator') ?>" class="nav-link" title="Manage Admin">
                            <i class="icon-user"></i> <span>Manage Moderator</span>
                        </a>
                        </li>
                    <li class="nav-item">
                        <a href="<?= site_url('admin/user/rejectedUsername') ?>" class="nav-link"
                           title="Manage User Name">
                            <i class="icon-diff-renamed"></i> <span>Manage User Name</span>
                        </a>
                    </li>
                    <!-- /main -->
                    </ul>
                </div> <!-- /main navigation -->
            </div> <!-- /sidebar content -->
        </div> <!-- /main sidebar -->
        <!-- Main content -->
        <div class="content-wrapper"> <?= $body ?>
            <!-- Footer -->
            <div class="navbar navbar-expand-lg navbar-light">
                <div class="navbar-collapse" id="navbar-footer"> <span class="navbar-text"> &copy; <?= date('Y') ?>. <?= SITE_NAME ?> </span> </div>
            </div> <!-- /footer -->
        </div> <!-- /main content -->
    </div> <!-- /page content -->
</body>
</html>