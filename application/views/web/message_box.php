<?php
include('header.php');
include('toparea.php');
$toID = $this->uri->segment(2);
?>
<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php
                        include('shortcuts.php');
                        ?>
                        <div class="col-lg-9">
                            <div class="central-meta">
                                <div class="messages">
                                    <h5 class="f-title"><i class="ti-bell"></i>All Messages <span class="more-options"><i class="fa fa-ellipsis-h"></i></span></h5>
                                    <div class="message-box">
                                        <ul class="peoples">
                                            <?php
                                            if (!empty($followersName)) {
                                                foreach ($followersName as $followers) { ?>
                                                    <li>
                                                        <figure>
                                                            <?php
                                                            if (isset($followers['profileImage']) && $followers['profileImage'] != "") { ?>
                                                                <img src="<?php echo $followers['profileImage']; ?>" alt="" style="height:32px; width:34px;">
                                                            <?php } else { ?>
                                                                <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="" style="height:32px; width:34px;">
                                                            <?php }
                                                            ?>
                                                            <span class="status f-online"></span>
                                                        </figure>
                                                        <a href="<?php echo base_url('message-box/' . $followers['followerID']) ?>">
                                                            <div class="people-name">
                                                                <span><?php echo $followers['username']; ?></span>
                                                            </div>
                                                        </a>

                                                    </li>
                                            <?php }
                                            } ?>
                                        </ul>

                                        <div class="peoples-mesg-box">
                                            <?php
                                            $toUserID = $this->uri->segment(2);
                                            if ($toUserID == "") { ?>
                                                <ul class="chatting-area" id="">
                                                    <p class="text-center text-danger">Please Select User</p>
                                                </ul>
                                            <?php } else { ?>
                                                <div class="conversation-head">
                                                    <figure>
                                                        <?php
                                                        $otherUserImg = base_url().'/upload/userprofile/defaultProfile.png';
                                                        if (isset($otherUserDetails['profileImage']) && $otherUserDetails['profileImage'] != "") {
                                                            $otherUserImg = $otherUserDetails['profileImage'];
                                                        } ?>
                                                        <img src="<?php echo $otherUserImg; ?>" alt="" class="chatUserPic">
                                                    </figure>
                                                    <span><?php if (isset($otherUserDetails['username'])) {
                                                        echo $otherUserDetails['username'];
                                                    } ?> <i>online</i></span>
                                                </div>
                                                <ul class="chatting-area" id="messageList">
                                                    <li>Loading...</li>
                                                </ul>

                                                <div class="newpst-input" style="width: 100%;">
                                                    <form action="#" method="post" enctype="multipart/form-data">
                                                        <p class="lead emoji-picker-container">
                                                            <textarea rows="2" placeholder="Add a Message" id="message" name="text" class="" data-emojiable="true" data-emoji-input="unicode"></textarea>
                                                        </p>
                                                        <input type="hidden" name="toID" value="<?php echo $toID ?>" id="toID">
                                                        <input type="hidden" name="userimg" value="<?php echo (isset($userDetails['profileImage'])) ? $userDetails['profileImage'] : '';  ?>" id="userimg">
                                                        <input type="hidden" name="otherUserImg" value="<?php echo $otherUserImg; ?>" id="otherUserImg" />
                                                        <input type="file" id="Videoupload" style="display:none" accept="video/mp4"/>
                                                        <!-- <input type="file" name="video" id="vide" style="display:none" accept="video/mp4"> -->
                                                        <div class="attachments">
                                                            <ul class="cursor-point">
                                                                <li id="emoji">
                                                                    <i class="fa fa-smile-o"></i>
                                                                    <label class="fileContainer"> </label>
                                                                </li>
                                                                <li id="image">
                                                                    <i class="fa fa-camera" id="" onclick="document.getElementById('id01').style.display='block'" accept="image/*"></i>
                                                                </li>
                                                                <li id="">
                                                                    <i class="fa fa-video-camera" id="" onclick="document.getElementById('id02').style.display='block'" accept="video/mp4"></i>    
                                                                </li>
                                                                <li id="">
                                                                    <i class="" id="" onclick="document.getElementById('id03').style.display='block'"><b>GIF</b></i>
                                                                </li>
                                                                <li>
                                                                    <button title="send" class="" id="save"><i class="fa fa-paper-plane text-white"></i></button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </form>
                                                </div>
                                            <?php } ?>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- centerl meta -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div id="id01" class="w3-modal">
    <div class="w3-modal-content">
        <form method="post" id="imagesend">
            <header class="w3-container text-white webbgcolor">
                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                <h3><i class="fa fa-camera"></i> Send Chat Image</h3>
            </header>
            <div class="container">
                <input type="hidden" name="to" value="<?= $toUserID ?>">
                <div class="input_fields_wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div><input type="file" class="form-control" name="file" id="file" onchange="readURL(this);" required></div>
                        </div>
                        <div class="col-md-6">
                            <img id="blah" src="http://placehold.it/180" alt="your image" class="" style="height:100px; width:100px;" />
                        </div>
                    </div>
                </div>
                <input class="btn btn-success mt10" id="chatsubmit" type="submit" name="poll" value="Send" />
            </div>
        </form>
    </div>
</div>

<div id="id02" class="w3-modal">
    <div class="w3-modal-content">
        <form method="post" id="videosend">
            <header class="w3-container text-white webbgcolor">
                <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                <h3><i class="fa fa-camera"></i> Send Video </h3>
            </header>
            <div class="container">
                <input type="hidden" name="to" value="<?= $toUserID ?>">
                <div class="input_fields_wrap">
                    <div class="row">
                        <div class="col-md-6">
                            <div><input type="file" name="video" id="vide" accept="video/mp4" required></div>
                        </div>
                    </div>
                </div>
                <input class="btn btn-success mt10" id="btnuploadVideo" type="submit" name="uploadVideo" value="Send" />
            </div>
        </form>
        
        <canvas id="can" style="height:1px; width:100%;"> </canvas>
        <video controls class="mt10" id="vibox">
            <source type="video/mp4">
            <canvas class="postvideo mt10" id="thumb" style="display:block;"></canvas>
        </video>
    </div>
</div>

<div id="id03" class="w3-modal">
    <div class="w3-modal-content">
        <form method="post" id="gifsend">
            <header class="w3-container text-white webbgcolor">
                <span onclick="document.getElementById('id03').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                <h3><i class="fa fa-image"></i> Send GIF </h3>
            </header>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="buttons" style="display: none"></div>
                        <input type="hidden" name="toID" value="<?php echo $toID ?>" id="toID">
                        <input type="hidden" name="to" value="<?= $toUserID ?>">
                        <div class="search-bar">
                            <input type="text" name="query" class="form-control" id="user-search" placeholder="Search GIF" />
                            <div class="mt20">
                                <input id="submit" type="button" value="Search" class="btn btn-sm btn-success btnSearchGif" />
                                <input type="button" class="trending btn btn-sm btn-danger btnSearchGif" value="See what's trending" />
                            </div>
                        </div>
                        <input type="hidden" id="gif" name="gif" value="" required />
                    </div>
                </div>
                <div class="mt20 mb">
                    <div id="gifresults" class="row"></div>
                </div>
                <div class="">
                    <input class="btn btn-success" id="btnUploadGif" type="submit" name="uploadGif" value="Send" />
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#blah').show();
                $('#blah').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#blah').hide();
    $('#file').click(function() {
        
    })
    $('#chatsubmit').click(function() {
        $('#blah').hide();
    })

    $('#imagesend').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url('home/chatImageSend'); ?>",
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                $("#imagesend")[0].reset();
                // $('#messageList').html(html);
                $("#id01").hide();
            }
        });
    });
    $('#videosend').submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "<?php echo base_url('home/chatVideoSend'); ?>",
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            success: function(data) {
                $("#id02").hide();
            }
        });
    });

    $('#gifsend').submit(function(e) {
        e.preventDefault();
        var gif = $('#gif').val();
        if(gif){
            $.ajax({
                url: "<?php echo base_url('home/userMsgSave'); ?>",
                type: "post",
                data: new FormData(this),
                processData: false,
                contentType: false,
                cache: false,
                async: false,
                success: function(data) {
                    $("#id03").hide();
                }
            });
        }
    });

    $('#OpenImgUpload').click(function() {
        $('#finput').trigger('click');
    });
    // $('#OpenVideoUpload').click(function() {
    //     $('#vide').trigger('click');
    // });

    $('#can').hide();
    $('#finput').click(function() {
        $('#can').show();
    });
    $('#vibox').hide();
    $('#vide').click(function() {
        $('#can').hide();
    });

    function upload() {
        var imgcanvas = document.getElementById("can");
        var fileinput = document.getElementById("finput");
        var filename = fileinput.value;
        var image = new SimpleImage(fileinput);
        image.drawTo(imgcanvas);
        //alert( "You chose " + filename);
    }

    $(function() {
        var video = $("video");
        var thumbnail = $("#thumb");
        var input = $("#vide");
        var ctx = thumbnail.get(0).getContext("2d");
        var duration = 0;
        var img = $("#thumb");

        input.on("change", function(e) {
            var file = e.target.files[0];
            // Validate video file type
            if (["video/mp4"].indexOf(file.type) === -1) {
                alert("Only 'MP4' video format allowed.");
                return;
            }
            // Set video source
            video.find("source").attr("src", URL.createObjectURL(file));
            // Load the video
            video.get(0).load();
            $('#vibox').show();
            // Load metadata of the video to get video duration and dimensions
            video.on("loadedmetadata", function(e) {
                duration = video.get(0).duration;
                // Set canvas dimensions same as video dimensions
                thumbnail[0].width = video[0].videoWidth;
                thumbnail[0].height = video[0].videoHeight;
                // Set video current time to get some random image
                video[0].currentTime = Math.ceil(duration / 2);
                // Draw the base-64 encoded image data when the time updates
                video.one("timeupdate", function() {
                    ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                    img.attr("src", thumbnail[0].toDataURL());
                });
            });
        });
    });

    $("#save").click(function(event) {
        event.preventDefault();

        //get value of message
        var message = $('#message').val();
        var toID = $('#toID').val();
        var groupimg = $('#groupimg').val();
        //check if value is not empty
        if (message.trim()=="") {
            $('#error_message').html("Please enter message");
            $('#message').focus();
            return false;
        } else {
            $("#error_message").html("");
            $('.emoji-wysiwyg-editor').html('');
        }
        if(message){
            //Ajax call to send data to the insert.php
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('home/userMsgSave'); ?>",
                data: {
                    message: message,
                    toID: toID,
                    groupimg: groupimg
                },
                cache: false,
                success: function(data) {
                    $('#message').val('');
                    $('.emoji-wysiwyg-editor').html('');
                }
            });
        }
    });
</script>

<script>
$(document).ready(function() {
    setInterval(function() {
        //$('#messageList').empty();
        var toID = $("#toID").val();
        if(toID){
            $.ajax({
                type: "post",
                url: '<?php echo base_url('home/userMsgList'); ?>',
                data: {
                    toID: toID
                },
                dataType: 'JSON',
                success: function(response) {
                    var html = '';
                    var id = <?= $this->session->userdata('userID') ?>;
                    var toID = $("#toID").val();
                    var userimg = $("#userimg").val();
                    var otherUserImg = $("#otherUserImg").val();
                    //alert(userPath);
                    $.each(response, function(key, val) {
                        //console.log(val);
                        //alert(val);
                        var media_type = val.media_type;
                        
                        var video = "";
                        if(media_type==""){
                            var chatImg = val.gif;
                        }else{
                            if(media_type==2){
                                video = val.media;
                            }else{
                                var chatImg = val.media;
                            }
                        }
                        var media = val.text.trim();

                        if (val.userID == id) {
                            if (val.text.trim()!="") {
                                html += '<li class="me">' +
                                    '<figure>' +
                                    '<img src=" ' + userimg + ' "style="height: 30px; width: 30px;">' +
                                    '</figure>' +
                                    '<p>' + val.text + '</p>' +
                                    '</li>'
                            } else if (val.text.trim() == "") {
                                html += '<li class="me">' +
                                    '<figure>' +
                                    '<img src=" ' + userimg + ' "style="height: 30px; width: 30px;">' +
                                    '</figure>';
                                if(media_type==2){
                                    // html += '<a href="' + video + '" title="" data-strip-group="mygroup" class="strip" data-strip-options="width: 700,height: 450">'+
                                    //     '<img src="' + userimg + '" alt="">'+
                                    //     '<i><svg version="1.1" class="play" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" height="50px" width="50px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">'+
                                    //     '<path class="stroke-solid" fill="none" stroke=""  d="M49.9,2.5C23.6,2.8,2.1,24.4,2.5,50.4C2.9,76.5,24.7,98,50.3,97.5c26.4-0.6,47.4-21.8,47.2-47.7C97.3,23.7,75.7,2.3,49.9,2.5"/>'+
                                    //     '<path class="icon" fill="" d="M38,69c-1,0.5-1.8,0-1.8-1.1V32.1c0-1.1,0.8-1.6,1.8-1.1l34,18c1,0.5,1,1.4,0,1.9L38,69z"/>'+
                                    //     '</svg></i></a>';
                                    html += '<video controls class="" style="width: 250px;height: 150px;">'+
                                        '<source type="video/mp4" src="' + video + '">'+
                                    '</video>';
                                }else{
                                    html += '<a class="strip" href="' + chatImg + '" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">'+
                                    '<img src=" ' + chatImg + ' "style="height: 150px; width: 150px;"></a>' +
                                    '</li>'
                                }
                            }
                        } else {
                            if (val.text.trim()!="") {
                                html += '<li class="you">' +
                                    '<figure>' +
                                    '<img src=" ' + otherUserImg + ' "style="height: 30px; width: 30px;">' +
                                    '</figure>' +
                                    '<p>' + val.text + '</p>' +
                                    '</li>'
                            } else if (val.text.trim() == "") {
                                html += '<li class="you">' +
                                    '<figure><img src=" ' + otherUserImg + ' "style="height: 30px; width: 30px;">' +
                                    '</figure>';
                                    if(media_type==2){
                                html += '<video controls class="" style="width: 250px;height: 150px;">'+
                                        '<source type="video/mp4" src="' + video + '">'+
                                    '</video>';
                                }else{
                                    html += '<a class="strip" href="' + chatImg + '" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">'+
                                    '<img src=" ' + chatImg + ' "style="height: 150px; width: 150px;"></a>' +
                                    '</li>'
                                };
                            }
                        }
                    });
                    $('#messageList').html(html);
                }
            });
        }
    }, 3000);

    $('.emoji-picker').hide();
    $('#emoji').click(function() {
        $('.emoji-picker').click();
    });
});
;(function($, window, document, undefined) {
// $(function() {
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '<?= base_url('assets/emoji/img/') ?>/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();
// });
})(jQuery, window, document);

// for GIF script
var topics = ["Internet Cats", "Meme's", "Typing", "Space", "Rick and Morty"];
function addSearchBtns() {
  $("#buttons").html("");
  for (i = 0; i < topics.length; i++) {
    var $button = $("<input type='button' class='btn btn-sm search-btn' />");
    $button.val(topics[i]);
    $("#buttons").append($button);
  }
}
addSearchBtns();
$(document).on("click", ".btnSearchGif", function() {
  $("#gifresults").html("");
  // Beginning API call
  var queryURL = "https://api.giphy.com/v1/gifs/search?";
  var query;
  var params = {
    q: query,
    limit: 6,
    api_key: "e5frANRyrhZN0icIH2vlPlB7cYx68knL",
    fmt: "json"
  };
  if ($(this).hasClass("search-btn")) {
    query = $(this).val();
  } else if ($("#user-search").val() !== "") {
    query = $("#user-search").val();
    topics.push(query);
    if (topics.length > 6) {
      topics.shift();
    }
    addSearchBtns();
  }
  params.q = query;

  if ($(this).hasClass("trending")) {
    queryURL = "https://api.giphy.com/v1/gifs/trending?";
    delete params.q;
  }
  $.ajax({
    url: queryURL + $.param(params),
    method: "GET",
    success: function(r) {
      for (i = 0; i < params.limit; i++) {
        var $img = $("<img>");
        var $div = $("<div>");
        var $rating = $("<h6>");
        var gifObj = r.data[i];
        var gif = gifObj.images;
        console.log(gif);

        // Image builder object
        $img.attr({
            // "width": "200px",
            src: gif.fixed_height_still.url,
            "data-animate": gif.fixed_height.url,
            "data-still": gif.fixed_height_still.url,
            "data-state": "still",
            "data-src": gif.original.url,
            // src: gif.original.url,
            class: "newgif"
        });
        // $div.attr("id", "gif-" + i);
        $div.addClass("gif-box col-sm-4");
        // $rating.text("Rating: " + gifObj.rating);
        $div.append($img, $rating);
        $("#gifresults").append($div);
      }
      
      $(".newgif").on("click", function() {
        var getGifUrl = $(this).attr("data-src");
        $('#gifresults .gif-box .gifBorder').removeClass('gifBorder');
        $(this).addClass('gifBorder');
        var gifArr = getGifUrl.split('?cid=');
        if(gifArr[0]){
            $('#gif').val(gifArr[0]);
        }
        
        var state = $(this).attr("data-state");
        var setgif = $(this).attr("src");
        // $('#gif').val(setgif);
        if (state === "still") {
          $(this).attr("src", $(this).attr("data-animate"));
          $(this).attr("data-state", "animate");
        } else {
          $(this).attr("src", $(this).attr("data-still"));
          $(this).attr("data-state", "still");
        }
      });
    }
  });
});
</script>
<?php
include('footer.php');
?>