<?php
include('header.php');
?>

<?php
include('toparea.php');
?>
<div class="gap gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row merged20" id="page-contents">
                    <?php
                    //include('shortcuts.php');
                    ?>
                    <!-- Shortcuts -->
                    <!-- sidebar -->
                    <div class="col-lg-12">
                        <div class="central-meta">
                            <div class="frnds">
                                <a class="" href="#frends-req" data-toggle="tab"><b>Group Members</b></a>
                                <!-- <button class="btn text-white webbgcolor float-right"  onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-plus"></i> Add Member</button>-->
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <style>
                                        .w1075 {
                                            width: 1075px !important;
                                        }
                                        @media only screen and (max-width: 600px) {
                                            .w1075 {
                                                width: 315px !important;
                                            }
                                        }
                                    </style>

                                    <!-- <div class="tab-pane fade" id="frends-req"> -->
                                    <div class="" style="background: #fdfdfd none repeat scroll 0 0; display: inline-block;">
                                        <ul class="nearby-contct w1075" id="people-list">
                                            <?php
                                            if (!empty($groupmember)) {
                                                foreach ($groupmember as $member) { ?>
                                                    <li id="removeMember">
                                                        <div class="nearly-pepls">
                                                            <figure>
                                                                <?php
                                                                if (isset($member['profileImage']) && $member['profileImage'] != "") { ?>
                                                                    <a href="javascript:void(0)" title=""><img src="<?php echo $member['profileImage']; ?>" alt="" style="height:50px; width:50px;"></a>
                                                                <?php } else { ?>
                                                                    <a href="javascript:void(0)" title=""><img src="<?php echo  base_url('assets/images/fav.png'); ?>" alt=""></a>
                                                                <?php }
                                                                ?>
                                                            </figure>
                                                            <div class="pepl-info">
                                                                <h4><a href="javascript:void(0)" title=""><?php echo $member['username']; ?></a></h4>
                                                                <span><?php echo $member['name']; ?></span>
                                                                <a href="javascript:void(0)" title="" class="add-butn remove_group_member" id="<?php echo $member['userGroupID']; ?>" onclick="remove_cart()">Remove</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }
                                            } else { ?>
                                                <li id="conData">
                                                    <div class="nearly-pepls">
                                                        <div class="pepl-info">
                                                            <span>No any Users Found.</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php }
                                            ?>
                                        </ul>
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div><!-- centerl meta -->
                    <?php
                    //include('friends.php');
                    ?>
                    <!-- sidebar -->
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<div id="id01" class="w3-modal">
    <div class="w3-modal-content" style="border-radius:25px; height:360px;">
        <form method="post" action="<?php echo base_url('home/addGroupMember'); ?>" enctype="multipart/form-data">
            <header class="w3-container text-white webbgcolor">
                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                <!-- <h2 style="font-family: Monospace;"><i class="fa fa-users"></i> Add Group Member</h2> -->
            </header>
            <div class="container" style="margin-top:20px;">
                <b class="text-black">Group Name</b> <input type="text" name="name" class="form-control" value="">
                <!-- <div class="input_fields_wrap"> -->
                <br>
                <!-- <b class="text-black">Profile</b> <input type="file"  name="picture" class="form-control" required> -->
                <input type="hidden" name="groupID" value="<?php echo $this->uri->segment(2); ?>">
                <br>
                <b>Select Member</b>
                <select class="mdb-select md-form" name="membersID[]" multiple>
                    <?php
                    if (!empty($followingName)) {
                        foreach ($followingName as $member) { ?>
                            <option value="<?php echo $member['followerID']; ?>"><?php echo $this->emoji->Decode($member['name']); ?></option>
                    <?php }
                    } ?>
                </select>
                <!-- </div> -->
                <input class="btn btn-success mt10" type="submit" name="submit" value="Save">
            </div>
        </form>
    </div>
</div>

<script>
    function remove_cart() {
        event.preventDefault();
        var btn = event.target;
        setTimeout(function() {
            $(btn)
                .closest("#removeMember")
                .fadeOut("fast");
        }, 100);
    }
</script>

<script>
    $(document).ready(function() {
        $(".remove_group_member").click(function() {
            var remove_group_member = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteGroupMember') ?>',
                data: 'remove_group_member=' + remove_group_member,
                success: function(data) {}
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $(".delete_data").click(function() {
            var delete_data = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteRequest') ?>',
                data: 'delete_data=' + delete_data,
                success: function(data) {}
            });
        });
    });
</script>

<?php
include('footer.php');
?>