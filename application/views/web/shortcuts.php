<div class="col-lg-3">
    <aside class="sidebar static" id="sidebarMenu">
        <div class="widget">
            <h4 class="widget-title">Shortcuts</h4>
            <ul class="naves">
                <li>
                    <i class="ti-image"></i>
                    <a href="<?php echo base_url() ?>" title="">Home</a>
                </li>
                <li>
                    <i class="fa fa-user"></i>
                    <a href="<?php echo base_url('time-line') ?>" title="">Profile</a>
                </li>
                <li>
                    <i class="fa fa-group"></i>
                    <a href="<?php echo base_url('usergroup') ?>" title="">Groups</a>
                </li>
                <li>
                    <i class="fa fa-bookmark"></i>
                    <a href="<?php echo base_url('bookmark') ?>" title="">Bookmarks</a>
                </li>
                <li>
                    <i class="fa fa-lock"></i>
                    <a href="<?php echo base_url('veteran-feed') ?>" title="">Veteran Feed</a>
                </li>
                <li>
                    <i class="fa fa-fire"></i>
                    <a href="<?php echo base_url('trending') ?>" title="">Trending</a>
                </li>
                <li>
                    <i class="fa fa-hashtag"></i>
                    <a href="<?php echo base_url('hashtag-trending') ?>" title="">Hashtag Trending</a>
                </li>
                <li>
                    <i class="ti-user"></i>
                    <a href="<?php echo base_url('friends') ?>" title="">Friend Requests</a>
                </li>
                <li>
                    <i class="fa fa-comments"></i>
                    <a href="<?php echo base_url('message-box') ?>" title="">Messenger</a>
                </li>
                <li>
                    <i class="fa fa-info-circle"></i>
                    <a href="<?php echo base_url('about'); ?>" title="">About</a>
                </li>
                <li>
                    <i class="ti-power-off"></i>
                    <a href="<?php echo base_url('home/logout') ?>" title="">Logout</a>
                </li>
            </ul>
        </div>
    </aside>
</div>