<?php
include('header.php');
include('toparea.php');
?>
<!-- top area -->
<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <!-- Shortcuts -->
                        <?php
                        //include('shortcuts.php');
                        ?>
                        <div class="col-lg-12">
                            <div class="central-meta h478">
                                <div class="groups">
                                    <span><i class="fa fa-users"></i><b>My Groups</b></span>
                                    <!-- <span><i class="fa fa-users"></i><b>My Groups</b></span> -->
                                    <button class="btn text-white webbgcolor float-right" onclick="document.getElementById('id01').style.display='block'"><i class="fa fa-plus"></i> Create Group</button>
                                </div>
                                <!-- Button trigger modal -->
                                <div id="searchDirgroup" class="mt20"></div>
                                <ul class="nearby-contct h340" id="people-list">
                                    <?php
                                    if (!empty($grouplist)) {
                                        foreach ($grouplist as $group) { ?>
                                            <li id="conData">
                                                <div class="nearly-pepls">
                                                    <figure>
                                                        <?php
                                                        if (isset($group['groupProfile']) && $group['groupProfile'] != "") { ?>
                                                            <a href="<?php echo base_url('groupmember/' . $group['groupID']); ?>" title=""><img src="<?php echo $group['groupProfile']; ?>" class="hw50" alt=""></a>
                                                        <?php } else { ?>
                                                            <a href="<?php echo base_url('groupmember/' . $group['groupID']); ?>" title=""><img src="<?php echo  base_url('assets/images/fav.png'); ?>" alt=""></a>
                                                        <?php }
                                                        ?>
                                                    </figure>
                                                    <a href="<?php echo base_url('group-message-box/' . $group['groupID']); ?>"><i class="fa fa-comment fa-2x webcolor float-right mt5" aria-hidden="true"></i></a>
                                                    <div class="pepl-info">
                                                        <h4><a href="<?php echo base_url('groupmember/' . $group['groupID']); ?>" title=""><?php echo $group['groupName'] ?></a></h4>
                                                        <!-- <span>public group</span> -->
                                                        <span><?php echo $group['totalmembers'] ?> Members</span>
                                                        <a href="javascript:void(0)" title="" class="add-butn delete_data" id="<?php echo $group['groupID']; ?>" onclick="remove_cart()">leave group</a>
                                                    </div>
                                                </div>
                                            </li>
                                    <?php }
                                    } ?>
                                </ul>
                                <!-- <div class="lodmore"><button class="btn-view btn-load-more"></button></div> -->
                            </div><!-- photos -->
                        </div><!-- centerl meta -->
                        <!-- friends -->
                        <?php
                        //include('friends.php');
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="id01" class="w3-modal">
    <div class="w3-modal-content" style="border-radius:25px; height:360px;">
        <form method="post" action="<?php echo base_url('home/createGroup'); ?>" enctype="multipart/form-data">
            <header class="w3-container text-white webbgcolor">
                <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                <h2 style="font-family: Monospace;"><i class="fa fa-users"></i> Create Group</h2>
            </header>
            <div class="container" style="margin-top:20px;">
                <b class="text-black">Group Name</b> <input type="text" name="name" class="form-control" required>
                <!-- <div class="input_fields_wrap"> -->
                <br>
                <b class="text-black">Profile</b> <input type="file" name="picture" class="form-control" required>
                <br>
                <b>Select Member</b>
                <select class="mdb-select md-form" name="memberID[]" multiple>
                    <?php
                    if (!empty($followingName)) {
                        foreach ($followingName as $member) { ?>
                            <option value="<?php echo $member['followerID']; ?>"><?php echo $this->emoji->Decode($member['name']); ?></option>
                        <?php }
                    } ?>
                </select>
                <!-- </div> -->
                <input class="btn btn-success mt10" type="submit" name="submit" value="Save">
            </div>
        </form>
    </div>

</div>

<script>
    $(document).ready(function() {
        $('.mdb-select').materialSelect();
    });
</script>

<script>
    function remove_cart() {
        event.preventDefault();
        var btn = event.target;
        setTimeout(function() {
            $(btn)
                .closest("#conData")
                .fadeOut("fast");
        }, 100);
    }
</script>

<script>
    $(document).ready(function() {
        $(".delete_data").click(function() {
            var del_id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteGroup') ?>',
                data: 'delete_id=' + del_id,
                success: function(data) {}
            });
        });
    });
</script>


<script>
    //===== Search Filter =====//
    (function($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[':'].Contains = function(a, i, m) {
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };

        function listFilter(searchDirgroup, list) {
            var form = $("<form>").attr({
                "class": "filterform",
                "action": "#"
            }),
            input = $("<input>").attr({
                "class": "filterinputgroup",
                "type": "text",
                "placeholder": "Search Group"
            });
            $(form).append(input).appendTo(searchDirgroup);
            
            $(input).change(function() {
                var filter = $(this).val();
                if (filter) {
                    $(list).find("li:not(:Contains(" + filter + "))").slideUp();
                    $(list).find("li:Contains(" + filter + ")").slideDown();
                } else {
                    $(list).find("li").slideDown();
                }
                return false;
            }).keyup(function() {
                $(this).change();
            });
        }

        //search friends widget
        $(function() {
            listFilter($("#searchDirgroup"), $("#people-list"));
        });
    }(jQuery));
</script>

<?php
include('footer.php');
?>