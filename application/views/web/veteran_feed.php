<?php
include('header.php');
?>

<?php
//$userID = $this->session->userdata('userID');
?>

    <div class="fixed-sidebar right">
        <div class="chat-friendz">
            <ul class="chat-users">
                <?php
                if($followersName){
                    foreach($followersName as $online){ ?>
                        <li>
                            <div class="author-thmb">
                                <?php
                                if(isset($online['profileImage']) && $online['profileImage']!=""){ ?>
                                    <img src="<?php echo $online['profileImage']; ?>" alt="" class="hw3234">
                                <?php }else{ ?>
                                    <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="" class="hw3234">
                                <?php }
                                ?>
                                
                                <span class="status f-online"></span>
                            </div>
                        </li>
                    <?php }
                }
                ?>
                <!-- <li>
                    <div class="author-thmb">
                        <img src="<?php //echo base_url('assets/images/resources/side-friend4.jpg') ?>" alt="">
                        <span class="status f-offline"></span>
                    </div>
                </li> -->
   
            </ul>
        </div>

        

        <div class="chat-box" class="mb138">
            <div class="chat-head">
                <span class="status f-online"></span>
                <h6>Bucky Barnes</h6>
                <div class="more">
                    <span class="more-optns"><i class="ti-more-alt"></i>
                        <ul>
                            <li>block chat</li>
                            <li>unblock chat</li>
                            <li>conversation</li>
                        </ul>
                    </span>
                    <span class="close-mesage"><i class="ti-close"></i></span>
                </div>
            </div>
            <div class="chat-list">
                <ul>
                    <li class="me">
                        <div class="chat-thumb"><img src="<?= base_url('assets/images/resources/avatar.png') ?>" alt=""></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
                        </div>
                    </li>
                    <li class="you">
                        <div class="chat-thumb"><img src="<?= base_url('assets/images/resources/avatar.png') ?>" alt=""></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
                        </div>
                    </li>
                    <li class="me">
                        <div class="chat-thumb"><img src="<?= base_url('assets/images/resources/avatar.png') ?>" alt=""></div>
                        <div class="notification-event">
                            <span class="chat-message-item">
                                Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
                            </span>
                            <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
                        </div>
                    </li>
                </ul>
                <form class="text-box">
                    <textarea placeholder="Post enter to post..."></textarea>
                    <button type="submit"></button>
                </form>
            </div>
        </div>

    </div>
    <!-- right sidebar user chat -->

    <section>
        <div class="gap gray-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row merged20" id="page-contents">

                                    <!-- Shortcuts -->
                                    <?php
                                    include('shortcuts.php');
                                    ?>


                                    <!-- who's following -->
                                    <?php
                                    //include('followingList.php');
                                    ?>

                            <div class="col-lg-6">
                                    <?php
                                    if(!empty($veteranFeed_list)){ ?>
                                    <div class="loadMore">
                                    <?php
                                        $count = 0;
                                        foreach($veteranFeed_list as $post){
                                            //echo "<pre>"; print_r($post); die();
                                            ?>
                                            <div class="central-meta item">
                                                <div class="user-post">
                                                    <div class="friend-info">

                                                        <?php
                                                        if(isset($post['isReshare']) && $post['isReshare'] == 1){ ?>
                                                        <i class="fa fa-retweet colorblue"></i> <span class="webcolor">You Quoted-</span> <br><br>
                                                        <figure>
                                                            <?php
                                                            if(isset($post['profileImage']) && $post['profileImage']!=""){ ?>
                                                                <img src="<?php echo $post['profileImage'] ?>" alt="" class="hw3250">
                                                            <?php }else{ ?>
                                                                <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="">
                                                            <?php }
                                                            ?>
                                                        </figure>
                                                        <div class="friend-name">
                                                            <ins><a href="<?= base_url('time-line') ?>" title=""><?php echo $post['username']; ?></a></ins>
                                                            <p><?php echo preg_replace("#https://([\S]+?)#Uis", '<a class="colorlink" rel="nofollow" href="http://\\1">\\1</a>', $post['ResharePostContent']); ?></p>
                                                        </div>
                                                        <hr>
                                                        <?php }
                                                        ?>


                                                        <figure>
                                                            <?php
                                                            if(isset($post['profileImage']) && $post['profileImage']!=""){ ?>
                                                                <img src="<?php echo $post['profileImage'] ?>" alt="" class="hw3250">
                                                            <?php }else{ ?>
                                                                <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="">
                                                            <?php }
                                                            ?>
                                                        </figure>
                                                        <div class="friend-name">
                                                            <ins><a href="<?= base_url('time-line') ?>" title=""><?php echo $post['username']; ?></a></ins>
                                                            <span style="color:#615a5a!important;"><b>
                                                            <?php
                                                            $today = new DateTime(date('Y-m-d H:i:s'));
                                                            $pastDate = $today->diff(new DateTime($post['createdDateTime']));
                                                            if($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0 && $pastDate->h == 0 && $pastDate->i == 0){
                                                                echo $pastDate->s.' second ago'; 
                                                            }elseif($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0 && $pastDate->h == 0){
                                                                echo $pastDate->i.' minute ago';
                                                            }elseif($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0){
                                                                echo $pastDate->h.' hours ago'; 
                                                            }elseif($pastDate->y == 0 && $pastDate->m == 0){
                                                                echo $pastDate->d.' days ago';
                                                            }elseif($pastDate->y == 0){
                                                                //echo $pastDate->m.' month ' . $pastDate->d.' day ago';
                                                                $startTimeStamp = strtotime($post['createdDateTime']);
                                                                $endTimeStamp = strtotime(date('Y-m-d'));
                                                                $timeDiff = abs($endTimeStamp - $startTimeStamp);
                                                                $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                                                                // and you might want to convert to integer
                                                                $numberDays = intval($numberDays);
                                                                echo $numberDays .' days ago';
                                                            }else{
                                                                echo $pastDate->y.' year ago'; 
                                                            }
                                                            ?>
                                                            </b></span>
                                                        </div>
                                                        <div class="post-meta">
                                                            <div class="description">
                                                                <p><?php echo preg_replace("#http://([\S]+?)#Uis", '<a class="colorlink" rel="nofollow" href="http://\\1">\\1</a>', $post['text']); ?></p>
                                                            </div>
                                                            <?php
                                                            if(isset($post['image']) && $post['image']!=""){ ?>
                                                            <img src="<?= base_url('assets/images/resources/user-post.jpg') ?>" alt="">
                                                            <?php }else{ ?>

                                                            <?php }
                                                            ?>
                                                            <div class="we-video-info">
                                                                <ul>
                                                                    <li>
                                                                        <span class="comment" data-toggle="tooltip" title="Comments">
                                                                            <i class="fa fa-comments-o"></i>
                                                                            <ins><?php echo $post['commentCount']; ?></ins>
                                                                        </span>
                                                                    </li>

                                                                    <?php
                                                                    if(isset($post['isLiked']) && $post['isLiked'] == 1){ ?>
                                                                        <?php //echo "<pre>"; print_r($post['isLiked']);?>
                                                                        <li id="dishide<?php echo $count; ?>">
                                                                            <span id="" class="like" data-toggle="tooltip" title="like">
                                                                            <a href="javascript:void(0)" title="" class="likepost" id="<?php echo $post['postID']; ?>"><i class="fa fa-heart colorred" id="likecomm<?php echo $count; ?>"></i></a>
                                                                            <ins class="colorblack"><?php echo $post['likesCount']; ?></ins>
                                                                            </span>
                                                                        </li>

                                                                        <li id="disshow<?php echo $count; ?>">
                                                                            <span id="" class="like" data-toggle="tooltip" title="like">
                                                                            <a href="javascript:void(0)" title="" class="likepost" id="<?php echo $post['postID']; ?>"><i class="fa fa-heart-o webcolor" id="dislikecomm<?php echo $count; ?>"></i></a>
                                                                            <ins class="colorblack"><?php echo $post['likesCount'] - 1; ?></ins>
                                                                            </span>
                                                                        </li>


                                                                    <?php }elseif(isset($post['isLiked']) && $post['isLiked'] == 0){ ?>
                                                                        <?php //echo "<pre>"; print_r($post['isLiked']);?>

                                                                        <li id="hide<?php echo $count; ?>">
                                                                        <span id="" class="like" data-toggle="tooltip" title="like">
                                                                        <a href="javascript:void(0)" title="" class="likepost" id="<?php echo $post['postID']; ?>"><i class="fa fa-heart-o webcolor" id="like<?php echo $count; ?>"></i></a>
                                                                        <ins class="colorblack"><?php echo $post['likesCount']; ?></ins>
                                                                        </span>
                                                                        </li>

                                                                        <li id="show<?php echo $count; ?>">
                                                                            <span id="" class="like" data-toggle="tooltip" title="like">
                                                                            <a href="javascript:void(0)" title="" class="likepost" id="<?php echo $post['postID']; ?>"><i class="fa fa-heart colorred" id="dislike<?php echo $count; ?>"></i></a>
                                                                            <ins class="colorblack"><?php echo $post['likesCount'] + 1; ?></ins>
                                                                            </span>
                                                                        </li>

                                                                    <?php }
                                                                    ?>

                                                                    <?php
                                                                    if(isset($post['isDisLiked']) && $post['isDisLiked'] == 1){ ?>
                                                                    <li id="dislikehide<?php echo $count; ?>">
                                                                        <span id="" class="dislike" data-toggle="tooltip" title="dislike">
                                                                        <a href="javascript:void(0)" title="" class="dislikepost" id="<?php echo $post['postID']; ?>"><i class="fa fa-heartbeat" aria-hidden="true" style="color:purple;" id="likeheart<?php echo $count; ?>"></i></a>
                                                                        <ins class="colorblack"><?php echo $post['disLikeCount']; ?></ins>
                                                                        </span>
                                                                    </li>

                                                                    <li id="dislikeshow<?php echo $count; ?>">
                                                                        <span id="" class="dislike" data-toggle="tooltip" title="dislike">
                                                                        <a href="javascript:void(0)" title="" class="dislikepost" id="<?php echo $post['postID']; ?>"><i class="ti-heart-broken webcolor" id="dislikeheart<?php echo $count; ?>"></i></a>
                                                                        <ins class="colorblack"><?php echo $post['disLikeCount'] - 1; ?></ins>
                                                                        </span>
                                                                    </li>

                                                                    <?php }elseif(isset($post['isDisLiked']) && $post['isDisLiked'] == 0){ ?>

                                                                    <li id="likehide<?php echo $count; ?>">
                                                                        <span id="" class="dislike" data-toggle="tooltip" title="dislike">
                                                                        <a href="javascript:void(0)" title="" class="dislikepost" id="<?php echo $post['postID']; ?>"><i class="ti-heart-broken webcolor" id="likeheart<?php echo $count; ?>"></i></a>
                                                                        <ins class="colorblack"><?php echo $post['disLikeCount']; ?></ins>
                                                                        </span>
                                                                    </li>

                                                                    <li id="likeshow<?php echo $count; ?>">
                                                                        <span id="" class="dislike" data-toggle="tooltip" title="dislike">
                                                                        <a href="javascript:void(0)" title="" class="dislikepost" id="<?php echo $post['postID']; ?>"><i class="fa fa-heartbeat" id="dislikeheart<?php echo $count; ?>" aria-hidden="true" style="color:purple;"></i></a>
                                                                        <ins class="colorblack"><?php echo $post['disLikeCount'] + 1; ?></ins>
                                                                        </span>
                                                                    </li>

                                                                    <?php }
                                                                    ?>

                                                                    <?php
                                                                    if(isset($post['isReshare']) && $post['isReshare'] == 1 || isset($post['isResharePost']) && $post['isResharePost'] == 1){ ?>
                                                                    <li class="social-media">
                                                                        <span class="share" data-toggle="tooltip" title="Share">
                                                                            <i class="fa fa-retweet colorblue"></i>
                                                                            <ins><?php echo $post['reShareCount']; ?></ins>
                                                                        </span>
                                                                    </li>
                                                                   <?php }else{ ?>
                                                                    <li class="social-media">
                                                                        <span class="share" data-toggle="tooltip" title="Share" id="showshare<?php echo $count; ?>">
                                                                            <a onClick="ShowModal(this)" data-id="<?php echo $post['postID']; ?>"><i class="fa fa-retweet" onclick="document.getElementById('id02').style.display='block'"></i></a>
                                                                            <!-- <button onClick="ShowModal(this)" data-id="217496D-P078"></button> -->
                                                                            <ins><?php echo $post['reShareCount']; ?></ins>
                                                                        </span>
                                                                    </li>
                                                                    <?php }
                                                                    ?>

                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <script>
                                                    $('#show<?php echo $count; ?>').hide();
                                                    $('#like<?php echo $count; ?>').click(function(){
                                                        $('#hide<?php echo $count; ?>').hide();
                                                        $('#show<?php echo $count; ?>').show();
                                                    })
                                                    </script>

                                                    <script>
                                                    $('#dislike<?php echo $count; ?>').click(function(){
                                                        $('#show<?php echo $count; ?>').hide();
                                                        $('#hide<?php echo $count; ?>').show();
                                                    })
                                                    </script>


                                                    <script>
                                                    $('#disshow<?php echo $count; ?>').hide();
                                                    $('#likecomm<?php echo $count; ?>').click(function(){
                                                        $('#dishide<?php echo $count; ?>').hide();
                                                        $('#disshow<?php echo $count; ?>').show();
                                                    })
                                                    </script>

                                                    <script>
                                                    $('#dislikecomm<?php echo $count; ?>').click(function(){
                                                        $('#disshow<?php echo $count; ?>').hide();
                                                        $('#dishide<?php echo $count; ?>').show();
                                                    })
                                                    </script>


                                                    <script>
                                                    $('#likeshow<?php echo $count; ?>').hide();
                                                    $('#likeheart<?php echo $count; ?>').click(function(){
                                                        $('#likehide<?php echo $count; ?>').hide();
                                                        $('#likeshow<?php echo $count; ?>').show();
                                                    })
                                                    </script>

                                                    <script>
                                                    $('#dislikeheart<?php echo $count; ?>').click(function(){
                                                        $('#likeshow<?php echo $count; ?>').hide();
                                                        $('#likehide<?php echo $count; ?>').show();
                                                    })
                                                    </script>


                                                    <script>
                                                    $('#dislikeshow<?php echo $count; ?>').hide();
                                                    $('#likeheart<?php echo $count; ?>').click(function(){
                                                        $('#dislikehide<?php echo $count; ?>').hide();
                                                        $('#dislikeshow<?php echo $count; ?>').show();
                                                    })
                                                    </script>

                                                    <script>
                                                    $('#dislikeheart<?php echo $count; ?>').click(function(){
                                                        $('#dislikeshow<?php echo $count; ?>').hide();
                                                        $('#dislikehide<?php echo $count; ?>').show();
                                                    })
                                                    </script>


                                                    <div class="coment-area">
                                                        <ul class="we-comet">
                                                           
                                                                        <?php
                                                                        error_reporting(0);
                                                                        $comments = $this->post->getComments($post['postID'], '', $post['userID']);
                                                                        if(!empty($comments)){
                                                                                foreach($comments as $comment){
                                                                                    //echo "<pre>"; print_r($comment);
                                                                                    ?>
                                                                                 <li>
                                                                                <div class="comet-avatar">
                                                                                <?php
                                                                                if(isset($comment['profileImage']) && $comment['profileImage']!=""){ ?>
                                                                                    <img src="<?php echo $comment['profileImage'] ?>" alt="" class="hw3250">
                                                                                <?php }else{ ?>
                                                                                    <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="" class="hw3250">
                                                                                <?php }
                                                                                ?>
                                                                                </div>
                                                                                <div class="we-comment bg-white mt5">
                                                                                <div class="coment-head">
                                                                                <h5><a href="<?= base_url('time-line') ?>" title=""><?php echo $post['username'] ?></a></h5>
                                                                                <span style="color:#615a5a!important;"><b>
                                                                                <?php
                                                                                $today = new DateTime(date('Y-m-d H:i:s'));
                                                                                $pastDate = $today->diff(new DateTime($comment['createdDateTime']));
                                                                                if($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0 && $pastDate->h == 0 && $pastDate->i == 0){
                                                                                    echo $pastDate->s.' second ago';
                                                                                }elseif($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0 && $pastDate->h == 0){
                                                                                    echo $pastDate->i.' minute ago';
                                                                                }elseif($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0){
                                                                                    echo $pastDate->h.' hours ago';
                                                                                }elseif($pastDate->y == 0 && $pastDate->m == 0){
                                                                                    echo $pastDate->d.' days ago';
                                                                                }elseif($pastDate->y == 0){
                                                                                    //echo $pastDate->m.' month ' . $pastDate->d.' day ago';
                                                                                    $startTimeStamp = strtotime($post['createdDateTime']);
                                                                                    $endTimeStamp = strtotime(date('Y-m-d'));
                                                                                    $timeDiff = abs($endTimeStamp - $startTimeStamp);
                                                                                    $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                                                                                    // and you might want to convert to integer
                                                                                    $numberDays = intval($numberDays);
                                                                                    echo $numberDays .' days ago';
                                                                                }else{
                                                                                    echo $pastDate->y.' year ago';
                                                                                }
                                                                                ?>
                                                                                </b></span>
                                                                                <a class="we-reply" href="javascript:void(0)" title="Reply"><i class="fa fa-reply"></i></a>
                                                                                </div>
                                                                                <p><?php echo preg_replace("#http://([\S]+?)#Uis", '<a class="colorlink" rel="nofollow" href="http://\\1">\\1</a>', $comment['text']); ?></p>
                                                                                <?php
                                                                                if(isset($comment['commentImage']) && $comment['commentImage']!=""){ ?>
                                                                                <img src="<?php echo $comment['commentImage'] ?>" alt="" class="hw308430 mt10" style="height:100px; width:100px;">
                                                                                <?php }
                                                                                ?>

                                                                                <div  class="post-comt-box" style="margin-top:15px; padding-left:2px!important;">
                                                                                <form action="<?php echo base_url('home/addPostReplayComment') ?>" method="post" enctype="multipart/form-data">
                                                                                        <input type="hidden" name="postID" value="<?php echo $post['postID']; ?>">
                                                                                        <input type="hidden" name="parentcommentID" value="<?php echo $comment['postCommentID']; ?>">
                                                                                        <?php $path = $this->uri->segment(1); ?>
                                                                                        <input type="hidden" name="path" value="<?php echo $path; ?>">
                                                                                        <textarea name="text" placeholder="Replay This Comment" class="ml20 pl25" style="width:380px;"></textarea>
                                                                                        <div class="add-smiles" style="top:30% !important;">
                                                                                        
                                                                                        <!-- <input type="submit" name="repcomment" id="commentposttest<?php echo $comm; ?>" style="display:none; right:-40px !important;"/> 
                                                                                        <i class="fa fa-paper-plane webcolor" aria-hidden="true" id="submitcommenttest<?php echo $comm; ?>" style="float:right; margin-right:-18px; margin-top:15px;"></i> -->
                                                                                        <div style="margin-top:30px;">
                                                                                        <button name="repcomment" style="right:-30px !important;">
                                                                                        <i class="fa fa-paper-plane webcolor" aria-hidden="true" style="float:right; margin-right:-18px; margin-top:15px;"></i>
                                                                                        </button>
                                                                                        </div>
                                                                                    
                                                                                        </div>  
                                                                                </form>
                                                                                </div>

                                                                                </div>

                                                                                <?php
                                                                                // error_reporting(0);
                                                                                $replay = $comment['comments'];
                                                                                foreach($replay as $replay){
                                                                                    error_reporting(0);
                                                                                        //echo "<pre>"; print_r($replay);
                                                                                    ?>
                                                                                <ul>
                                                                                    <li>
                                                                                        <div class="comet-avatar">
                                                                                        <?php
                                                                                            if(isset($replay['profileImage']) && $replay['profileImage']!=""){ ?>
                                                                                                <img src="<?php echo $replay['profileImage'] ?>" alt="" class="hw3250">
                                                                                            <?php }else{ ?>
                                                                                                <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="">
                                                                                            <?php }
                                                                                            ?>
                                                                                        </div>
                                                                                        <div class="we-comment bg-white">
                                                                                            <div class="coment-head">
                                                                                                <h5><a href="<?= base_url('time-line') ?>" title=""><?php echo $replay['username']; ?></a></h5>
                                                                                                <span style="color:#615a5a!important;"><b>
                                                                                                <?php
                                                                                                $today = new DateTime(date('Y-m-d H:i:s'));
                                                                                                $pastDate = $today->diff(new DateTime($replay['createdDateTime']));
                                                                                                if($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0 && $pastDate->h == 0 && $pastDate->i == 0){
                                                                                                    echo $pastDate->s.' second ago';
                                                                                                }elseif($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0 && $pastDate->h == 0){
                                                                                                    echo $pastDate->i.' minute ago';
                                                                                                }elseif($pastDate->y == 0 && $pastDate->m == 0 && $pastDate->d == 0){
                                                                                                    echo $pastDate->h.' hours ago';
                                                                                                }elseif($pastDate->y == 0 && $pastDate->m == 0){
                                                                                                    echo $pastDate->d.' days ago';
                                                                                                }elseif($pastDate->y == 0){
                                                                                                    //echo $pastDate->m.' month ' . $pastDate->d.' day ago';
                                                                                                    $startTimeStamp = strtotime($post['createdDateTime']);
                                                                                                    $endTimeStamp = strtotime(date('Y-m-d'));
                                                                                                    $timeDiff = abs($endTimeStamp - $startTimeStamp);
                                                                                                    $numberDays = $timeDiff/86400;  // 86400 seconds in one day
                                                                                                    // and you might want to convert to integer
                                                                                                    $numberDays = intval($numberDays);
                                                                                                    echo $numberDays .' days ago';
                                                                                                }else{
                                                                                                    echo $pastDate->y.' year ago';
                                                                                                }
                                                                                                ?>
                                                                                                </b></span>
                                                                                                <a class="we-reply" href="javascript:void(0)" title="Reply"><i class="fa fa-reply"></i></a>
                                                                                            </div>
                                                                                            <p><?php echo preg_replace("#http://([\S]+?)#Uis", '<a class="colorlink" rel="nofollow" href="http://\\1">\\1</a>', $replay['text']); ?></p>
                                                                                        </div>
                                                                                    </li>

                                                                                </ul>
                                                                                <?php } 
                                                                                ?>
                                                                                </li>
                                                                                <?php 
                                                                            }//die();
                                                                        }
                                                                        ?>
                                                            
                                                
                                                            <li class="post-comment">
                                                                <div class="comet-avatar">
                                                                <?php
                                                                if(isset($userDetails['profileImage']) && $userDetails['profileImage']!=""){ ?>
                                                                    <img src="<?php echo $userDetails['profileImage'] ?>" alt="" class="hw3250">
                                                                <?php }else{ ?>
                                                                    <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="">
                                                                <?php }
                                                                ?>
                                                                </div>
                                                                <div class="post-comt-box">
                                                                <form action="<?php echo base_url('home/addPostComment') ?>" method="post" enctype="multipart/form-data">
                                                                        <input type="hidden" name="postID" value="<?php echo $post['postID']; ?>">
                                                                        <?php
                                                                        $url = $this->uri->segment(1); 
                                                                        ?>
                                                                        <input type="hidden" name="path" value="<?php echo $url; ?>">

                                                                        <textarea name="text" placeholder="Post your comment" class="ml20 commwidth pl25"></textarea>
                                                                        <div class="add-smiles" style="top:30% !important;">

                                                                        <input type="file" name="commentImage" id="imgupload<?php echo $count; ?>" class="" style="display:none" accept="image/*"/> 
                                                                        <i class="fa fa-camera webcolor commpost"  id="OpenImgUpload<?php echo $count; ?>"></i>
                                                              
                                                                        <input type="submit" name="comment" id="commentpost<?php echo $count; ?>" style="display:none; right:-40px !important;"/> 
                                                                        <i class="fa fa-paper-plane webcolor" aria-hidden="true" id="submitcomment<?php echo $count; ?>" style="float:right; margin-right:-18px; margin-top:15px;"></i>
                                                                        </div>
                                                                </form>
                                                                </div>
                                                            </li>
                                                        </ul>

                                                        <hr />
                                                        <br />
                                                        <br />
                                                        <div id="dvPreview<?php echo $count; ?>">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <script language="javascript" type="text/javascript">
                                            $(function () {
                                                $("#imgupload<?php echo $count; ?>").change(function () {
                                                    $("#dvPreview<?php echo $count; ?>").html("");
                                                    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
                                                    if (regex.test($(this).val().toLowerCase())) {
                                                            if (typeof (FileReader) != "undefined") {
                                                                $("#dvPreview<?php echo $count; ?>").show();
                                                                $("#dvPreview<?php echo $count; ?>").append("<img />");
                                                                var reader = new FileReader();
                                                                reader.onload = function (e) {
                                                                    $("#dvPreview<?php echo $count; ?> img").attr("src", e.target.result);
                                                                }
                                                                reader.readAsDataURL($(this)[0].files[0]);
                                                            }
                                                        
                                                    }
                                                });
                                            });
                                            </script>

                                            <script>
                                            $('#OpenImgUpload<?php echo $count; ?>').click(function(){ $('#imgupload<?php echo $count; ?>').trigger('click'); });
                                            </script>

                                            <script>
                                            $('#submitcomment<?php echo $count; ?>').click(function(){ $('#commentpost<?php echo $count; ?>').trigger('click'); });
                                            </script>

                                        <?php $count++; } //die();
                                        ?>
                                        </div>
                                        <?php
                                    }else{ ?>
                                    <div class="central-meta item" class="h85">
                                        <div class="user-post">
                                                    <div class="description">
                                                        <p class="text-center text-danger">Veteran Feed are not available.</p>
                                                    </div>
                                        </div>
                                    </div>
                                    <?php }
                                    ?>
                            </div>
                            <!-- centerl meta -->


                            <?php
                            include('myprofile.php');
                            ?>
                            <!-- sidebar -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="id02" class="w3-modal" style="border-radius:25px;">
        <div class="w3-modal-content" style="border-radius:25px; height:360px;">
        <form method="post" action="<?php echo base_url('home/sharePost'); ?>" enctype="multipart/form-data">
        <header class="w3-container text-white webbgcolor"> 
            <span onclick="document.getElementById('id02').style.display='none'" class="w3-button w3-display-topright" id="closeshare">&times;</span>
            <h2 style="font-family: Monospace;"><i class="fa fa-retweet"></i> Share Post</h2>
        </header>
        <div class="container" style="margin-top:20px;">
            <b class="text-black">Somthing Write</b>
            <input type="hidden" id="pid" name="postid">
            <textarea class="form-control" name="post" style="height:220px;" required></textarea>
            <input class="btn btn-success mt10 webbgcolor" type="submit" name="share" value="SHARE">
        </div>
        </form>
        </div>
    </div>
    
    <script>
    $(document).ready(function(){
        $(".likepost").click(function(){
            var likepost = $(this).attr('id');
                //alert(likepost);
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url('home/likePost') ?>',
                    data:'likepost='+likepost,
                    success:function(data) {
                    }
                });
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".dislikepost").click(function(){
            var dislikepost = $(this).attr('id');
                //alert(dislikepost);
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url('home/disLikePost') ?>',
                    data:'dislikepost='+dislikepost,
                    success:function(data) {
                    }
                });
        });
    });
    </script>

<?php
include('footer.php');
?>
