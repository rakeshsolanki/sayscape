<?php
// echo "<pre>"; print_r($postData); die();
$postID = $postData['postID'];
?>
<div class="central-meta new-pst" id="posthide">
    <div class="new-postbox">
        <figure>
            <?php
            if (isset($postData['profileImage']) && $postData['profileImage'] != "") { ?>
                <img src="<?= $postData['profileImage'] ?>" alt="" class="wh-40">
            <?php } else { ?>
                <img src="<?= base_url('assets/images/resources/admin2.png') ?>" alt="">
            <?php } ?>
        </figure>
        <div class="newpst-input">
            <!-- divEmojiPicker  -->
            <form  method="post" enctype="multipart/form-data">
            <input type="hidden" name="postID" value="<?php echo $postID; ?>" />
                <p class="lead">
                    <textarea rows="2" placeholder="write something" name="text" class="" ><?php echo (isset($postData['text']))? $this->emoji->Decode($postData['text']):""; ?></textarea>
                </p>
                <div class="attachments">
                    <ul class="cursor-point">
                        <li id="image">
                            <i class="fa fa-camera"></i>
                            <label class="fileContainer">
                                <input type="file" name="picture[]" id="files<?php echo $postID; ?>" multiple accept="image/*" onchange="readmodelFile(this);">
                            </label>
                        </li>
                        <li id="video">
                            <i class="fa fa-video-camera"></i>
                            <label class="fileContainer">
                                <input type="file" name="video" id="videmodel<?php echo $postID; ?>" multiple accept="video/*">
                            </label>
                        </li>
                        <!-- <li>
                            <i class="fa fa-pie-chart" id="pollhide" onclick="document.getElementById('id01').style.display='block'"></i>
                            <label class="fileContainer">
                            </label>
                        </li> -->
                        <li>
                            <input type="submit" class="btn webbgcolor text-white" name="post" value="POST" id="normalpost<?php echo $postID; ?>">
                            <input type="submit" class="btn webbgcolor text-white" name="video" value="POST" id=videopostmodel<?php echo $postID; ?>>
                            <input type="submit" class="btn webbgcolor text-white" name="image" value="POST" id="imagepostmodel<?php echo $postID; ?>">
                            <!-- <button title="send"><i class="fa fa-paper-plane"></i></button> -->
                        </li>
                    </ul>
                </div>
                <div class="mt8">
                    <b>Expires On</b>
                    <input type="date" class="form-control" name="expiryDateTime" value="<?php echo (isset($postData['expiryDateTime']))?$postData['expiryDateTime']:""; ?>"">
                </div>
            </form>

            <div class="row">
                <div class="col-md-4">
                    <div id="sh"></div>
                </div>
            </div>
        </div>
        <video controls class="postvideo mt10" id="viboxmodel<?php echo $postID; ?>" style="display: inline;width: 100%;height: 250px;">
            <source type="video/mp4">
            <canvas class="postvideo mt10" id="thumb<?php echo $postID; ?>" style="display:block;"></canvas>
        </video>
        <!-- <div id="status"></div> -->
        <div id="modelphotos<?php echo $postID; ?>" class="row"></div>
    </div>

    <!-- <div id="id01" class="w3-modal">
        <div class="w3-modal-content" style="border-radius:25px; height:500px; overflow: scroll;">
            <form method="post" action="<?php echo base_url('home/poll'); ?>">
                <header class="w3-container text-white webbgcolor">
                    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <h2 style="font-family: Monospace;"><i class="fa fa-pie-chart"></i> Poll</h2>
                </header>
                <div class="container">
                    <b class="text-black">Ask Question:</b> <input type="text" name="question" class="form-control" required>
                    <div class="input_fields_wrap">
                        <button class="add_field_button btn btn-primary mt10">Add More Option</button>
                        <div><input type="text" class="form-control mt10" name="option[]"></div>
                    </div>
                    <input class="btn btn-success mt10" type="submit" name="poll" value="Save">
                </div>
            </form>
        </div>
    </div> -->

</div><!-- add post new box -->

<script>
    function readmodelFile(input) {
        $("#status").html('Processing...');
        counter = input.files.length;
        for (x = 0; x < counter; x++) {
            if (input.files && input.files[x]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $("#modelphotos<?php echo $postID; ?>").append('<div class="col-md-3 col-sm-3 col-xs-3"><img style="height:100px; width:100px; margin-top:15px;" src="' + e.target.result + '" class="img-thumbnail"></div>');
                };
                reader.readAsDataURL(input.files[x]);
            }
        }
        if (counter == x) {
            $("#status").html('');
        }
    }
</script>

<script>
    $('#viboxmodel<?php echo $postID; ?>').hide();
    $('#videmodel<?php echo $postID; ?>').click(function() {
        $('#modelphotos<?php echo $postID; ?>').hide();
    });
    $('#videopostmodel<?php echo $postID; ?>').hide();
    $('#imagepostmodel<?php echo $postID; ?>').hide();
    $('#videmodel<?php echo $postID; ?>').click(function() {
        $('#videopostmodel<?php echo $postID; ?>').show();
        $('#normalpost<?php echo $postID; ?>').hide();
        $('#imagepostmodel<?php echo $postID; ?>').hide();
    });
    $('#videopostmodel<?php echo $postID; ?>').hide();
    $('#imagepostmodel<?php echo $postID; ?>').hide();
    $('#files<?php echo $postID; ?>').click(function() {
        $('#imagepostmodel<?php echo $postID; ?>').show();
        $('#videopostmodel<?php echo $postID; ?>').hide();
        $('#normalpost<?php echo $postID; ?>').hide();
    });

    // $('#pollhide').click(function() {
    //     $('#viboxmodel').hide();
    // });
    $('#files<?php echo $postID; ?>').click(function() {
        $('#viboxmodel<?php echo $postID; ?>').hide();
    });
</script>

<script>
    $(function() {
        var video = $("#viboxmodel<?php echo $postID; ?>");
        var thumbnail = $("#thumb<?php echo $postID; ?>");
        var input = $("#videmodel<?php echo $postID; ?>");
        var ctx = thumbnail.get(0).getContext("2d");
        var duration = 0;
        var img = $("#thumb<?php echo $postID; ?>");

        input.on("change", function(e) {
            var file = e.target.files[0];
            // Validate video file type
            if (["video/mp4"].indexOf(file.type) === -1) {
                alert("Only 'MP4' video format allowed.");
                return;
            }
            // Set video source
            video.find("source").attr("src", URL.createObjectURL(file));
            // Load the video
            video.get(0).load();
            $('#viboxmodel<?php echo $postID; ?>').show();
            
            // Load metadata of the video to get video duration and dimensions
            video.on("loadedmetadata", function(e) {
                duration = video.get(0).duration;
                // Set canvas dimensions same as video dimensions
                thumbnail[0].width = video[0].videoWidth;
                thumbnail[0].height = video[0].videoHeight;
                // Set video current time to get some random image
                video[0].currentTime = Math.ceil(duration / 2);
                // Draw the base-64 encoded image data when the time updates
                video.one("timeupdate", function() {
                    ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                    img.attr("src", thumbnail[0].toDataURL());
                });
            });
            $('#vibox').show();
        });
    });
</script>