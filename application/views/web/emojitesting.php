<?php
include('header.php');
?>
    <br>
    <br>
    <br>
    <br>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="<?= base_url('assets/emoji/css/emoji.css') ?>" rel="stylesheet">
    <script src="<?= base_url('assets/emoji/js/config.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/util.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/jquery.emojiarea.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/emoji-picker.js') ?>"></script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="text-left">
                    <p class="lead emoji-picker-container">
                        <input type="email" class="form-control" placeholder="Input field" data-emojiable="true">
                    </p>
                    <p class="lead emoji-picker-container">
                        <input type="email" class="form-control" placeholder="Input with max length of 10" data-emojiable="true" maxlength="10">
                    </p>
                    <p class="lead emoji-picker-container">
                        <textarea class="form-control textarea-control" rows="3" placeholder="Textarea with emoji image input" data-emojiable="true"></textarea>
                    </p>
                    <p class="lead emoji-picker-container">
                        <textarea class="form-control textarea-control" rows="3" placeholder="Textarea with emoji Unicode input" data-emojiable="true" data-emoji-input="unicode"></textarea>
                    </p>
                </div>
            </div>
        </div>
    </div>

<?php
include('footer.php');
?>

<script>
    $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
            emojiable_selector: '[data-emojiable=true]',
            assetsPath: "<?= base_url('lib/img/') ?>",
            popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
    });
</script>
<script>
    // Google Analytics
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-49610253-3', 'auto');
    ga('send', 'pageview');
</script>
