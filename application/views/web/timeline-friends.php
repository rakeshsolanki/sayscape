<?php
include('header.php');
?>


<?php
include('toparea.php');
?>
<div class="gap gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row merged20" id="page-contents">
                    <?php
                    //include('shortcuts.php');
                    ?>
                    <!-- Shortcuts -->
                    <!-- sidebar -->
                    <div class="col-lg-12">
                        <div class="central-meta">
                            <div class="frnds">
                                <ul class="nav nav-tabs">
                                    <!-- <li class="nav-item"><a class="active" href="#frends" data-toggle="tab">My Friends</a>
                                              <span>55</span> 
                                            </li> -->
                                    <li class="nav-item"><a class="" href="#frends-req" data-toggle="tab"><b>Friend Requests</b></a>
                                        <!-- <span>60</span> -->
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <!-- <div class="tab-pane active fade show " id="frends">
                                           <div class="" style="background: #fdfdfd none repeat scroll 0 0; display: inline-block;">
                                                <ul class="nearby-contct" id="people-list"> -->
                                    <?php
                                    // if(!empty($followingName)){
                                    //     foreach($followingName as $following){ 
                                    ?>
                                    <!-- <li>
                                                        <div class="nearly-pepls">
                                                            <figure>
                                                            <?php
                                                            //if(isset($following['profileImage']) && $following['profileImage']!=""){ 
                                                            ?>
                                                                <img src="<?php //echo $following['profileImage']; 
                                                                            ?>" alt="" style="height:32px; width:32px;">
                                                            <?php //}else{ 
                                                            ?>
                                                                <img src="http://softtual.com/sayscape/upload/userprofile/defaultProfile.png" alt="" style="height:32px; width:32px;">
                                                            <?php //}
                                                            ?>
                                                            </figure>
                                                            <div class="pepl-info">
                                                                <h4><a href="time-line.html" title=""><?php //echo $following['username']; 
                                                                                                        ?></a></h4>
                                                                <span>ftv model</span>
                                                                <a href="javascript:void(0)" title="" class="add-butn more-action" data-ripple="">unfriend</a>
                                                                <a href="javascript:void(0)" title="" class="add-butn" data-ripple="">add friend</a>
                                                            </div>
                                                        </div>
                                                    </li> -->
                                    <?php //}
                                    //}
                                    ?>
                                    <!-- </ul>
                                          </div>
                                        </div> -->

                                    <!-- <div class="tab-pane fade" id="frends-req"> -->

                                    <div class="">
                                        <ul class="nearby-contct w1075" id="people-list">
                                            <?php
                                            if (!empty($requestList)) {
                                                foreach ($requestList as $request) { ?>
                                                    <li id="conData" class="">
                                                        <div class="nearly-pepls">
                                                            <figure>
                                                                <?php
                                                                if (isset($request['groupProfile']) && $request['groupProfile'] != "") { ?>
                                                                    <a href="javascript:void(0)" title=""><img src="<?php echo $request['groupProfile']; ?>" alt="" class="hw50"></a>
                                                                <?php } else { ?>
                                                                    <a href="javascript:void(0)" title=""><img src="<?php echo  base_url('assets/images/fav.png'); ?>" alt=""></a>
                                                                <?php }
                                                                ?>
                                                            </figure>
                                                            <div class="pepl-info">
                                                                <h4><a href="javascript:void(0)" title=""><?php echo $request['username']; ?></a></h4>
                                                                <span><?php echo $request['name']; ?></span>
                                                                <a href="javascript:void(0)" title="" class="add-butn more-action delete_data" id="<?php echo $request['userID']; ?>" onclick="remove_cart()">delete Request</a>
                                                                <a href="javascript:void(0)" title="" class="add-butn confirm_data" id="<?php echo $request['userID']; ?>" onclick="remove_cart()">Confirm</a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php }
                                            } else { ?>
                                                <li id="conData">
                                                    <div class="nearly-pepls">
                                                        <div class="pepl-info">
                                                            <span>No any Request Found.</span>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php }
                                            ?>
                                        </ul>
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div><!-- centerl meta -->

                    <?php
                    //include('friends.php');
                    ?>
                    <!-- sidebar -->
                </div>
            </div>
        </div>
    </div>
</div>
</section>



<script>
    function remove_cart() {
        event.preventDefault();
        var btn = event.target;
        setTimeout(function() {
            $(btn)
                .closest("#conData")
                .fadeOut("fast");
        }, 100);
    }

    $(document).ready(function() {
        $(".confirm_data").click(function() {
            var confirm_data = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/confirmRequest') ?>',
                data: 'confirm_data=' + confirm_data,
                success: function(data) {}
            });
        });
    });

    $(document).ready(function() {
        $(".delete_data").click(function() {
            var delete_data = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteRequest') ?>',
                data: 'delete_data=' + delete_data,
                success: function(data) {}
            });
        });
    });
</script>




<?php
include('footer.php');
?>