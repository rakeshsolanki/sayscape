<?php
$loginUserID = $this->session->userdata('userID');
?>
<div class="col-lg-3">
    <aside class="sidebar static" id="userListSidebar">
        <div class="widget">
            <h4 class="widget-title">My Profile</h4>
            <div class="your-page">
                <figure>
                    <?php
                    if (isset($userDetails['profileImage']) && $userDetails['profileImage'] != "") { ?>
                        <img src="<?php echo $userDetails['profileImage'] ?>" alt="" style="height:60px; width:60px;">
                    <?php } else { ?>
                        <img src="<?= base_url('assets/') ?>images/resources/admin.png" alt="" style="height:60px; width:60px;">
                    <?php }
                    ?>
                </figure>
                <div class="page-meta">
                    <a href="javascript:void(0)" title="" class="underline"><?php if (isset($userDetails['username'])) {
                        echo $userDetails['username'];
                    } ?></a>
                    <span>
                        <?php if (isset($userDetails['name'])) {
                            echo preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $userDetails['name']);
                        } ?>
                    </span>
                </div>
                <div class="page-likes">
                    <ul class="nav nav-tabs likes-btn">
                        <li class="nav-item">
                            <a class="active" href="#link1" data-toggle="tab"><?php echo $totalFollowing; ?> Following</a>
                        </li>
                        <li class="nav-item"><a class="" href="#link2" data-toggle="tab"><?php echo $totalFollowers; ?> Followers</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <?php
        //\\u1F603!
        // $text = "This is fun \u1F603"; // this has just one backslash, it had to be escaped 
        // echo "Database has: $text<br>";
        // $html = preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $text);
        // echo "Browser shows: $html<br>";
        ?>
        <div class="tab-content">
            <div class="tab-pane active fade show" id="link1">
                <div class="widget friend-list ">
                    <h4 class="widget-title">FOLLOWING</h4>
                    <!-- <div id="searchFollowingDir"></div> -->
                    <ul id="people-list" class="">
                        <?php
                        if ($followingName) {
                            foreach ($followingName as $following) {
                                // echo "<pre>"; print_r($following); echo "</pre>";
                                ?>
                                <li id="removeMember">
                                    <figure>
                                        <?php
                                        //if(isset($following['profileImage']) && $following['profileImage']!=""){ 
                                        ?>
                                        <img src="<?php echo $following['profileImage']; ?>" alt="" style="height:32px; width:32px;">
                                        <?php //}else{ ?>
                                        <!-- <img src="http://softtual.com/sayscape/upload/userprofile/defaultProfile.png" alt=""> -->
                                        <?php //} ?>
                                        <?php
                                        $onlineStatus = $this->user->getUserOnlineStatus($following['followerID']);
                                        ?>
                                        <span class="status f-<?php echo $onlineStatus; ?>"></span>
                                    </figure>
                                    <div class="friendz-meta">
                                        <a href="<?php echo base_url('time-line/' . $following['followerID']) ?>"><?php echo $following['username'] ?></a>
                                        <i><a href="<?php echo base_url('time-line/' . $following['followerID']) ?>" class="__cf_email__" data-cfemail="93f1f2e1fdf6e0d3f4fef2faffbdf0fcfe"><?php echo preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $following['name']);  ?></a></i>
                                    </div>
                                    <?php
                                    if(!empty($following) && $following['followerID']!=$loginUserID){
                                        if ($following['isfollow'] == 1) { ?>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>">Following</a>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="1" id="userFollow<?php echo $following['followerID']; ?>" style="display: none">Follow</a>
                                        <?php }
                                        elseif ($following['isfollow'] == 4) { ?>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>">Request</a>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="4" id="userFollow<?php echo $following['followerID']; ?>" style="display: none">Follow</a>
                                        <?php } else {
                                            if($following['privateProfileStatus']== 1){ ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>" style="display: none">Request</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="4" id="userfollowRequest<?php echo $following['followerID']; ?>">Follow</a>
                                            <?php }else{ ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>" style="display: none">Following</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="1" id="userFollow<?php echo $following['followerID']; ?>">Follow</a>
                                            <?php } ?>
                                        <?php }
                                        if ($following['isFollowMe'] == 1) { ?>
                                            <span class="followyou">Follows You</span>
                                        <?php }
                                    } ?>
                                    <!-- <a href="<?php //echo base_url('message-box/'.$following['followerID']); 
                                    ?>"><i class="fa fa-comment" aria-hidden="true" style="float:right; color:#5a0d28; margin-top:10px;"></i></a> -->
                                </li>
                        <?php }
                        } ?>
                    </ul>
                </div>
            </div>

            <div class="tab-pane fade" id="link2">
                <div class="widget friend-list ">
                    <h4 class="widget-title">FOLLOWERS</h4>
                    <!-- <div id="searchFollowerDir"></div> -->
                    <ul id="people-list" class="">
                        <?php
                        if ($followersName) {
                            foreach ($followersName as $follower) {
                                // echo "<pre>"; print_r($follower); echo "</pre>";
                                ?>
                                <li>
                                    <figure>
                                        <?php
                                        //if(isset($follower['profileImage']) && $follower['profileImage']!=""){ 
                                        ?>
                                        <img src="<?php echo $follower['profileImage']; ?>" alt="" style="height:32px; width:32px;">
                                        <?php //}else{ ?>
                                        <!-- <img src="http://softtual.com/sayscape/upload/userprofile/defaultProfile.png" alt=""> -->
                                        <?php //} ?>
                                        <?php
                                        $onlineStatus = $this->user->getUserOnlineStatus($follower['followerID']);
                                        ?>
                                        <span class="status f-<?php echo $onlineStatus; ?>"></span>
                                    </figure>
                                    <div class="friendz-meta">
                                        <a href="<?php echo base_url('time-line/' . $follower['followerID']) ?>"><?php echo $follower['username'] ?></a>
                                        <i><a href="<?php echo base_url('time-line/' . $follower['followerID']) ?>" class="__cf_email__" data-cfemail="93f1f2e1fdf6e0d3f4fef2faffbdf0fcfe"><?php echo  preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $follower['name']); ?></a></i>
                                    </div>
                                    <?php
                                    if(!empty($follower) && $follower['followerID']!=$loginUserID){
                                        if ($follower['isfollow'] == 1) { ?>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>">Following</a>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="1" id="userFollow<?php echo $follower['followerID']; ?>" style="display: none">Follow</a>
                                        <?php }
                                        elseif ($follower['isfollow'] == 4) { ?>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>">Request</a>
                                            <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="4" id="userFollow<?php echo $follower['followerID']; ?>" style="display: none">Follow</a>
                                        <?php } else {
                                            if($follower['privateProfileStatus']== 1){ ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>" style="display: none">Request</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="4" id="userfollowRequest<?php echo $follower['followerID']; ?>">Follow</a>
                                            <?php }else{ ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>" style="display: none">Following</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="1" id="userFollow<?php echo $follower['followerID']; ?>">Follow</a>
                                            <?php } ?>
                                        <?php }
                                        if ($follower['isFollowMe'] == 1) { ?>
                                            <span class="followyou">Follows You</span>
                                        <?php }
                                    } ?>
                                    <!-- <a href="<?php //echo base_url('message-box/'.$follower['followerID']) 
                                    ?>"><i class="fa fa-comment" aria-hidden="true" style="float:right; color:#5a0d28; margin-top:10px;"></i></a> -->
                                </li>
                        <?php }
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
</div>

<script>
function remove_cart() {
    event.preventDefault();
    var btn = event.target;
    setTimeout(function() {
        $(btn)
            .closest("#removeMember")
            .fadeOut("fast");
    }, 100);
}
</script>