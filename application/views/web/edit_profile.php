<?php
include('header.php');
?>

    <?php
    include('toparea.php');
    ?>
	<!-- top area -->

	<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row merged20" id="page-contents">
									

                                    <!-- shortcuts -->
                                    <?php
                                    include('shortcuts.php');
                                    ?>
								<!-- sidebar -->
							<div class="col-lg-9">
								<div class="central-meta">
									<div class="editing-info">
										<h5 class="f-title"><i class="ti-info-alt"></i> Edit Basic Information</h5>
                                        <?php
                                        //echo "<pre>"; print_r($userDetails); die();
                                        ?>
										<form method="post" action="<?php echo base_url('home/editProfile') ?>" enctype="multipart/form-data">
											<div class="form-group">	
											  <input type="text" id="input" name="name" required="required" value="<?php if(isset($userDetails['name'])){ echo $userDetails['name']; } ?>"/>
											  <label class="control-label" for="input">Display Name</label><i class="mtrl-select"></i>
                                            </div>
                                            <div class="form-group">	
											  <input type="text" id="input" name="username" required="required" value="<?php if(isset($userDetails['username'])){ echo $userDetails['username']; } ?>"/>
											  <label class="control-label" for="input">User Name</label><i class="mtrl-select"></i>
											</div>
											<div class="form-group">	
											  <?php
											  if(isset($userDetails['profileImage']) && $userDetails['profileImage']!=""){ ?>
													<img src="<?php echo $userDetails['profileImage'] ?>" id="hideimg" style="height:100px; width:100px;">
											  <?php }
											  ?>
											  <img id="blah" src="#" alt="your image" style="height:100px; width:100px;"/>
											  <input type="file" id="imgInp"/>
											</div>

											

											<div class="form-group">	
											  <input type="text" name="email" required="required" value="<?php if(isset($userDetails['email'])){ echo $userDetails['email']; } ?>"/>
											  <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
                                            </div>

<!--                                            <div class="form-group">	-->
<!--											  <input type="password" id="input"  name="password"/>-->
<!--											  <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>-->
<!--											</div>-->

                                            <div class="form-group">
                                                <input type="text" id="input" name="website" value="<?php if(isset($userDetails['website'])){ echo $userDetails['website']; } ?>"/>
                                                <label class="control-label" for="input">Website</label><i class="mtrl-select"></i>
                                            </div>

                                            <div class="form-group">
                                                <input type="date" id="input" name="dob" value="<?php if(isset($userDetails['dob'])){ echo $userDetails['dob']; } ?>"/>
                                                <label class="control-label" for="input">Birth Date</label><i class="mtrl-select"></i>
                                            </div>


                                            <div class="form-group">
                                                <input type="text" id="input" name="phone" value="<?php if(isset($userDetails['phone'])){ echo $userDetails['phone']; } ?>"/>
                                                <label class="control-label" for="input">Phone</label><i class="mtrl-select"></i>
                                            </div>

                                            <div class="form-group">
                                                <input type="text" id="input" name="location" value="<?php if(isset($userDetails['location'])){ echo $userDetails['location']; } ?>"/>
                                                <label class="control-label" for="input">Location</label><i class="mtrl-select"></i>
                                            </div>

                                            <div class="form-group">
                                                <input type="text" id="input" name="about" value="<?php if(isset($userDetails['about'])){ echo $userDetails['about']; } ?>"/>
                                                <label class="control-label" for="input">Bio</label><i class="mtrl-select"></i>
                                            </div>


											<div class="submit-btns">
                                            <input type="submit" name="edit" class="btn" value="Edit" style="background-color:#5a0d28; color:white;">
											</div>
										</form>
									</div>
								</div>	
							</div><!-- centerl meta -->
							<div class="col-lg-3">
								<aside class="sidebar static">
									<!-- <div class="widget">
											<h4 class="widget-title">Your page</h4>	
											<div class="your-page">
												<figure>
													<a title="" href="#"><img alt="" src="images/resources/friend-avatar9.jpg"></a>
												</figure>
												<div class="page-meta">
													<a class="underline" title="" href="#">My page</a>
													<span><i class="ti-comment"></i>Messages <em>9</em></span>
													<span><i class="ti-bell"></i>Notifications <em>2</em></span>
												</div>
												<div class="page-likes">
													<ul class="nav nav-tabs likes-btn">
														<li class="nav-item"><a data-toggle="tab" href="#link1" class="active">likes</a></li>
														 <li class="nav-item"><a data-toggle="tab" href="#link2" class="">views</a></li>
													</ul>
													<div class="tab-content">
													  <div id="link1" class="tab-pane active fade show">
														<span><i class="ti-heart"></i>884</span>
														  <a title="weekly-likes" href="#">35 new likes this week</a>
														  <div class="users-thumb-list">
														  	<a data-toggle="tooltip" title="" href="#" data-original-title="Anderw">
																<img alt="" src="images/resources/userlist-1.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="frank">
																<img alt="" src="images/resources/userlist-2.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Sara">
																<img alt="" src="images/resources/userlist-3.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Amy">
																<img alt="" src="images/resources/userlist-4.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Ema">
																<img alt="" src="images/resources/userlist-5.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Sophie">
																<img alt="" src="images/resources/userlist-6.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Maria">
																<img alt="" src="images/resources/userlist-7.jpg">  
															</a>  
														  </div>
													  </div>
													  <div id="link2" class="tab-pane fade">
														  <span><i class="ti-eye"></i>445</span>
														  <a title="weekly-likes" href="#">440 new views this week</a>
														  <div class="users-thumb-list">
														  	<a data-toggle="tooltip" title="" href="#" data-original-title="Anderw">
																<img alt="" src="images/resources/userlist-1.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="frank">
																<img alt="" src="images/resources/userlist-2.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Sara">
																<img alt="" src="images/resources/userlist-3.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Amy">
																<img alt="" src="images/resources/userlist-4.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Ema">
																<img alt="" src="images/resources/userlist-5.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Sophie">
																<img alt="" src="images/resources/userlist-6.jpg">  
															</a>
															<a data-toggle="tooltip" title="" href="#" data-original-title="Maria">
																<img alt="" src="images/resources/userlist-7.jpg">  
															</a>  
														  </div>
													  </div>
													</div>
												</div>
											</div>
										</div> -->
                                        
                                    <?php
                                    //include('followingList.php');
                                    ?>
                                    <!-- who's following -->
								</aside>
							</div><!-- sidebar -->
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</section>

    <?php
    include('footer.php');
    ?>