<?php
include('header.php');
?>

    <?php
    include('toparea.php');
    ?>

  <!-- top area -->
  <style>
    #searchDirhashtag .filterinputhashtag {
        border: 1px solid #eaeaea;
        color: #575757;
        font-size: 14px;
        padding: 5px 10px;
        width: 100%;
    }

    #searchDirhashtag {
        padding: 0 20px;
    }

    #people-listhashtag{
        margin-top: 20px;
        max-height: 450px;
        position: relative;
    }

    #remov {
        background-color: Transparent;
        background-repeat:no-repeat;
        border: none;
        cursor:pointer;
        overflow: hidden;
        outline:none;
    }
    </style>

    <section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row merged20" id="page-contents">
									<?php
									//include('shortcuts.php');
									?>

                      
							<div class="col-lg-12">
                            <form action="<?php echo base_url('hashtag-post'); ?>" method="post">
								<div class="central-meta h478">
									<div class="groups">
										<span><i class="fa fa-hashtag" aria-hidden="true"></i><b>HASHTAG TRENDING</b></span>
                                    </div>
                                    <!-- <div id="searchDirhashtag" class="" style="margin-top:20px;"></div> -->
                                    <ul class="nearby-contct h340" id="people-list">
                                    <div class="row">
                                        <?php
                                        if(!empty($hashtagTrendingList)){
                                            foreach($hashtagTrendingList as $hashtagTrending){
                                                ?>
                                           
                                            <div class="col-md-3 mt5">
                                            <li>
                                                <div class="nearly-pepls" id="conData">
                                                    <div class="pepl-info">
                                                        <h4>
                                                            <!-- <input type="submit" name="hashtag" value="<?php //echo $hashtagTrending; ?>" id="remov"> -->
                                                            <button class="btn"><?php echo $hashtagTrending; ?></button>
                                                        </h4>
                                                        <!-- <h4><?php //echo $hashtagTrending; ?></h4> -->
                                                    </div>
                                                        
                                                </div>                                                                                                      
                                            </li>
                                            </div>
                                           
                                            <?php }
                                        }
                                        ?>
                                    </div>
									</ul>
									<!-- <div class="lodmore"><button class="btn-view btn-load-more"></button></div> -->
                                </div><!-- photos -->
                                </form>
                            </div><!-- centerl meta -->
                            

							<!-- friends -->
							<?php
							//include('friends.php');
							?>

						</div>	
					</div>
				</div>
			</div>
		</div>	
	</section>




<script>
//===== Search Filter =====//
(function ($) {
    // custom css expression for a case-insensitive contains()
    jQuery.expr[':'].Contains = function (a, i, m) {
        return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    function listFilter(searchDirhashtag, list) {
        var form = $("<form>").attr({"class": "filterform", "action": "#"}),
            input = $("<input>").attr({
                "class": "filterinputhashtag",
                "type": "text",
                "placeholder": "Search Hashtag"
            });
        $(form).append(input).appendTo(searchDirhashtag);

        $(input).change(function () {
                var filter = $(this).val();
                if (filter) {
                    $(list).find("li:not(:Contains(" + filter + "))").slideUp();
                    $(list).find("li:Contains(" + filter + ")").slideDown();
                } else {
                    $(list).find("li").slideDown();
                }
                return false;
            })
            .keyup(function () {
                $(this).change();
            });
    }

//search friends widget
    $(function () {
        listFilter($("#searchDirhashtag"), $("#people-list"));
    });
}(jQuery));
</script>



<script>
function remove_cart() {
    event.preventDefault();
    var btn = event.target;
        setTimeout(function() {
            $(btn)
            .closest("#conData")
            .fadeOut("fast");
        }, 100);
}
</script>

<script>
$(document).ready(function(){
    $(".delete_data").click(function(){
        var del_id = $(this).attr('id');
            $.ajax({
                type:'POST',
                url:'<?php echo base_url('home/deleteGroup') ?>',
                data:'delete_id='+del_id,
                success:function(data) {
                }
            });
    });
});
</script>


<?php
include('footer.php');
?>