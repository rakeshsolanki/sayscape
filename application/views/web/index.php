<?php
include('header.php');
?>
<style>
    .mb138 {
        margin-bottom: 138px;
    }
</style>
<?php
//$userID = $this->session->userdata('userID');
?>
<div class="fixed-sidebar right">
    <div class="chat-friendz">
        <ul class="chat-users">
            <?php
            if ($followersName) {
                foreach ($followersName as $online) { ?>
                    <li>
                        <div class="author-thmb">
                            <a href="<?php echo base_url('message-box/' . $online['followerID']) ?>">
                                <?php
                                if (isset($online['profileImage']) && $online['profileImage'] != "") { ?>
                                    <img src="<?php echo $online['profileImage']; ?>" alt="" class="hw3234">
                                <?php } else { ?>
                                    <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="" class="hw3234">
                                <?php } ?>
                                <span class="status f-online"></span>
                            </a>
                        </div>
                    </li>
            <?php }
            } ?>
            <!-- <li>
                <div class="author-thmb">
                    <img src="<?php //echo base_url('assets/images/resources/side-friend4.jpg') ?>" alt="">
                    <span class="status f-offline"></span>
                </div>
            </li> -->
        </ul>
    </div>
</div>
<!-- right sidebar user chat -->
<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <!-- Shortcuts -->
                        <?php
                        include('shortcuts.php');
                        ?>


                        <!-- who's following -->
                        <?php
                        //include('followingList.php');
                        ?>
                        <div class="col-lg-6">
                            <?php
                            include('post.php');
                            ?>
                             <!-- add post new box -->
                                <div class="loadMore2" id="faq-result">
                                    <?php
                                    if(!empty($post_list)){
                                        $count = 1;
                                        foreach($post_list as $post){
                                            $datas['post'] =$post;
                                            $datas['count'] =$count;
                                                $this->load->view('web/post_list',$datas);
                                            $count ++;
                                             //break;
                                        }
                                    }
                                    ?>
                                </div>
                            </div>

<!--                            <div id="loader-icon">Load more Data<div>-->
                            <!-- centerl meta -->


                            <?php
                            include('myprofile.php');
                            ?>
                            <!-- sidebar -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="id02" class="w3-modal">
        <div class="w3-modal-content" style="border-radius:25px; height:360px;">
        <form method="post" action="<?php echo base_url('home/sharePost'); ?>" enctype="multipart/form-data">
        <header class="w3-container text-white webbgcolor"> 
            <span onclick="document.getElementById('id02').style.display='none'" 
            class="w3-button w3-display-topright">&times;</span>
            <h2 style="font-family: Monospace;"><i class="fa fa-retweet"></i> Share Post</h2>
        </header>
        <div class="container" style="margin-top:20px;">
            <b class="text-black">Somthing Write</b>
            <input type="hidden" id="pid" name="postid">
            <textarea class="form-control" name="post"></textarea>
            <input class="btn btn-success mt10 webbgcolor" type="submit" name="share" value="SHARE">
        </div>
        </form>
        </div>
    </div>

<div class="modal fade" id="editpostModel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<input type="hidden" name="baseurl" id="baseurl" value="<?= base_url(); ?>;">
<script>
    $(document).ready(function(){
        function getresult(url) {
            //$(".pagenum").val(url);
            $.ajax({
                url: "<?= base_url('home/ajaxPost?page=');?>"+url,
                type: "GET",
                data:  {rowcount:$("#rowcount").val()},
                beforeSend: function(){
                    $('#loader-icon').show();
                },
                complete: function(){
                    $('#loader-icon').hide();
                },
                success: function(data){
                    $("#faq-result").append(data);
                },
                error: function(){}
            });
        }
        $(window).scroll(function(){
            if ($(window).scrollTop() == $(document).height() - $(window).height()){
                if($(".pagenum:last").val() <= $(".total-page").val()) {
                     var pagenum = parseInt($(".pagenum:last").val()) + 1;
                    //var pagenum = parseInt($(".pagenum").val()) + 1;
                    getresult(pagenum);
                }
            }
        });
    });
</script>
    <script>
        function ShowModal(elem){
        var dataId = $(elem).data("id");
        //alert(dataId);
        $('#pid').val(dataId);
    }
    </script>

    <script>
    $(document).ready(function(){
        $(".likepost").click(function(){
            var likepost = $(this).attr('id');
                //alert(likepost);
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url('home/likePost') ?>',
                    data:'likepost='+likepost,
                    success:function(data) {
                    }
                });
        });
    });
    </script>

    <script>
    $(document).ready(function(){
        $(".dislikepost").click(function(){
            var dislikepost = $(this).attr('id');
                //alert(dislikepost);
                $.ajax({
                    type:'POST',
                    url:'<?php echo base_url('home/disLikePost') ?>',
                    data:'dislikepost='+dislikepost,
                    success:function(data) {
                    }
                });
        });
    });

    ;(function ($, window, document, undefined) {
        $('document').ready(function () {

        });
    })(jQuery, window, document);

    </script>
<script>
    $('#showcomm<?php echo $comm; ?>').hide();
    $('#likecomme<?php echo $comm; ?>').click(function() {
        $('#hidecomm<?php echo $comm; ?>').hide();
        $('#showcomm<?php echo $comm; ?>').show();
    });

    $('#dislikecomme<?php echo $comm; ?>').click(function() {
        $('#showcomm<?php echo $comm; ?>').hide();
        $('#hidecomm<?php echo $comm; ?>').show();
    });

    $('#discommshow<?php echo $comm; ?>').hide();
    $('#likecomment<?php echo $comm; ?>').click(function() {
        $('#discommhide<?php echo $comm; ?>').hide();
        $('#discommshow<?php echo $comm; ?>').show();
    });

    $('#dislikecomment<?php echo $comm; ?>').click(function() {
        $('#discommshow<?php echo $comm; ?>').hide();
        $('#discommhide<?php echo $comm; ?>').show();
    });

    $(document).ready(function() {
        $(".dislikepost").click(function() {
            var dislikepost = $(this).attr('id');
            //alert(dislikepost);
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/disLikePost') ?>',
                data: 'dislikepost=' + dislikepost,
                success: function(data) {}
            });
        });
    });

    $(document).ready(function() {
        $(".likecomment").click(function() {
            var likecomment = $(this).attr('id');
            //alert(likecomment);
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/likePostComment') ?>',
                data: 'likecomment=' + likecomment,
                success: function(data) {}
            });
        });
    });
</script>

<?php
include('footer.php');
?>
