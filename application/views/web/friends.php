<div class="col-lg-3">
<aside class="sidebar static">
<div class="widget friend-list stick-widget">
    <h4 class="widget-title">Followers</h4>
    <!-- <div id="searchDirfrnd" class=""></div> -->
    <ul id="people-list" class="friendz-list">
        <?php
        if($followersName){
            foreach($followersName as $Followers){ ?>
            <li>
                <figure>
                <?php
                if(isset($Followers['profileImage']) && $Followers['profileImage']!=""){ ?>
                    <img src="<?php echo $Followers['profileImage']; ?>" alt="" style="height:32px; width:34px;">
                <?php }else{ ?>
                    <img src="<?= base_url() ?>/assets/images/resources/admin.png" alt="" style="height:32px; width:34px;">
                <?php }
                ?>
                <span class="status f-online"></span>
                </figure>
                <div class="friendz-meta">
                    <a href="<?php echo base_url('time-line/'.$Followers['followerID']) ?>"><?php echo $Followers['username']; ?></a>
                    <i><a href="<?php echo base_url('time-line/'.$Followers['followerID']) ?>"><?php echo $Followers['name']; ?></a></i>
                </div>
            </li>
            <?php }
        } ?>

        <!-- <li>
            <figure>
                <img src="<?php //echo base_url('assets/')?>images/resources/friend-avatar3.jpg" alt="">
                <span class="status f-off"></span>
            </figure>
            <div class="friendz-meta">
                <a href="<?php //echo base_url('time-line') ?>">jason borne</a>
                <i><a href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="fa909b89959498ba9d979b9396d4999597">[email&#160;protected]</a></i>
            </div>
        </li> -->
    </ul>
    <!-- <div class="chat-box">
        <div class="chat-head">
            <span class="status f-online"></span>
            <h6>Bucky Barnes</h6>
            <div class="more">
                <span><i class="ti-more-alt"></i></span>
                <span class="close-mesage"><i class="ti-close"></i></span>
            </div>
        </div>
        <div class="chat-list">
            <ul>
                <li class="me">
                    <div class="chat-thumb"><img src="<?= base_url('assets/')?>images/resources/chatlist1.jpg" alt=""></div>
                    <div class="notification-event">
                        <span class="chat-message-item">
                            Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
                        </span>
                        <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
                    </div>
                </li>
                <li class="you">
                    <div class="chat-thumb"><img src="<?= base_url('assets/')?>images/resources/chatlist2.jpg" alt=""></div>
                    <div class="notification-event">
                        <span class="chat-message-item">
                            Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
                        </span>
                        <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
                    </div>
                </li>
                <li class="me">
                    <div class="chat-thumb"><img src="<?= base_url('assets/')?>images/resources/chatlist1.jpg" alt=""></div>
                    <div class="notification-event">
                        <span class="chat-message-item">
                            Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks
                        </span>
                        <span class="notification-date"><time datetime="2004-07-24T18:18" class="entry-date updated">Yesterday at 8:10pm</time></span>
                    </div>
                </li>
            </ul>
            <form class="text-box">
                <textarea placeholder="Post enter to post..."></textarea>
                <div class="add-smiles">
                    <span title="add icon" class="em em-expressionless"></span>
                </div>
                <div class="smiles-bunch">
                    <i class="em em---1"></i>
                    <i class="em em-smiley"></i>
                    <i class="em em-anguished"></i>
                    <i class="em em-laughing"></i>
                    <i class="em em-angry"></i>
                    <i class="em em-astonished"></i>
                    <i class="em em-blush"></i>
                    <i class="em em-disappointed"></i>
                    <i class="em em-worried"></i>
                    <i class="em em-kissing_heart"></i>
                    <i class="em em-rage"></i>
                    <i class="em em-stuck_out_tongue"></i>
                </div>
                <button type="submit"></button>
            </form>
        </div>
    </div> -->
    </div>
</div>


<script>
    //===== Search Filter =====//
    (function ($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[':'].Contains = function (a, i, m) {
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };

        function listFilter(searchDirfrnd, list) {
            var form = $("<form>").attr({"class": "filterform", "action": "#"}),
                input = $("<input>").attr({
                    "class": "filterinputfrnd",
                    "type": "text",
                    "placeholder": "Search Friend"
                });
            $(form).append(input).appendTo(searchDirfrnd);

            $(input).change(function () {
                    var filter = $(this).val();
                    if (filter) {
                        $(list).find("li:not(:Contains(" + filter + "))").slideUp();
                        $(list).find("li:Contains(" + filter + ")").slideDown();
                    } else {
                        $(list).find("li").slideDown();
                    }
                    return false;
                })
                .keyup(function () {
                    $(this).change();
                });
        }

    //search friends widget
        $(function () {
            listFilter($("#searchDirfrnd"), $("#people-listfrnd"));
        });
    }(jQuery));
    </script>