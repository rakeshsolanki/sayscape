<section>
    <div class="feature-photo">
        <figure>
            <?php
            $mainUrl = $this->uri->segment(1);
            $urlSegment2 = $this->uri->segment(2);
            $friendID = $this->user->getUserId($urlSegment2);
            if(isset($userDetails['coverImage']) && $userDetails['coverImage']!=""){ ?>
                <img src="<?php echo $userDetails['coverImage'] ?>" alt="" class="userCoverPic" />
            <?php }else{ ?>
                <img src="" alt="" class="userCoverPic" />
            <?php }
            ?>
        </figure>
        <div class="add-btn">
            <!-- <span><?php echo $totalFollowing; ?> Following</span>
            <span><?php echo $totalFollowers; ?> Followers</span> -->
            <?php
            $loginUserID = $this->session->userdata('userID');
            if($mainUrl=='time-line' && $friendID!=""){
                $otherUserdata = $this->user->getUserdetails($friendID, $loginUserID);
                // echo "<pre>"; print_r($otherUserdata); die();
                if(!empty($otherUserdata) && $otherUserdata['userID']!=$loginUserID){
                    if ($otherUserdata['isfollow'] == 1) { ?>
                        <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="0" id="userUnfollow<?php echo $otherUserdata['userID']; ?>">Following</a>
                        <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="1" id="userFollow<?php echo $otherUserdata['userID']; ?>" style="display: none">Follow</a>
                    <?php }
                    elseif ($otherUserdata['isfollow'] == 4) { ?>
                        <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="0" id="userUnfollow<?php echo $otherUserdata['userID']; ?>">Request</a>
                        <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="4" id="userFollow<?php echo $otherUserdata['userID']; ?>" style="display: none">Follow</a>
                    <?php } else {
                        if($otherUserdata['privateProfileStatus']== 1){ ?>
                            <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="0" id="userUnfollow<?php echo $otherUserdata['userID']; ?>" style="display: none">Request</a>
                            <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="4" id="userfollowRequest<?php echo $otherUserdata['userID']; ?>">Follow</a>
                        <?php }else{ ?>
                            <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="0" id="userUnfollow<?php echo $otherUserdata['userID']; ?>" style="display: none">Following</a>
                            <a href="javascript:void(0)" title="" class="btnFollowUnfollow" data-id="<?php echo $otherUserdata['userID']; ?>" data-status="1" id="userFollow<?php echo $otherUserdata['userID']; ?>">Follow</a>
                        <?php } ?>
                    <?php }
                } ?>
            <?php } ?>
        </div>
        <!-- <form class="edit-phto" action="<?php //echo base_url('home/coverImg') ?>" method="post" enctype="multipart/form-data">
            <i class="fa fa-camera-retro"></i>
            <label class="fileContainer">
                Edit Cover Photo
                <input id="pic" type="file" name="picture"/>
            </label>
            <input  type="submit" name="submit" value="Set" style="background-color:#5a0d28; color:white;"/>
        </form> -->

        <div class="container-fluid">
            <div class="row merged">
                <div class="col-lg-2 col-sm-3">
                    <div class="user-avatar">
                        <figure>
                            <?php
                            if(isset($userDetails['profileImage']) && $userDetails['profileImage']!=""){ ?>
                                <img src="<?php echo $userDetails['profileImage'] ?>" alt="" style="height:185px; width:190px;">
                            <?php }else{ ?>
                                <img src="<?= base_url('assets/')?>images/resources/admin.png" alt="" style="height:185px; width:190px;">
                            <?php }
                            ?>
                            <!-- <form class="edit-phto" action="" method="POST" enctype="multipart/form-data">
                                <i class="fa fa-camera-retro"></i>
                                <label class="fileContainer">
                                    Edit Display Photo
                                    <input type="file" name="picture"/>
                                </label>
                            </form> -->
                        </figure>
                    </div>
                </div>
                <?php
                $cur_tab = $this->uri->segment(1);
                ?>
                <div class="col-lg-10 col-sm-9">
                    <div class="timeline-info">
                        <ul>
                            <li class="admin-name">
                                <h5><?php if(isset($userDetails['username'])){ echo $userDetails['username']; } ?></h5>
                                <!-- <span>Group Admin</span> -->
                            </li>
                            <li>
                                <a class="<?php echo ($cur_tab == '') ? "active" : "" ?>" href="<?= base_url(''); ?>"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                                <a class="<?php echo ($cur_tab == 'time-line') ? "active" : "" ?>" href="<?= base_url('time-line'); ?>"><i class="fa fa-clock-o" aria-hidden="true"></i> Profile</a>
                                <a class="<?php echo ($cur_tab == 'usergroup') ? "active" : "" ?>" href="<?php echo base_url('usergroup'); ?>"><i class="fa fa-users" aria-hidden="true"></i> Groups</a>   
                                <a class="<?php echo ($cur_tab == 'bookmark') ? "active" : "" ?>" href="<?php echo base_url('bookmark'); ?>"><i class="fa fa-bookmark" aria-hidden="true"></i> Bookmarks</a>
                                <a class="<?php echo ($cur_tab == 'veteran-feed') ? "active" : "" ?>" href="<?php echo base_url('veteran-feed'); ?>"><i class="fa fa-lock" aria-hidden="true"></i> Verified Veteran Feed</a> 
                                <a class="<?php echo ($cur_tab == 'trending') ? "active" : "" ?>" href="<?php echo base_url('trending'); ?>" title="" data-ripple=""><i class="fa fa-fire" aria-hidden="true"></i> Trending</a>
                                <!-- <a class="<?php echo ($cur_tab == 'hashtag-trending') ? "active" : "" ?>" href="<?php echo base_url('hashtag-trending'); ?>" title="" data-ripple=""><i class="fa fa-hashtag" aria-hidden="true"></i> Hashtag Trending</a>
                                <a class="<?php echo ($cur_tab == 'friends') ? "active" : "" ?>" href="<?php echo base_url('friends'); ?>"><i class="fa fa-user" aria-hidden="true"></i> Friend Requests</a>
                                <a class="<?php echo ($cur_tab == 'message-box') ? "active" : "" ?>" href="<?php echo base_url('message-box'); ?>"><i class="fa fa-comments" aria-hidden="true"></i> Messenger</a>
                                <a class="<?php echo ($cur_tab == 'about') ? "active" : "" ?>" href="<?php echo base_url('about'); ?>"><i class="fa fa-info-circle" aria-hidden="true"></i> about</a> -->
                                <a class="" href="#sidebarMenu" title="" data-ripple="">more</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- top area -->
