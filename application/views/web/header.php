<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <title><?php echo (isset($title)) ? $title.' | '.$this->config->item('site_title') : $this->config->item('site_title'); ?></title>
    <link rel="icon" href="<?php echo  base_url('assets/images/fav.png'); ?>" type="image/png" sizes="16x16">
    <!-- <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"> -->
    <link rel="stylesheet" href="<?= base_url('assets/css/w3.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/main.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/color.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/Jcrop.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/mycss.css') ?>">
    
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/Jcrop.min.js') ?>"></script>
    
    <!-- Emoji picker -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="<?= base_url('assets/emoji/css/emoji.css') ?>" rel="stylesheet">
    <script src="<?= base_url('assets/emoji/js/config.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/util.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/jquery.emojiarea.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/emoji-picker.js') ?>"></script>

    <!-- mantion js -->
    <script src="<?= base_url('assets/mantion/jquery.atwho.min.js'); ?>"></script>
    <script src="<?= base_url('assets/mantion/jquery.caret.min.js'); ?>"></script>
</head>

<body>
<!--<div class="se-pre-con"></div>-->

<?php
$userID = $this->session->userdata('userID'); 
if(!empty($userID)){
   $userDetail =  $this->user->getUserdetails($userID);
}
?>
<div class="theme-layout">
    <div class="postoverlay"></div>
    <div class="responsive-header">
        <div class="mh-head first Sticky">
			<span class="mh-btns-left">
				<a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
			</span>
            <span class="mh-text">
				<a href="<?php echo base_url(); ?>" title="">Sayscape</a>
			</span>
        </div>
        <div class="mh-head second">
            <form class="mh-form">
                <input placeholder="search"/>
                <a href="#" class="fa fa-search"></a>
            </form>
        </div>
        <!-- <nav id="menu" class="res-menu">
            <ul class="naves">
                <li>
                    <i class="ti-clipboard"></i>
                    <a href="#" title="">News feed</a>
                </li>
                <li>
                    <i class="ti-user"></i>
                    <a href="#" title="">friends</a>
                </li>
                <li>
                    <i class="ti-image"></i>
                    <a href="#" title="">Post</a>
                </li>
                <li>
                    <i class="ti-comment"></i>
                    <a href="#" title="">Comments</a>
                </li>
                <li>
                    <i class="ti-video-clapper"></i>
                    <a href="#" title="">Media</a>
                </li>
                <li>
                    <i class="fa fa-group"></i>
                    <a href="#" title="">Groups</a>
                </li>
                <li>
                    <i class="ti-bell"></i>
                    <a href="#" title="">Notifications</a>
                </li>
                <li>
                    <i class="ti-share"></i>
                    <a href="#" title="">People Nearby</a>
                </li>
                <li>
                    <i class="ti-power-off"></i>
                    <a href="<?php echo base_url('home/logout'); ?>" title="">Logout</a>
                </li>
            </ul>
        </nav> -->
    </div>

    <div class="topbar stick">
        <div class="logo">
            <a title="" href="<?php echo base_url(); ?>"></a>
        </div>

        <div class="top-area">
            <ul class="main-menu setting-area">
                <li>
                    <a href="<?php echo base_url(); ?>"><h3>Sayscape</h3></a>
                </li>
            </ul>
            <ul class="setting-area">
                <li><a href="<?php echo base_url(); ?>" title="Home"><i class="ti-home"></i></a></li>
            </ul>
            <?php
            if(!empty($userID)){ ?>
            <div class="user-img">
                <?php
                // if(!empty($userDetail)){
                if(isset($userDetail['profileImage']) && $userDetail['profileImage']!=""){ ?>
                    <img src="<?php echo $userDetail['profileImage'] ?>" alt="" style="height:45px; width:45px;">
                <?php }else{ ?>
                    <img src="<?= base_url('assets/images/resources/admin.png') ?>" alt="">
                <?php }  //}
                ?>
                
                <span class="status f-online"></span>
                <div class="user-setting" style="margin-right:24px;">
                    <!-- <a href="#" title=""><span class="status f-online"></span>online</a>
                    <a href="#" title=""><span class="status f-away"></span>away</a>
                    <a href="#" title=""><span class="status f-off"></span>offline</a> -->
                    <a href="<?php echo base_url('time-line') ?>" title=""><i class="ti-user"></i> view profile</a>
                    <a href="<?php echo base_url('edit-profile') ?>" title=""><i class="ti-pencil-alt"></i>edit profile</a>
                    <a href="<?php echo base_url('changepassword') ?>" title=""><i class="fa fa-sign-out"></i>change password</a>
                    <!-- <a href="#" title=""><i class="ti-target"></i>activity log</a> -->
                    <!-- <a href="#" title=""><i class="ti-settings"></i>account setting</a> -->
                    <a href="<?php echo base_url('logout'); ?>" title=""><i class="ti-power-off"></i>log out</a>
                </div>
            </div>
            <?php }
            ?>
        </div>
    </div><!-- topbar -->


    