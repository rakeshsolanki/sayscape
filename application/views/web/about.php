<?php
include('header.php');
?>

<?php
include('toparea.php');

?>
<!-- top area -->
<section>
    <div class="gap gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php
                        //include('shortcuts.php');
                        ?>
                        <!-- sidebar -->
                        <div class="col-lg-12">
                            <div class="central-meta">
                                <div class="about">
                                    <div class="personal">
                                        <h5 class="f-title"><i class="ti-info-alt"></i><b>Personal Info</b></h5>
                                        <p><?php if (isset($userDetails['about']) && $userDetails['about'] != "") {
                                                echo $userDetails['about'];
                                            } else {
                                                echo "Bio is not available";
                                            } ?></p>
                                    </div>
                                    <div class="d-flex flex-row mt-2">

                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="basic">
                                                <ul class="basics">
                                                    <li><i class="ti-user"></i><?php if (isset($userDetails['username'])) {
                                                                                    echo $userDetails['username'];
                                                                                } ?></li>
                                                    <li><i class="ti-gift"></i><?php if (isset($userDetails['dob']) && $userDetails['dob'] != "") {
                                                                                    echo $userDetails['dob'];
                                                                                } else {
                                                                                    echo "-";
                                                                                } ?></li>
                                                    <li><i class="ti-map-alt"></i><?php if (isset($userDetails['location']) && $userDetails['location'] != "") {
                                                                                        echo 'live in' . $userDetails['location'];
                                                                                    } else {
                                                                                        echo "-";
                                                                                    } ?></li>
                                                    <li><i class="ti-mobile"></i><?php if (isset($userDetails['phone']) && $userDetails['phone']) {
                                                                                        echo $userDetails['phone'];
                                                                                    } else {
                                                                                        echo "-";
                                                                                    } ?></li>
                                                    <li><i class="ti-email"></i><a href="javascript:void(0)" class="__cf_email__" data-cfemail="760f1903041b171f1a36131b171f1a5815191b">[<?php if (isset($userDetails['email'])) {
                                                                                                                                                                                                echo $userDetails['email'];
                                                                                                                                                                                            } ?>]</a></li>
                                                    <li><i class="ti-world"></i><?php if (isset($userDetails['website']) && $userDetails['website']) { ?><a class="webcolor" target="_blank" href="https://<?php echo $userDetails['website']; ?>"><?php echo $userDetails['website']; ?></a> <?php } else {
                                                                                                                                                                                                                                                                                            echo "-";
                                                                                                                                                                                                                                                                                        } ?></li>
                                                </ul>
                                            </div>
                                            <div class="tab-pane fade" id="location" role="tabpanel">
                                                <div class="location-map">
                                                    <div id="map-canvas"></div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="work" role="tabpanel">
                                                <div>

                                                    <a href="#" title="">Envato</a>
                                                    <p>work as autohr in envato themeforest from 2013</p>
                                                    <ul class="education">
                                                        <li><i class="ti-facebook"></i> BSCS from Oxford University</li>
                                                        <li><i class="ti-twitter"></i> MSCS from Harvard Unversity</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="interest" role="tabpanel">
                                                <ul class="basics">
                                                    <li>Footbal</li>
                                                    <li>internet</li>
                                                    <li>photography</li>
                                                </ul>
                                            </div>
                                            <div class="tab-pane fade" id="lang" role="tabpanel">
                                                <ul class="basics">
                                                    <li>english</li>
                                                    <li>french</li>
                                                    <li>spanish</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- centerl meta -->

                        <?php
                        // include('followingList.php');
                        ?>
                        <!-- who's following -->

                        <!-- sidebar -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include('footer.php');
?>