<?php
include('header.php');
?>

    <?php
    include('toparea.php');
    ?>
    <!-- top area -->

	<section>
		<div class="gap gray-bg">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="row merged20" id="page-contents">
							<div class="col-lg-3">
								<aside class="sidebar static">
									<div class="widget">
											<h4 class="widget-title">Recent Photos</h4>
											<ul class="recent-photos">
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-11.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-1.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-22.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-2.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-33.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-3.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-44.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
													<img src="<?= base_url('assets/') ?>images/resources/recent-4.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-55.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-5.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-66.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-6.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-77.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-7.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-88.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-8.jpg" alt=""></a>
												</li>
												<li>
													<a class="strip" href="<?= base_url('assets/') ?>images/resources/recent-99.jpg" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
														<img src="<?= base_url('assets/') ?>images/resources/recent-9.jpg" alt=""></a>
												</li>
											</ul>
										</div><!-- recent photos-->
                                       
                                        <!-- shortcuts -->
                                        <?php
                                        include('shortcuts.php');
                                        ?>

								</aside>
                            </div><!-- sidebar -->

							<div class="col-lg-6">
								<div class="central-meta">
									<div class="editing-interest">
									<figure>

										<div class="notification-box">
											<ul id="people-list" class="friendz-list">
													<a href="<?php echo base_url('friends') ?>">
                                                    <li>
                                                        <figure>
                                                        <img src="<?= base_url('') ?>upload/notification/defaultNotificationImage.png" alt="" style="background-color:#5a0d28;">
                                                        </figure>
                                                        <div class="notifi-meta">
                                                            <p style="color:black;"><b>Follows requests</b></p>
                                                            <span>Approve or ignore request</span>
                                                        </div>
                                                    </li>
													</a>
											</ul>
										</div>

										<hr>
										<!-- <h5 class="f-title"><i class="ti-bell"></i>All Notifications </h5>
										<div class="notification-box">
											<ul id="people-list" class="friendz-list" style="max-height:600px;">
                                                <?php
                                                // if(!empty($notificationList)){
                                                //     foreach($notificationList as $notification){ ?>
                                                    <li>
                                                        <figure>
                                                        <img src="<?php //echo $notification['image'] ?>" alt="" style="background-color:#5a0d28;">
                                                        </figure>
                                                        <div class="notifi-meta">
                                                            <p><?php //echo $notification['text']; ?></p>
                                                            <span>30 mints ago</span>
                                                        </div>
                                                        <i class="del fa fa-close" style="color:red;"></i>
                                                    </li>
                                                    <?php //}
                                                //}
                                                ?>
											</ul>
										</div> -->
									</div>
								</div>	
                            </div><!-- centerl meta -->
                            
                            
							<div class="col-lg-3">
								<aside class="sidebar static">
                                        <!-- who's following -->
                                        <?php
                                        include('followingList.php');
                                        ?>

								</aside>
                            </div>
                            <!-- sidebar -->
						</div>	
					</div>
				</div>
			</div>
		</div>	
	</section>

<?php
include('footer.php');
?>