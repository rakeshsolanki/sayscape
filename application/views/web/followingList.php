<div class="col-lg-3">
<aside class="sidebar static">
<div class="widget stick-widget">
    <h4 class="widget-title">Who's follownig</h4>
    <div id="searchDirfollowing" class=""></div>
    <ul class="followers" id="people-listfollowing" style="margin-top:12px;">
        <?php
        if(!empty($followingName)){
            foreach($followingName as $Following){ ?>
            <li>
                <figure>
                    <?php
                    if(isset($Following['profileImage']) && $Following['profileImage']!=""){ ?>
                        <img src="<?php echo $Following['profileImage']; ?>" alt="" style="height:32px; width:32px;">
                    <?php }else{ ?>
                        <img src="http://softtual.com/sayscape/upload/userprofile/defaultProfile.png" alt="" style="height:32px; width:32px;">
                    <?php }
                    ?>
                </figure>
                <div class="friend-meta">
                    <h4><a href="<?php echo base_url('time-line/'.$Following['followerID']) ?>" title=""><?php echo $Following['username']; ?></a></h4>
                    <a href="javascript:void(0)" title="" class="underline">
                        <button class="btn" style="background-color:#5a0d28; color:white;">FOLLOW</button>
                    </a>
                </div>
            </li>
            <?php }
        }
        ?>

    </ul>
</div><!-- who's following -->
</aside>
</div>



<script>
    //===== Search Filter =====//
    (function ($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[':'].Contains = function (a, i, m) {
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };

        function listFilter(searchDirfollowing, list) {
            var form = $("<form>").attr({"class": "filterform", "action": "#"}),
                input = $("<input>").attr({
                    "class": "filterinputfollowing",
                    "type": "text",
                    "placeholder": "Search Following"
                });
            $(form).append(input).appendTo(searchDirfollowing);

            $(input).change(function () {
                    var filter = $(this).val();
                    if (filter) {
                        $(list).find("li:not(:Contains(" + filter + "))").slideUp();
                        $(list).find("li:Contains(" + filter + ")").slideDown();
                    } else {
                        $(list).find("li").slideDown();
                    }
                    return false;
                })
                .keyup(function () {
                    $(this).change();
                });
        }

    //search friends widget
        $(function () {
            listFilter($("#searchDirfollowing"), $("#people-listfollowing"));
        });
    }(jQuery));
    </script>
