<div class="central-meta new-pst" id="posthide">
    <div class="new-postbox">
        <figure>
            <?php
            if (isset($userDetailsPost['profileImage']) && $userDetailsPost['profileImage'] != "") { ?>
                <img src="<?= $userDetailsPost['profileImage'] ?>" alt="" class="hw52">
            <?php } else { ?>
                <img src="<?= base_url('assets/images/resources/admin2.png') ?>" alt="">
            <?php } ?>
        </figure>

        <div class="newpst-input">
            <!-- divEmojiPicker -->
            <form action="<?php echo base_url('home/createPost') ?>" method="post" enctype="multipart/form-data">
                <p class="lead emoji-picker-container">
                    <textarea rows="2" id="postTextarea" placeholder="write something" name="text" class="" data-emojiable="true" data-emoji-input="unicode"></textarea>
                </p>
                <div class="attachments">
                    <ul class="cursor-point">
                        <li id="emoji">
                            <i class="fa fa-smile-o"></i>
                            <label class="fileContainer"> </label>
                        </li>
                        <li id="image">
                            <i class="fa fa-camera"></i>
                            <label class="fileContainer">
                                <input type="file" name="picture[]" id="files" multiple accept="image/*" onchange="readFile(this);">
                            </label>
                        </li>
                        <li id="video">
                            <i class="fa fa-video-camera"></i>
                            <label class="fileContainer">
                                <input type="file" name="video" id="vide" multiple accept="video/*">
                            </label>
                        </li>
                        <li>
                            <i class="fa fa-pie-chart" id="pollhide" onclick="document.getElementById('id01').style.display='block'"></i>
                            <label class="fileContainer">
                            </label>
                        </li>
                        <li id="">
                            <i class="" id="" onclick="document.getElementById('id03').style.display='block'"><b>GIF</b></i>
                        </li>
                        <li>
                            <input type="hidden" id="gif" name="gif" value="" />
                            <input type="submit" class="btn webbgcolor text-white" name="post" value="POST" id="normal">
                            <input type="submit" class="btn webbgcolor text-white" name="video" value="POST" id=videopost>
                            <input type="submit" class="btn webbgcolor text-white" name="image" value="POST" id="imagepost">
                            <!-- <button title="send"><i class="fa fa-paper-plane"></i></button> -->
                        </li>
                    </ul>
                </div>
                <div class="mt8">
                    <b>Expires On</b>
                    <input type="date" class="form-control" name="expiryDateTime">
                </div>
            </form>

            <div class="row">
                <div class="col-md-4">
                    <div id="sh"></div>
                </div>
            </div>
        </div>
        <video controls class="postvideo mt10" id="vibox">
            <source type="video/mp4">
            <canvas class="postvideo mt10" id="thumb" style="display:block;"></canvas>
        </video>
        <!-- <div id="status"></div> -->
        <div id="photos" class="row"></div>
        <div id="postGifView" class="row" style="display: none"></div>
    </div>

    <div id="id01" class="w3-modal">
        <div class="w3-modal-content">
            <form method="post" action="<?php echo base_url('home/poll'); ?>">
                <header class="w3-container text-white webbgcolor">
                    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <h3><i class="fa fa-pie-chart"></i> Poll </h3>
                </header>
                <div class="container">
                    <b class="text-black">Ask Question:</b> <input type="text" name="question" class="form-control" required>
                    <div class="input_fields_wrap">
                        <button class="add_field_button mt10 btn-sm">Add More Option</button>
                        <div><input type="text" class="form-control mt10" name="option[]" placeholder="Enter option"></div>
                    </div>
                    <input class="btn btn-success mt10" type="submit" name="poll" value="Save">
                </div>
            </form>
        </div>
    </div>

    <div id="id03" class="w3-modal">
        <div class="w3-modal-content">
            <header class="w3-container text-white webbgcolor">
                <span onclick="document.getElementById('id03').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                <h3><i class="fa fa-image"></i> Send GIF </h3>
            </header>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="buttons" style="display: none"></div>
                        <div class="search-bar">
                            <input type="text" name="query" class="form-control" id="user-search" placeholder="Search GIF" />
                            <div class="mt20">
                                <input id="submit" type="button" value="Search" class="btn btn-sm btn-success btnSearchGif" />
                                <input type="button" class="trending btn btn-sm btn-danger btnSearchGif" value="See what's trending" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt20 mb">
                    <div id="gifresults" class="row"></div>
                </div>
                <div class="">
                    <input class="btn btn-success" id="btnUploadGif" type="button" name="uploadGif" value="Send" onclick="document.getElementById('id03').style.display='none'" />
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    function readFile(input) {
        $("#status").html('Processing...');
        counter = input.files.length;
        for (x = 0; x < counter; x++) {
            if (input.files && input.files[x]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + e.target.result + '" class="img-thumbnail"></div>');
                };
                reader.readAsDataURL(input.files[x]);
            }
        }
        if (counter == x) {
            $("#status").html('');
        }
    }
</script>

<script>
    $('#vibox').hide();
    $('#vide').click(function() {
        $('#photos').hide();
    });
    $('#videopost').hide();
    $('#imagepost').hide();
    $('#vide').click(function() {
        $('#videopost').show();
        $('#normal').hide();
        $('#imagepost').hide();
    });
    $('#videopost').hide();
    $('#imagepost').hide();
    $('#files').click(function() {
        $('#imagepost').show();
        $('#videopost').hide();
        $('#normal').hide();
    });

    $('#pollhide').click(function() {
        $('#vibox').hide();
    });
    $('#files').click(function() {
        $('#vibox').hide();
    });
</script>

<script>
    $(function() {
        var video = $("video");
        var thumbnail = $("#thumb");
        var input = $("#vide");
        var ctx = thumbnail.get(0).getContext("2d");
        var duration = 0;
        var img = $("#thumb");

        input.on("change", function(e) {
            var file = e.target.files[0];
            // Validate video file type
            if (["video/mp4"].indexOf(file.type) === -1) {
                alert("Only 'MP4' video format allowed.");
                return;
            }
            // Set video source
            video.find("source").attr("src", URL.createObjectURL(file));
            // Load the video
            video.get(0).load();

            // Load metadata of the video to get video duration and dimensions
            video.on("loadedmetadata", function(e) {
                duration = video.get(0).duration;
                // Set canvas dimensions same as video dimensions
                thumbnail[0].width = video[0].videoWidth;
                thumbnail[0].height = video[0].videoHeight;
                // Set video current time to get some random image
                video[0].currentTime = Math.ceil(duration / 2);
                // Draw the base-64 encoded image data when the time updates
                video.one("timeupdate", function() {
                    ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                    img.attr("src", thumbnail[0].toDataURL());
                });
            });
            $('#vibox').show();
        });
    });
    
    $(function() {
        $('.emoji-picker').hide();
        $('#emoji').click(function() {
            $('.emoji-picker').click();
        });
    });
    ;(function($, window, document, undefined) {
        window.emojiPicker = new EmojiPicker({
            emojiable_selector: '[data-emojiable=true]',
            assetsPath: '<?= base_url('assets/emoji/img/') ?>/',
            popupButtonClasses: 'fa fa-smile-o'
        });
        window.emojiPicker.discover();

        // mansion js
        var mantionUsers = <?= $this->user->getUsernameJson(); ?>;
        $('.emoji-wysiwyg-editor').atwho({
            at: "@",
            data: mantionUsers
        });
        
    })(jQuery, window, document);

    // for GIF script
    var topics = ["Cool", "Meme's", "Funny", "Space", "Laugh"];

    function addSearchBtns() {
        $("#buttons").html("");
        for (i = 0; i < topics.length; i++) {
            var $button = $("<input type='button' class='btn btn-sm search-btn' />");
            $button.val(topics[i]);
            $("#buttons").append($button);
        }
    }
    addSearchBtns();
    $(document).on("click", ".btnSearchGif", function() {
        $("#gifresults").html("");
        // Beginning API call
        var queryURL = "https://api.giphy.com/v1/gifs/search?";
        var query;
        var params = {
            q: query,
            limit: 6,
            api_key: "e5frANRyrhZN0icIH2vlPlB7cYx68knL",
            fmt: "json"
        };
        if ($(this).hasClass("search-btn")) {
            query = $(this).val();
        } else if ($("#user-search").val() !== "") {
            query = $("#user-search").val();
            topics.push(query);
            if (topics.length > 6) {
                topics.shift();
            }
            addSearchBtns();
        }
        params.q = query;

        if ($(this).hasClass("trending")) {
            queryURL = "https://api.giphy.com/v1/gifs/trending?";
            delete params.q;
        }
        $.ajax({
            url: queryURL + $.param(params),
            method: "GET",
            success: function(r) {
                for (i = 0; i < params.limit; i++) {
                    var $img = $("<img>");
                    var $div = $("<div>");
                    var $rating = $("<h6>");
                    var gifObj = r.data[i];
                    var gif = gifObj.images;
                    console.log(gif);

                    // Image builder object
                    $img.attr({
                        // "width": "200px",
                        src: gif.fixed_height_still.url,
                        "data-animate": gif.fixed_height.url,
                        "data-still": gif.fixed_height_still.url,
                        "data-state": "still",
                        "data-src": gif.original.url,
                        // src: gif.original.url,
                        class: "newgif"
                    });
                    // $div.attr("id", "gif-" + i);
                    $div.addClass("gif-box col-sm-4");
                    // $rating.text("Rating: " + gifObj.rating);
                    $div.append($img, $rating);
                    $("#gifresults").append($div);
                }

                $(".newgif").on("click", function() {
                    var getGifUrl = $(this).attr("data-src");
                    $('#gifresults .gif-box .gifBorder').removeClass('gifBorder');
                    $(this).addClass('gifBorder');
                    var gifArr = getGifUrl.split('?cid=');
                    if (gifArr[0]) {
                        $('#gif').val(gifArr[0]);
                        $("#postGifView").show();
                        $("#postGifView").html('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + gifArr[0] + '" class="img-thumbnail"></div>');
                    }

                    var state = $(this).attr("data-state");
                    var setgif = $(this).attr("src");
                    // $('#gif').val(setgif);
                    if (state === "still") {
                        $(this).attr("src", $(this).attr("data-animate"));
                        $(this).attr("data-state", "animate");
                    } else {
                        $(this).attr("src", $(this).attr("data-still"));
                        $(this).attr("data-state", "still");
                    }
                });
            }
        });
    });
</script>
<?php //exit(); 
?>