<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <title>Sayscape Social Network</title>
    <link rel="icon" href="<?php echo  base_url('assets/images/fav.png'); ?>" type="image/png" sizes="16x16">

    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/main.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/style.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/color.css">
    <link rel="stylesheet" href="<?= base_url('assets/') ?>css/responsive.css">

</head>
<body>
<div class="theme-layout">
    <div class="container-fluid pdng0">
        <div class="row merged">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="land-featurearea">
                    <div class="land-meta">
                        <h1>Sayscape</h1>
                        <p>
                            Sayscape is free to use for as long as you want with two active projects.
                        </p>
                        <div class="friend-logo">
                            <span><img src="<?= base_url('assets/') ?>images/logo.png" alt=""></span>
                        </div>
                        <a href="#" title="" class="folow-me">Follow Us on</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="login-reg-bg">
                    <div class="log-reg-area sign">
                        <h2 class="log-title">Login</h2>
                        <p>
                            Don’t use Sayscape Yet? <a href="#" title="">Take the tour</a> or <a href="#" title="">Join
                            now</a>
                        </p>

                        <form action="<?php echo base_url('login'); ?>" method="post">
                            <div class="form-group">
                                <input type="text" id="input" name="email"/>
                                <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password"/>
                                <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked="checked"/><i class="check-box"></i>Always Remember
                                    Me.
                                </label>
                            </div>
                            <a href="#" title="" class="forgot-pwd">Forgot Password?</a>
                            <p class="text-danger"><?=$this->session->flashdata('success')?></p>
                            <div class="submit-btns">
                                <!-- <button class="mtr-btn signin" name="login" type="button"><span>Login</span></button> -->
                                <input type="submit" class="btn signin" name="login" value="Login" style="background-color:#5a0d28; color:white">
                                <button class="mtr-btn signup" type="button"><span>Register</span></button>
                               
                            </div>
                        </form>


                    </div>
                    <div class="log-reg-area reg">
                        <h2 class="log-title">Register</h2>
                        <p>
                            Don’t use Sayscape Yet? <a href="#" title="">Take the tour</a> or <a href="#" title="">Join
                            now</a>
                        </p>
                        <form action="<?php echo base_url('home/register') ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" required="required" name="name"/>
                                <label class="control-label" for="input">Display Name</label><i
                                    class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                <input type="text" required="required" name="username"/>
                                <label class="control-label" for="input">User Name</label><i class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                    <b>Profile</b> <input type="file" required="required" name="picture"/>
                                <!-- <label class="control-label" for="input">Profile</label><i class="mtrl-select"></i> -->
                            </div>
                            <div class="form-group"> 
                                <input type="email" required="required" name="email"/>
                                <label class="control-label" for="input">Email</label><i class="mtrl-select"></i>
                            </div>
                            <div class="form-group">
                                <input type="password" required="required" name="password"/>
                                <label class="control-label" for="input">Password</label><i class="mtrl-select"></i>
                            </div>
                            <!-- <div class="form-radio">
                                <div class="radio">
                                    <label> 
                                        <input type="radio" name="radio" checked="checked"/><i class="check-box"></i>Male
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="radio"/><i class="check-box"></i>Female
                                    </label>
                                </div>
                            </div> -->
                            <!-- <div class="form-group">
                                <input type="text" required="required"/>
                                <label class="control-label" for="input"><a
                                        href="https://wpkixx.com/cdn-cgi/l/email-protection" class="__cf_email__"
                                        data-cfemail="a0e5cdc1c9cce0">[email&#160;protected]</a></label><i
                                    class="mtrl-select"></i>
                            </div> -->
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" checked="checked"/><i class="check-box"></i>Accept Terms &
                                    Conditions ?
                                </label>
                            </div>
                            <a href="#" title="" class="already-have">Already have an account</a>
                            <div class="submit-btns">
                                <!-- <button class="mtr-btn signup" type="button"><span>Register</span></button> -->
                                <input type="submit" class="btn signup" name="register" value="Register" style="background-color:#5a0d28; color:white">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
<script src="<?= base_url('assets/') ?>js/main.min.js"></script>
<script src="<?= base_url('assets/') ?>js/script.js"></script>

</body>

<!-- Mirrored from wpkixx.com/html/Sayscape/landing.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Sep 2019 12:28:11 GMT -->
</html>