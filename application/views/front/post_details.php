<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php include('sidebar_menu.php'); ?>
                        </div>
                        <!-- sidebar -->
                        <div class="col-lg-6">
                            <div class="loadMore">
                                <?php
                                if(!empty($postList)){
                                    $count = 1;
                                    $datas['post'] = $postList;
                                    $datas['count'] = $count;
                                    $this->load->view('front/post_view',$datas);
                                    $count ++;
                                } ?>
                            </div>
                        </div><!-- centerl meta -->
                        <div class="col-lg-3">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>