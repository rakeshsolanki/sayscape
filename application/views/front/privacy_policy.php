<section class="privacy">
	<div class="page-header">
		<div class="header-inner">
			<h2>Privacy & Policy</h2>
		</div>
		<figure><img src="<?= base_url('assets/'); ?>images/resources/baner-badges.png" alt=""></figure>
	</div>
</section>
<style>
.privacy-meta p{
	letter-spacing: unset;
	line-height: unset;
	text-align: justify;
}
</style>
<section>
	<div class="gap gray-bg">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="privacy">
						<div class="d-flex flex-row mt-2">
							<ul class="policy nav nav-tabs nav-tabs--vertical nav-tabs--left">
								<li class="nav-item">
									<a href="#terms" class="nav-link active" data-toggle="tab">Terms Of Services</a>
								</li>
								<li class="nav-item">
									<a href="#privecy" class="nav-link" data-toggle="tab">Privacy Policy</a>
								</li>
							</ul>
							<div class="tab-content central-meta">
								<div class="tab-pane fade show active" id="terms">
									<div class="privacy-meta">
										<div class="set-title">
											<h5>Terms Policy</h5>
										</div>
										<div>
											<h6>1. Who Can Use SayScape</h6>
											<p>You may use the Services only if you agree to form a binding contract with SayScape and are
											not a person barred from receiving services under the laws of the applicable jurisdiction. In any
											case, you must be at least 17 years old to use the Services. If you are accepting these Terms
											and using the Services on behalf of a company, organization, government, or other legal entity,
											you represent and warrant that you are authorized to do so and have the authority to bind such
											entity to these Terms, in which case the words “you” and “your” as used in these Terms shall
											refer to such entity.</p>

											<h6>2. Privacy</h6>
											<p>Our Privacy Policy describes how we handle the information you provide to us when you use
											our Services. You understand that through your use of the Services you consent to the
											collection and use (as set forth in the Privacy Policy) of this information, including the transfer of
											this information to the United States, for storage, processing and use by SayScape.</p>
											
											<h6>3. Content on the Services</h6>
											<p>You are responsible for your use of the Services and for any Content you provide, including
											compliance with applicable laws, rules, and regulations. You should only provide Content that
											you are comfortable sharing with others.</p>
											<p>Any use or reliance on any Content or materials posted via the Services or obtained by you
											through the Services is at your own risk. We do not endorse, support, represent or guarantee
											the completeness, truthfulness, accuracy, or reliability of any Content or communications posted
											via the Services or endorse any opinions expressed via the Services. You understand that by
											using the Services, you may be exposed to Content that might be offensive, harmful, inaccurate
											or otherwise inappropriate, or in some cases, postings that have been mislabeled or are
											otherwise deceptive. All Content is the sole responsibility of the person who originated such
											Content.</p>

											<p>We may not monitor or control the Content posted via the Services and, we cannot take
											responsibility for such Content. We reserve the right to remove Content that violates the User
											Agreement, including for example, copyright or trademark violations, impersonation, unlawful
											conduct.</p>

											<p>Your Rights and Grant of Rights in the Content
											You retain your rights to any Content you submit, post or display on or through the Services.
											What’s yours is yours — you own your Content (and your incorporated audio, photos and videos
											are considered part of the Content).</p>
											
											<p>By submitting, posting or displaying Content on or through the Services, you grant us a
											worldwide, non-exclusive, royalty-free license (with the right to sublicense) to use, copy,
											reproduce, process, adapt, modify, publish, transmit, display and distribute such Content in any
											and all media or distribution methods (now known or later developed). This license authorizes
											us to make your Content available to the rest of the world and to let others do the same. You
											agree that this license includes the right for SayScape to provide, promote, and improve the
											Services and to make Content submitted to or through the Services available to other
											companies, organizations or individuals for the syndication, broadcast, distribution, promotion or
											publication of such Content on other media and services, subject to our terms and conditions for
											such Content use. Such additional uses by SayScape, or other companies, organizations or
											individuals, may be made with no compensation paid to you with respect to the Content that you
											submit, post, transmit or otherwise make available through the Services. SayScape has an
											evolving set of rules for how ecosystem partners can interact with your Content on the Services.
											These rules exist to enable an open ecosystem with your rights in mind. You understand that we
											may modify or adapt your Content as it is distributed, syndicated, published, or broadcast by us
											and/or make changes to your Content in order to adapt the Content to different media.
											You represent and warrant that you have, or have obtained, all rights, licenses, consents,
											permissions, power and/or authority necessary to grant the rights granted herein for any
											Content that you submit, post or display on or through the Services. You agree that such
											Content will not contain material subject to copyright or other proprietary rights, unless you have
											necessary permission or are otherwise legally entitled to post the material and to grant
											SayScape the license described above.</p>

											<h6>4. Using the Services</h6>
											<p>Please review the SayScape Rules, which are part of the User Agreement and outline what is
											prohibited on the Services. You may use the Services only in compliance with these Terms and
											all applicable laws, rules and regulations.</p>

											<p>Our Services evolve constantly. As such, the Services may change from time to time, at our
											discretion. We may stop (permanently or temporarily) providing the Services or any features
											within the Services to you or to users generally. We also retain the right to create limits on use
											and storage at our sole discretion at any time. We may also remove or refuse to distribute any
											Content on the Services, suspend or terminate users, and reclaim usernames without liability to
											you.</p>

											<p>In consideration for SayScape granting you access to and use of the Services, you agree that
											SayScape and its third-party providers and partners may place advertising on the Services or in
											connection with the display of Content or information from the Services whether submitted by
											you or others. You also agree not to misuse our Services, for example, by interfering with them
											or accessing them using a method other than the interface and the instructions that we provide.
											You may not do any of the following while accessing or using the Services: (i) access, tamper
											with, or use non-public areas of the Services, SayScape’s computer systems, or the technical
											delivery systems of SayScape’s providers; (ii) probe, scan, or test the vulnerability of any
											system or network or breach or circumvent any security or authentication measures; (iii) access
											or search or attempt to access or search the Services by any means (automated or otherwise)
											other than through our currently available, published interfaces that are provided by SayScape
											(and only pursuant to the applicable terms and conditions), unless you have been specifically
											allowed to do so in a separate agreement with SayScape. Scraping the Services without the
											prior consent of SayScape is expressly prohibited; (iv) forge any TCP/IP packet header or any
											part of the header information in any email or posting, or in any way use the Services to send
											altered, deceptive or false source-identifying information; or (v) interfere with, or disrupt, (or
											attempt to do so), the access of any user, host or network, including, without limitation, sending
											a virus, overloading, flooding, spamming, mail-bombing the Services, or by scripting the creation
											of Content in such a manner as to interfere with or create an undue burden on the Services. We
											also reserve the right to access, read, preserve, and disclose any information as we reasonably
											believe is necessary to (i) satisfy any applicable law, regulation, legal process or governmental
											request, (ii) enforce the Terms, including investigation of potential violations hereof, (iii) detect,
											prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support
											requests, or (v) protect the rights, property or safety of SayScape, its users and the public.
											SayScape does not disclose personally-identifying information to third parties except in
											accordance with our Privacy Policy.</p>

											<h6>Your Account</h6>
											<p>You need to create an account to use our Services. You are responsible for safeguarding your
											account, so use a strong password and limit its use to this account. We cannot and will not be
											liable for any loss or damage arising from your failure to comply with the above. You can control
											most communications from the Services. We may need to provide you with certain
											communications, such as service announcements and administrative messages. These
											communications are considered part of the Services and your account, and you may not be able
											to opt-out from receiving them. If you added your phone number to your account and you later
											change or deactivate that phone number, you must update your account information to help
											prevent us from communicating with anyone who acquires your old number.

											<h6>Your License to Use the Services</h6>
											<p>SayScape gives you a personal, worldwide, royalty-free, non-assignable and non-exclusive
											license to use the software provided to you as part of the Services. This license has the sole
											purpose of enabling you to use and enjoy the benefit of the Services as provided by SayScape,
											in the manner permitted by these Terms.</p>
											<p>The Services are protected by copyright, trademark, and other laws of both the United States
											and foreign countries. Nothing in the Terms gives you a right to use the SayScape name or any
											of the SayScape trademarks, logos, domain names, and other distinctive brand features. All
											right, title, and interest in and to the Services (excluding Content provided by users) are and will
											remain the exclusive property of SayScape and its licensors. Any feedback, comments, or
											suggestions you may provide regarding SayScape, or the Services is entirely voluntary and we
											will be free to use such feedback, comments or suggestions as we see fit and without any
											obligation to you.</p>

											<h6>Ending These Terms</h6>
											<p>You may end your legal agreement with SayScape at any time by deactivating your accounts
											and discontinuing your use of the Services. See the Privacy Policy for more information on what
											happens to your information.</p>
											
											<p>We may suspend or terminate your account or cease providing you with all or part of the
											Services at any time for any or no reason, including, but not limited to, if we reasonably believe:
											(i) you have violated these Terms or the SayScape, (ii) you create risk or possible legal
											exposure for us; or (iii) your account should be removed due to prolonged inactivity. We will
											make reasonable efforts to notify you by the email address associated with your account or the
											next time you attempt to access your account, depending on the circumstances. In all such
											cases, the Terms shall terminate, including, without limitation, your license to use the Services,
											except that the following sections shall continue to apply: II, III, V, and VI.</p>

											<h6>5. Disclaimers and Limitations of Liability</h6>
											<p>The Services are Available "AS-IS" Your access to and use of the Services or any Content are
											at your own risk. You understand and agree that the Services are provided to you on an “AS IS”
											and “AS AVAILABLE” basis. The “Colossal Entities” refers to SayScape, its parents, affiliates,
											related companies, officers, directors, employees, agents, representatives, partners, and
											licensors. Without limiting the foregoing, to the maximum extent permitted under applicable law,
											THE COLOSSAL ENTITIES DISCLAIM ALL WARRANTIES AND CONDITIONS, WHETHER
											EXPRESS OR IMPLIED, OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE,
											OR NON-INFRINGEMENT. The Colossal Entities make no warranty or representation and
											disclaim all responsibility and liability for: (i) the completeness, accuracy, availability, timeliness,
											security or reliability of the Services or any Content; (ii) any harm to your computer system, loss
											of data, or other harm that results from your access to or use of the Services or any Content; (iii)
											the deletion of, or the failure to store or to transmit, any Content and other communications
											maintained by the Services; and (iv) whether the Services will meet your requirements or be
											available on an uninterrupted, secure, or error-free basis. No advice or information, whether oral
											or written, obtained from the Colossal Entities or through the Services, will create any warranty
											or representation not expressly made herein.</p>
											
											<p>Limitation of Liability TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE
											COLOSSAL ENTITIES SHALL NOT BE LIABLE FOR ANY INDIRECT, INCIDENTAL, SPECIAL,
											CONSEQUENTIAL OR PUNITIVE DAMAGES, OR ANY LOSS OF PROFITS OR REVENUES,
											WHETHER INCURRED DIRECTLY OR INDIRECTLY, OR ANY LOSS OF DATA, USE,
											GOODWILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM (i) YOUR ACCESS TO
											OR USE OF OR INABILITY TO ACCESS OR USE THE SERVICES; (ii) ANY CONDUCT OR
											CONTENT OF ANY THIRD PARTY ON THE SERVICES, INCLUDING WITHOUT LIMITATION,
											ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF OTHER USERS OR THIRD
											PARTIES; (iii) ANY CONTENT OBTAINED FROM THE SERVICES; OR (iv) UNAUTHORIZED
											ACCESS, USE OR ALTERATION OF YOUR TRANSMISSIONS OR CONTENT. IN NO EVENT
											SHALL THE AGGREGATE LIABILITY OF THE COLOSSAL ENTITIES EXCEED THE
											GREATER OF ONE HUNDRED U.S. DOLLARS (U.S. $100.00) OR THE AMOUNT YOU PAID
											COLOSSAL, IF ANY, IN THE PAST SIX MONTHS FOR THE SERVICES GIVING RISE TO
											THE CLAIM. THE LIMITATIONS OF THIS SUBSECTION SHALL APPLY TO ANY THEORY OF
											LIABILITY, WHETHER BASED ON WARRANTY, CONTRACT, STATUTE, TORT (INCLUDING
											NEGLIGENCE) OR OTHERWISE, AND WHETHER OR NOT THE COLOSSAL ENTITIES
											HAVE BEEN INFORMED OF THE POSSIBILITY OF ANY SUCH DAMAGE, AND EVEN IF A
											REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.

											<h6>6. General</h6>
											<p>We may revise these Terms from time to time. The changes will not be retroactive, and the most
											current version of the Terms, which will always be at sayscape.com/tos, will govern our
											relationship with you. We will try to notify you of material revisions, for example via a service
											notification or an email to the email associated with your account. By continuing to access or
											use the Services after those revisions become effective, you agree to be bound by the revised
											Terms. The laws of the Commonwealth of Massachusetts, excluding its choice of law
											provisions, will govern these Terms and any dispute that arises between you and SayScape. All
											disputes related to these Terms or the Services will be brought solely in the federal or state
											courts located in Barnstable County, Massachusetts, United States, and you consent to
											personal jurisdiction and waive any objection as to inconvenient forum. If you are a federal,
											state, or local government entity in the United States using the Services in your official capacity
											and legally unable to accept the controlling law, jurisdiction or venue clauses above, then those
											clauses do not apply to you. For such U.S. federal government entities, these Terms and any
											action related thereto will be governed by the laws of the United States of America (without
											reference to conflict of laws) and, in the absence of federal law and to the extent permitted
											under federal law, the laws of the Commonwealth of Massachusetts (excluding choice of law).
											In the event that any provision of these Terms is held to be invalid or unenforceable, then that
											provision will be limited or eliminated to the minimum extent necessary, and the remaining
											provisions of these Terms will remain in full force and effect. SayScape’s failure to enforce any
											right or provision of these Terms will not be deemed a waiver of such right or provision. These
											Terms are an agreement between you and Colossal, Inc., 66 Hamblins Hayway, MA, 02648
											U.S.A. If you have any questions about these Terms, please contact us. Effective: April 12th,
											2019</p>
										</div>
									</div>
								</div><!-- general setting -->

								<div class="tab-pane fade" id="privecy" role="tabpanel">
									<div class="set-title">
										<h5>Privacy & Policy</h5>
									</div>
									<div class="privacy-meta">
										<h6>1. Basic Account Information</h6>
										<p>You do have to create an account to use SayScape service and features. If you do choose to
										create an account, you must provide us with some personal data so that we can provide our
										services to you. On SayScape this includes a display name (for example, “John Doe”), a
										username (for example, @JohnDoe), a password, and an email address or phone number. Your
										display name and username are always public, but you can use either your real name or a
										pseudonym. You can also create and manage multiple SayScape accounts if you wish</p>
										
										<h6>2. Public Information</h6>
										<p>Most activity on SayScape is public, including your profile information, your posts and certain
										information about your posts like the date, and time. You also may choose to publish your
										location and birth-date in your SayScape profile. The people you follow and who follow you, and
										posts you Like or Quote are also public. Other people may tag you in a photo or mention you in
										a post. You are responsible for your posts and other information you provide through our
										services, and you should think carefully about what you make public, especially if it is sensitive
										information. If you update your public information on SayScape, such as by deleting a post or
										deactivating your account, we will reflect your updated content on SayScape.com, SayScape for
										iOS, and SayScape for Android. At no time will Colossal Inc (SayScape) sell for profit any
										information or willfully transfer any Personally Identifiable Information and/or content unrelated
										to your public profile.</p>
										
										<h6>3. Contact Information</h6>
										<p>We use your contact information, such as your email address or phone number, to authenticate
										your account and keep it - and our services - secure, and to help prevent spam, fraud, and
										abuse. We also use contact information to personalize our services, enable certain account
										features (for example, for login verification or SayScape via SMS), and to send you information
										about our services. If you provide us with your phone number, you agree to receive text
										messages from SayScape to that number as your country’s laws allow. SayScape also uses
										your contact information to help others find your account if your settings permit. You can use
										your settings for email and mobile notifications to control notifications you receive from
										SayScape. You can also unsubscribe from a notification.</p>

										<h6>4. Direct Messages and Non-Public Communications</h6>
										<p>We provide certain features that let you communicate more privately or control who sees your
										content. For example, you can use Direct Messages to have non-public conversations on
										SayScape. When you communicate with others by sending or receiving Direct Messages, we
										will store and process your communications and information related to them. We use
										information about whom you have communicated with and when (but not the content of those
										communications) to better understand the use of our services, to protect the safety and integrity
										of our platform. We only share the content of your Direct Messages with the people you’ve sent
										them to. Note that if you interact in a way that would ordinarily be public with SayScape content
										shared with you via Direct Message, for instance by liking a post, those interactions will be
										public. When you use features like Direct Messages to communicate, remember that recipients
										have their own copy of your communications on SayScape - even if you delete your copy of
										those messages from your account - which they may duplicate, store, or re-share.</p>
										
										<h6>5. Private Verified Veteran Feed</h6>
										<p>SayScape contains a separate feed which remains private from the rest of the public areas to
										include, Home Feed, Profiles, and Groups. All posts created within this feed will not be visible to
										anyone while viewing from outside the feed or has not been granted access. Users must be
										verified through SayScape as members who are active and or have veteran status before being
										granted access. Although we verify veteran status, we cannot guarantee that manipulation has
										not occurred and falsification of status has allowed them access.</p>

										<h6>6. Private Verified FirstResponders Feed</h6>
										<p>SayScape contains a separate feed which remains private from the rest of the public areas to
										include, Home Feed, Profiles, and Groups. All posts created within this feed will not be visible to
										anyone while viewing from outside the feed or has not been granted access. Users must be
										verified through SayScape as members who are active and or have been verified as a first
										responder before being granted access. Although we verify status, we cannot guarantee that
										manipulation has not occurred and falsification of status has allowed them access.</p>
										
										<h6>7. Payment Information</h6>
										<p>In the future, you may provide us with payment information, including your credit or debit card
										number, card expiration date, CVV code, and billing address, in order to purchase offerings
										provided as part of our services. (Not offered at this time)</p>

										<h6>8. How You Control the Information</h6>
										<p>You Share with Us Your Privacy and safety settings let you decide: Whether you will be able to
										receive Direct Messages from anyone on SayScape What content containing keywords or
										hashtags you may choose to not see on SayScape Whether you want to block other SayScape
										accounts.</p>
									</div>
								</div><!-- notification -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>