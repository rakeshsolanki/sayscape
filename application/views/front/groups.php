<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?><!-- user profile banner -->
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-9">
                            <div class="central-meta">
                                <div class="title-block">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="align-left">
                                                <h5>Groups <span><?= count($grouplist); ?></span></h5>
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-6">
                                            <div class="row merged20">
                                                <div class="col-lg-10 col-md-10 col-sm-10">
                                                    <form class="filterform" method="post">
                                                        <input class="filterinputgroup"  type="text" placeholder="Search Group">
                                                        <button type="submit"><i class="fa fa-search"></i></button>
                                                    </form>
                                                </div>
                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                    <div class="option-list">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                        <ul>
                                                            <li><a title="" href="#">Show Friends Public</a></li>
                                                            <li><a title="" href="#">Show Friends Private</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div><!-- title block -->
                            <div class="central-meta padding30">
                                <div class="row">
                                    <div class="col-lg-3 col-md-4 col-sm-4">
                                        <div class="addgroup">
                                            <div class="item-upload">
                                                <i class="fa fa-plus-circle"></i>
                                                <div class="upload-meta">
                                                    <h5>Create Group</h5>
                                                    <span>It only takes a few minutes!</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if (!empty($grouplist)) {
                                        foreach ($grouplist as $group) { ?>
                                            <div class="col-lg-3 col-md-4 col-sm-4">
                                                <div class="group-box">
                                                    <figure>
                                                        <?php
                                                        if (isset($group['groupProfile']) && $group['groupProfile'] != "") { ?>
                                                            <img class="wh120" src="<?php echo $group['groupProfile']; ?>" alt="">
                                                        <?php } else { ?>
                                                            <img class="" src="<?= base_url('assets/'); ?>images/resources/group3.jpg" alt="">
                                                        <?php } ?>
                                                    </figure>
                                                    <a href="<?php echo base_url('groupDetails/' . $group['groupID']); ?>" title="Chat this group"><?php echo $this->emoji->Decode($group['groupName']); ?></a>
                                                    <!-- <a href="<?php // echo base_url('group-message/' . $group['groupID']); ?>" title="Chat this group"><?php // echo $group['groupName'] ?></a> -->
                                                    <span><?= $group['totalmembers'] ?> Members</span>
                                                    <a class="memberlist" href="<?= base_url('groupDetails/'.$group['groupID']); ?>"><span> Details </span></a>
                                                    <button><a href="javascript:void(0)"  class="delete_data" id="<?php echo $group['groupID']; ?>" onclick="remove_cart()">Delete Group</a></button>
                                                </div>
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                    <!-- <span>Viewing 1-11 of 302 Groups</span>-->
                                <!-- <div class="lodmore">
                                    <button class="btn-view btn-load-more"></button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="popup-wraper4">
    <div class="popup creat-group">
        <span class="popup-closed"><i class="ti-close"></i></span>
        <div class="popup-meta">
            <div class="popup-head">
                <h5>Create Group</h5>
            </div>
            <div class="group-adding">
                <div class="friend-group">
                    <form class=""  method="post" action="<?php echo base_url('home/createGroup'); ?>" enctype="multipart/form-data">
                        <div class="change-photo">
                            <input type="file" name="picture">
                        </div>
                        <input type="text" name="name" placeholder="Group Name">
                        <select class="" name="memberID[]" multiple>
                            <?php if (!empty($followingList)) {
                                foreach ($followingList as $member) { ?>
                                    <option value="<?php echo $member['followerID']; ?>"><?php echo $this->emoji->Decode($member['name']); ?></option>
                                <?php }
                            } ?>
                        </select>
                        <button type="submit" name="submit" value="submit" class="main-btn">Create Group</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><!-- Create friends Group -->

<script>
    function remove_cart() {
        event.preventDefault();
        var btn = event.target;
        setTimeout(function() {
            $(btn)
                .closest(".group-box")
                .fadeOut("fast");
        }, 100);
    }

    $(document).ready(function() {
        $(".delete_data").click(function() {
            var del_id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteGroup') ?>',
                data: 'delete_id=' + del_id,
                success: function(data) {}
            });
        });
    });

    //===== Search Filter =====//
    (function($) {
        // custom css expression for a case-insensitive contains()
        jQuery.expr[':'].Contains = function(a, i, m) {
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };

        function listFilter(searchDirgroup, list) {
            var form = $("<form>").attr({
                    "class": "filterform",
                    "action": "#"
                }),
                input = $("<input>").attr({
                    "class": "filterinputgroup",
                    "type": "text",
                    "placeholder": "Search Group"
                });
            $(form).append(input).appendTo(searchDirgroup);

            $(input).change(function() {
                var filter = $(this).val();
                if (filter) {
                    $(list).find("li:not(:Contains(" + filter + "))").slideUp();
                    $(list).find("li:Contains(" + filter + ")").slideDown();
                } else {
                    $(list).find("li").slideDown();
                }
                return false;
            }).keyup(function() {
                $(this).change();
            });
        }

        //search friends widget
        $(function() {
            listFilter($("#searchDirgroup"), $("#people-list"));
        });
    }(jQuery));
</script>