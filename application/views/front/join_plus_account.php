<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
						<?php include('profile_topbar.php'); ?>
						<?php $loginUserID = $this->session->userdata('userID'); ?>
						<div class="col-lg-8">
							<div class="central-meta">
								<div class="text-center">
									<h5>Unlocked Plus benefits.</h5>
									<!-- <p>You have to achieve the targeted amount to unlock a badge for the community</p> -->
								</div>
								<div class="row justify-content-md-center">
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="badge-box">
											<figure><img src="<?php echo base_url('assets/') ?>images/badges/badge19.png" alt=""></figure>
											<h5>Highlighted posts</h5>
											<p>Highlight all posts with a gradient color</p>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="badge-box">
											<figure><img src="<?php echo base_url('assets/') ?>images/badges/badge2.png" alt=""></figure>
											<h5>Profile Frames</h5>
											<p>Options to outline their main profile picture</p>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="badge-box">
											<figure><img src="<?php echo base_url('assets/') ?>images/badges/badge9.png" alt=""></figure>
											<h5>Additional Pictures</h5>
											<p>Two Spots for additional pictures in profile</p>
										</div>
									</div>
								</div>
								<div class="row justify-content-md-center">
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="badge-box">
											<figure><img src="<?php echo base_url('assets/') ?>images/badges/badge6.png" alt=""></figure>
											<h5>Feature Friends</h5>
											<p>Pickup 5 feature friends and display in profile</p>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-6">
										<div class="badge-box">
											<figure><img src="<?php echo base_url('assets/') ?>images/badges/badge8.png" alt=""></figure>
											<h5>Custom Emojis</h5>
											<p>Use Custom Emojis for visible everyone</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="row justify-content-md-center">
								<?php
								$monthPackage = array();
								if(!empty($packageData)){
									foreach($packageData as $package){
										$packageID = $package['packageID'];
										?>
										<div class="col-md-12 col-sm-6">
											<div class="pricingTable2 <?php echo ($packageID==1)?'blue':'pink'; ?>">
												<div class="pricingTable-header">
													<h3 class="title"><?php echo (isset($package['title']))?$package['title']:""; ?></h3>
												</div>
												<ul class="pricing-content">
													<li><i class="fa fa-check"></i>$<?php echo (isset($package['amount']))?$package['amount']:0; ?>/<?php echo (isset($package['duration']))?$package['duration']:''; ?></li>
												</ul>
												<form id="frmPlan<?php echo $packageID; ?>" action="<?php echo base_url('payment-process'); ?>">
													<input type="hidden" name="plan" value="<?php echo $packageID; ?>" />
													<a href="javascipt:void(0)" data-id="<?php echo $packageID; ?>" class="submitPlan pricingTable-signup">Subscribe Now</a>
												</form>
											</div>
										</div>
									<?php }
								} ?>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
;(function($, window, document, undefined) {
	$(document).ready(function() {
		$(document).delegate(".submitPlan", "click", function (e){
			e.preventDefault();
			var packageID = $(this).attr('data-id');
			if(packageID){
				$('#frmPlan'+packageID).submit();
			}
		});
    });
})(jQuery, window, document);
</script>