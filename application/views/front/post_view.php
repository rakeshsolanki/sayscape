<?php
// echo "<pre>"; print_r($post);
$loginUserID = $this->session->userdata('userID');
$postID = (isset($post['postID'])) ? $post['postID'] : "";
$sessionUsername = $this->session->userdata('username');
// $otherUserID = (isset($watchUserID)) ? $watchUserID : (isset($otherUserID)) ? $otherUserID : $loginUserID;
$otherUserID = (isset($watchUserID)) ? $watchUserID : $loginUserID;
$postuserDetails = $this->user->getUserdetails($post['userID'], $loginUserID);
$isResharePost = (isset($post['isResharePost']) && $post['isResharePost'] == 1)?1:0;
$postShareID = (isset($post['postShareID']) && !empty($post['postShareID']))?$post['postShareID']:'';

$highlight = $this->user->checkUserPlusAccount($post['userID'], array('highlightPost'=>'highlightPost'));
$highlightcolor = $highlight['highlightPost']??'fff';
//echo '<pre>';print_r($highlightcolor); 
?>
<div class="central-meta item singlePostView" id="<?php echo ($isResharePost==1)?'divSharePost_'.$postID:'divPost_'.$postID ?>" style=" box-shadow: inset 0 0 50px <?= '#'.$highlightcolor; ?>">
    <div class="user-post">
        <div class="friend-info">
            <?php
            if ($isResharePost==1) {
                $resharePostUserid = (isset($post['ResharePostUserid']))?$post['ResharePostUserid']:$post['userID'];
                $postReshareUserDetails = $this->user->getUserdetails($resharePostUserid, $loginUserID);
                ?>
                <figure>
                    <?php
                    if (isset($post['ReshareUserImage']) && $post['ReshareUserImage'] != "") { ?>
                        <img src="<?php echo $post['ReshareUserImage'] ?>" alt="" class="wh-40">
                    <?php } else { ?>
                        <img src="<?= base_url() ?>/assets/images/resources/admin.png" class="wh-40" alt="">
                    <?php } ?>
                </figure>
                <div class="friend-name">
                    <?php
                    if($post['ResharePostUserid']==$loginUserID){ ?>
                        <div class="more">
                            <div class="more-post-optns"><i class="ti-more-alt"></i>
                                <ul>
                                    <li><a href="javascript:;" class="unquote_post" data-id="<?php echo $post['postID']; ?>"><i class="fa fa-retweet colorblue"></i> Unquote This Post</a> </li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                    <ins><a href="<?= base_url('time-line/'.$post['reshareName']) ?>" title=""><?php echo ucfirst($post['reshareName']); ?> </a>
                    <?php
                    if(isset($postReshareUserDetails['isOffical']) && $postReshareUserDetails['isOffical']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/blue.png'); ?>" /><?php }
                    if(isset($postReshareUserDetails['isVeteran']) && $postReshareUserDetails['isVeteran']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/green.png'); ?>" /><?php }
                    if(isset($postReshareUserDetails['isResponder']) && $postReshareUserDetails['isResponder']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/red.png'); ?>" /><?php }
                    ?></a></ins>
                    <ins class="usrnm"><?php echo ($postReshareUserDetails['username'])?$postReshareUserDetails['username']:""; ?></ins>
                    <p class="nomar">
                    <i class="fa fa-retweet colorblue"></i> <?php if(isset($post['ResharePostEmoji'])){ echo $this->emoji->Decode($post['ResharePostEmoji']); } ?>
                    <span class="displayMomentDate" datetime="<?php echo (isset($post['ResharePostDate'])) ? $post['ResharePostDate'] : ""; ?>"></span>
                    </p>
                </div>
                <div class="post-meta">
                    <div class="description resharePostdesc"><?php if(isset($post['ResharePostContent'])){ echo preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $this->emoji->Decode($post['ResharePostContent'])); } ?></div>
                </div>
            <?php } ?>
            <?php if ($isResharePost==1) { ?>
                <div class="friend-info resharePostViewSection">
                <?php } ?>
                    <figure>
                        <?php
                        if (isset($post['profileImage']) && $post['profileImage'] != "") { ?>
                            <img src="<?php echo $post['profileImage'] ?>" alt="" class="wh-40">
                        <?php } else { ?>
                            <img src="<?= base_url() ?>/assets/images/resources/admin.png" class="wh-40" alt="">
                        <?php } ?>
                    </figure>
                    <div class="friend-name">
                        <div class="more">
                            <div class="more-post-optns"><i class="ti-more-alt"></i>
                                <ul>
                                <?php
                                if (isset($post['userID']) && $post['userID'] == $loginUserID) {
                                    if(isset($post['isTop']) && $post['isTop']==1){ ?>
                                        <li><a href="<?= base_url('unpin-this-post/' . $post['postID']); ?>"> <i class="fa fa-thumb-tack"></i> Unpin This Post</a> </li>
                                    <?php }else{ ?>
                                        <li><a href="<?= base_url('pin-this-post/' . $post['postID']); ?>"> <i class="fa fa-thumb-tack"></i> Pin This Post</a> </li>
                                    <?php }
                                    $today = new DateTime(date('Y-m-d H:i:s'));
                                    $pastDate = $today->diff(new DateTime($post['createdDateTime']));
                                    $postMinute = (isset($pastDate->i))?$pastDate->i:2;
                                    // $postMinute = 1;
                                    if ($postMinute <= 2 && empty($post['pollPostOptions'])) { ?>
                                        <li><a href="javascript:;" class="edit_post" data-posttype="<?php echo isset($post['postType']) ? $post['postType'] : ''; ?>" data-id="<?php echo $post['postID']; ?>" data-toggle="modal" data-target="#Modeleditpost"><i class="fa fa-pencil-square-o"></i> Edit Post</a> </li>
                                    <?php } ?>
                                    <li><a href="javascript:;" class="delete_post" data-id="<?php echo $post['postID']; ?>"><i class="fa fa-trash-o"></i> Delete This Post</a> </li>
                                <?php }else{ ?>
                                    <li><a href="javascript:;" class="hide_post" data-id="<?php echo $post['postID']; ?>"><i class="fa fa-eye-slash"></i> Hide This Post</a> </li>
                                    <li><a href="javascript:;" class="report_post" data-reporttype="0" data-id="<?php echo $post['postID']; ?>"><i class="fa fa-flag"></i> Report Post</a> </li>
                                <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <ins>
                            <a href="<?= base_url('time-line/'.$post['username']) ?>" title="">
                            <?php echo $this->emoji->Decode(ucfirst($post['username'])); ?>
                            <?php
                            if(isset($postuserDetails['isOffical']) && $postuserDetails['isOffical']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/blue.png'); ?>" /><?php }
                            if(isset($postuserDetails['isVeteran']) && $postuserDetails['isVeteran']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/green.png'); ?>" /><?php }
                            if(isset($postuserDetails['isResponder']) && $postuserDetails['isResponder']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/red.png'); ?>" /><?php }
                            ?></a>
                        </ins>
                        <span class="usrnm"><?php echo $post['username']; ?></span>
                        <span class="displayMomentDate" datetime="<?php echo (isset($post['createdDateTime'])) ? $post['createdDateTime'] : ""; ?>"></span>
                    </div>
                    <div class="post-meta">
                        <?php
                        if($post['text']!=""){ ?>
                            <div class="description nomar">
                                <p><?php echo $this->emoji->Decode(ucfirst($post['text'])); ?></p>
                                <!-- <p><?php //echo preg_replace("/\\\\u([0-9A-F]{2,5})/i", "&#x$1;", $this->emoji->Decode(ucfirst($post['text']))); ?></p> -->
                            </div>
                        <?php } 
                        if (!empty($post['gif'])) { ?>
                            <div class="">
                                <div class="col-md-12">
                                    <a class="strip" href="<?php echo $post['gif']; ?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                    <img src="<?php echo $post['gif']; ?>" alt="" class="postpicture"></a>
                                </div>
                            </div>
                        <?php }
                        if (!empty($post['media'])) { ?>
                            <div class="">
                                <?php
                                // echo "<pre>"; print_r($post['media']);
                                foreach ($post['media'] as $media) {
                                    if (isset($media['type']) && $media['type'] == 1) {
                                        if (!empty($media['filename'])) { ?>
                                            <div class="col-md-12">
                                            <a class="strip" href="<?php echo $media['filename']; ?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                                <img src="<?php echo $media['filename']; ?>" alt="" class="postpicture"></a>
                                            </div>
                                        <?php }
                                    }elseif (isset($media['type']) && $media['type'] == 2 && (!empty($media['filename']))) { 
                                        ?>
                                        <div class="col-md-12">
                                            <video class="videosize" controls>
                                                <source src="<?php echo $media['filename']; ?>" type="video/mp4">
                                            </video>
                                        </div>
                                <?php }
                                } ?>
                            </div>
                        <?php } elseif (!empty($post['pollPostOptions'])) {
                            $hasAnswer = $post['hasPollPostAnswer'];
                            $totalPollAnswerCount = count($this->CommonModel->get_all(TBL_POST_POLL_ANSWER, ['*'], ["postID" => $postID, "isActive" => 1, "isDelete" => 0]));
                            ?>
                            <div class="">
                                <div class="col-sm-12 pollOptionDiv<?php echo $postID; ?> pollOptionDiv" style="<?php echo ($hasAnswer == 1) ? 'display: none' : ''; ?>">
                                    <ul class="tutor-links">
                                        <?php
                                        // echo "<pre>"; print_r($post['pollPostOptions']); die();
                                        foreach ($post['pollPostOptions'] as $poll) { ?>
                                            <li><a href="javascript:void(0)" class="getPollAnswer" data-id="<?php echo $poll['optionID']; ?>" data-postid="<?php echo $postID; ?>" title=""><i class="fa fa-check-square-o"></i> <?php echo $poll['option']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="col-sm-12 pollAnswerDiv<?php echo $postID; ?>" style="<?php echo ($hasAnswer == 0) ? 'display: none' : ''; ?>">
                                    <?php
                                    foreach ($post['pollPostOptions'] as $poll) {
                                        $totalPollAnswer = count($this->CommonModel->get_all(TBL_POST_POLL_ANSWER, ['*'], ["postID" => $postID,"optionID" => $poll['optionID'], "isActive" => 1, "isDelete" => 0]));
                                        $totalPollAnswer = (!empty($totalPollAnswer))?'('.$totalPollAnswer.')':"";
                                        ?>
                                        <label class="nomar"><?php echo $poll['option']; ?></label>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $poll['avgAnswer']; ?>" aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $poll['avgAnswer'] . '%'; ?>"> <?php echo $poll['avgAnswer'] . '% ' .$totalPollAnswer; ?></div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div><p>Total Votes: <?php echo $totalPollAnswerCount; ?></p></div>
                            </div>
                        <?php } ?>
                    </div>
                <?php if ($isResharePost==1) { ?>
                </div>
            <?php }
            // if ($isResharePost!=1) { ?>
                <div class="we-video-info postCountSection">
                    <ul>
                        <li>
                            <a href="javascript:void(0)" class="loadPostCountUsers" data-toggle="modal" data-target="#modalPostCountUsers" data-id="<?php echo $postID; ?>" data-type="like"><ins><?php echo (isset($post['likesCount']))?$post['likesCount']:"0"; ?> Likes </ins></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="loadPostCountUsers" data-toggle="modal" data-target="#modalPostCountUsers" data-id="<?php echo $postID; ?>" data-type="dislike"><ins><?php echo (isset($post['disLikeCount']))?$post['disLikeCount']:"0"; ?> Dislikes </ins></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)" class="loadPostCountUsers" data-toggle="modal" data-target="#modalPostCountUsers" data-id="<?php echo $postID; ?>" data-type="reShare"><ins><?php echo (isset($post['reShareCount']))?$post['reShareCount']:"0"; ?> Quotes </ins></a>
                        </li>
                    </ul>
                </div>
                <div class="we-video-info">
                    <ul>
                        <li>
                            <span class="comment loadCommentSection" data-id="<?php echo $postID; ?>" data-userid="<?php echo (isset($otherUserID))?$otherUserID:""; ?>" title="Comments" data-postshareid="<?php echo $postShareID; ?>"><i class="fa fa-commenting"></i> 
                            <ins id="postCommentCount<?php echo $postID; ?>"> <?php echo (isset($post['commentCount']))?$post['commentCount']:"0"; ?></ins></span>
                        </li>
                        <li>
                            <span class="likes heart postLike <?php echo (isset($post['isLiked']) && $post['isLiked']==1)?'happy':''; ?>" title="Like" data-id="<?php echo $postID; ?>" data-userid="<?php echo (isset($otherUserID))?$otherUserID:""; ?>" data-postshareid="<?php echo $postShareID; ?>"><i class="fa fa-heart"></i>
                            <span id="postLikeCount<?php echo $postID; ?>"><?php echo (isset($post['likesCount']))?$post['likesCount']:"0"; ?></span></span>
                        </li>
                        <li>
                            <span class="likes heart postDislike <?php echo (!empty($post['isDisLiked']))?'happy':''; ?>" title="Dislike" data-id="<?php echo $postID; ?>" data-userid="<?php echo (isset($otherUserID))?$otherUserID:""; ?>" data-postshareid="<?php echo $postShareID; ?>"><i class="ti-heart-broken"></i>
                            <span id="postDislikeCount<?php echo $postID; ?>"><?php echo (isset($post['disLikeCount']))?$post['disLikeCount']:"0"; ?></span></span>
                        </li>
                        <li>
                            <span class="sharePost" title="Quote" data-id="<?php echo $postID; ?>" data-userid="<?php echo (isset($otherUserID))?$otherUserID:""; ?>" data-toggle="modal" data-target="#modalSharePost" data-postshareid="<?php echo $postShareID; ?>"><i class="fa fa-retweet"></i>
                            <ins> <?php echo (isset($post['reShareCount']))?$post['reShareCount']:"0"; ?></ins></span>
                        </li>
                    </ul>
                </div>
                
                <div class="coment-area">
                    <ul class="we-comet viewComment" id="viewCommentList_<?php echo $postID; ?>">
                        <?php //include('post_comment_view.php'); ?>
                    </ul>
                </div>
            <?php //} ?>
        </div>
    </div>
</div>
