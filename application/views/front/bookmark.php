<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <!-- sidebar -->
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-6">
                            <?php
                            $loginUserID = $this->session->userdata('userID');
                            if (!empty($postList)) {
                                ?><div class="loadMorePosts"><?php
                                $count = 1;
                                foreach ($postList as $post) {
                                    $datas['post'] = $post;
                                    $datas['count'] = $count;
                                    $this->load->view('front/post_view', $datas);
                                    $count++;
                                    //break;
                                } ?></div>
                                <input type="hidden" id="pageno" class="pageno" value="1" />
                                <button class="postLoader btn-view btn-load-more" data-user="<?php echo $loginUserID; ?>" data-type="bookmark">Load More</button>
                            <?php }else{ ?>
                                <div class="central-meta item">
                                    <div class="user-post">
                                        <div class="description">
                                            <p class="text-center text-danger">Bookmark are not available.</p>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-3">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>