<?php
//following data
$loginUserID = $this->session->userdata('userID');
$watchUserID = (isset($watchUserID)) ? $watchUserID : $loginUserID;
$userDetails = $this->user->getUserdetails($watchUserID, $loginUserID);
$followingList = $this->user->getFollowingList($loginUserID, $watchUserID);
// echo "<pre>"; print_r($followingList); die();
if(!empty($followingList)){
    foreach ($followingList as &$followinguser) {
        $user['privateProfileStatus'] = "0";
        $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $followinguser['followerID'], 'isActive' => 1, 'isDelete' => 0]);
        if (!empty($settingsData)) {
            $followinguser['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";
        }
        //for check user follow me or not
        $followinguser['isFollowMe'] = "0";
        $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $followinguser['followerID'], 'followerID' => $watchUserID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);
        if (!empty($checkUserFollowData)) {
            $followinguser['isFollowMe'] = "1";
        }
        $followinguser['onlineStatus'] = ($this->user->getUserOnlineStatus($followinguser['followerID'])=='online')?1:0;
    }
}
$sortColumns = array_column($followingList, 'onlineStatus');
array_multisort($sortColumns, SORT_DESC, $followingList);

//followers data
$followersList = $this->user->getFollowersList($loginUserID, $watchUserID);
if(!empty($followersList)){
    foreach ($followersList as &$followeruser) {
        $user['privateProfileStatus'] = "0";
        $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $followeruser['followerID'], 'isActive' => 1, 'isDelete' => 0]);
        if (!empty($settingsData)) {
            $followeruser['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";
        }
        //for check user follow me or not
        $followeruser['isFollowMe'] = "0";
        $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $followeruser['followerID'], 'followerID' => $watchUserID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);
        if (!empty($checkUserFollowData)) {
            $followeruser['isFollowMe'] = "1";
        }
        $followeruser['onlineStatus'] = ($this->user->getUserOnlineStatus($followeruser['followerID'])=='online')?1:0;
    }
}
$sortColumns = array_column($followersList, 'onlineStatus');
array_multisort($sortColumns, SORT_DESC, $followersList);

//for hash tag trading
$postsdatas = $this->post->getPosts($loginUserID, 0);
if (!empty($postsdatas)) {
    $hashtagArray = array();
    foreach ($postsdatas as &$value) {
        preg_match_all('/#(\w+)/', $value['text'], $hashtagMatches);
        if (!empty($hashtagMatches[0])) {
            foreach ($hashtagMatches[0] as $match) {
                $hashtagArray[] = $match;
            }
        }
    }
    // echo "<pre>"; print_r($hashtagArray); die();
    $newHashtagsArray = array_count_values($hashtagArray);
    arsort($newHashtagsArray);
    $hashtags = array_keys($newHashtagsArray);
    //$data['hashtagTrendingList'] = $hashtags;
    //$this->load->view('web/hashtag_trending',$data);
} 
$recommendUser = $this->user->getRecommendUser($loginUserID);
?>
<aside class="sidebar static right d-none d-sm-block">
    <div class="widget">
        <h4 class="widget-title">Your Profile</h4>
        <div class="your-page">
            <figure>
                <?php
                if (isset($userDetails['profileImage']) && $userDetails['profileImage'] != "") { ?>
                    <a href="#" title=""><img src="<?php echo $userDetails['profileImage'] ?>" alt="" class="wh-50"> </a>
                <?php } else { ?>
                    <a href="#" title=""><img src="<?php echo base_url('assets/') ?>images/resources/friend-avatar9.jpg" class="wh-50" alt=""></a>
                <?php } ?>
            </figure>
            <div class="page-meta">
                <a href="#" title="" class="underline">
                <?php if (isset($userDetails['username'])) {
                    echo $userDetails['username'];
                } ?></a>
                <span>
                <?php if (isset($userDetails['name'])) {
                    echo $this->emoji->Decode($userDetails['name']);
                } ?></span>
            </div>
            <div class="page-likes">
                <ul class="nav nav-tabs likes-btn">
                    <li class="nav-item"><a class="active" href="#tabFollowings" data-toggle="tab" data-ripple=""><?php echo count($followingList); ?> Following</a></li>
                    <li class="nav-item"><a class="" href="#tabFollowers" data-toggle="tab" data-ripple=""><?php echo count($followersList); ?> Followers</a></li>
                </ul>
            </div>
        </div>
    </div><!-- page like widget -->
    <div class="widget rightSideMenu">
        <div class="tab-content">
           <div class="tab-pane" id="tabFollowers">
                <div class="widget">
                    <h4 class="widget-title">Followers</h4>
                    <ul class="followers pd-l-10">
                        <?php
                        if (!empty($followersList)) {
                            foreach ($followersList as $follower) { ?>
                                <li>
                                    <figure>
                                        <img src="<?php echo $follower['profileImage']; ?>" alt="" class="wh-40">
                                        <span class="statusnew f-<?php echo $this->user->getUserOnlineStatus($follower['followerID']); ?>"></span>
                                    </figure>
                                    <div class="friend-meta">
                                        <h4><a href="<?php echo base_url('time-line/' . $follower['username']) ?>"><?php echo $follower['username'] ?></a></h4>
                                        <?php
                                        if (!empty($follower) && $follower['followerID'] != $loginUserID) {
                                            if ($follower['isfollow'] == 1) { ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>">Following</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="1" id="userFollow<?php echo $follower['followerID']; ?>" style="display: none">Follow</a>
                                            <?php } elseif ($follower['isfollow'] == 4) { ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>">Request</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="4" id="userFollow<?php echo $follower['followerID']; ?>" style="display: none">Follow</a>
                                                <?php } else {
                                                if (!empty($follower['privateProfileStatus']) && $follower['privateProfileStatus'] == 1) { ?>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>" style="display: none">Request</a>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="4" id="userfollowRequest<?php echo $follower['followerID']; ?>">Follow</a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $follower['followerID']; ?>" style="display: none">Following</a>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $follower['followerID']; ?>" data-status="1" id="userFollow<?php echo $follower['followerID']; ?>">Follow</a>
                                                <?php } ?>
                                            <?php }
                                            if ($follower['isFollowMe'] == 1) { ?>
                                                <span class="followyou">Follows You</span>
                                        <?php }
                                        } ?>
                                    </div>
                                </li>
                        <?php }
                        } ?>
                    </ul>
                </div>
            </div> 
            <div class="tab-pane active fade show" id="tabFollowings">
                <div class="widget">
                    <h4 class="widget-title">Following</h4>
                    <ul class="followers pd-l-10 ps-container ps-theme-default ps-active-y" data-ps-id="d2b53c72-36fb-f7dc-2553-7f3bbd4fbccb">
                        <?php
                        if (!empty($followingList)) {
                            foreach ($followingList as $following) { ?>
                                <li>
                                    <figure>
                                        <img src="<?php echo $following['profileImage']; ?>" alt="" class="wh-40">
                                        <span class="statusnew f-<?php echo $this->user->getUserOnlineStatus($following['followerID']); ?>"></span>
                                        <!-- <span class="statusnew f-online"></span>-->
                                        <!-- <span class="statusnew f-away"></span>-->
                                    </figure>
                                    <div class="friend-meta">
                                        <h4><a href="<?php echo base_url('time-line/' . $following['username']) ?>"><?php echo $following['username'] ?></a></h4>
                                        <!-- <a href="#" title="" class="underline">Add Friend</a>-->
                                        <?php
                                        if ($following['followerID'] != $loginUserID) {
                                            if ($following['isfollow'] == 1) { ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>">Following</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="1" id="userFollow<?php echo $following['followerID']; ?>" style="display: none">Follow</a>
                                            <?php } elseif ($following['isfollow'] == 4) { ?>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>">Request</a>
                                                <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="4" id="userFollow<?php echo $following['followerID']; ?>" style="display: none">Follow</a>
                                                <?php } else {
                                                if (!empty($following['privateProfileStatus']) && $following['privateProfileStatus'] == 1) { ?>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>" style="display: none">Request</a>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="4" id="userfollowRequest<?php echo $following['followerID']; ?>">Follow</a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>" style="display: none">Following</a>
                                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="1" id="userFollow<?php echo $following['followerID']; ?>">Follow</a>
                                                <?php } ?>
                                            <?php }
                                            if ($following['isFollowMe'] == 1) { ?>
                                                <span class="followyou">Follows You</span>
                                        <?php }
                                        } ?>
                                    </div>
                                </li>
                        <?php }
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php if(!empty($recommendUser)){ ?>
        <div class="widget ">
            <h4 class="widget-title">Recommend User</h4>
            <ul class="followers pd-l-10 ps-container ps-theme-default ps-active-y">
            <?php 
            foreach($recommendUser as $recom){ ?>
                     <li>
                        <figure>
                            <img src="<?php echo isset($recom['profileImage']) ? $recom['profileImage']:''; ?>" alt="" class="wh-40">
                            <span class="statusnew f-<?php echo $this->user->getUserOnlineStatus($recom['userID']); ?>"></span>
                        </figure>
                        <div class="friend-meta">
                            <h4><a href="<a href="<?php echo base_url('time-line/' .isset($recom['username']) ? $recom['username']:'') ?>"><?= isset($recom['username']) ? $recom['username']:''; ?></a></h4>
                            <?php
                                if (!empty($recom) && $recom['userID'] != $loginUserID) { ?>
                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo isset($recom['userID']) ? $recom['userID']:''; ?>" data-status="0" id="userUnfollow<?php echo isset($recom['userID']) ? $recom['userID']:''; ?>" style="display: none">Following</a>
                                    <a href="javascript:void(0)" title="" class="add-butn btnFollowUnfollow" data-id="<?php echo isset($recom['userID']) ? $recom['userID']:''; ?>" data-status="1" id="userFollow<?php echo isset($recom['userID']) ? $recom['userID']:''; ?>">Follow</a>
                            <?php  } ?>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </div>
    <?php } ?>
    <div class="widget stick-widget">
        <h4 class="widget-title">#Tags Trending:</h4>
        <div class="hash-tags followers pd-l-10 ps-container ps-theme-default ps-active-y">
            <?php 
            if(!empty($hashtags)){
                foreach($hashtags as $hashtagTrending){
                    echo $this->emoji->Decode($hashtagTrending);
                }
            } ?>
        </div>
    </div>
</aside>
<style>
    .followyou{
        bottom: 0;
        right: 0px;
    }
</style>