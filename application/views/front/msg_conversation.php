<?php if(!empty($messagesdata)){
    $loginUserID = $this->session->userdata('userID');
    foreach ($messagesdata as $msgdata){
        if($msgdata['userID']== $loginUserID){  ?>
            <input type="hidden" class="lastmessage" name="messageID" value="<?= $msgdata['messageID']?>">
            <li class="me">
                <figure><img src="<?= $msgdata['fromUserPic'];?>" alt="" class="wh-40"></figure>
                <div class="text-box">
                    <p><?= isset($msgdata['text']) ? $msgdata['text']:'';
                        if($msgdata['gif']!=''){ ?>
                            <a class="strip" href="<?= $msgdata['gif'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                <img src="<?= $msgdata['gif'];?>" alt="">
                            </a>
                        <?php }
                        if($msgdata['media_type']==1){ ?>
                            <a class="strip" href="<?= $msgdata['media'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                <img src="<?= $msgdata['media'];?>" alt="">
                            </a>
                        <?php }elseif($msgdata['media_type']==2){?>
                            <video controls class="" style="width: 250px;height: 150px;">
                                <source type="video/mp4" src="<?= $msgdata['media'];?>">
                            </video>
                        <?php } ?>
                    </p>
                    <span class="displayMomentDateOnly" datetime="<?php echo (isset($msgdata['createdDateTime'])) ? $msgdata['createdDateTime'] : ""; ?>"></span>
                </div>
            </li>
    <?php }else{ ?>
            <input type="hidden" class="lastmessage" name="messageID" value="<?= $msgdata['messageID']?>">
        <li class="you">
            <figure><img src="<?= $msgdata['fromUserPic'];?>" alt="" class="wh-40"></figure>
            <div class="text-box">
                <p><?= isset($msgdata['text']) ? $msgdata['text']:''; if($msgdata['gif']!=''){ ?>
                        <a class="strip" href="<?= $msgdata['gif'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                            <img src="<?= $msgdata['gif'];?>" alt="">
                        </a>
                    <?php } if($msgdata['media_type']==1){ ?>
                        <a class="strip" href="<?= $msgdata['media'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                            <img src="<?= $msgdata['media'];?>" alt="">
                        </a>
                    <?php }elseif($msgdata['media_type']==2){?>
                        <video controls class="" style="width: 250px;height: 150px;">
                            <source type="video/mp4" src="<?= $msgdata['media'];?>">
                        </video>
                    <?php } ?>
                </p>
                <span class="displayMomentDateOnly" datetime="<?php echo (isset($msgdata['createdDateTime'])) ? $msgdata['createdDateTime'] : ""; ?>"></span>
            </div>
        </li>
    <?php } } } ?>
