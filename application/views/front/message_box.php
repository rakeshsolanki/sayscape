<?php $toID = $this->uri->segment(2); ?>
<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?><!-- user profile banner -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- content -->
<section>
    <div class="gap2 no-gap gray-bg">
        <div class="container no-padding">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-content messenger">
                        <div class="tab-pane active fade show " id="link1" >
                            <div class="row merged">
                                <div class="col-lg-12">
                                    <div class="mesg-area-head">
                                        <div class="active-user">
                                            <figure>
                                                <?php
                                                $otherUserImg = base_url().'/upload/userprofile/defaultProfile.png';
                                                if (isset($otherUserDetails['profileImage']) && $otherUserDetails['profileImage'] != "") {
                                                    $otherUserImg = $otherUserDetails['profileImage'];
                                                } ?>
                                                <img src="<?php echo $otherUserImg; ?>" alt="" class="wh-40">
                                                <!-- <span class="status f-online"></span> -->
                                            </figure>
                                            <div>
                                                <h6 class="unread"><?= (isset($otherUserDetails['username'])) ? $otherUserDetails['username']:'Select user' ; ?></h6>
                                                <span>Online</span>
                                            </div>
                                        </div>
                                        <?php if(!empty($otherUserDetails['userID'])){ ?>
                                        <ul class="live-calls">
                                            <li>
                                                <div class="dropdown">
                                                    <button class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="ti-view-grid"></i>
                                                    </button>
                                                    
                                                        <div class="dropdown-menu dropdown-menu-right">
                                                            <a href="<?= base_url('user/deletegroupChatHistory/'.$otherUserDetails['userID'].'/0'); ?>" class="dropdown-item"><i class="ti-server"></i>Clear History</a>
                                                        </div>
                                                    
                                                </div>
                                            </li>
                                        </ul>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!--                                <div class="col-lg-4 col-md-4">-->
                                <div class="message-users">
                                    <div class="message-head">
                                        <h4>All Groups</h4>
                                        <div class="more d-none">
                                            <div class="more-post-optns"><i class="ti-settings"></i>
                                                <ul>
                                                    <li><i class="fa fa-wrench"></i>Setting</li>
                                                    <li><i class="fa fa-envelope-open"></i>Active Contacts</li>
                                                    <li><i class="fa fa-folder-open"></i>Archives Chats</li>
                                                    <li><i class="fa fa-eye-slash"></i>Unread Chats</li>
                                                    <li><i class="fa fa-flag"></i>Report a problem</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="mesg-peple">
                                        <ul class="nav nav-tabs nav-tabs--vertical msg-pepl-list">
                                            <?php if (!empty($followersName)) {
                                                foreach ($followersName as $followers) {
                                                    $checkClearData = $this->CommonModel->get_row(TBL_MESSAGE_CLEAR, ['*'], ['isActive'=>1,'userID' => $loginUserID, 'toUserID'=>$followers['toUserID']]);
                                                    if(!empty($checkClearData) && ($checkClearData['messageID'] >= $followers['messageID']) ){

                                                    }elseif($followers['toUserID']!=''){ ?>
                                                    <li class="nav-item">
                                                        <a class="<?php if($followers['toUserID'] == $toID){ echo 'active'; } ?>" href="<?php echo base_url('message-box/' . $followers['toUserID']); ?>" >
                                                            <figure>
                                                                <?php if (isset($followers['profileImage']) && $followers['profileImage'] != "") { ?>
                                                                    <img src="<?php echo $followers['profileImage']; ?>" alt="" class="wh-40">
                                                                <?php } else { ?>
                                                                    <img src="<?= base_url('assets/'); ?>images/resources/friend-avatar3.jpg" alt="">
                                                                <?php } ?>

                                                                <span class="status f-<?php echo $this->user->getUserOnlineStatus($followers['toUserID']); ?>"></span>
                                                            </figure>
                                                            <div class="user-name">
                                                                <h6 class=""><?php echo isset($followers['username']) ? $followers['username']:''; ?></h6>
                                                            </div>
                                                        </a>
                                                    </li>
                                                <?php } } } ?>
                                        </ul>
                                    </div>
                                </div>
                                <!--                                </div>-->
                                <div class="col-lg-8 col-md-8"> 
                                        <div class="mesge-area">

                                            <ul class="conversations">
                                                
                                                
                                                <?php if(!empty($messageData)){ ?>
                                                    <div class="lodmore" id="lodmore">
                                                        <input type="hidden" name="page" id="page" value="2">
                                                        <button class="btn-view btn-load-more lodoldmore"></button>
                                                    </div>
                                                    <?php foreach ($messageData as $msgdata){
                                                        if($msgdata['userID']== $loginUserID){  ?>
                                                            <input type="hidden" class="lastmessage" name="messageID" value="<?= $msgdata['messageID']?>">
                                                            <li class="me">
                                                                <figure><img src="<?= $msgdata['fromUserPic'];?>" alt="" class="wh-26"></figure>
                                                                <div class="text-box">
                                                                    <p><?= isset($msgdata['text']) ? $msgdata['text']:''; if($msgdata['gif']!=''){ ?>
                                                                            <a class="strip" href="<?= $msgdata['gif'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                                                                <img src="<?= $msgdata['gif'];?>" alt="">
                                                                            </a>
                                                                        <?php } if($msgdata['media_type']==1){ ?>
                                                                            <a class="strip" href="<?= $msgdata['media'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                                                                <img src="<?= $msgdata['media'];?>" alt="">
                                                                            </a>
                                                                        <?php }elseif($msgdata['media_type']==2){?>
                                                                            <video controls class="" style="width: 250px;height: 150px;">
                                                                                <source type="video/mp4" src="<?= $msgdata['media'];?>">
                                                                            </video>
                                                                        <?php } ?>
                                                                    </p>
                                                                    <!-- <span><i class="ti-check"></i><i class="ti-check"></i> 2:35PM</span> -->
                                                                    <span class="displayMomentDateOnly" datetime="<?php echo (isset($msgdata['createdDateTime'])) ? $msgdata['createdDateTime'] : ""; ?>"></span>
                                                                </div>
                                                            </li>
                                                        <?php }else{ ?>
                                                            <input type="hidden" class="lastmessage" name="messageID" value="<?= $msgdata['messageID']?>">
                                                            <li class="you">
                                                                <figure><img src="<?= $msgdata['fromUserPic'];?>" alt="" class="wh-26"></figure>
                                                                <div class="text-box">
                                                                    <p><?= isset($msgdata['text']) ? $msgdata['text']:''; if($msgdata['gif']!=''){ ?>
                                                                        <a class="strip" href="<?= $msgdata['gif'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                                                            <img src="<?= $msgdata['gif'];?>" alt="">
                                                                        </a>
                                                                        <?php } if($msgdata['media_type']==1){ ?>
                                                                            <a class="strip" href="<?= $msgdata['media'];?>" title="" data-strip-group="mygroup" data-strip-group-options="loop: false">
                                                                            <img src="<?= $msgdata['media'];?>" alt="">
                                                                            </a>
                                                                        <?php }elseif($msgdata['media_type']==2){?>
                                                                            <video controls class="" style="width: 250px;height: 150px;">
                                                                                <source type="video/mp4" src="<?= $msgdata['media'];?>">
                                                                            </video>
                                                                        <?php } ?>
                                                                    </p>
                                                                    <span class="displayMomentDateOnly" datetime="<?php echo (isset($msgdata['createdDateTime'])) ? $msgdata['createdDateTime'] : ""; ?>"></span>
                                                                    <!-- <span><i class="ti-check"></i><i class="ti-check"></i>2:35PM</span> -->
                                                                </div>
                                                            </li>
                                                        <?php } } }?>

                                            </ul>
                                        </div>
                                         <?php if(!empty($otherUserDetails)){ ?>
                                        <div class="message-writing-box">
                                            <form action="#" method="post" enctype="multipart/form-data">
                                                <p class="lead emoji-picker-container">
                                                    <textarea rows="2" placeholder="Add a Message" id="comment" name="text" class="" data-emojiable="true" data-emoji-input="unicode"></textarea>
                                                </p>
                                                <input type="hidden" name="toID" value="<?php echo $toID ?>" id="toID">
                                                <input type="hidden" name="userimg" value="<?php echo (isset($userDetails['profileImage'])) ? $userDetails['profileImage'] : '';  ?>" id="userimg">
                                                <input type="hidden" name="otherUserImg" value="<?php echo $otherUserImg; ?>" id="otherUserImg" />
                                                <input type="file" id="Videoupload" style="display:none" accept="video/mp4"/>
                                                <!-- <input type="file" name="video" id="vide" style="display:none" accept="video/mp4"> -->
                                                <div class="attachments">
                                                    <ul class="cursor-point text-right pd-b-15">
                                                        <li id="emoji">
                                                            <i class="fa fa-smile-o"></i>
                                                            <label class="fileContainer"> </label>
                                                        </li>
                                                        <li id="image">
                                                            <i class="fa fa-camera" id="" data-toggle="modal" data-target="#imgModal" accept="image/*"></i>
                                                        </li>
                                                        <li id="">
                                                            <i class="fa fa-video-camera" id="" data-toggle="modal" data-target="#videoModal" accept="video/mp4"></i>    
                                                        </li>
                                                        <li id="">
                                                            <i class="" id="btnShowGif" data-toggle="modal" data-target="#gifModal"><b>GIF</b></i>
                                                        </li>
                                                        <li>
                                                            <button title="send" class="messagesave" id="save"><i class="fa fa-paper-plane text-white"></i></button>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </form>
                                        </div>
                                        <?php } ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal fade" id="gifModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"><i class="fa fa-image"></i> Send GIF </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form method="post" id="gifsend">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="buttons" style="display: none"></div>
                            <div class="search-bar">
                                <input type="text" name="query" class="form-control" id="user-search" placeholder="Search GIF" />
                                <div class="mt20">
                                    <input id="submit" type="button" value="Search" class="btn btn-sm btn-success btnSearchGif" />
                                    <input type="button" class="trending btn btn-sm btn-danger btnSearchGif" value="See what's trending" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="toID" value="<?php echo $toID ?>" id="toID">
                    <input type="hidden" id="gif" name="gif" value="" required />
                    <div class="mb">
                        <div id="gifresults" class="row p10"></div>
                    </div>
                    <div class="">
                        <input class="btn btn-success" id="btnUploadGif"   type="submit" name="uploadGif" value="Send" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="imgModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"><i class="fa fa-image"></i> Send Image </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form method="post" id="imagesend">
                <div class="modal-body">
                    <div class="input_fields_wrap">
                        <div class="row">
                            <div class="col-md-6">
                                <div><input type="file" class="form-control" name="file" id="file" onchange="readURL(this);" required></div>
                            </div>
                            <div class="col-md-6">
                                <img id="blah" src="http://placehold.it/180" alt="your image" class="" style="height:100px; width:100px;" />
                            </div>
                        </div>
                    </div>
                    
                    <input type="hidden" name="toID" value="<?php echo $toID ?>" id="toID">
                    <input type="hidden" id="gif" name="gif" value="" required />
                    <div class="">
                        <input class="btn btn-success" id="btnUploadGif"  type="submit" name="uploadGif" value="Send" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="videoModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"><i class="fa fa-image"></i> Send video </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form method="post" id="videosend">
                <div class="modal-body">
                    <div class="input_fields_wrap">
                        <div class="row">
                            <div class="col-md-6">
                                <div><input type="file" name="video" id="vide" accept="video/mp4" required></div>
                            </div>
                        </div>
                    </div>
                    <canvas id="can" style="height:1px; width:100%;"> </canvas>
                    <video controls class="mt10" id="vibox">
                        <source type="video/mp4">
                        <canvas class="postvideo mt10" id="thumb" style="display:block;"></canvas>
                    </video>
                    <input type="hidden" name="toID" value="<?php echo $toID ?>" id="toID">
                    <input type="hidden" id="gif" name="gif" value="" required />
                    <div class="">
                        <input class="btn btn-success" id="btnUploadGif"  type="submit" name="uploadGif" value="Send" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<input type="hidden" name="toID" value="<?php echo $toID ?>" id="toID">
<script>
    
    ;(function($, window, document, undefined) {
        $(document).ready(function() {

            $('.mesge-area, .conversations').animate({scrollTop: $('.mesge-area, .conversations')[0].scrollHeight}, 1000);

            $(".lodoldmore").click(function(event) {
                event.preventDefault();
                //get value 
                var toID = $('#toID').val();
                var page = $('#page').val();
                //Ajax call to get data
                if(page){
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('user/getOldMessage') ?>",
                        data: {
                            page: page,
                            toID: toID,
                            type:0
                        },
                        cache: false,
                        success: function(result) {
                            if(result){
                                $(result).insertAfter(".lodmore");
                                $('#page').val(parseInt(page)+parseInt(1));
                            }else {
                                $('#lodmore').hide();
                            }
                        }
                    });
                }
            });

            $(".messagesave").click(function(event) {
                event.preventDefault();
                //get value of message
                var message = $('#comment').val();
                var toID = $('#toID').val();
                //check if value is not empty
                /*if (message.trim()=="") {
                    $('#error_message').html("Please enter message");
                    $('#comment').focus();
                    return false;
                } else {
                    $("#error_message").html("");
                    $('.emoji-wysiwyg-editor').html('');
                }*/
                //Ajax call to send data to the insert.php
                if(message){
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url('user/userMsgSave') ?>",
                        data: {
                            message: message,
                            toID: toID,
                        },
                        cache: false,
                        success: function(data) {
                            $('#comment').val('');
                            $('.emoji-wysiwyg-editor').html('');
                            $('.mesge-area, .conversations').animate({scrollTop: $('.mesge-area, .conversations')[0].scrollHeight}, 1000);
                        }
                    });
                }
            });

            setInterval(function() {
                var toID = $("#toID").val();
                var lastmsgid = $( ".lastmessage" ).last().val();
                if(toID){
                    $.ajax({
                        type: "post",
                        url: "<?php echo base_url('user/lastMessage'); ?>",
                        data: {
                            type: 0,
                            toID: toID,
                            lastmsgid:lastmsgid
                        },
                        // dataType: 'JSON',
                        success: function(result) {
                            $(".conversations").append(result);
                            // $('.mesge-area, .conversations').animate({scrollTop: $('.mesge-area, .conversations')[0].scrollHeight}, 1000);
                        }
                    });
                }
            }, 3000);

            $('#gifsend').submit(function(e) {
                e.preventDefault();
                var gif = $('#gif').val();
                console.log(gif);
                if(gif){
                    $.ajax({
                        url: "<?php echo base_url('user/userMsgSave'); ?>",
                        type: "post",
                        data: new FormData(this),
                        processData: false,
                        contentType: false,
                        cache: false,
                        async: false,
                        success: function(data) {
                            $("#gifModal").modal("hide");
                        }
                    });
                }
            });

            $('#imagesend').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "<?php echo base_url('user/chatImageSend'); ?>",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    success: function(data) {
                        $("#imagesend")[0].reset();
                        $("#imgModal").modal("hide");
                    }
                });
            });

            $('#videosend').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "<?php echo base_url('user/chatVideoSend'); ?>",
                    type: "post",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    cache: false,
                    async: false,
                    success: function(data) {
                        $("#videosend").modal("hide");
                    }
                });
            });

        });
    })(jQuery, window, document);


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#blah').show();
            $('#blah').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


$(function() {
    $('#vibox').hide();
    $('#postvideo').click(function() {
        $('#photos').hide();
    });
    $('#pollhide').click(function() {
        $('#vibox').hide();
    });
    $('#files').click(function() {
        $('#vibox').hide();
    });

    var video = $("video");
    var thumbnail = $("#thumb");
    var input = $("#postvideo");
    // var ctx = thumbnail.get(0).getContext("2d");
    var duration = 0;
    var img = $("#thumb");

    input.on("change", function(e) {
        var file = e.target.files[0];
        // Validate video file type
        if (["video/mp4"].indexOf(file.type) === -1) {
            alert("Only 'MP4' video format allowed.");
            return;
        }
        // Set video source
        video.find("source").attr("src", URL.createObjectURL(file));
        // Load the video
        video.get(0).load();

        // Load metadata of the video to get video duration and dimensions
        video.on("loadedmetadata", function(e) {
            duration = video.get(0).duration;
            // Set canvas dimensions same as video dimensions
            thumbnail[0].width = video[0].videoWidth;
            thumbnail[0].height = video[0].videoHeight;
            // Set video current time to get some random image
            video[0].currentTime = Math.ceil(duration / 2);
            // Draw the base-64 encoded image data when the time updates
            video.one("timeupdate", function() {
                ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                img.attr("src", thumbnail[0].toDataURL());
            });
        });
        $('#vibox').show();
    });
});

$(function() {
    $('.emoji-picker').hide();
    $('#emoji').click(function() {
        $('.emoji-picker').click();
    });
});
;(function($, window, document, undefined) {
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '<?= base_url('assets/emoji/img/') ?>/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();

    // mansion js
    var mantionUsers = <?= $this->user->getUsernameJson(); ?>;
    $('.emoji-wysiwyg-editor').atwho({
        at: "@",
        data: mantionUsers
    });
    
})(jQuery, window, document);

// for GIF script
var topics = ["Cool", "Meme's", "Funny", "Space", "Laugh"];
function addSearchBtns() {
    $("#buttons").html("");
    for (i = 0; i < topics.length; i++) {
        var $button = $("<input type='button' class='btn btn-sm search-btn' />");
        $button.val(topics[i]);
        $("#buttons").append($button);
    }
}
addSearchBtns();
$(document).on("click", ".btnSearchGif", function() {
    $("#gifresults").html("");
    // Beginning API call
    var queryURL = "https://api.giphy.com/v1/gifs/search?";
    var query;
    var params = {
        q: query,
        limit: 6,
        api_key: "e5frANRyrhZN0icIH2vlPlB7cYx68knL",
        fmt: "json"
    };
    if ($(this).hasClass("search-btn")) {
        query = $(this).val();
    } else if ($("#user-search").val() !== "") {
        query = $("#user-search").val();
        topics.push(query);
        if (topics.length > 6) {
            topics.shift();
        }
        addSearchBtns();
    }
    params.q = query;

    if ($(this).hasClass("trending")) {
        queryURL = "https://api.giphy.com/v1/gifs/trending?";
        delete params.q;
    }
    $.ajax({
        url: queryURL + $.param(params),
        method: "GET",
        success: function(r) {
            for (i = 0; i < params.limit; i++) {
                var $img = $("<img>");
                var $div = $("<div>");
                var $rating = $("<h6>");
                var gifObj = r.data[i];
                var gif = gifObj.images;
                console.log(gif);

                // Image builder object
                $img.attr({
                    // "width": "200px",
                    src: gif.fixed_height_still.url,
                    "data-animate": gif.fixed_height.url,
                    "data-still": gif.fixed_height_still.url,
                    "data-state": "still",
                    "data-src": gif.original.url,
                    // src: gif.original.url,
                    class: "newgif"
                });
                // $div.attr("id", "gif-" + i);
                $div.addClass("gif-box col-sm-4");
                // $rating.text("Rating: " + gifObj.rating);
                $div.append($img, $rating);
                $("#gifresults").append($div);
            }

            $(".newgif").on("click", function() {
                var getGifUrl = $(this).attr("data-src");
                $('#gifresults .gif-box .gifBorder').removeClass('gifBorder');
                $(this).addClass('gifBorder');
                var gifArr = getGifUrl.split('?cid=');
                if (gifArr[0]) {
                    $('#gif').val(gifArr[0]);
                    $("#postGifView").show();
                    $("#postGifView").html('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + gifArr[0] + '" class="img-thumbnail"></div>');
                }

                var state = $(this).attr("data-state");
                var setgif = $(this).attr("src");
                // $('#gif').val(setgif);
                if (state === "still") {
                    $(this).attr("src", $(this).attr("data-animate"));
                    $(this).attr("data-state", "animate");
                } else {
                    $(this).attr("src", $(this).attr("data-still"));
                    $(this).attr("data-state", "still");
                }
            });
        }
    });
});
</script>
