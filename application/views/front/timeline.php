<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?>
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-6">
                            <?php
                            $loginUserID = $this->session->userdata('userID');
                            // echo "<pre>"; print_r($userDetails); die();
                            if(($loginUserID != $otherUserID) && (isset($userDetails['privateProfileStatus']) && $userDetails['privateProfileStatus']==1) && (isset($userDetails['isfollow']) && $userDetails['isfollow']==0)){
                                ?><div class="central-meta item">
                                    <div class="user-post">
                                        <h6 class="text-center">This Private Profile. </h6>
                                    </div>
                                </div><?php
                            }else{
                                $postListArr = array('posts','comments','media','likes');
                                if(in_array($pageType, $postListArr)){
                                    if($loginUserID == $otherUserID){
                                        $this->load->view('front/post_form');
                                    }
                                    if(!empty($postList)){
                                        ?><div class="loadMorePosts"><?php
                                        $count = 1;
                                        foreach($postList as $post){
                                            $datas['post'] = $post;
                                            $datas['count'] = $count;
                                            $datas['otherUserID'] = $otherUserID;
                                            $this->load->view('front/post_view',$datas);
                                            $count ++;
                                            //break;
                                        } ?></div>
                                        <input type="hidden" id="pageno" class="pageno" value="1" />
                                        <button class="postLoader btn-view btn-load-more" data-user="<?php echo $otherUserID; ?>" data-type="<?php echo $pageType; ?>">Load More</button>
                                    <?php }else{ ?>
                                        <div class="central-meta item">
                                            <div class="user-post">
                                                <h6 class="text-center"><?php echo getMessage('notFoundPosts'); ?></h6>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                }elseif($pageType=='about'){
                                    include('about.php');
                                }
                            } ?>
                        </div>
                        <div class="col-lg-3">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .followyou{
        bottom: 0;
        right: 0px;
    }
</style>