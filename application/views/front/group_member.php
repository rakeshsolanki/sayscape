<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?>
                        <!-- user profile banner -->
                        <div class="col-lg-12">
                            <div class="central-meta">
                                <div class="title-block">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="align-left">
                                                <h5>Groups Member <span><?= count($groupmember); ?></span></h5>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="text-right">
                                                <a class="btn btn-sm btn-info" href="<?= base_url('groups'); ?>"><h5><?= $groupDetails['groupName']; ?></h5></a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div><!-- title block -->
                            <div class="central-meta padding30">
                                <div class="row">
                                    <?php
                                    if (!empty($groupmember)) {
                                        foreach ($groupmember as $member) { ?>
                                            <div class="col-lg-2 col-md-4 col-sm-4" id="removeMember">
                                                <div class="group-box" >
                                                    <figure>
                                                        <?php
                                                        if (isset($member['profileImage']) && $member['profileImage'] != "") { ?>
                                                            <img class="wh120" src="<?php echo $member['profileImage']; ?>" alt="">
                                                        <?php } else { ?>
                                                            <img class="" src="<?= base_url('assets/'); ?>images/resources/group1.jpg" alt="">
                                                        <?php }
                                                        ?>
                                                    </figure>
                                                    <a href="<?php echo base_url('time-line/' . $member['username']); ?>" title="Chat this group"><?php echo $member['username'] ?></a>
                                                    <?php if(!empty($remove)){ ?>
                                                    <button><a href="javascript:void(0)" class="delete_data remove_group_member" id="<?php echo $member['userGroupID']; ?>">Remove</a></button>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                    <?php }
                                    } ?>
                                </div>
                                <!--                                    <span>Viewing 1-11 of 302 Groups</span>-->
                                <!-- <div class="lodmore">
                                    <button class="btn-view btn-load-more"></button>
                                </div> -->
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- content -->

<script>
    function remove_cart() {
        event.preventDefault();
        var btn = event.target;
        setTimeout(function() {
            $(btn)
                .closest("#removeMember")
                .fadeOut("fast");
        }, 100);
    }
    $(document).ready(function() {
        $(".remove_group_member").click(function() {
            var remove_group_member = $(this).attr('id');
            var btn = event.target;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteGroupMember') ?>',
                data: 'remove_group_member=' + remove_group_member,
                success: function(data) {
                    $(btn)
                .closest("#removeMember")
                .fadeOut("fast");
                }
            });
        });
    });
</script>