<?php
$userID = $this->session->userdata('userID');
$plusAccount = $this->user->checkUserPlusAccount($userID);
$customEmojis = $this->CommonModel->get_all(TBL_CUSTOM_EMOJI, ['*'], ['isActive' => 1, 'isDelete' => 0]);
?>
<form action="<?php echo base_url('home/createPost') ?>" method="post" enctype="multipart/form-data" id="postform">
<div class="central-meta postbox">
    <span class="create-post">Create post</span>
    <div class="new-postbox">
        <div class="newpst-input">
            <p class="lead emoji-picker-container">
                <textarea rows="2" id="postTextarea" placeholder="Say what’s on your mind" name="text" class="" data-emojiable="true" data-emoji-input="unicode"></textarea>
            </p>
        </div>
        <div class="attachments">
            <ul class="cursor-point">
                <?php
                if(!empty($plusAccount)){ ?>
                    <li id="">
                        <i class="" id="btnShowCustomEmoji" data-toggle="modal" data-target="#customEmojiModal"><b>Plus</b></i>
                    </li>
                <?php } ?>
                <li id="emoji">
                    <i class="fa fa-smile-o"></i>
                    <label class="fileContainer"> </label>
                </li>
                <li id="image">
                    <i class="fa fa-camera"></i>
                    <label class="fileContainer">
                        <input type="file" name="picture[]" id="files" multiple accept="image/*" onchange="readFile(this);">
                    </label>
                </li>
                <li id="video">
                    <i class="fa fa-video-camera"></i>
                    <label class="fileContainer">
                        <input type="file" name="video" id="postvideo" multiple accept="video/*">
                    </label>
                </li>
                <li>
                    <i class="fa fa-pie-chart" id="pollhide" data-toggle="modal" data-target="#pollModal"></i>
                    <label class="fileContainer">
                    </label>
                </li>
                <li id="">
                    <i class="" id="btnShowGif" data-toggle="modal" data-target="#gifModal"><b>GIF</b></i>
                </li>
                <li>
                    <input type="hidden" id="gif" name="gif" value="" />
                    <input type="hidden" id="postType" name="postType" value="<?php echo (isset($postType))?$postType:'0' ?>" />
                    <button class="btn webbgcolor text-white" type="submit" data-ripple="" name="post" value="POST" id="btnSavePost">Say</button>
                </li>
            </ul>
        </div>
        <div class="mt8 row">
            <div class="col-sm-6">
                <b>Expires On</b>
                <input type="date" class="form-control " name="expiryDateTime">
            </div>
            <div class="col-sm-6">
                <b>Limit: </b><b id="postCharcount">500</b>
                
                <!-- for meter -->
                <div id="wrapper">
                    <svg id="meter">
                        <circle id="outline_curves" class="circle outline"  cx="50%" cy="50%">
                        </circle>
                        <circle id="low" class="circle range" cx="50%" cy="50%" stroke="#FDE47F">
                        </circle>
                        <circle id="avg" class="circle range" cx="50%" cy="50%" stroke="#7CCCE5">
                        </circle>
                        <circle id="high" class="circle range" cx="50%" cy="50%" stroke="#E04644">
                        </circle>
                        <circle id="mask" class="circle" cx="50%" cy="50%" >
                        </circle>
                        <circle id="outline_ends" class="circle outline" cx="50%" cy="50%">
                        </circle>
                    </svg>
                    <img id="meter_needle" src="<?= base_url('assets/images/'); ?>svg-meter-gauge-needle.png" alt="" />
                    <input id="slider" type="hidden" min="0" max="100" value="0" /> 
                    <div id="slider"><div id="postcharbarbox"><div id="postcharbarboxInner"></div></div></div>
                    <label id="lbl" id="value" for="">0%</label>
                </div>

            </div>
        </div>
        <div class="mt8">
            <video controls class="viewPostvideo mt10" id="vibox" style="display: none;    width: 100%; height: 260px;">
                <source type="video/mp4">
                <canvas class="viewPostvideo mt10" id="thumb" style="display:block;"></canvas>
                <input type="hidden" id="video_thumb" name="video_thumb" value="" />
            </video>
        </div>
        <!-- <div id="status"></div> -->
        <div id="photos" class="row" style="display: none"></div>
        <div id="postGifView" class="row" style="display: none"></div>
    </div>
</div><!-- add post new box -->
</form>

<div class="modal fade" id="pollModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"><i class="fa fa-pie-chart"></i> Poll </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            <!-- action="<?php //echo base_url('home/savePollPost'); ?>" -->
                <form class="savePollPostForm" method="post">
                    <b class="text-black">Ask Question:</b> <input type="text" name="question" class="form-control" required>
                    <div class="input_fields_wrap">
                        <button class="add_field_button mt10 btn-sm">Add More Option</button>
                        <div><input type="text" class="form-control mt10" name="option[]" placeholder="Enter option" required></div>
                        <div><input type="text" class="form-control mt10" name="option[]" placeholder="Enter option" required></div>
                    </div>
                    <input class="btn webbgcolor text-white mt10" type="submit" name="poll" value="Save">
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="gifModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"><i class="fa fa-image"></i> Send GIF </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="buttons" style="display: none"></div>
                        <div class="search-bar">
                            <input type="text" name="query" class="form-control" id="user-search" placeholder="Search GIF" />
                            <div class="mt20">
                                <input id="submit" type="button" value="Search" class="btn btn-sm webbgcolor text-white btnSearchGif" />
                                <input type="button" class="trending btn btn-sm btn-danger btnSearchGif" value="See what's trending" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb">
                    <div id="gifresults" class="row p10"></div>
                </div>
                <div class="">
                    <input class="btn webbgcolor text-white" id="btnUploadGif" data-dismiss="modal" type="button" name="uploadGif" value="Send" />
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="customEmojiModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"><i class="fa fa-image"></i> Emoji </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row cemojiview">
                    <?php
                    if(!empty($customEmojis)){
                        foreach($customEmojis as $cemoji){ ?>
                            <div class="col-sm-2">
                                <a href="javascript:void(0)"><img class="cimg" src="<?php echo base_url().'assets/custom_emoji/'.$cemoji['emoji']; ?>" data-id="<?php echo $cemoji['emoji']; ?>" /></a>
                            </div>
                        <?php }
                    } ?>
                </div>
                <div class="p10">
                    <input class="btn webbgcolor text-white" id="btnAddEmoji" data-dismiss="modal" type="button" name="addEmoji" value="Use" />
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function readFile(input) {
    $("#status").html('Processing...');
    counter = input.files.length;
    for (x = 0; x < counter; x++) {
        if (input.files && input.files[x]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $("#photos").append('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + e.target.result + '" class="img-thumbnail"></div>');
            };
            reader.readAsDataURL(input.files[x]);
        }
    }
    if (counter == x) {
        $("#status").html('');
    }
}

$(function() {
    $('#postvideo').click(function() {
        $('#photos').hide();
        $('#postGifView').hide();
    });
    $('#pollhide').click(function() {
        $('#vibox').hide();
    });
    $('#files').click(function() {
        $('#vibox').hide();
        $('#photos').show();
    });
    $('#btnShowGif').click(function() {
        $('#photos').hide();
        $('#postGifView').hide();
        $('#vibox').hide();
    });

    var video = $("video");
    var thumbnail = $("#thumb");
    var input = $("#postvideo");
    var ctx = thumbnail.get(0).getContext("2d");
    var duration = 0;
    var img = $("#thumb");

    input.on("change", function(e) {
        var file = e.target.files[0];
        // Validate video file type
        if (["video/mp4"].indexOf(file.type) === -1) {
            alert("Only 'MP4' video format allowed.");
            return;
        }
        // Set video source
        video.find("source").attr("src", URL.createObjectURL(file));
        // Load the video
        video.get(0).load();

        // Load metadata of the video to get video duration and dimensions
        video.on("loadedmetadata", function(e) {
            duration = video.get(0).duration;
            // Set canvas dimensions same as video dimensions
            thumbnail[0].width = video[0].videoWidth;
            thumbnail[0].height = video[0].videoHeight;
            // Set video current time to get some random image
            video[0].currentTime = Math.ceil(duration / 2);
            // Draw the base-64 encoded image data when the time updates
            video.one("timeupdate", function() {
                ctx.drawImage(video[0], 0, 0, video[0].videoWidth, video[0].videoHeight);
                img.attr("src", thumbnail[0].toDataURL());
                $('#video_thumb').val(thumbnail[0].toDataURL());
            });
        });
        $('#vibox').show();
    });
});

$(function() {
    $('.emoji-picker').hide();
    $('#emoji').click(function() {
        $('.emoji-picker').click();
    });
});
;(function($, window, document, undefined) {
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '<?= base_url('assets/emoji/img/') ?>/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();

    // mansion js
    var mantionUsers = <?= $this->user->getUsernameJson(); ?>;
    $('.emoji-wysiwyg-editor').atwho({
        at: "@",
        data: mantionUsers
    });
    
    // $(".emoji-wysiwyg-editor").keyup(function(){
    $(document).on('keyup', '.emoji-wysiwyg-editor', function(e) {
        // var box = $(this).val();
        var box = $(this).text();
        var main = box.length *100;
        var value = (main / 500);
        var count= 500 - box.length;
        
        check_postcharcount(e);
        if(box.length <= 500){
            
            /*bind range slider event*/
            var slider = document.querySelector('#slider');
            var lbl = document.querySelector("#lbl");
            var mask = document.querySelector('#mask');
            var meter_needle =  document.querySelector('#meter_needle');
            var percent = (box.length*100)/500;
            document.getElementById("slider").value = percent;
            var meter_value = semi_cf - ((percent * semi_cf) / 100);
            mask.setAttribute('stroke-dasharray', meter_value + ',' + cf);
            meter_needle.style.transform = 'rotate(' + (270 + ((percent * 180) / 100)) + 'deg)';
            lbl.textContent = percent + '%';
            
            $('#postCharcount').html(count);
            $('#postcharbarboxInner').animate({
                "width": value+'%',
            }, 1);
        }else{
            e.preventDefault();
            return false;
        }
    });
    
    $(document).on('keydown paste', '.emoji-wysiwyg-editor', function(e) {
        check_postcharcount(e);
    });

    $(".cimg").on("click", function() {
        $('.cemojiview .col-sm-2 .cimg.gifBorder').removeClass('gifBorder');
        $(this).addClass('gifBorder');
    });

    $("#btnAddEmoji").on("click", function() {
        var selectedImgID = $('.cemojiview .cimg.gifBorder').attr('data-id');
        var selectedImg = $('.cemojiview .cimg.gifBorder').attr('src');
        // console.log(selectedImg);
        if(selectedImg || selectedImgID){
            var emojiHtml = '<span class="customEmjiName">('+selectedImgID+')</span>';
            emojiHtml += '<img class="wh25" src="'+selectedImg+'" />&nbsp;';
            $('.emoji-wysiwyg-editor').append(emojiHtml);
        }
    });

    /*$(document).delegate("#btnSavePost", "click", function (event){
        event.preventDefault();
        var postText = $('.emoji-wysiwyg-editor').val();
        var gif = $('#gif').val();
        var video = $('.viewPostvideo').attr('src');
        var images = $('#photos .img-thumbnail').attr('src');
        if(postText=='' && gif=='' && (!video || video=='') && (!images || images=='')){
            alert('Say what’s on your mind');
            $('.emoji-wysiwyg-editor').focus();
            return false;
        }
    });*/
})(jQuery, window, document);

// for GIF script
var topics = ["Cool", "Meme's", "Funny", "Space", "Laugh"];
function addSearchBtns() {
    $("#buttons").html("");
    for (i = 0; i < topics.length; i++) {
        var $button = $("<input type='button' class='btn btn-sm search-btn' />");
        $button.val(topics[i]);
        $("#buttons").append($button);
    }
}
addSearchBtns();
$(document).on("click", ".btnSearchGif", function() {
    $("#gifresults").html("");
    // Beginning API call
    var queryURL = "https://api.giphy.com/v1/gifs/search?";
    var query;
    var params = {
        q: query,
        limit: 6,
        api_key: "e5frANRyrhZN0icIH2vlPlB7cYx68knL",
        fmt: "json"
    };
    if ($(this).hasClass("search-btn")) {
        query = $(this).val();
    } else if ($("#user-search").val() !== "") {
        query = $("#user-search").val();
        topics.push(query);
        if (topics.length > 6) {
            topics.shift();
        }
        addSearchBtns();
    }
    params.q = query;

    if ($(this).hasClass("trending")) {
        queryURL = "https://api.giphy.com/v1/gifs/trending?";
        delete params.q;
    }
    $.ajax({
        url: queryURL + $.param(params),
        method: "GET",
        success: function(r) {
            for (i = 0; i < params.limit; i++) {
                var $img = $("<img>");
                var $div = $("<div>");
                var $rating = $("<h6>");
                var gifObj = r.data[i];
                var gif = gifObj.images;
                console.log(gif);

                // Image builder object
                $img.attr({
                    // "width": "200px",
                    src: gif.fixed_height_still.url,
                    "data-animate": gif.fixed_height.url,
                    "data-still": gif.fixed_height_still.url,
                    "data-state": "still",
                    "data-src": gif.original.url,
                    // src: gif.original.url,
                    class: "newgif"
                });
                // $div.attr("id", "gif-" + i);
                $div.addClass("gif-box col-sm-4");
                // $rating.text("Rating: " + gifObj.rating);
                $div.append($img, $rating);
                $("#gifresults").append($div);
            }

            $(".newgif").on("click", function() {
                var getGifUrl = $(this).attr("data-src");
                $('#gifresults .gif-box .gifBorder').removeClass('gifBorder');
                $(this).addClass('gifBorder');
                var gifArr = getGifUrl.split('?cid=');
                if (gifArr[0]) {
                    $('#gif').val(gifArr[0]);
                    $("#postGifView").show();
                    $("#postGifView").html('<div class="col-md-3 col-sm-3 col-xs-3"><img src="' + gifArr[0] + '" class="img-thumbnail"></div>');
                }

                var state = $(this).attr("data-state");
                var setgif = $(this).attr("src");
                // $('#gif').val(setgif);
                if (state === "still") {
                    $(this).attr("src", $(this).attr("data-animate"));
                    $(this).attr("data-state", "animate");
                } else {
                    $(this).attr("src", $(this).attr("data-still"));
                    $(this).attr("data-state", "still");
                }
            });
        }
    });
});
 
function check_postcharcount(e){
    if(e.which != 8 && $('.emoji-wysiwyg-editor').text().length >= 500) {
        e.preventDefault();
    }
}

/* set radius for all circles */
var r = 34;
var circles = document.querySelectorAll('.circle');
var total_circles = circles.length;
for (var i = 0; i < total_circles; i++) {
    circles[i].setAttribute('r', r);
}
/* set meter's wrapper dimension */
var meter_dimension = (r * 2) + 5;
var wrapper = document.querySelector('#wrapper');
wrapper.style.width = meter_dimension + 'px';
wrapper.style.height = meter_dimension + 'px';
/* add strokes to circles  */
var cf = 2 * Math.PI * r; /**2piR */
var semi_cf = cf / 2;
var semi_cf_1by3 = semi_cf / 3;
var semi_cf_2by3 = semi_cf_1by3 * 2;
document.querySelector('#outline_curves')
    .setAttribute('stroke-dasharray', semi_cf + ',' + cf);
document.querySelector('#low')
    .setAttribute('stroke-dasharray', semi_cf + ',' + cf);
document.querySelector('#avg')
    .setAttribute('stroke-dasharray', semi_cf_2by3 + ',' + cf);
document.querySelector('#high')
    .setAttribute('stroke-dasharray', semi_cf_1by3 + ',' + cf);
document.querySelector('#outline_ends')
    .setAttribute('stroke-dasharray', 2 + ',' + (semi_cf - 2));
document.querySelector('#mask')
    .setAttribute('stroke-dasharray', semi_cf + ',' + cf);
</script>