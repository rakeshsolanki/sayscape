<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?><!-- user profile banner -->
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-6">
                            <div class="central-meta">
                                <div class="editing-interest">
                                    <span class="create-post"><i class="ti-bell"></i> All Notifications </span>
                                    <div class="notification-box">
                                        <ul>
                                        <?php if(!empty($notificationList)){
                                            foreach($notificationList as $notification){
                                                $username = $this->user->getUserNamerow($notification['userID']);
                                                $url='#';
                                                if($notification['type']==0){
                                                    $url= base_url('post-details/'.$notification['redirectID']);
                                                }elseif($notification['type']==1){
                                                    $url= base_url('time-line/'.$username['username']);
                                                }elseif($notification['type']==2){
                                                    $url= base_url('message-box/'.$notification['redirectID']);
                                                }
                                                if($notification['image']=='defaultNotificationImage.png'){
                                                    $notificationImg = base_url().'upload/notification/'.$notification['image'];
                                                }else{
                                                    $notificationImg = base_url().'upload/userprofile/'.$notification['image'];
                                                }
                                                // echo "<pre>"; print_r($notification); echo "</pre>";
                                                $notifyText = isset($notification['text']) ? $this->emoji->Decode($notification['text']):'';
                                                $notifyTime = (isset($notification['createdDateTime'])) ? $notification['createdDateTime'] : "";
                                                $imagArr = array();
                                                if(isset($notification['totalCount']) && $notification['totalCount']>1){
                                                    $filterData = array(
                                                        'userID'=>$notification['userID'],
                                                        'otherID'=>$notification['otherID'],
                                                        'notifyType'=>$notification['notifyType']
                                                    );
                                                    $getRelatedData = $this->notification->getRelatedNotificationList($filterData);
                                                    if(!empty($getRelatedData)){
                                                        // echo "<pre>"; print_r($getRelatedData); die();
                                                        $totalusercount = (count($getRelatedData) - 1);
                                                        $notifyText = (isset($getRelatedData[0]['text']))?$this->emoji->Decode($getRelatedData[0]['text']):$notifyText;
                                                        $notifyText = str_replace("has","and ".$totalusercount." others",$notifyText);
                                                        $notificationImg = (isset($getRelatedData[0]['image']))?base_url().'upload/userprofile/'.$getRelatedData[0]['image']:$notificationImg;
                                                        $notifyTime = (isset($getRelatedData[0]['createdDateTime']))?$getRelatedData[0]['createdDateTime']:$notifyTime;
                                                        foreach($getRelatedData as $key=>$rel){
                                                            $anotherUdata = $this->CommonModel->get_row(TBL_USER, ["userID", "username", "name","profileImage"], ["userID" => $rel['senderID']]);
                                                            if($rel['senderID']!=$getRelatedData[0]['senderID']){
                                                                $imagArr[] = array(
                                                                    'userID' => $rel['senderID'],
                                                                    'profileImage' => (isset($anotherUdata['profileImage']))?base_url().'upload/userprofile/'.$anotherUdata['profileImage']:"",
                                                                    'profileLink' => (isset($anotherUdata['username']))?base_url('time-line/'.$anotherUdata['username']):"javascript:void(0)",
                                                                );
                                                            }
                                                            if($key > 4){ break; }
                                                        }
                                                    }
                                                } ?>
                                                <li>
                                                    <figure style="background-color: black;">
                                                    <img src="<?= $notificationImg; ?>" alt=""></figure>
                                                    <div class="notifi-meta">
                                                        <!-- <h6><?php //echo isset($notification['title']) ? $notification['title']:''; ?></h6> -->
                                                        <a href="<?= $url; ?>"><p class="nomar"><?= $notifyText; ?></p></a>
                                                        <?php if(!empty($imagArr)){ ?>
                                                            <div class="usrImgList">
                                                            <?php foreach($imagArr as $img){ ?>
                                                                <a href="<?php echo $img['profileLink'] ?>">
                                                                <img src="<?php echo $img['profileImage'] ?>" />
                                                                </a>
                                                            <?php } ?>
                                                            </div>
                                                        <?php } ?>
                                                        <span class="displayMomentDateOnly" datetime="<?php echo $notifyTime; ?>"></span>
                                                    </div>
                                                    <!-- <i class="del ti-close visible" title="Remove"></i> -->
                                                </li>
                                            <?php }} ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>	
                        </div>
                        <div class="col-lg-3">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
				</div>
            </div>
        </div>
    </div>
</section>