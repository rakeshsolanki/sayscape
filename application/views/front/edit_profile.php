<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
						<?php include('profile_topbar.php'); ?>
						<?php include('topfive_user.php');  ?>
						<?php $loginUserID = $this->session->userdata('userID'); 
						// echo "<pre>"; print_r($userDetails);  echo "</pre>"; ?>						
						<div class="col-lg-9">
							<div class="central-meta">
								<div class="about" id="userEditProfile">
									<div class="d-flex flex-row mt-2">
										<ul class="nav nav-tabs nav-tabs--vertical nav-tabs--left" >
											<li class="nav-item">
												<a href="#edit-profile" class="nav-link active" data-toggle="tab" ><i class="fa fa-pencil"></i> Edit Profile</a>
											</li>
											<li class="nav-item">
												<a href="#gen-setting" class="nav-link" data-toggle="tab" ><i class="fa fa-gear"></i> General Setting</a>
											</li>
											<li class="nav-item">
												<a href="#theme-setting" class="nav-link" data-toggle="tab" ><i class="fa fa-gear"></i> Theme Setting</a>
											</li>
											<li class="nav-item">
												<a href="#block-user" class="nav-link" data-toggle="tab" ><i class="fa fa-ban"></i> Blocked user </a>
											</li>
											<li class="nav-item">
												<a href="#remove-all-user" class="nav-link" data-toggle="tab" ><i class="fa fa-user-times"></i> Remove All posts</a>
											</li>
											<!-- <li class="nav-item">
												<a href="#plus-accounts" class="nav-link" data-toggle="tab" ><i class="fa fa-graduation-cap"></i> Plus accounts </a>
											</li> -->
											<li class="nav-item">
												<a href="<?= base_url('privacy-policy'); ?>" class="nav-link "><i class="fa fa-shield"></i> Privacy &amp; Data</a>
											</li>
											
										</ul>
										<div class="tab-content">
											<div class="tab-pane fade show active" id="edit-profile" >
												<form method="post" class="c-form" enctype="multipart/form-data">
												<div class="set-title">
													<h5>Edit Profile</h5>
												</div>
												<div class="setting-meta">
													<div class="change-photo">
														<figure>
															<img id="editProfilePic" src="<?php echo (!empty($userDetails['profileImage']))?$userDetails['profileImage']:base_url('assets/images/resources/admin.png'); ?>" alt="" class="">
														</figure>
														<div class="edit-img">
															<label class="fileContainer">
																<i class="fa fa-camera-retro"></i> Chage Profile Picture
																<input type="file" name="profileImage" onchange="readFile(this,'profile');" />
															</label>
														</div>
													</div>
												</div>
												<div class="setting-meta pt10">
													<div class="change-photo">
														<div class="edit-img">
															<label class="fileContainer">
																 Custom Profile Frames 
																<input type="text" class="jscolor" id="customFrame"   value="<?= (!empty($userSetting['customFrame'])) ? $userSetting['customFrame']:'fffff' ?>" name="bordercolor">
															</label>
														</div>
													</div>
												</div>
												
												<div class="setting-meta pt10">
													<div class="change-photo">
														<figure>
															<img id="editCoverPic" class="coverImg" src="<?php echo (!empty($userDetails['coverImage']) && $userDetails['coverImage'])?$userDetails['coverImage']:base_url('assets/images/resources/profile-image.jpg'); ?>" alt="">
														</figure>
														<div class="edit-img">
															<label class="fileContainer">
																<i class="fa fa-camera-retro"></i> Chage Cover Image
																<input type="file" name="coverImage" onchange="readFile(this,'cover');" />
															</label>
														</div>
													</div>
												</div>
												<div class="stg-form-area">
													<div id="divName">
														<label>Display Name</label>
														<div class="emojiBtnView"><i class="emojiPicker fa fa-smile-o"></i></div>
														<p class="lead emoji-picker-container">
															<input name="name" required="required" type="text" class="" placeholder="Jack Carter" data-emojiable="true" data-emoji-input="unicode" value="<?php if(isset($userDetails['name'])){ echo $this->emoji->Decode($userDetails['name'],true); } ?>" />
														</p>
													</div>
													<div class="">
														<label>User Name</label>
														<!-- <span>www.pitnik.com/</span> -->
														<input name="username" required="required" type="text" placeholder="John Doe" value="<?php if(isset($userDetails['username'])){ echo $userDetails['username']; } ?>" />
													</div>
													<div id="divAboutYou">
														<label>About You</label>
														<div class="emojiBtnView"><i class="emojiPicker2 fa fa-smile-o"></i></div>
														<p class="lead emoji-picker-container">
															<textarea class="" name="about" rows="2" placeholder="write someting about yourself" data-emojiable="true" data-emoji-input="unicode">
															<?php if(isset($userDetails['about'])){ echo $this->emoji->Decode($userDetails['about'],true); } ?></textarea>
														</p>
													</div>
													<div>
														<label>Email Address</label>
														<input type="text" name="email" required placeholder="abc@mail.com" value="<?php if(isset($userDetails['email'])){ echo $userDetails['email']; } ?>" />
													</div>
													<div>
														<label>Birth Date</label>
														<input type="date" id="dob" name="dob" value="<?php if(isset($userDetails['dob'])){ echo $userDetails['dob']; } ?>"/>
													</div>
													<div>
														<label>Phone</label>
														<input type="text" name="phone" value="<?php if(isset($userDetails['phone'])){ echo $userDetails['phone']; } ?>" />
													</div>
												
													<div>
														<label>Website</label>
														<input type="text" name="website" placeholder="www.google.com" value="<?php if(isset($userDetails['website'])){ echo $userDetails['website']; } ?>" />
													</div>
													<div>
														<label>Location</label>
														<input type="text" name="location" placeholder="Ex. San Francisco, CA" value="<?php if(isset($userDetails['location'])){ echo $userDetails['location']; } ?>" />
													</div>
													<div>
														<button type="submit" name="btnSubmitProfile" value="Save" data-ripple="">Save</button>
													</div>
												</div>
												</form>
											</div><!-- edit profile -->
											<div class="tab-pane fade" id="gen-setting">
												<div class="set-title">
													<h5>General Setting</h5>
												</div>
												<div class="onoff-options ">
													<form method="post">
														<div class="">
															<label>Blacklist Keyword</label>
															<textarea rows="2" name="blacklistKeyword" class="form-control" placeholder="Enter Keyword"><?php echo (isset($userSetting['blacklistKeyword']))?$userSetting['blacklistKeyword']:''; ?></textarea><br>
														</div>
														<div class="setting-row">
															<p>Make Profile Private</p>
															<input type="checkbox" id="privateProfileStatus" name="privateProfileStatus" value="1" <?php echo (isset($userSetting['privateProfileStatus']) && $userSetting['privateProfileStatus']==1)?'checked':''; ?> /> 
															<label for="privateProfileStatus" data-on-label="ON" data-off-label="OFF"></label>
														</div>
														<div class="setting-row">
															<p>Turn On Notifiaction</p>
															<input type="checkbox" id="notificationStatus" name="notificationStatus" value="1" <?php echo (isset($userSetting['notificationStatus']) && $userSetting['notificationStatus']==1)?'checked':''; ?> /> 
															<label for="notificationStatus" data-on-label="ON" data-off-label="OFF"></label>
														</div>
														<div class="setting-row">
															<p>Turn off Private Messaging</p>
															<input type="checkbox" id="privateMessageStatus" name="privateMessageStatus" value="1" <?php echo (isset($userSetting['privateMessageStatus']) && $userSetting['privateMessageStatus']==1)?'checked':''; ?> /> 
															<label for="privateMessageStatus" data-on-label="ON" data-off-label="OFF"></label>
														</div>
														<div class="setting-row">
															<p>Hide Birthday</p>
															<input type="checkbox" id="hideBirthday" name="hideBirthday" value="1" <?php echo (isset($userSetting['hideBirthday']) && $userSetting['hideBirthday']==1)?'checked':''; ?> /> 
															<label for="hideBirthday" data-on-label="ON" data-off-label="OFF"></label>
														</div>
														<div class="setting-row">
															<p>Notification Follow Only </p>
															<input type="checkbox" id="isNotificationFollowOnly" name="" value="1" <?php echo (isset($userSetting['isNotificationFollowOnly']) && $userSetting['isNotificationFollowOnly']==1)?'checked':''; ?> /> 
															<label for="isNotificationFollowOnly" data-on-label="ON" data-off-label="OFF"></label>
														</div>
														<div class="setting-row">
															<p>Dark Mode </p>
															<input type="checkbox" id="isDarkmode" name="isDarkmode" value="1" <?php echo (isset($userSetting['isDarkmode']) && $userSetting['isDarkmode']==1)?'checked':''; ?> /> 
															<label for="isDarkmode" data-on-label="ON" data-off-label="OFF"></label>
														</div>
														<!-- <div class="setting-row">
															<p>Night Mode</p>
															<input type="checkbox" id="switch03" /> 
															<label for="switch03" data-on-label="ON" data-off-label="OFF"></label>
														</div> -->
														<div class="submit-btns">
															<button type="submit" class="main-btn" name="btnSubmitSetting" value="Save" data-ripple=""><span>Save</span></button>
														</div>
													</form>
												</div>
												<div class="account-delete">
													<h5>Account Changes</h5>
													<!-- <div>
														<span>Hide Your Posts and profile </span>
														<button type="button" class=""><span>Deactivate account</span></button>
													</div>	 -->
													<div>
														<span>Delete your account and data </span>
														<button type="button" class=""><span>close account</span></button>
													</div>
												</div>
											</div><!-- general setting -->
											<div class="tab-pane fade" id="block-user">
												<div class="row">
													<?php
														if (!empty($blockuserlist)) {
															foreach ($blockuserlist as $userdata) { 
																$member = $this->user->getUserdetails($userdata['blockUserID']);
																//echo '<pre>';print_r($member); ?>
																<div class="col-lg-2 col-md-4 col-sm-4" id="removeMember">
																	<div class="group-box" >
																	<figure>
																	<a href="<?php  echo base_url('time-line/' . $member['username']); ?>" title="Block User">
																			<?php
																			if (isset($member['profileImage']) && $member['profileImage'] != "") { ?>
																				<img class="wh-70" src="<?php echo $member['profileImage']; ?>" alt="">
																			<?php } else { ?>
																				<img class="wh-70" src="<?= base_url('assets/'); ?>images/resources/group1.jpg" alt="">
																			<?php }
																			?>
																		</a></figure>
																		<a href="<?php  echo base_url('time-line/' . $member['username']); ?>" title="Block User"><?php echo $member['username'] ?></a>
																		<button class="m-0"><a href="javascript:void(0)" class="delete_data remove_block_member" id="<?php echo $member['userID']; ?>">Unblock</a></button>
																		
																	</div>
																</div>
														<?php } }else{ echo '<h6>Record not found</h6>'; } ?>
													<?php // echo '<pre>';print_r($blockuser); ?>
												</div>
											</div>
											<div class="tab-pane fade" id="remove-all-user">
												<div class="row">
													<div class="set-title">
														<h5>Remove All Post</h5>
														<span>You once thought. Because once deleted, your post will not be returned.</span>
														<span>Total Post : <b id="totalpost"> <?php echo isset($totalPostCount)?$totalPostCount:0; ?>  </b></span>
													</div>
													<?php if(!empty($totalPostCount) && $totalPostCount > 0){ ?>
														<a href="javascript:void(0)" class="btn webbgcolor text-white remove_all_post" id="<?php echo $loginUserID; ?>">Remove All</a>
													<?php } ?>
												</div>
											</div>
											
											<div id="theme-setting" class="tab-pane fade">
												<div class="set-title"  data-toggle="collapse" data-target="#demo">
													<h5>Theme Setting &nbsp;&nbsp;&nbsp;<i class="fa fa-pencil"></i> </h5>
												</div>
												<div class="central-meta " id="demo">
													<div class="color-palet">
														<form action="" method="post" >
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4">
																	<div class="color-box theme-box1">
																	</div>
																	<input type="radio" name="themecolor" value="f81212" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='f81212') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">
																	<div class="color-box theme-box2">
																		
																	</div>
																	<input type="radio" name="themecolor" value="1238f8" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='1238f8') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box3">
																		
																	</div>
																	<input type="radio" name="themecolor" value="f012f8" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='f012f8') ?'checked':'' ?>>
																</div>
																
															</div>
															<hr>
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box4">
																		
																	</div>
																	<input type="radio" name="themecolor" value="1af812" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='1af812') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box5">
																		
																	</div>
																	<input type="radio" name="themecolor" value="f87d12" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='f87d12') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box6">
																		<!-- <span>#34465d</span> -->
																	</div>
																	<input type="radio" name="themecolor" value="f8f812" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='f8f812') ?'checked':'' ?>>
																</div>
															</div>
															<hr>
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box7">
																	</div>
																	<input type="radio" name="themecolor" value="12d9f8" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='12d9f8') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box8">
																		
																	</div>
																	<input type="radio" name="themecolor" value="8512f8" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='8512f8') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box9">
																		<!-- <span>#34465d</span> -->
																	</div>
																	<input type="radio" name="themecolor" value="12b1f8" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='12b1f8') ?'checked':'' ?>>
																</div>
															</div>
															<hr>
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box10">
																		
																	</div>
																	<input type="radio" name="themecolor" value="12f86e" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='12f86e') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box11">
																		
																	</div>
																	<input type="radio" name="themecolor" value="0000e8" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='0000e8') ?'checked':'' ?>>
																</div>
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-box12">
																		<!-- <span>#34465d</span> -->
																	</div>
																	<input type="radio" name="themecolor" value="08950c" <?= (isset($userSetting['themeColor']) && $userSetting['themeColor']=='08950c') ?'checked':'' ?>>
																</div>
																
															</div>
															<hr>
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4">	
																	<div class="color-box theme-default">
																		<span>Default</span>
																	</div>
																	<input type="radio" name="themecolor" value="5a0d28" <?= (empty($userSetting['themeColor']) || $userSetting['themeColor']=='5a0d28') ?'checked':'' ?>>
																</div>
															</div>
															<div class="submit-btns">
																<button type="submit" class="main-btn" name="btnthemechange" value="Save" data-ripple=""><span>Save</span></button>
															</div>
														</form>
													</div>
												</div>
												<div class="set-title"  data-toggle="collapse" data-target="#posts">
													<h5>Highlighted posts in a custom color border &nbsp;&nbsp;&nbsp;<i class="fa fa-pencil"></i> </h5>
												</div>
												<div class="central-meta collapse" id="posts">
													<div class="card-body text-center d-flex justify-content-center align-items-center flex-column">
														<p>Custom color border</p>
														<div class=" row">
														<div class="col-sm-12">
																<div class="color-box ">
																	<div class="setting-row ">
																		<input type="checkbox" id="highlightStatus" name="highlightStatus"  value="1" <?= (!empty($userSetting['highlightStatus']) && $userSetting['highlightStatus'] ==1) ? 'checked':'' ?> /> 
																		<label for="highlightStatus" id="highlightStatus" data-on-label="ON" data-off-label="OFF"></label>
																	</div>
																</div>
																<form class="c-form">
																	<label class="fileContainer">
																		<input type="button" class="jscolor btn" id="highlightPost"   value="<?= (!empty($userSetting['highlightPost'])) ? $userSetting['highlightPost']:'fffff' ?>" name="bordercolor">
																	</label>
																</form>
															</div>
														</div>
														<p class="rect" id="myDiv">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
						</div>

						<div class="col-sm-3 ">
							<aside class="sidebar">
								<div class="widget">
									<h4 class="widget-title">Additional Pictures  </h4>
									<?php 
										$imgurl = base_url('assets/images/resources/album1.jpg');
										if(!empty($aditionalImage)){
											$aditionalID1 = $aditionalImage[0]['aditionalID'];
											$aditionalID2 = isset($aditionalImage[1]['aditionalID']) ? $aditionalImage[1]['aditionalID']:'';
											$imgurl1 = isset($aditionalImage[0]['filename']) ? base_url(ADITIONAL_IMG.$aditionalImage[0]['filename']):'';
											$imgurl2 = isset($aditionalImage[1]['filename']) ? base_url(ADITIONAL_IMG.$aditionalImage[1]['filename']):'';
										}
									?>

									<div class="">
										<label class="fileContainer btn">
											<img class="aditional-img" id="img1" src="<?= !empty($imgurl1) ? $imgurl1: $imgurl ?>" alt="" >
											<input class="aditional" name="picture" id="myFile" type="file" data-types="img1"  data-id="<?= isset($aditionalID1) ? $aditionalID1:''; ?>"  >
											<span class="btn main-btn ">Upload New</span>
										</label>
									</div>
									<div class="">
										<label class="fileContainer btn">
											<img class="aditional-img" id="img2" src="<?= !empty($imgurl2) ? $imgurl2 : $imgurl ?>" alt="" >
											<input class="aditional" name="picture" id="myFile" type="file" data-types="img2"  data-id="<?= isset($aditionalID2) ? $aditionalID2:''; ?>"  >
											<span class="btn main-btn ">Upload New</span>
										</label>
									</div>
									
								</div>
							</aside>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

function readFile(input, type) {
    counter = input.files.length;
    for (x = 0; x < counter; x++) {
        if (input.files && input.files[x]) {
            var reader = new FileReader();
            reader.onload = function(e) {
				if(type=='cover'){
					$("#editCoverPic").attr('src',e.target.result);
				}else{
					$("#editProfilePic").attr('src',e.target.result);
				}
            };
            reader.readAsDataURL(input.files[x]);
        }
    }
}


document.getElementById("myDiv").style.boxShadow = "inset 0 0 " + 30 +"px #<?php echo (!empty($userSetting['highlightPost'])) ? $userSetting['highlightPost']:'red' ?>";
document.getElementById("editProfilePic").style.border = "6px solid #<?php echo (!empty($userSetting['customFrame'])) ? $userSetting['customFrame']:'fffff' ?>";


;(function($, window, document, undefined) {
	$(document).ready(function() {
		// $(".emoji-picker-icon").attr("style", "display:block !important")
		$('.emoji-picker').hide();
		
        $(".remove_block_member").click(function() {
            var userid = $(this).attr('id');
            var btn = event.target;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('user/unBlockUser') ?>',
                data: 'userid=' + userid,
                success: function(data) {
                    $(btn)
                .closest("#removeMember")
                .fadeOut("fast");
                }
            });
        });
		
        $(".remove_all_post").click(function() {
            var userid = $(this).attr('id');
            var btn = event.target;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('user/removeAllPosts') ?>',
                data: 'userid=' + userid,
                success: function(data) {
                   $('#totalpost').html(0);
				   $(btn).hide();
                }
            });
        });

		

		$(".emojiPicker").click(function(event) {
			event.preventDefault();
			$(this).closest('#divName').find('.emoji-picker-icon').click();
		});

		$(".emojiPicker2").click(function(event) {
			event.preventDefault();
			$(this).closest('#divAboutYou').find('.emoji-picker-icon').click();
		});
    });
})(jQuery, window, document);
</script>
<script src="<?= base_url('assets/js/jscolor.js') ?>"></script>