<?php

$loginUserID = $this->session->userdata('userID');
?>
<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?>
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-9">
                            <div class="sidebar">
                                <div class="widget">
                                    <h4 class="widget-title">#Tags Trending:</h4>
                                    <div class="hash-tags">
                                        <?php  if(!empty($hashtagTrendingList)){
                                        foreach($hashtagTrendingList as $hashtagTrending){
                                            echo $this->emoji->Decode($hashtagTrending);
                                            }} ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>