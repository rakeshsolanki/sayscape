<?php
$loginUserID = $this->session->userdata('userID');
$watchUserID = (isset($watchUserID)) ? $watchUserID : $loginUserID;
$userDetails = $this->user->getUserdetails($watchUserID, $loginUserID);
$page = (isset($pageType))?$pageType:"";
$otherUser = (isset($otherUsername)) ? '/'.$otherUsername : '';
$coverImage = (!empty($userDetails['coverImage']) && $userDetails['coverImage'])?$userDetails['coverImage']:base_url('assets/images/resources/profile-image.jpg');
$muteUser = $this->CommonModel->get_row(TBL_MUTEUSER, [], ['userID' => $loginUserID, 'muteUserID' => $watchUserID]);
$blockUser = $this->CommonModel->get_row(TBL_BLOCKUSER, [], ['userID' => $loginUserID, 'blockUserID' => $watchUserID]);
$subscribuser = $this->CommonModel->get_row(TBL_POST_SUBSCRIBE, [], ['userID' => $loginUserID, 'followerID' => $watchUserID]);
//$userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $watchUserID, 'isActive' => 1, 'isDelete' => 0]);
//echo "<pre>"; print_r($blockUser); die();
$segment = $this->uri->segment(1);
//echo $segment;
$plususer = $this->user->checkUserPlusAccount($watchUserID,array('customFrame'=>'customFrame'));
if($segment == 'time-line'){
    if(!empty($plususer)){
        $aditionalImg = $this->CommonModel->get_all(TBL_ADITIONAL_IMG, [], ['userID' => $watchUserID, 'isActive' => 1, 'isDelete' => 0]);
        $imgurl = base_url('assets/images/resources/album1.jpg');
        if(!empty($aditionalImg)){
            // $aditionalID1 = $aditionalImg[0]['aditionalID'];
            $aditionalID2 = isset($aditionalImg[1]['aditionalID']) ? $aditionalImg[1]['aditionalID']:'';
            // $imgurl1 = isset($aditionalImg[0]['filename']) ? base_url(ADITIONAL_IMG.$aditionalImg[0]['filename']):'';
            $imgurl2 = isset($aditionalImg[1]['filename']) ? base_url(ADITIONAL_IMG.$aditionalImg[1]['filename']):'';
        } ?>
    <style>
        .customFrame{
            border: 9px solid #<?= $plususer['customFrame']; ?> !important;
        }
    </style>

   <?php } } ?>

<div class="user-profile">
    <figure class="profile-bg">
        <img class="coverImg" src="<?php echo $coverImage; ?>" alt="">
        <ul class="profile-controls">
            <!-- <li><a href="#" title="Add friend" data-toggle="tooltip"><i class="fa fa-user-plus"></i></a></li> -->
            <?php
            if ($userDetails['userID'] != $loginUserID) {  
                if(empty($blockUser['isActive'])){ ?>
                <li>
                    <form class="" action="<?= base_url('user/subscribeUserPost'); ?>" method="POST">
                        <input type="hidden" name="followerID" value="<?= isset($userDetails['userID']) ? $userDetails['userID']:''; ?>">
                        <input type="hidden" name="status" value="<?= (isset($subscribuser['status']) && $subscribuser['status']==1) ? 0:'1' ?>">
                        <button class="mutebtn <?= (empty($subscribuser['status'])) ? 'unmute' :'' ?>" 
                        title='Subscribe' href="javascript:void(0)" type="submit" ><i class="fa fa-bell"></i></button>
                    </form>
                </li>
                <?php } ?>
                <li>
                    <?php
                    if ($userDetails['isfollow'] == 1) { ?>
                        <a href="javascript:void(0)" class="live btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="0" id="userUnfollow<?php echo $userDetails['userID']; ?>" data-toggle="tooltip" title="Following"><i class="fa fa-user-times"></i></a>
                        <a href="javascript:void(0)" class=" btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="1" id="userFollow<?php echo $userDetails['userID']; ?>" style="display: none" data-toggle="tooltip" title="Follow"><i class="fa fa-user-plus"></i></a>
                    <?php } elseif ($userDetails['isfollow'] == 4) { ?>
                        <a href="javascript:void(0)" class="live btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="0" id="userUnfollow<?php echo $userDetails['userID']; ?>" data-toggle="tooltip" title="Request"><i class="fa fa-user-plus"></i></a>
                        <a href="javascript:void(0)" class="btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="4" id="userFollow<?php echo $userDetails['userID']; ?>" style="display: none" data-toggle="tooltip" title="Follow"><i class="fa fa-user-plus"></i></a>
                        <?php } else {
                        if ($userDetails['privateProfileStatus'] == 1) { ?>
                            <a href="javascript:void(0)" class="live btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="0" id="userUnfollow<?php echo $userDetails['userID']; ?>" style="display: none" data-toggle="tooltip" title="Request"><i class="fa fa-user-plus"></i></a>
                            <a href="javascript:void(0)" class="btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="4" id="userfollowRequest<?php echo $userDetails['userID']; ?>" data-toggle="tooltip" title="Follow"><i class="fa fa-user-plus"></i></a>
                        <?php } else { ?>
                            <a href="javascript:void(0)" class="live btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="0" id="userUnfollow<?php echo $userDetails['userID']; ?>" style="display: none" data-toggle="tooltip" title="Following"><i class="fa fa-user-times"></i></a>
                            <a href="javascript:void(0)" class="btnFollowUnfollow" data-id="<?php echo $userDetails['userID']; ?>" data-status="1" id="userFollow<?php echo $userDetails['userID']; ?>" data-toggle="tooltip" title="Follow"><i class="fa fa-user-plus"></i></a>
                        <?php } ?>
                    <?php } ?>
                </li>
                <li><a class="" href="<?php echo base_url('message-box/'.$userDetails['userID']); ?>" title="Send Message" data-toggle="tooltip"><i class="fa fa-comment"></i></a></li>
                <?php if($userDetails['userID'] != $loginUserID){ ?>
                <li>
                    <form class="" action="<?= base_url('home/muteUnmuteUser'); ?>" method="POST">
                        <input type="hidden" name="muteUserID" value="<?= isset($userDetails['userID']) ? $userDetails['userID']:''; ?>">
                        <input type="hidden" name="status" value="<?= (isset($muteUser['isActive']) && $muteUser['isActive']==1) ? 0:'1' ?>">
                        <button class="mutebtn <?= (empty($muteUser['isActive'])) ? 'unmute' :'' ?>" 
                        title='Mute' href="javascript:void(0)" type="submit" ><i class="fa fa-bell-slash-o"></i></button>
                    </form>
                </li>
                <li>
                    <form class="muteform" action="<?= base_url('home/blockUnblockUser'); ?>" method="POST">
                        <input type="hidden" name="blockUserID" value="<?= isset($userDetails['userID']) ? $userDetails['userID']:''; ?>">
                        <input type="hidden" name="status" value="<?= (isset($blockUser['isActive']) && $blockUser['isActive']==1) ? 0:'1' ?>">
                        <button class="mutebtn <?= (empty($blockUser['isActive'])) ? 'unmute' :'' ?>" title='Block' href="javascript:void(0)" type="submit"><i class="fa fa-ban"></i></button>
                    </form>
                </li>

            <?php } } ?>
        </ul>
    </figure>

    <div class="profile-section">
        <div class="row">
            <div class="col-lg-2 col-md-3">
                <div class="profile-author">
                    <a class="profile-author-thumb" href="javascript:void(0)">
                        <img src="<?php echo (!empty($userDetails['profileImage']))?$userDetails['profileImage']:base_url('assets/images/resources/admin.png'); ?>" alt="" class="customFrame">
                    </a>
                    <?php
                    if(!empty($plususer)){ ?>
                        <a class="profile-author-thumb userPlusImg" href="javascript:void(0)">
                        <img src="<?php echo base_url('assets/images/plus_icon.png'); ?>" alt="" class=""></a>
                    <?php } ?>
                    <div class="author-content">
                        <a class="h4 author-name" href="#">
                        <?php if (isset($userDetails['name'])) {
                            echo $this->emoji->Decode(ucfirst($userDetails['name']));
                        }
                        if(isset($userDetails['isOffical']) && $userDetails['isOffical']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/blue.png'); ?>" /><?php }
                        if(isset($userDetails['isVeteran']) && $userDetails['isVeteran']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/green.png'); ?>" /><?php }
                        /* if(isset($userDetails['isResponder']) && $userDetails['isResponder']==1){ ?><img class="badgeIcon" src="<?= base_url('/assets/images/icon/red.png'); ?>" /><?php } */
                        ?></a>
                        <div>@<?php echo ($userDetails['username'])?$userDetails['username']:""; ?></div>
                        <!-- <div class="country"><?php //echo (isset($userDetails['location']))?$userDetails['location']:""; ?></div> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-10 col-md-9">
                <ul class="profile-menu">
                    <li>
                        <a class="<?php echo ($page =='about')?'active':'' ?>" href="<?php echo base_url('time-line').$otherUser.'?type=about'; ?>">About</a>
                    </li>
                    <li>
                        <a class="<?php echo ($page =='posts')?'active':'' ?>" href="<?php echo base_url('time-line').$otherUser; ?>">Posts</a>
                    </li>
                    <li>
                        <a class="<?php echo ($page =='media')?'active':'' ?>" href="<?php echo base_url('time-line').$otherUser.'?type=media'; ?>">Media</a>
                    </li>
                    <li>
                        <a class="<?php echo ($page =='comments')?'active':'' ?>" href="<?php echo base_url('time-line').$otherUser.'?type=comments'; ?>">Comments</a>
                    </li>
                    <li>
                        <a class="<?php echo ($page =='likes')?'active':'' ?>" href="<?php echo base_url('time-line').$otherUser.'?type=likes'; ?>">Likes</a>
                    </li>
                    <?php
                    if ($watchUserID == $loginUserID) { ?>
                        <li>
                            <a class="<?php echo ($page =='groups')?'active':'' ?>" href="<?php echo base_url('groups'); ?>">Groups</a>
                        </li>
                        <!-- <li>
                            <a class="" href="<?php echo base_url('message'); ?>">messages</a>
                        </li> -->
                    <?php } ?>
                    <!--  -->
                    <!-- <li>
                        <div class="more">
                            <i class="fa fa-ellipsis-h"></i>
                            <ul class="more-dropdown">
                                <li>
                                    <a href="#">block  User</a>
                                </li>
                                <li>
                                    <a href="#">Mute User</a>
                                </li>
                            </ul>
                        </div>
                    </li> -->
                </ul>
            </div>
        </div>
    </div>
</div>