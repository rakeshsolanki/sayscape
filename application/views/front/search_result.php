<?php
$cur_tab = $this->uri->segment(1);
$cur_tab2 = $this->uri->segment(2);
$loginUserID = $this->session->userdata('userID');
$keyword= $_GET['keyword'];

?>
<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-6">
                            <div class="central-meta new-pst text-center">
                                <a href="<?= base_url('search/user?keyword='.$keyword); ?>"><button class="btn w120 <?php echo ($cur_tab2 == 'user') ? "text-white webbgcolor" : "" ?>">Users</button></a>
                                <a href="<?= base_url('search?keyword='.$keyword); ?>"><button class="btn w120 <?php echo ($cur_tab2 != 'user') ? "text-white webbgcolor" : "" ?>">Posts</button></a>
                            </div>
                            <?php
                            if(!empty($postList)){
                                ?><div class="loadMorePosts"><?php
                                $count = 1;
                                foreach($postList as $post){
                                    $datas['post'] = $post;
                                    $datas['count'] = $count;
                                    $this->load->view('front/post_view',$datas);
                                    $count ++;
                                } ?> </div>
                                <input type="hidden" id="pageno" class="pageno" value="1" />
                                <button class="postLoader btn-view btn-load-more" data-user="<?php echo $loginUserID; ?>" data-type="searchPosts" data-other="<?php echo (isset($keyword))?$keyword:''; ?>">Load More</button>
                            <?php } 
                            if(!empty($userList)){ ?>
                                    <div class=" central-meta item notification-box">
                                        <ul>
                                    <?php foreach ($userList as $row) { ?>
                                        <li>
                                            <div>
                                                <figure>
                                                    <img src="<?php echo $row['profileImage']; ?>" alt="" class="img-fluid wh-40">
                                                </figure>
                                                <div class="mesg-meta">
                                                    <h6><a href="<?php echo base_url('time-line/' . $row['username']) ?>"><?php echo $this->emoji->Decode($row['name']); ?></a></h6>
                                                    <p>@<?php echo $row['username']; ?></p>
                                                </div>
                                            </div>
                                        </li>
                                <?php } ?>
                                        </ul>
                                    </div>
                                 <?php }
                            if(empty($postList) && empty($userList)){ ?>
                                <div class="central-meta item">
                                    <div class="user-post">
                                        <h6 class="text-center">No record found</h6>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-3">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>