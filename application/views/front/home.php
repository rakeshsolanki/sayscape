
<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <!-- sidebar -->
                        <div class="col-lg-6">
                            <?php $this->load->view('front/post_form'); ?>
                            <!-- <div class="loadMore"> -->
                            <div class="loadMorePosts">
                                <?php
                                if(!empty($postList)){
                                    $count = 1;
                                    foreach($postList as $post){
                                        $datas['post'] = $post;
                                        $datas['count'] = $count;
                                        $this->load->view('front/post_view',$datas);
                                        $count ++;
                                        //break;
                                    }
                                } ?>
                            </div>
                            <input type="hidden" id="pageno" class="pageno" value="1" />
                            <button class="postLoader btn-view btn-load-more" data-type="home">Load More</button>
                        </div><!-- centerl meta -->
                        <div class="col-lg-3 ">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

