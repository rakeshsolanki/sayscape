<?php
// echo "<pre>"; print_r($userDetails); die();
?>
<div class="col-lg-12 col-md-12">
    <div class="central-meta stick-widget">
        <span class="create-post">Personal Info</span>
        <div class="personal-head">
            <span class="f-title"><i class="fa fa-user"></i> About Me:</span>
            <p><?php echo (!empty($userDetails['about']))?$this->emoji->Decode(ucfirst($userDetails['about'])):"Bio is not available"; ?></p>
            <?php
            if(isset($settingsData['hideBirthday']) && $settingsData['hideBirthday']==0){ ?>
                <span class="f-title"><i class="fa fa-birthday-cake"></i> Birthday:</span>
                <p> <?php echo (!empty($userDetails['dob']))?$userDetails['dob']:"-"; ?> </p>
            <?php } ?>
            <?php /* <span class="f-title"><i class="fa fa-phone"></i> Phone Number:</span>
            <p><?php echo (!empty($userDetails['phone']))?$userDetails['phone']:"-"; ?></p> */ ?>
            <span class="f-title"><i class="fa fa-globe"></i> Location:</span>
            <p><?php echo (!empty($userDetails['location']))?$userDetails['location']:"-"; ?></p>
            <span class="f-title"><i class="fa fa-envelope"></i> Website:</span>
            <p>
            <?php
            if(!empty($userDetails['website'])){ ?>
                <a href="<?php echo $userDetails['website']; ?>"><?php echo $userDetails['website']; ?></a>
            <?php } ?></p>
            <span class="f-title"><i class="fa fa-handshake-o"></i> Joined:(Since <span class="displayMomentDateOnly" datetime="<?php echo (isset($userDetails['createdDateTime'])) ? $userDetails['createdDateTime'] : ""; ?>"></span>)</span>
            <p><?php echo (!empty($userDetails['createdDateTime']))? date('m-d-Y',strtotime($userDetails['createdDateTime'])):"-"; ?></p>
        </div>
    </div>
</div>