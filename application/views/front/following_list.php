<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?>
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-9">
                            <div class="central-meta">
                                <div class="title-block">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="align-left">
                                                <h5><?php echo $sectopnTitle; ?><span><?php echo $followCount; ?></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="central-meta padding30">
                            <div class="row merged20 followList">
                                <?php if(!empty($followinguser)){
                                    foreach($followinguser as $following){ ?>    
                                        <div class="col-lg-4 col-md-6 col-sm-6">
											<div class="friend-box">
												<div class="frnd-meta">
                                                    <img src="<?php echo $following['profileImage'] ?>" alt="" class="wh-85">
													<div class="frnd-name">
                                                        <a href="<?= base_url('time-line/'.$following['username']); ?>" title=""><?php echo isset($following['name']) ? $following['name']:''; ?></a>
														<span><?php echo isset($following['username']) ? $following['username']:''; ?></span>
                                                    </div>
                                                    <div class="row text-center pt5">
                                                        <div class="<?php echo ($type=='Followers')?'col-6':'col-12 text-right'; ?>">
                                                            <a class="btn btn-sm webbgcolor text-white send-mesg" href="<?= base_url('message-box/'.$following['followerID']); ?>" title="">Message</a>
                                                        </div>
                                                        <?php
                                                        if($type=='Followers'){ ?>
                                                            <div class="col-6">
                                                                <?php
                                                                if (!empty($following) && $following['followerID'] != $loginUserID) {
                                                                    if ($following['isfollow'] == 1) { ?>
                                                                        <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>">Following</a>
                                                                        <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="1" id="userFollow<?php echo $following['followerID']; ?>" style="display: none">Follow</a>
                                                                    <?php } elseif ($following['isfollow'] == 4) { ?>
                                                                        <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>">Request</a>
                                                                        <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="4" id="userFollow<?php echo $following['followerID']; ?>" style="display: none">Follow</a>
                                                                        <?php } else {
                                                                        if (!empty($following['privateProfileStatus']) && $following['privateProfileStatus'] == 1) { ?>
                                                                            <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>" style="display: none">Request</a>
                                                                            <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="4" id="userfollowRequest<?php echo $following['followerID']; ?>">Follow</a>
                                                                        <?php } else { ?>
                                                                            <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="0" id="userUnfollow<?php echo $following['followerID']; ?>" style="display: none">Following</a>
                                                                            <a href="javascript:void(0)" title="" class="btn btn-sm webbgcolor text-white btnFollowUnfollow" data-id="<?php echo $following['followerID']; ?>" data-status="1" id="userFollow<?php echo $following['followerID']; ?>">Follow</a>
                                                                        <?php } ?>
                                                                    <?php }
                                                                    if ($following['isFollowMe'] == 1) { ?>
                                                                        <span>Follows You</span>
                                                                    <?php }
                                                                } ?>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input class="checktopfive" value="<?php echo $following['followerID']; ?>" data-id="<?php echo $following['followerID']; ?>" type="checkbox" name="topfive" <?php if(!empty($topfiveArray) && in_array($following['followerID'],$topfiveArray)){  echo 'checked'; } ?> ><i class="check-box"></i>
                                                                            Select In Top Five
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
											</div>
										</div>
								<?php } } ?>	
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script> 
;(function($, window, document, undefined) {
	$(document).ready(function() {
        var limit = 5;
        $('input.checktopfive').on('change', function(evt) {
            //console.log($('.single-checkbox:checked').length);
            if($('.checktopfive:checked').length > limit) {
                this.checked = false;
            }else{
                var array = []; 
                $("input:checkbox[name=topfive]:checked").each(function() { 
                    array.push($(this).val()); 
                }); 
                $.ajax({
					type: 'POST',
					url: "<?php echo base_url('user/addTopFiveFollower'); ?>",
					data: {
                        followers:array,
					},
					success: function(data){
					}
				});
                console.log(array);
            }
        }); 

    });
})(jQuery, window, document);
    </script> 