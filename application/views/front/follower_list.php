<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?>
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <!-- user profile banner -->
                        <div class="col-lg-9">
                            <div class="central-meta">
                                <div class="title-block">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="align-left">
                                                <h5>Followers Member <span><?= isset($followinguser) ? count($followinguser):'' ?></span></h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- title block -->
                            <div class="central-meta padding30">
                            <div class="row merged20">
                                <?php if(!empty($followinguser)){
                                    foreach($followinguser as $following){ ?>    
									<div class="col-lg-3 col-md-6 col-sm-6">
										<div class="friend-block">
											<!-- <div class="more-opotnz">
												<i class="fa fa-ellipsis-h"></i>
												<ul>
													<li><a href="#" title="">Block</a></li>
													<li><a href="#" title="">UnBlock</a></li>
													<li><a href="#" title="">Mute Notifications</a></li>
													<li><a href="#" title="">hide from friend list</a></li>
												</ul>
											</div> -->
											<figure>
												<img src="<?php echo $following['profileImage'] ?>" alt="" class="wh-85">
											</figure>

											<div class="frnd-meta">
												<div class="frnd-name">
													<a href="<?= base_url('time-line/'.$following['username']); ?>" title=""><?php echo isset($following['name']) ? $following['name']:''; ?></a>
													<span><?php echo isset($following['username']) ? $following['username']:''; ?></span>
												</div>
												<a class="send-mesg" href="<?= base_url('message-box/'.$following['followerID']); ?>" title="">Message</a>
											</div>
										</div>
									</div>
								<?php } } ?>	
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- content -->