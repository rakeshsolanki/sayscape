<?php
include('header.php');
?>

        <?php
        include('toparea.php');
        ?>
	   <!-- top area -->

		<section>
			<div class="gap gray-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="row merged20" id="page-contents">
								<?php
								include('shortcuts.php');
								?>
							   <!-- sidebar -->
								<div class="col-lg-9">
									<div class="central-meta">
										<div class="editing-info">
											<h5 class="f-title"><i class="ti-lock"></i>Change Password</h5>
											
											<form action="<?php  echo base_url('home/changePassword'); ?>" method="post">
												<!-- <div class="form-group">	
												  <input type="password" name="currntpass"  id="chckpass" required/>
												  <label class="control-label" for="input">Current password</label><i class="mtrl-select"></i>
												</div> -->
												<div class="form-group">	
												  <input type="password" name="newpass" id="password" required/>
												  <label class="control-label" for="input">New password</label><i class="mtrl-select"></i>
												</div>
												<div class="form-group">	
												  <input type="password" id="confirm_password" required/>
												  <label class="control-label" for="input">Confirm password</label><i class="mtrl-select"></i>
												</div>
												<a class="forgot-pwd underline" title="" href="javascript:void(0)">Forgot Password?</a>
												<div class="submit-btns">
                                                    <input type="submit" name="change" class="btn" value="Update" style="background-color:#5a0d28; color:white;">
												</div>

											</form>
										</div>
									</div>	
								</div><!-- centerl meta -->


                               <script>
                               var password = document.getElementById("password")
                                   , confirm_password = document.getElementById("confirm_password");
                               function validatePassword(){
                                   if(password.value != confirm_password.value) {
                                       confirm_password.setCustomValidity("Passwords Don't Match");
                                   } else {
                                       confirm_password.setCustomValidity('');
                                   }
                               }
                               password.onchange = validatePassword;
                               confirm_password.onkeyup = validatePassword;
                               </script>



								<div class="col-lg-3">
									<aside class="sidebar static">
                                        <!-- who's following -->
                                        <?php
                                        //include('followingList.php');
                                        ?>
									</aside>
								</div><!-- sidebar -->
							</div>	
						</div>
					</div>
				</div>
			</div>	
		</section>

<?php
include('footer.php');
?>