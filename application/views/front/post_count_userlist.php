<div class="">
    <ul class="drops-menu nomar">
        <?php
        $loginUserID = $this->session->userdata('userID');
        // echo "<pre>"; print_r($userList); die();
        if (!empty($userList)) {
            foreach ($userList as $row) { ?>
                <li>
                    <div>
                        <figure>
                            <img src="<?php echo $row['profileImage']; ?>" alt="" class="wh-40">
                        </figure>
                        <div class="mesg-meta">
                            <h6><a href="<?php echo base_url('time-line/' . $row['username']) ?>"><?php echo $this->emoji->Decode($row['name']); ?></a></h6>
                            <p>@<?php echo $row['username']; ?></p>
                        </div>
                        <div class="add-del-friends">
                            <?php
                            if($loginUserID!=$row['userID']){ ?>
                                <a href="<?php echo base_url('message-box/'.$row['userID']);?>" title=""><i class="fa fa-envelope"></i></a>
                            <?php } ?>
                        </div>
                    </div>
                </li>
        <?php }
        }else{ ?>
            <li><h6>No record found</h6></li>
        <?php } ?>
    </ul>
</div>