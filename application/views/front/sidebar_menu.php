<?php
$pageCurrUrl = "";
$pageurl = $this->uri->segment(1);
if($pageurl == 'home' || $pageurl == 'time-line' || $pageurl=='' || $pageurl=='veteran-feed'){
    $url = '#postform';
}else{
    $url = base_url('#postform');
}
$userID = $this->session->userdata('userID');
$getUnreadMessageData = $this->CommonModel->get_all('tblmessage', ['*'], ['toUserID' => $userID, 'isRead' => 0, 'isActive' => 1, 'isDelete' => 0]);
$sidegrouplist = $this->group->getGroupList($userID);
// echo '<pre>';print_r($sidegrouplist);die;
?>
<aside class="sidebar static left ">
    <div class="widget stick-widget">
        <h4 class="widget-title">Shortcuts</h4>
        <ul class="naves">
            <li>
                <i class="ti-image"></i>
                <a href="<?= base_url(); ?>" title="">Home</a>
            </li>
            <li>
                <i class="fa fa-user"></i>
                <a href="<?= base_url('time-line'); ?>" title="">Profile</a>
            </li>
            <!-- <li>
                <i class="fa fa-group"></i>
                <a href="<?= base_url('groups'); ?>" title="">Groups</a>
            </li> -->
            <li>
                <div class="dropdown">
                    <i class="fa fa-group"></i>
                    <a class="dropdown-toggle" href="<?= base_url('groups'); ?>" title="" data-toggle="dropdown">Groups
                    <span class="countnumber"><?= isset($sidegrouplist) ? count($sidegrouplist):'0' ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php if(!empty($sidegrouplist)){
                         foreach($sidegrouplist as $grouplistrow){ ?>
                        <li class="pd-l-10">
                            <a href="<?= base_url('groupDetails/'.$grouplistrow['groupID']) ?>">
                                <figure>
                                    <img src="<?= isset($grouplistrow['groupProfile'])? $grouplistrow['groupProfile']:''; ?>" class="wh-40 rounded-circle" alt="">
                                    <?= isset($grouplistrow['groupName'])? $grouplistrow['groupName']:''; ?>
                                    <span class="countnumber" style="top: 10px"><?= isset($grouplistrow['totalmembers']) ? $grouplistrow['totalmembers']:'0' ?></span>
                                </figure>
                            </a>
                        </li>
                        <?php } } ?>
                    </ul>
                </div>
            </li>
            <li>
                <i class="fa fa-bookmark"></i>
                <a href="<?= base_url('bookmark'); ?>" title="">Bookmarks</a>
            </li>
            <li>
                <i class="fa fa-lock"></i>
                <a href="<?= base_url('veteran-feed'); ?>" title="">Veteran Feed</a>
            </li>
            <li>
                <i class="fa fa-lock"></i>
                <a href="<?= base_url('responder-feed'); ?>" title="">Responder Feed</a>
            </li>
            <li>
                <i class="fa fa-fire"></i>
                <a href="<?= base_url('trending'); ?>" title="">Trending</a>
            </li>
            <li>
                <i class="fa fa-bell"></i>
                <a href="<?= base_url('notification'); ?>" title="">Notifications</a>
            </li>
            <li>
                <i class="fa fa-hashtag"></i>
                <a href="<?= base_url('hashtag-trending'); ?>" title="">Hashtag Trending</a>
            </li>
            <li>
                <i class="ti-user"></i>
                <a href="<?= base_url('friends-request'); ?>" title="">Follower Requests</a>
            </li>
            <li>
                <i class="ti-face-smile"></i>
                <a href="<?= base_url('following'); ?>" title="">Following</a>
            </li>
            <li>
                <i class="ti-face-smile"></i>
                <a href="<?= base_url('followers'); ?>" title="">Followers</a>
            </li>
            <li>
                <i class="fa fa-comments"></i>
                <a href="<?= base_url('message-box'); ?>" title="">Messenger&nbsp;
                    <span class="countnumber"><?= isset($getUnreadMessageData) ? count($getUnreadMessageData):'0' ?></span>
                </a>
            </li>
            <!--<li>-->
            <!--    <i class="fa fa-info-circle"></i>-->
            <!--    <a href="<?= base_url('time-line?type=about'); ?>" title="">About</a>-->
            <!--</li>-->
            <!--<li>-->
            <!--    <i class="fa fa-question-circle"></i>-->
            <!--    <a href="<?= base_url('privacy-policy'); ?>" title="">Privacy & Policy</a>-->
            <!--</li>-->
            <li>
                <i class="ti-settings"></i>
                <a href="<?= base_url('edit-profile'); ?>" title="">Account setting</a>
            </li>
            <li>
                <i class="ti-power-off"></i>
                <a href="<?= base_url('logout'); ?>" title="">Logout</a>
            </li>
        </ul>
    
        <div class="text-center" style="background: #edf2f6;">
            <a href="<?= $url; ?>" id="postcontrol"> <img src="<?= base_url('assets/images/icon/saysicon.png'); ?>" width="100"></a>
        </div>

    </div><!-- Shortcuts -->
   
</aside>