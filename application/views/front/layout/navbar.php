<?php $userID = $this->session->userdata('userID');
if($userID!=""){
    $notificationcount = count($this->CommonModel->get_all(TBL_USER_NOTIFICATION, ['un_id'], ['userID' => $userID, 'isRead' => 0]));
    $notificationlist= $this->notification->getuneradNotificationList($userID);
    //echo '<pre>';print_r($notificationlist);die;    
} ?>
<div class="se-pre-con"></div>
<div class="theme-layout">
    <div class="postoverlay"></div>
    <?php
    if($userID!=""){ ?>
    <div class="responsive-header">
        <div class="mh-head first Sticky">
			<span class="mh-btns-left">
				<a class="" href="#menu"><i class="fa fa-align-justify"></i></a>
			</span>
            <span class="mh-text">
				<a href="<?php echo base_url() ?>" title="" class="ctmLogo"><img src="<?php echo base_url('assets/') ?>images/logo.png" alt=""></a>
			</span>
        </div>
        <div class="mh-head second">
            <form class="mh-form" method="get" class="mh-form" action="<?php echo base_url('search/user'); ?>">
                <input type="text" placeholder="Search" name="keyword" />
                <a href="#/ " class="fa fa-search"></a>
                <!-- <button type="submit" data-ripple><i class="ti-search"></i></button> -->
            </form>
        </div>
        <nav id="menu" class="res-menu">
            <ul class="">
                <li><a class="" href="<?php echo base_url() ?>" title=""><span> Timeline</span></a> </li>
                <li><a class="" href="javascript:void(0)" title=""><span> Profile</span></a>
                    <ul>
                        <li><a href="<?php echo base_url('time-line') ?>" title=""> View Profile</a></li>
                        <li><a href="<?php echo base_url('edit-profile') ?>" title=""> Account setting</a></li>
                        <li><a href="<?php echo base_url('friends-request') ?>" title=""> Follower Requests</a></li>
                    </ul>
                </li>
                <li><a class="" href="<?php echo base_url('groups') ?>" title=""><span> Groups</span></a></li>
                <li><a class="" href="<?php echo base_url('bookmark') ?>" title=""><span> Bookmarks</span></a> </li>
                <li><a class="" href="<?php echo base_url('veteran-feed') ?>" title=""><span> Veteran Feed</span></a> </li>
                <li><a class="" href="<?php echo base_url('trending') ?>" title=""><span> Trending</span></a> </li>
                <li><a class="" href="<?php echo base_url('hashtag-trending') ?>" title=""><span> Hashtag Trending</span></a> </li>
                <li><a class="" href="<?php echo base_url('notification') ?>" title=""><span> Notifications</span></a> </li>
                <li><a class="" href="<?php echo base_url('following') ?>" title=""><span> Following</span></a> </li>
                <li><a class="" href="<?php echo base_url('followers') ?>" title=""><span> Followers</span></a> </li>
                <li><a class="" href="<?php echo base_url('message-box') ?>" title=""><span> Messenger</span></a> </li>
                <li><a class="" href="javascript:void(0)" title=""> Other</a>
                    <ul>
                        <li><a href="<?= base_url('time-line?type=about'); ?>" title=""> About</a></li>
                        <li><a href="<?= base_url('privacy-policy'); ?>" title=""> Privacy & Policy</a></li>
                    </ul>
                </li>
                <li><a class="" href="<?php echo base_url('logout') ?>" title=""><span> Logout</span></a> </li>
            </ul>
        </nav>
    </div><!-- responsive header -->
                
    <div class="topbar stick">
        <div class="logo">
            <a title="" href="<?= base_url() ?>" class="ctmLogo"><img src="<?php echo base_url('assets/') ?>images/logo.png" alt=""></a>
        </div>
        <div class="top-area">
            <!-- <div class="main-menu">
				<span>
			    	<i class="ti-menu"></i>
			    </span>
            </div> -->
            <div class="top-search">
                <form method="get" class="" action="<?php echo base_url('search/user'); ?>">
                    <input type="text" placeholder="Search" name="keyword" />
                    <button type="submit" data-ripple><i class="ti-search"></i></button>
                </form>
            </div>
            <ul class="setting-area">
                <li><a href="#" title="Home" onclick="gotoHome()" ><i class="fa fa-home"></i></a> </li>
                <li>
                    <!-- <a href="<?= base_url('notification'); ?>" title="Notification" >
                        <i class="fa fa-bell"></i>
                        <em class="bg-red"><?= isset($notificationcount) ? $notificationcount:''; ?></em>
                    </a> -->
                    <a href="#" title="Notification" data-ripple="">
						<i class="fa fa-bell"></i><em class="bg-purple"><?= isset($notificationcount) ? $notificationcount:''; ?></em>
					</a>
					<div class="dropdowns">
						<span><?= isset($notificationcount) ? $notificationcount:''; ?> New Notifications 
                        <a href="javascript:void(0)" title="" class="more-mesg">Clear All</a>
                        </span>
						<ul class="drops-menu">
                        <?php if(!empty($notificationlist)){
                         foreach($notificationlist as $notification){ ?>
							<li>
								<a href="#" title="">
									<figure>
										<img src="<?= isset($notification['image']) ? $notification['image']:''; ?>" class="wh-40 webbgcolor" alt="">
										<!-- <span class="status f-online"></span> -->
									</figure>
									<div class="mesg-meta">
										<h6><?= isset($notification['title']) ? $notification['title']:''; ?></h6>
										<span><?= isset($notification['text']) ? $notification['text']:''; ?></span>
										<!-- <i>2 min ago</i> -->
									</div>
								</a>
                            </li>
                        <?php } } ?>
						</ul>
						<a href="<?= base_url('notification'); ?>" title="" class="more-mesg">View All</a>
					</div>
                </li>
                <li class="main-menu"><span class="ctmTopMenu"><i class="fa fa-caret-square-o-down"></i></span></li>
            </ul>
            <?php
            $loginUserID = $this->session->userdata('userID'); 
            $userOnlineStatus = $this->user->userOnlineStatus($loginUserID);
            $loginUsername = $this->session->userdata('username');
            $profilePic = ($this->session->userdata('profileImage'))?base_url().'upload/userprofile/'.$this->session->userdata('profileImage'):base_url().'assets/images/resources/admin.jpg';
            $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $loginUserID, 'isActive' => 1, 'isDelete' => 0]);
            $isPlus = $this->user->checkUserPlusAccount($loginUserID);
            ?>
            <div class="user-img">
                <h5 class="usrname"><?php echo $this->session->userdata('username'); ?></h5>
                <img src="<?php echo $profilePic ?>" alt="" class="wh-50" />
                <span class="status <?php echo ($userOnlineStatus==1)?'f-online':'f-offline' ?>" id="userStatusIndi"></span>
                <div class="user-setting">
                    <span class="seting-title">User Status</span>
                    <div class="setting-row ctmSetting">
                        <input type="checkbox" id="changeuserOnlineStatus" name="changeuserOnlineStatus" value="1" <?php echo ($userOnlineStatus==1)?'checked':'' ?> /> 
                        <label for="changeuserOnlineStatus" id="changeOnlineStatus" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <span class="seting-title">Dark Mode</span>
                    <div class="setting-row ctmSetting">
                        <input type="checkbox" id="changeThemeDarkMode" name="changeThemeDarkMode" value="1" <?php echo (isset($userSetting['isDarkmode']) && $userSetting['isDarkmode']==1)?'checked':''; ?> /> 
                        <label for="changeThemeDarkMode" id="changeThemeDarkModeLabel" data-on-label="ON" data-off-label="OFF"></label>
                    </div>
                    <span class="seting-title">User setting</span>
                    <ul class="log-out">
                        <li><a href="<?php echo base_url('edit-profile'); ?>" title=""><i class="ti-pencil-alt"></i>Edit profile</a></li>
                        <li><a href="<?php echo base_url('time-line'); ?>" title=""><i class="ti-user"></i> view profile</a></li>
                        <li><a href="<?php echo base_url('edit-profile'); ?>" title=""><i class="ti-settings"></i>account setting</a></li>
                        <li><a href="<?php echo base_url('changepassword'); ?>" title=""><i class="ti-lock"></i>Change Password</a></li>
                        <li><a href="<?php echo base_url('logout') ;?>" title=""><i class="ti-power-off"></i>log out</a></li>
                    </ul>
                </div>
            </div>
            <?php
            if(empty($isPlus)){ ?>
                <a href="<?php echo base_url('upgrade-account'); ?>" class="btn btn-warning btn-sm btnplus" title="Home">Plus+</a>
            <?php } ?>
        </div>
        <nav>
            <!-- style="display: block;" -->
            <ul class="nav-list">
                <li><a class="" href="<?php echo base_url() ?>" title=""><i class="ti-clipboard"></i> Timeline</a> </li>
                <li><a class="" href="#" title=""><i class="ti-user"></i> Profile</a>
                    <ul>
                        <li><a href="<?php echo base_url('time-line') ?>" title=""> View Profile</a></li>
                        <li><a href="<?php echo base_url('time-line?type=about') ?>" title=""> About</a></li>
                        <li><a href="<?php echo base_url('friends-request') ?>" title=""> Follower Requests</a></li>
                    </ul>
                </li>
                <li><a class="" href="<?php echo base_url('groups') ?>" title=""><i class="fa fa-group"></i> Groups</a></li>
                <li><a class="" href="<?php echo base_url('bookmark') ?>" title=""><i class="fa fa-bookmark"></i> Bookmarks</a> </li>
                <li><a class="" href="<?php echo base_url('veteran-feed') ?>" title=""><i class="fa fa-lock"></i> Veteran Feed</a> </li>
                <li><a class="" href="<?php echo base_url('responder-feed') ?>" title=""><i class="fa fa-lock"></i> Responder Feed</a> </li>
                <li><a class="" href="<?php echo base_url('trending') ?>" title=""><i class="fa fa-fire"></i> Trending</a> </li>
                <li><a class="" href="<?php echo base_url('hashtag-trending') ?>" title=""><i class="fa fa-hashtag"></i> Hashtag Trending</a> </li>
                <li><a class="" href="<?php echo base_url('message-box') ?>" title=""><i class="fa fa-comments"></i> Messenger</a> </li>
                <li><a class="" href="<?php echo base_url('privacy-policy') ?>" title=""><i class="fa fa-gears"></i> Privacy & Settings</a> </li>
            </ul>
        </nav><!-- nav menu -->
    </div><!-- topbar -->

     <?php /* <div class="fixed-sidebar left">
		<div class="menu-left">
			<ul class="left-menu">
				<!-- <li>
					<a class="menu-small" href="<?= base_url(); ?>" title="">
						<i class="fa fa-home"></i>
					</a>
				</li> -->
				<li>
					<a href="<?= base_url(); ?>" title="Newsfeed Page" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('time-line'); ?>" title="Profile" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-user"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('groups'); ?>" title="Groups" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-group"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('bookmark'); ?>" title="Bookmark" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-bookmark"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('veteran-feed'); ?>" title="Veteran Feed" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-lock"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('trending'); ?>" title="Trending" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-fire"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('notification'); ?>" title="Notification" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-bell-o"></i>
					</a>
				</li>
				
				
				
				<li>
					<a href="<?= base_url('hashtag-trending'); ?>" title="Hashtag Trending" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-hashtag">
						</i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('friends-request'); ?>" title="Friend Requests" data-toggle="tooltip" data-placement="right">
						<i class="ti-user"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('message-box'); ?>" title="Messenger" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-comments"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('time-line?type=about'); ?>" title="About" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-info-circle"></i>
					</a>
                </li>
                <li>
					<a href="<?= base_url('privacy-policy'); ?>" title="Privacy & Settings" data-toggle="tooltip" data-placement="right">
						<i class="fa fa-question-circle"></i>
					</a>
				</li>
				<li>
					<a href="<?= base_url('home/logout'); ?>" title="Logout" data-toggle="tooltip" data-placement="right">
						<i class="ti-power-off"></i>
					</a>
				</li>
			</ul>
		</div>
    </div> */ ?>
    <?php } ?>