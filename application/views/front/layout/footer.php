

<div class="bottombar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="copyright">© Sayscape <?php echo date('Y'); ?>. All rights reserved.</span>
                <!-- <i><img src="<?= base_url('assets/'); ?>images/credit-cards.png" alt=""></i> -->
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="modalSharePost">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"><i class="fa fa-retweet"></i> Share Post </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" class="formsharePost">
                    <input type="hidden" id="sharepid" name="postID" value="" required>
                    <!-- <b class="text-black">Emotion:</b> -->
                    <div class="shareEmoji">
                        <label>
                            <input type="radio" name="emoji" value="Happy \ud83d\ude00">
                            <img src="<?= base_url('assets/') ?>images/smiles/happy-2.png" title="Happy" />
                        </label>
                        <label>
                            <input type="radio" name="emoji" value="Sad \ud83d\ude22">
                            <img src="<?= base_url('assets/') ?>images/smiles/unhappy.png" title="Sad" />
                        </label>
                        <label>
                            <input type="radio" name="emoji" value="Angry uD83D\uDE20">
                            <img src="<?= base_url('assets/') ?>images/smiles/angry.png" title="Angry" />
                        </label>
                        <label>
                            <input type="radio" name="emoji" value="Love \ud83d\ude0d">
                            <img src="<?= base_url('assets/') ?>images/smiles/in-love.png" title="Love" />
                        </label>
                        <label>
                            <input type="radio" name="emoji" value="Funny \uD83E\uDD23">
                            <img src="<?= base_url('assets/') ?>images/smiles/happy-1.png" title="Funny" />
                        </label>
                    </div>
                    <b class="text-black">Write Something:</b>
                    <p class="lead emoji-picker-container">
                        <textarea rows="2" id="sharePostTextarea" placeholder="Share some what you are thinking?" name="text" class="minh60" data-emojiable="true" data-emoji-input="unicode"></textarea>
                    </p>
                    <div class="attachments text-right">
                        <ul class="cursor-point">
                            <li id="sharePostEmoji">
                                <i class="fa fa-smile-o"></i>
                                <label class="fileContainer"> </label>
                            </li>
                            <li>
                                <input class="btn btn-success mt10" type="submit" name="sharePost" value="Share" />
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="Modeleditpost">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"> Edit Post </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                    
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalPostCountUsers">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"> User List </h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                    
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="baseUrl" value="<?php echo base_url(); ?>" />
<!-- <script src="../../../cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8c55_YHLvDHGACkQscgbGLtLRdxBDCfI"></script>
<script src="<?= base_url('assets/') ?>js/locationpicker.jquery.js"></script> -->
<!-- <script src="<?= base_url('assets/') ?>js/map-init.js"></script> -->
<script src="<?= base_url('assets/') ?>js/script.js"></script>
<script src="<?= base_url('assets/') ?>js/moment.js"></script> 
<script>
jQuery(document).ready(function($) {
    
});
$(function() {
    $('.emoji-picker').hide();
    $('#sharePostEmoji').click(function() {
        $('.emoji-picker').click();
    });
});
;(function($, window, document, undefined) {
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '<?= base_url('assets/emoji/img/') ?>/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();

    // mansion js
    var mantionUsers = <?= $this->user->getUsernameJson(); ?>;
    $('.emoji-wysiwyg-editor').atwho({
        at: "@",
        data: mantionUsers
    });
    
})(jQuery, window, document);

</script>

</body>
</html>