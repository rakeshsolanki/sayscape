<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
	<title><?php echo (isset($title)) ? $title.' | '.$this->config->item('site_title') : $this->config->item('site_title'); ?></title>
    <link rel="icon" href="<?php echo base_url('assets/') ?>images/fav.png" type="image/png" sizes="16x16"> 
    
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/main.min.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/weather-icon.css">
	<link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/weather-icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/style.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/color.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/') ?>css/responsive.css">
    <style>
        @media only screen and (max-width: 600px) {
            .login-big{
                height: unset;
            }
            .we-login-register{
                margin-bottom: 1px;
            }
            .barcode {
                margin-top: 5px;
            }
        }
        .we-login-register{
            padding: 30px 35px 30px;
        }
        .spanc, .spanc .we-account{
            text-transform: unset !important;
        }
        .gap.signin .bg-image{
            height: 100%;
        }
    </style>
</head>
<body>
	<div class="www-layout">
        <section>
        	<div class="gap no-gap signin whitish medium-opacity">
                <div class="bg-image" style="background-image:url(<?php echo base_url('assets/'); ?>images/theme-bg.png);z-index: 0;"></div>
                <div class="container">
                	<div class="row">
                        <div class="col-lg-8">
                            <div class="col login-big" style="text-align: justify;">
                                <figure class="text-center"><img src="<?php echo base_url('assets/') ?>images/logo_transparent.png" alt="" width="70%"></figure>
                                <!-- <h1>Sayscape</h1> -->
                                <div class="d-none d-sm-block">
                                    <p> SayScape is the future of social media and the way people communicate by providing a place to be yourself without fear of unfair bias. Military veterans and first responders have a special place here on SayScape.</p>
                                    <p>SayScape brings people together no matter what their background or beliefs. It’s designed around the understanding that everyone deserves a voice. SayScape is not a political platform. It’s not targeted towards any specific affiliation or ideology. The people need an unbiased platform to collaborate, discuss, share ideas, and debate key topics. SayScape will not attempt to silence legal content or sell your private data. That is what SayScape is all about.</p>    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="we-login-register">
                                <?php $this->load->view('front/flash_messages'); ?>
                                <?php
                                if(empty($userID)){ ?>
                                    <div class="form-title">
                                        <i class="fa fa-key"></i>login
                                        <span>sign in now and meet the awesome Friends around the world.</span>
                                    </div>
                                    <form action="<?php echo base_url('login'); ?>" class="we-form" method="post">
                                        <input type="text" name="email" placeholder="Email/Username" required>
                                        <input type="password" name="password" placeholder="Password" required>
                                        <input type="checkbox" checked><label class="">remember me</label>
                                        <button type="submit" data-ripple="" name="login" value="Sign in">Sign in</button>
                                        <a class="forgot underline" href="#" title="">forgot password?</a>
                                    </form>
                                <?php }else{ ?>
                                    <div class="form-title">
                                        <i class="fa fa-key"></i>Login
                                        <span>Please check the email used for registration to SayScape for the OTP.</span>
                                    </div>
                                    <form action="<?php echo base_url('login'); ?>" class="we-form" method="post">
                                        <input  type="hidden" name="userID" value="<?= isset($userID)? $userID:''; ?>" >
                                        <input class="partitioned" type="text" name="otp" placeholder=""  maxlength="4" required>
                                        <button type="submit" data-ripple="" name="verify" value="Verify">Verify Otp</button>
                                    </form>
                                <?php } ?>
                                <span>Don't use Sayscape Yet? <a class="we-account underline" href="<?= base_url('register');?>" title="">Join now</a></span>
                                <span class="spanc"> For More Information <a class="we-account underline" href="https://sayscape.info" title="">sayscape.info</a></span>
                            </div>
                            <div class="barcode">
                                <!--<figure><img src="<?php echo base_url('assets/') ?>images/resources/Barcode.jpg" alt=""></figure>-->
                                <div class="app-download">
                                    <!--<span>Download Mobile App to login</span>-->
                                    <ul class="colla-apps text-center"> 
                                        <li><a class="text-center" title="" href="https://play.google.com/store?hl=en"><img src="<?php echo base_url('assets/') ?>images/android.png" alt="">android</a></li>
                                        <li><a class="text-center" title="" href="https://www.apple.com/lae/ios/app-store/"><img src="<?php echo base_url('assets/') ?>images/apple.png" alt="">iPhone</a></li>
                                        <li><a title="" href="javascript:void(0)"><img src="<?php echo base_url('assets/') ?>images/Comodo-Secure.png" style="margin-top: 5px;height: 50px;" alt=""></a></li>
                                        <!-- <li><a title="" href="https://www.microsoft.com/store/apps"><img src="<?php echo base_url('assets/') ?>images/windows.png" alt="">Windows</a></li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <style>
    .partitioned {
    padding-left: 15px;
    letter-spacing: 42px;
    border: 0;
    background-image: linear-gradient(to left, black 70%, rgba(255, 255, 255, 0) 0%);
    background-position: bottom;
    background-size: 50px 1px;
    background-repeat: repeat-x;
    background-position-x: 35px;
    width: 220px;
}
</style>

</body>	

</html>