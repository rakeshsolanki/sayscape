<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title><?php echo (isset($title)) ? $title.' | '.$this->config->item('site_title') : $this->config->item('site_title'); ?></title>
    <link rel="icon" href="<?php echo base_url('assets/') ?>images/fav.png" type="image/png" sizes="16x16">
    
    <link rel="stylesheet" href="<?= base_url('assets/css/main.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/weather-icons.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/color.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/custom.css') ?>">
    <?php $userID = $this->session->userdata('userID');
     $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
     if(!empty($userSetting['isDarkmode'])){ ?>
        <link rel="stylesheet" href="<?= base_url('assets/css/dark-theme.css') ?>">
     <?php } if(empty($userSetting['isDarkmode']) && !empty($userSetting['themeColor']) && $userSetting['themeColor'] != '5a0d28'){ $themeColor = $userSetting['themeColor']; ?>
        <style>
            .topbar { background: <?= '#'.$themeColor; ?>; }
        </style>
     <?php } if(!empty($userSetting['highlightPost'])){ $highlightPost = $userSetting['highlightPost']; ?>
        <style>
            .highlightPost { box-shadow: inset 0 0 50px <?= '#'.$highlightPost; ?>; }
        </style>
    <?php } ?>
    <script src="<?= base_url('assets/') ?>js/main.min.js"></script>
    <script src="<?= base_url('assets/') ?>js/jquery-stories.js"></script>
    <script src="<?= base_url('assets/') ?>js/general.js"></script>

    <!-- Emoji picker -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="<?= base_url('assets/emoji/css/emoji.css') ?>" rel="stylesheet">
    <script src="<?= base_url('assets/emoji/js/config.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/util.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/jquery.emojiarea.js') ?>"></script>
    <script src="<?= base_url('assets/emoji/js/emoji-picker.js') ?>"></script>

    <!-- mantion js -->
    <script src="<?= base_url('assets/mantion/jquery.atwho.min.js'); ?>"></script>
    <script src="<?= base_url('assets/mantion/jquery.caret.min.js'); ?>"></script>
    
<?php
$userLoginArr = array(
    'userID' => $this->session->userdata('userID'),
    'username' => $this->session->userdata('username'),
    'name' => $this->session->userdata('name'),
    'profileImage' => ($this->session->userdata('profileImage'))?base_url().'upload/userprofile/'.$this->session->userdata('profileImage'):base_url().'assets/images/resources/admin.jpg',
);
// echo "<pre>"; print_r($userLoginArr); die();
?>
<script>
var userLoginArr= <?php echo json_encode($userLoginArr); ?>;
// for(var i=0;i<12;i++){
//     console.log(userLoginArr[i]);
// }
console.log(userLoginArr);
if (typeof(Storage) !== "undefined") {
    if (localStorage.getItem("loginUsers") === null) {
        localStorage.setItem("loginUsers", JSON.stringify(userLoginArr));
    }
    
    var retrievedData = localStorage.getItem("loginUsers");
}


for (i = 0; i <= localStorage.length-1; i++){
    key = sessionStorage.key(i);    
    val = sessionStorage.getItem(key);     
}
</script>

</head>
<body>
<?php $loginUserID = $this->session->userdata('userID'); ?>