<script src="<?= base_url('assets/') ?>js/jquery.card.js"></script>
<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
						<?php include('profile_topbar.php'); ?>
						<?php $loginUserID = $this->session->userdata('userID'); ?>
						<div class="col-lg-8">
							<div class="central-meta">
								<div class="text-center">
									<h5>Make payment for plus benefits.</h5>
								</div>
								<form id="frmPayment" method="POST" class="cardform">
									<div class="form-container">
										<div class="row">
											<div class="col-md-6">
												<input class="form-control" type="text" name="first-name" placeholder="First Name"/>
											</div>
											<div class="col-md-6">
												<input class="form-control" type="text" name="last-name" placeholder="Surname"/>
											</div>
											<div class="col-md-6">
												<input class="form-control"type="text" name="number" placeholder="Card Number"/>
											</div>
											<div class="col-md-3">
												<input class="form-control" type="text" name="expiry" placeholder="MM / YY"/>
											</div>
											<div class="col-md-3">
												<input class="form-control" type="password" name="cvc" placeholder="CCV"/>
											</div>
										</div>
										<div class="card-wrapper"></div>
										<div class="row">
											<div class="col-md-6">
												<input class="form-control"type="text" name="streetaddress" required="required" autocomplete="on" maxlength="45" placeholder="Streed Address"/>
											</div>
											<div class="col-md-6">
												<input class="form-control" type="text" name="city" required="required" autocomplete="on" maxlength="20" placeholder="City"/>
											</div>
											<div class="col-md-6">
												<input class="form-control" type="text" name="zipcode" required="required" autocomplete="on" pattern="[0-9]*" maxlength="5" placeholder="ZIP code"/>
											</div>
											<div class="col-md-6">
												<input class="form-control"type="email" name="email" required="required" autocomplete="on" maxlength="40" placeholder="Email"/>
											</div>
											<div class="col-md-6">
												<input class="btn btn-sm webbgcolor text-white mt10" id="btnSubmitPayment" name="btnSubmitPayment" type="submit" value="Make Payment"/>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
;(function($, window, document, undefined) {
	$(document).ready(function() {
		$('#frmPayment').card({
			container: '.card-wrapper',
			width: 280,
			formSelectors: {
				nameInput: 'input[name="first-name"], input[name="last-name"]'
			}
		});
    });
})(jQuery, window, document);
</script>
