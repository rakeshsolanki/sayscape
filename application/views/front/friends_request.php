<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <?php include('profile_topbar.php'); ?><!-- user profile banner -->
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-9">
                            <div class="central-meta">
                                <div class="title-block">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="align-left">
                                                <h5>Follower Requests <span><?= count($requestList); ?></span></h5>
                                            </div>
                                        </div>
                                        <!-- <div class="col-lg-6">
                                            <div class="row merged20">
                                                <div class="col-lg-10 col-md-10 col-sm-10">
                                                    <form class="filterform" method="post">
                                                        <input class="filterinputgroup"  type="text" placeholder="Search Group">
                                                        <button type="submit"><i class="fa fa-search"></i></button>
                                                    </form>
                                                </div>

                                                <div class="col-lg-2 col-md-2 col-sm-2">
                                                    <div class="option-list">
                                                        <i class="fa fa-ellipsis-v"></i>
                                                        <ul>
                                                            <li><a title="" href="#">Show Friends Public</a></li>
                                                            <li><a title="" href="#">Show Friends Private</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div><!-- title block -->
                            <div class="central-meta padding30">
                                <div class="row merged20">
                                    <?php
                                    if (!empty($requestList)) {
                                    foreach ($requestList as $request) { ?>
                                        <div class="col-lg-3 col-md-6 col-sm-6" id="conData">
                                            <div class="friend-block">
                                                <div class="more-opotnz">
                                                    <i class="fa fa-ellipsis-h"></i>
                                                    <ul>
                                                        <!-- <a href="javascript:void(0)" title="" class="add-butn more-action delete_data" id="--><?php //echo $request['userID']; ?><!--" onclick="remove_cart()">delete Request</a>-->
                                                        <!-- <a href="javascript:void(0)" title="" class="add-butn confirm_data" id="--><?php //echo $request['userID']; ?><!--" onclick="remove_cart()">Confirm</a>-->
                                                        <li><a class="delete_data" href="javascript:void(0)" id="<?= $request['userID']; ?>" title="">Delete Request</a></li>
                                                        <li><a class="confirm_data" href="javascript:void(0)" id="<?= $request['userID']; ?>" title="">Confirm</a></li>
                                                    </ul>
                                                </div>
                                                <figure>
                                                    <?php
                                                    if (isset($request['groupProfile']) && $request['groupProfile'] != "") { ?>
                                                        <a href="<?= base_url('time-line/'.$request['username']); ?>" title=""><img src="<?php echo $request['groupProfile']; ?>" alt="" class="wh-50"></a>
                                                    <?php } else { ?>
                                                        <a href="<?= base_url('time-line/'.$request['username']); ?>" title=""><img src="<?php echo  base_url('assets/images/fav.png'); ?>" alt=""></a>
                                                    <?php }
                                                    ?>
                                                </figure>

                                                <div class="frnd-meta">
                                                    <div class="frnd-name">
                                                        <a href="<?= base_url('time-line/'.$request['username']); ?>" title=""><?php echo $request['username']; ?></a>
                                                        <span><?php echo $request['name']; ?></span>
                                                    </div>
                                                    <a class="send-mesg" href="<?= base_url('time-line/'.$request['username']); ?>" title="">About</a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } }else{ echo 'No Record Found'; }; ?>
                                </div>
                                <!-- <div class="lodmore">
                                    <button class="btn-view btn-load-more"></button>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- content -->

<script>
    function remove_cart() {
        event.preventDefault();
        var btn = event.target;
        setTimeout(function() {
            $(btn)
                .closest("#conData")
                .fadeOut("fast");
        }, 100);
    }

    $(document).ready(function() {
        $(".confirm_data").click(function() {
            var confirm_data = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/confirmRequest') ?>',
                data: 'confirm_data=' + confirm_data,
                success: function(data) {
                    setTimeout(function() {
                        $(".confirm_data")
                            .closest("#conData")
                            .fadeOut("fast");
                    }, 100);
                }
            });
        });
    });

    $(document).ready(function() {
        $(".delete_data").click(function() {
            var delete_data = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteRequest') ?>',
                data: 'delete_data=' + delete_data,
                success: function(data) {
                    setTimeout(function() {
                        $(".delete_data")
                            .closest("#conData")
                            .fadeOut("fast");
                    }, 100);
                }
            });
        });
    });
</script>