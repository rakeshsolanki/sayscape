<?php
// echo "<pre>"; print_r($post); die();
$loginUserID = $this->session->userdata('userID');
if(!empty($post)){
    $comments = $this->post->getComments($post['postID'], '', $post['userID']);
}
$postID = (isset($postID))?$postID:"";
$postShareID = (isset($postShareID) && !empty($postShareID))?$postShareID:'';
if (!empty($comments)) {
    foreach ($comments as $comment) {
        // echo "<pre>"; print_r($comment);
        if(empty($userDetails)){
            $userDetails = $this->user->getUserdetails($loginUserID, $loginUserID);
        }
        $postID = $comment['postID'];
        $checkLikeStatus = $this->CommonModel->get_row(TBL_POST_COMMENT_LIKE, ["userID", "isLiked", "postID","postCommentID"], ["userID" => $loginUserID,"postID"=>$postID,"postCommentID"=>$comment['postCommentID'],'isLiked'=>1,"isDelete" => 0,"isActive" => 1]);
        $totalLikeCount = $this->post->totalPostCommentLikes($postID, $comment['postCommentID'], $loginUserID);
        
        $checkDisLikeStatus = $this->CommonModel->get_row(TBL_POST_COMMENT_LIKE, ["userID", "isLiked", "postID","postCommentID"], ["userID" => $loginUserID,"postID"=>$postID,"postCommentID"=>$comment['postCommentID'],'isLiked'=>0,"isDelete" => 0]);
        $totaldisLikeCount = $this->post->totalPostCommentLikes($postID, $comment['postCommentID'], $loginUserID, 0);
        ?>
        <li>
            <div class="comet-avatar">
                <img src="<?php echo (!empty($comment['profileImage'])) ? $comment['profileImage'] : base_url('/assets/images/resources/admin.png') ?>" alt="" class="w35">
            </div>
            <div class="we-comment">
                <div class="coment-head">
                    <h6><a href="<?= base_url('time-line/'.$comment['username']) ?>" title=""><?php echo ucfirst($comment['username']); ?></a></h6>
                    <p><?php echo $this->emoji->Decode($comment['text']); ?></p>
                    <?php
                    if (isset($comment['commentImage']) && $comment['commentImage']!="" ) { ?>
                        <img src="<?php echo $comment['commentImage'] ?>" alt="" class="commentImg postpicture" />
                    <?php } ?>
                    <div class="inline-itms">
                        <span class="displayMomentDateOnly" datetime="<?php echo (isset($comment['createdDateTime'])) ? $comment['createdDateTime'] : ""; ?>"></span>
                        <a href="javascript:void(0)" title="" class="likePostComment <?php echo (!empty($checkLikeStatus))?'active':'' ?>" data-id="<?php echo $comment['postCommentID']; ?>" data-postid="<?php echo $postID; ?>"><i class="fa fa-heart"></i>
                            <span id="postCommentlikeCount<?php echo $comment['postCommentID']; ?>"><?php echo $totalLikeCount; ?></span></a>
                        <a href="javascript:void(0)" title="" class="dislikePostComment <?php echo (!empty($checkDisLikeStatus))?'active':'' ?>" data-id="<?php echo $comment['postCommentID']; ?>" data-postid="<?php echo $postID; ?>"><i class="fa fa-heartbeat"></i>
                            <span id="postCommentDislikeCount<?php echo $comment['postCommentID']; ?>"><?php echo $totaldisLikeCount; ?></span></a>
                        <a class="we-reply" href="javascript:void(0)" title="Reply" data-toggle="collapse" data-target="#post_replay<?php echo $comment['postCommentID']; ?>"><i class="fa fa-reply"></i></a>
                    </div>
                    <div id="post_replay<?php echo $comment['postCommentID']; ?>" class="row collapse in" aria-expanded="true">
                        <div class="col-md-12">
                            <form method="post" class="commentForm" enctype="multipart/form-data">
                            <p class="emoji-picker-container" id="emoji-picker-container<?php echo $comment['postCommentID']; ?>">
                                <textarea class="form-control mantionUsers" name="comment" id="replay_post_comment<?php echo $comment['postCommentID']; ?>" data-id="<?php echo $postID; ?>" data-parent="<?php echo $comment['postCommentID']; ?>" placeholder="Write a comment..." data-emojiable="true" data-emoji-input="unicode"><?php echo (isset($comment['username']))?'@'.$comment['username']:""; ?> </textarea>
                            </p>
                            <input type="hidden" name="commentID" value="<?php echo $comment['postCommentID']; ?>" />
                            <input type="hidden" id="gif<?php echo $comment['postCommentID']; ?>" name="gif" value="" />
                            <input type="hidden" class="postID" name="postID" value="<?php echo $postID; ?>" />
                            <input type="hidden" name="postShareID" value="<?php echo $postShareID; ?>" />
                            <div class="attachments text-right">
                                <ul class="cursor-point">
                                    <li class="emojiPicker" data-id="<?php echo $comment['postCommentID']; ?>" id="emoji<?php echo $comment['postCommentID']; ?>">
                                        <i class="fa fa-smile-o"></i>
                                        <label class="fileContainer"> </label>
                                    </li>
                                    <li id="image<?php echo $comment['postCommentID']; ?>">
                                        <i class="fa fa-camera"></i>
                                        <label class="fileContainer">
                                            <input type="file" name="commentImage" id="files<?php echo $comment['postCommentID']; ?>" multiple accept="image/*" onchange="readFile(this);">
                                        </label>
                                    </li>
                                    <li>
                                        <button class="btn btn-sm webbgcolor text-white submit_post_replay mb15" type="submit" name="post" value="POST" data-id="<?php echo $postID; ?>" data-parent="<?php echo $comment['postCommentID']; ?>" id="btnSaveCommentReplay<?php echo $comment['postCommentID']; ?>">Send</button>
                                    </li>
                                </ul>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php $this->post->loadSubComments($comment); ?>
            </div>
        </li>
<?php }
} ?>
<li class="post-comment">
    <!-- <div class="comet-avatar">
        <img src="<?php echo (!empty($userDetails['profileImage'])) ? $userDetails['profileImage'] : base_url('/assets/images/resources/admin.png') ?>" alt="" class="w35">
    </div> -->
    <div class="">
        <form method="post" enctype="multipart/form-data" class="commentForm" data-id="<?php echo $postID; ?>">
            <p class="emoji-picker-container" id="emoji-picker-container<?php echo $postID; ?>">
                <textarea class="form-control mantionUsers" name="comment" id="post_comment<?php echo $postID; ?>" data-id="<?php echo $postID; ?>" placeholder="Write a comment..." data-emojiable="true" data-emoji-input="unicode"></textarea>
            </p>
            <input type="hidden" id="gif<?php echo $postID; ?>" name="gif" value="" />
            <input type="hidden" class="postID" name="postID" value="<?php echo $postID; ?>" />
            <input type="hidden" name="postShareID" value="<?php echo $postShareID; ?>" />
            <div class="attachments text-right">
                <ul class="cursor-point">
                    <li class="emojiPicker" id="emoji<?php echo $postID; ?>" data-id="<?php echo $postID; ?>">
                        <i class="fa fa-smile-o"></i>
                        <label class="fileContainer"> </label>
                    </li>
                    <li id="image<?php echo $postID; ?>">
                        <i class="fa fa-camera"></i>
                        <label class="fileContainer">
                            <input type="file" name="commentImage" id="files<?php echo $postID; ?>" multiple accept="image/*" onchange="readFile(this);">
                        </label>
                    </li>
                    <li>
                        <button class="btn btn-sm webbgcolor text-white submit_post_replay mb15" type="submit" name="post" value="POST" data-id="<?php echo $postID; ?>" id="btnSaveComment<?php echo $postID; ?>">Send</button>
                    </li>
                </ul>
            </div>
        </form>
    </div>
</li>
<style>

</style>

<script>
function readFile(){}
var d = new Date();
    var n = d.getTimezoneOffset();
    var ans = new Date(d.getTime() + n * 60 * 1000);
    var f = ans.getHours()+':'+ans.getMinutes()+':'+ans.getSeconds()

    var dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'numeric', day: '2-digit' });
    [{ value: mo },,{ value: da },,{ value: ye }] = dtf.formatToParts(d) ;
    var bb = `${ye}-${mo}-${da}`;
    var newnow =  bb+' '+f;
$('.displayMomentDateOnly').each(function(i, e) {
    var time = moment($(e).attr('datetime'));
    // console.log(now.diff(time, 'days'));
    //if(now.diff(time, 'days') <= 1) {
        $(e).html('<span>' + time.from(newnow) + '</span>');
    //}
});

// mansion js
var mantionUsers = <?= $this->user->getUsernameJson(); ?>;
$('.mantionUsers').atwho({
    at: "@",
    data: mantionUsers
});

$(function() {
    $('.emoji-picker').hide();
    $('.emojiPicker').click(function() {
        $('.emoji-picker').click();
        // var postID = $(this).attr('data-id');
        // $('.emoji-menu').hide();
        // $('#emoji-picker-container .emoji-menu').show();
    });
});
;(function($, window, document, undefined) {
    
    window.emojiPicker = new EmojiPicker({
        emojiable_selector: '[data-emojiable=true]',
        assetsPath: '<?= base_url('assets/emoji/img/') ?>/',
        popupButtonClasses: 'fa fa-smile-o'
    });
    window.emojiPicker.discover();

    // mansion js
    var mantionUsers = <?= $this->user->getUsernameJson(); ?>;
    $('.emoji-wysiwyg-editor').atwho({
        at: "@",
        data: mantionUsers
    });
    
})(jQuery, window, document);
</script>