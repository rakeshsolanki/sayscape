<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title><?php echo (isset($title)) ? $title.' | '.$this->config->item('site_title') : $this->config->item('site_title'); ?></title>
    <link rel="icon" href="<?php echo  base_url('assets/images/fav.png'); ?>" type="image/png" sizes="16x16">

    <link rel="stylesheet" href="<?= base_url('assets/css/main.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/weather-icon.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/weather-icons.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/color.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/css/responsive.css') ?>">
    <style>
        @media only screen and (max-width: 600px) {
            .login-big{
                height: unset;
            }
            .we-login-register{
                margin-bottom: 1px;
            }
            .barcode {
                margin-top: 5px;
            }   
        }
        .we-login-register .we-form p{
            line-height: 15px;
            margin: 2px;
        }
        .spanc, .spanc .we-account{
            text-transform: unset !important;
        }
        .we-login-register{
            padding: 30px 35px 30px;
        }
        .gap.signin .bg-image{
            height: 100%;
        }
    </style>
</head>
<body>
<div class="www-layout">
    <section>
        <div class="gap no-gap signin whitish medium-opacity register">
            <div class="bg-image" style="background-image:url(<?= base_url('assets/') ?>images/theme-bg.png);z-index: 0;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="col login-big" style="text-align: justify;">
                            <figure class="text-center"><img src="<?php echo base_url('assets/') ?>images/logo_transparent.png" alt="" width="70%"></figure>
                            <!-- <h1>Sayscape</h1> -->
                            <div class="d-none d-sm-block">
                                <p> SayScape is the future of social media and the way people communicate by providing a place to be yourself without fear of unfair bias. Military veterans and first responders have a special place here on SayScape.</p>
                                <p>SayScape brings people together no matter what their background or beliefs. It’s designed around the understanding that everyone deserves a voice. SayScape is not a political platform. It’s not targeted towards any specific affiliation or ideology. The people need an unbiased platform to collaborate, discuss, share ideas, and debate key topics. SayScape will not attempt to silence legal content or sell your private data. That is what SayScape is all about.</p>    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="we-login-register">
                            <?php $this->load->view('front/flash_messages') ?>
                            <div class="form-title">
                                <i class="fa fa-key"></i>Sign Up
                                <span>Sign Up now and meet the awesome friends around the world.</span>
                            </div>
                            <form class="we-form" method="post">
                                <span>Profile Picture</span>
                                <input type="file" name="picture" />
                                <input type="text" required placeholder="Display Name" name="name" value="<?= set_value('name'); ?>" />
                                <p><small>* Do not include @ in User Name</small></p>
                                <input type="text" required placeholder="User Name" name="username" value="<?= set_value('username'); ?>" />
                                <input type="email" required placeholder="Email" name="email" value="<?= set_value('email'); ?>" />
                                <input type="password" required placeholder="Password" name="password" value="<?= set_value('Password'); ?>" />
                                <p><small>* (Password length 8 characters minimum, 1 upper, 1 lower, 1 number, 1 special)</small></p>
                                <button type="submit" name="register" value="register">Register</button>
                            </form>
                            <span>already have an account? <a class="we-account underline" href="<?= base_url('login');?>" title="">Sign in</a></span>
                            <span class="spanc"> For More Information <a class="we-account underline" href="https://sayscape.info" title="">sayscape.info</a></span>
                        </div>
                        
                        <div class="barcode">
                            <!--<figure><img src="<?php echo base_url('assets/') ?>images/resources/Barcode.jpg" alt=""></figure>-->
                            <div class="app-download">
                                <!--<span>Download Mobile App to login</span>-->
                                <ul class="colla-apps text-center"> 
                                    <li><a class="text-center" title="" href="https://play.google.com/store?hl=en"><img src="<?php echo base_url('assets/') ?>images/android.png" alt="">android</a></li>
                                    <li><a class="text-center" title="" href="https://www.apple.com/lae/ios/app-store/"><img src="<?php echo base_url('assets/') ?>images/apple.png" alt="">iPhone</a></li>
                                    <li><a title="" href="javascript:void(0)"><img src="<?php echo base_url('assets/') ?>images/Comodo-Secure.png" style="margin-top: 5px;height: 50px;" alt=""></a></li>
                                    <!-- <li><a title="" href="https://www.microsoft.com/store/apps"><img src="<?php echo base_url('assets/') ?>images/windows.png" alt="">Windows</a></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</div>

</body>

</html>