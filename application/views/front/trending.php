<?php
$cur_tab = $this->uri->segment(2);
$loginUserID = $this->session->userdata('userID');
?>
<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-6">
                            <div class="central-meta new-pst text-center">
                                <a href="<?= base_url('trending'); ?>"><button class="btn w120 <?php echo ($cur_tab == 0) ? "text-white webbgcolor" : "" ?>">24 hours</button></a>
                                <a href="<?= base_url('trending/1'); ?>"><button class="btn w120 <?php echo ($cur_tab == 1) ? "text-white webbgcolor" : "" ?>">Week</button></a>
                                <a href="<?= base_url('trending/2'); ?>"><button class="btn w120 <?php echo ($cur_tab == 2) ? "text-white webbgcolor" : "" ?>">Month</button></a>
                                <a href="<?= base_url('trending/3'); ?>"><button class="btn w120 <?php echo ($cur_tab == 3) ? "text-white webbgcolor" : "" ?>">Year</button></a>
                            </div>
                            <?php
                            if(!empty($postList)){
                                ?><div class="loadMorePosts"><?php
                                $count = 1;
                                foreach($postList as $post){
                                    $datas['post'] = $post;
                                    $datas['count'] = $count;
                                    $this->load->view('front/post_view',$datas);
                                    $count ++;
                                    //break;
                                } ?> </div>
                                <input type="hidden" id="pageno" class="pageno" value="1" />
                                <button class="postLoader btn-view btn-load-more" data-user="<?php echo $loginUserID; ?>" data-type="trending" data-other="<?php echo (isset($cur_tab))?$cur_tab:0; ?>">Load More</button>
                            <?php }else{ ?>
                                <div class="central-meta item">
                                    <div class="user-post">
                                        <h6 class="text-center"><?php echo getMessage('notFoundPosts'); ?></h6>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-lg-3">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>