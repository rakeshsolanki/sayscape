<section>
    <div class="gap2 gray-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <div class="col-lg-3 d-none d-sm-block">
                            <?php $this->load->view('front/sidebar_menu'); ?>
                        </div>
                        <div class="col-lg-6">
                            <!-- <div class="widget stick-widget isVeteranSticky">
                                <div class="central-meta item">
                                    <div class="user-post">
                                        <h6 class="text-center"><span class="sitecolor">Crisis Line</span> 800-273-8255</h6>
                                        <h6 class="text-center"><span class="sitecolor">Text</span> 838255</h6>
                                    </div>
                                </div>
                            </div> -->
                            <?php
                            if(isset($userDetails['isResponder']) && $userDetails['isResponder']==1){
                                $this->load->view('front/post_form', array('postType'=>4));
                                if(!empty($postList)){
                                    ?><div class="loadMorePosts"><?php
                                    $count = 1;
                                    foreach($postList as $post){
                                        $datas['post'] = $post;
                                        $datas['count'] = $count;
                                        $this->load->view('front/post_view',$datas);
                                        $count ++;
                                        //break;
                                    } ?></div>
                                    <input type="hidden" id="pageno" class="pageno" value="<?= isset($page) ? $page:1 ?>" />
                                    <button class="postLoader btn-view btn-load-more" data-type="responder">Load More</button>
                            <?php
                                }else{ ?>
                                    <div class="central-meta item">
                                        <div class="user-post">
                                            <h6 class="text-center"><?php echo getMessage('notFoundPosts'); ?></h6>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php }else{ ?>
                                <div class="central-meta item">
                                    <div class="user-post">
                                        <h6 class="text-center"> Responder Post not allow.</h6>
                                    </div>
                                </div>
                            <?php } ?>
                        </div><!-- centerl meta -->
                        <div class="col-lg-3">
                            <?php include('left_sidebar.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
