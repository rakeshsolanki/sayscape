<?php $page = (isset($pageType))?$pageType:"";  ?>
<section>
    <div class="gap2 gray-bg" id="pageGroupDetail">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row merged20" id="page-contents">
                        <div class="user-profile">
                            <figure>
                                <img class="coverImg" src="<?= base_url('assets/images/resources/profile-image2.jpg'); ?>" alt="">
                                <ul class="profile-controls">
                                    <li><a class="" href="javascript:void(0)" title="Add Member" data-toggle="modal" data-target="#groupMemberEdit"><i class="fa fa-user-plus"></i></a></li>
                                </ul>
                            </figure>

                            <div class="profile-section">
                                <div class="row">
                                    <div class="col-lg-2 col-md-3">
                                        <div class="profile-author">
                                            <a class="profile-author-thumb" href="#">
                                                <img src="<?php echo (!empty($groupDetails['groupProfile']))?$groupDetails['groupProfile']:base_url('assets/images/resources/group13.jpg'); ?>" alt="" class="">
                                            </a>
                                            <div class="author-content">
                                                <a class="h4 author-name" href="#">
                                                <?php if (isset($groupDetails['groupName'])) {
                                                    echo $this->emoji->Decode(ucfirst($groupDetails['groupName']));
                                                } ?></a>
                                                <div class="country"><?php echo (isset($groupDetails['groupName']))?$groupDetails['groupName']:""; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-9">
                                        <ul class="profile-menu nav nav-tabs">
                                            <li>
                                                <a href="#textpost" class="nav-link active" data-toggle="tab"><i class="fa fa-user"></i> Post</a>
                                            </li>
                                            <li>
                                                <a href="#mediapost" class="nav-link " data-toggle="tab"><i class="fa fa-image"></i> Media</a>
                                            </li>
                                            <li>
                                                <a href="<?= base_url('group-message/' . $groupDetails['groupID']); ?>" class="nav-link " ><i class="fa fa-comments"></i> Message</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    <!-- user profile banner -->
                        
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="textpost" >
                            <div class="loadMore">
                                <?php
                                if(!empty($text_post_list)){
                                    $count = 1;
                                    foreach($text_post_list as $post){
                                        $datas['post'] = $post;
                                        $datas['count'] = $count;
                                        $this->load->view('front/post_view',$datas);
                                        $count ++;
                                        //break;
                                    }
                                }else{ ?>
                                    <div class="central-meta item">
                                        <div class="user-post">
                                            <div class="description">
                                                <p class="text-center text-danger">Post are not available.</p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane fade " id="mediapost" >
                            <div class="loadMore">
                                <?php
                                if(!empty($media_post_list)){
                                    $count = 1;
                                    foreach($media_post_list as $post){
                                        $datas['post'] = $post;
                                        $datas['count'] = $count;
                                        $this->load->view('front/post_view',$datas);
                                        $count ++;
                                        //break;
                                    }
                                }else{ ?>
                                    <div class="central-meta item">
                                        <div class="user-post">
                                            <div class="description">
                                                <p class="text-center text-danger">Post are not available.</p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>  
                </div>
                <div class="col-lg-4">
                    <aside class="sidebar static right">
                    
                        <div class="widget stick-widget ">
                            <div class="tab-content">
                                <div class="tab-pane active fade show " id="tabFollowers">
                                    <div class="widget ">
                                        <h4 class="widget-title">Group Member</h4>
                                        <ul class="followers pd-l-10">
                                            <?php
                                            if (!empty($groupmember)) {
                                                foreach ($groupmember as $member) { ?>
                                                    <li id="removeMember">
                                                        <figure>
                                                            <img src="<?php echo $member['profileImage']; ?>" alt="" class="wh-40">
                                                            <span class="statusnew f-<?php echo $this->user->getUserOnlineStatus($member['userID']); ?>"></span>
                                                        </figure>
                                                        <div class="friend-meta">
                                                            <h4><a href="<?php echo base_url('time-line/' . $member['username']) ?>"><?php echo $member['username'] ?></a></h4>
                                                            <?php if(!empty($remove) ){ ?>
                                                                <a href="javascript:void(0)" title="" class="add-butn delete_data remove_group_member"  id="<?php echo $member['userGroupID']; ?>" >Remove</a>
                                                            <?php } ?>
                                                        </div>
                                                    </li>
                                            <?php }
                                            } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div><!-- who's following -->
                    </aside>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="groupMemberEdit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Manage Members</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            <div class="group-adding">
                <div class="friend-group">
                <form class="" method="post" enctype="multipart/form-data">
                <div class="col-sm-12">
                        <?php
                        $groupMemberUsers = array();
                        if(!empty($groupmember)){
                            foreach($groupmember as $member){
                                $groupMemberUsers[] = $member['userID'];
                            }
                        }
                        if (!empty($followingList)) {
                            // echo "<pre>"; print_r($groupMemberUsers);
                            ?><div class="row"><?php
                            foreach ($followingList as $member) { ?>
                                <div class="col-sm-4 checkbox">
                                    <figure>
                                        <img src="<?php echo $member['profileImage']; ?>" alt="" class="wh-40">
                                        <!-- <span class="statusnew f-<?php echo $this->user->getUserOnlineStatus($member['userID']); ?>"></span> -->
                                    </figure>
                                    <label><input type="checkbox" name="memberID[]" <?php echo (in_array($member['followerID'],$groupMemberUsers))?"checked disabled":"";?> value="<?php echo $member['followerID']; ?>" />
                                    <i class="check-box"></i><?php echo ucfirst($this->emoji->Decode($member['name'])); ?></label>
                                </div>
                            <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-sm-12">
                    <hr>
                        <button type="submit" name="btnSaveMembers" value="submit" class="main-btn">Save</button>
                    </div>
                </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function() {
        $(".remove_group_member").click(function() {
            var remove_group_member = $(this).attr('id');
            var btn = event.target;
            $.ajax({
                type: 'POST',
                url: '<?php echo base_url('home/deleteGroupMember') ?>',
                data: 'remove_group_member=' + remove_group_member,
                success: function(data) {
                    $(btn)
                .closest("#removeMember")
                .fadeOut("fast");
                }
            });
        });
    });
</script>