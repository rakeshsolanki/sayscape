<script src="<?= BASE_URL ?>admin_assets/global_assets/js/plugins/media/fancybox.min.js"></script>
<div class="card" style="zoom: 1;">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Post List</h6>
    </div>
    <div class="card-body" style="">
        <ul class="nav nav-tabs nav-tabs-highlight mb-0">
            <!-- <li class="nav-item"><a href="#pill-tab1-active" class="nav-link legitRipple active show" data-toggle="tab"><span class="badge badge-success badge-pill mr-2" id="ActiveDTCount"></span> Active</a></li> -->
            <li class="nav-item"><a href="#pill-tab2-active" class="nav-link legitRipple active show" data-toggle="tab"><span class="badge badge-pill bg-warning mr-2" id="ReportDTCount">0</span> Report</a></li>
        </ul>
        <div class="tab-content card card-body border border-top-0 rounded-top-0 shadow-0 mb-0">
            <div class="tab-pane fade active show" id="pill-tab2-active">
                <table class="table table-bordered table-hover table-striped table-xs" id="reportPostList_table"></table>
            </div>
        </div>
    </div>
</div>
<div id="media_list" style="display:none">
</div>
<script src="<?=BASE_URL?>admin_assets/assets/scripts/moderator_post.js"></script>