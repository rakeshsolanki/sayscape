<?php

/**
* Prints array/object in pre-formatted
*
* @param array $array
* @return void
*/
function pr($var) {
    echo "<pre>";print_r($var);echo "</pre>";
}

/**
* Converts datetime string to given format
*
* @param string $datetimeValue
* @param string $datetimeFormat
* @return string
*/
function convertDateTime($datetimeValue, $datetimeFormat = null) {
    return date($datetimeFormat, strtotime($datetimeValue));
}

function getMessage($key=""){
    $result = "No Message Found";
    $messageData=array(
        'passPassword'=>'Please enter Password.',
        'passUsername'=>'Please enter Username.',
        'passDeviceType'=>'Please Pass Device Type.',
        'passDeviceToken'=>'Please pass Device Token.',
        'passToken'=>'Please pass Access Token.',
        'tokenExpire'=>'Token expired, Please login.',
        'invalidLogin'=>'Username Or Password Incorrect!',
        'usernameInvalid'=>'Username format is invalid',
        'loginSuccess'=>'Login Successfully!',
        'logoutSuccess'=>'Logout Successfully!',
        'userisBlock'=>'This User Is Blocked',
        'regSuccess'=>'User Registered Successfully',
        'insertSuccess'=>'Added Successfully',
        'updateSuccess'=>'Updated Successfully',
        'verifyAccount'=>'Please verify your Account.',
        'codeNotMatch'=>'Code not match, please enter Correct Code.',
        'passVerifyCode'=>'Please enter Verification Code.',
        'userVerifySuccess'=>'User Verify Successfully.',
        'resendOTPfail'=>'Re-Send OTP Failed.',
        'resendOTPsuccess'=>'Re-Send OTP Successfully.',
        'NotInsert'=>'Something went wrong, please try again!!',
        'NoData'=>'No Data Found',
        'DeleteSuccess'=>'Deleted Successfully',
        'emailExist'=>'Email already exist.',
        'phoneExist'=>'Phone no already exists',
        'usernameExist'=>'Username already exists',
        'passEmail'=>'Please enter Email ID',
        'passName'=>'Please enter Name',
        'regFail'=>'Fail to register User.',
        'passMobile'=>'Please enter Mobile Number.',
        'passUserId'=>'Please pass User ID.',
        'passFromUserId'=>'Please pass From User ID.',
        'passToUserId'=>'Please pass To User ID.',
        'updateProfileSuccess'=>'Profile Updated Successfully',
        'userNotFound'=>'User not found.',
        'userFound'=>'User Found Successfully.',
        'PostSuccess'=>'Post Created Successfully',
        'PostEditSuccess'=>'Post Updated Successfully',
        'passPostId'=>'Please Pass Post ID',
        'PostDeleteSuccess'=>'Post Deleted Successfully',
        'passMediaId'=>'Please Pass Media ID',
        'MediaDeleteSuccess'=>'Media Deleted Successfully',
        'PostLikeSuccess'=>'Post Liked Successfully', 
        'PostLikeRemoveSuccess'=>'Post Liked Removed Success',
        'PostLikeRemoveError'=>'Post Liked Removed Error',
        'PostLikeError'=>'Post Like Error',
        'PostLikeAlreadyError'=>'This Post Already like by user',
        'PostShareSuccess'=>'Post Shared Successfully',
        'PostShareError'=>'Post Share Error',
        'PostReShareSuccess'=>'Post ReShare Added Successfully',
        'PostAlreadyError'=>'Post Already Shared By User',
        'PostCommentSuccess'=>'Comment Added Successfully',
        'PostCommentDeleteSuccess'=>'Comment Deleted Successfully',
        'UserDeleteSuccess'=>'User Deleted Successfully',
        'PostBookmarkSuccess'=>'Bookmark Added Successfully',
        'PostDisLikeSuccess'=>'Post Disliked Successfully',
        'PostDisLikeError'=>'Post Disliked Error',
        'getPostRecordsSuccess'=>'Get Post Records Successfully',
        'notFoundComment'=>'Comment Not Found',
        'notFoundComments'=>'Comments Not Found',
        'FoundComments'=>'Comments Found Successfully',
        'notFoundPosts'=>'Posts Not Found',
        'PostDislikeRemoveError'=>'Post Dislike Removed Error', 
        'PostDisLikeAlreadyError'=>'This Post Already Dislike by user',
        'PostAlreadyShareError'=>'This Post Already Shared By user',
        'PassReportID'=>'Please Pass Report ID',
        'PassReportReason'=>'Please Pass Report Reason',
        'passfollowerID'=>'Please pass followerID',
        'followerSuccess'=>'Follow User Successfully',
        'followerError'=>'Follow User Error',
        'FollowAlready'=>'User Already Follow this user',
        'UnFollowSuccess'=>'UnFollow User Success',
        'UnfollowError'=>'UnFollow User Error',
        'NotFollowError'=>'User not follow this user',
        'UserOwnFollowError'=>'User not get Notification by own',
        'UserOwnSubscribeError'=>'User not Notification own post',
        'NotSubscribeError'=>'User not get Notification for this user',
        'UnsubscribeError'=>'notification User post Error',
        'UnsubscribeSuccess'=>'You are not get notification of this user in future',
        'subscribeSuccess'=>'Notification enable for this User',
        'subscribeError'=>'Notification get User post Error',
        'subscribeAlready'=>'User Already get notification of this user',
        'reqDeleteSuccess'=>'Request Deleted Successfully',
        'reqAcceptSuccess'=>'Request accept Successfully',
        'pollAnsExist'=>'Poll Answer already exists!',
        'pollAnsSuccess'=>'Poll Answer Successfully',
        'passOptionID'=>'Please Pass Option ID',
        'FolloweIDError'=>'Follower ID Not Exists',
        'alreadyExistGroup'=>'Group Already Exist.',
        'groupSuccess'=>'Group Created Successfully',
        'passgroupName'=>'Please pass GroupName',
        'passgroupId'=>'Please pass GroupID',
        'passmemberName'=>'Please pass Members Name',
        'passusergroupID'=>'Please pass User Group ID',
        'getRecordSuccess'=>'Get Records Successfully',
        'MembersAddedSuccess'=>'Members Added Successfully',
        'MembersRemovedSuccess'=>'Member Removed Successfully',
        'notRemoveMember'=>'Member Not Removed',
        'notFoundGroup'=>'Groups Not Found',
        'GroupFoundSuccess'=>'Groups Found Successfully',
        'notFoundMembers'=>'Members Not Found',
        'MembersFoundSuccess'=>'Members Found Successfully',
        'getGroupPostRecordsSuccess'=>'Group Posts Found Successfully',
        'notDeleteGroup'=>'Group Not Delete',
        'GroupDeleteSuccess'=>'Group Deleted Successfully',
        'notFoundMedia'=>'Media Not Found',
        'getMediaRecordsSuccess'=>'Media Found Successfully',
        'notFoundText'=>'Please pass text.',
        'MessageSuccess'=>'Message SuccessFully Send.',
        'MessageError'=>'Error occurred.',
        'PassLoggedINID'=>'Please pass LoggedIN ID',
        'PassWatchID'=>'Please pass watch ID',
        'passstatus'=>'Please Pass Status',
        'wrongstatus'=>'Please pass correct status',
        'passReportType'=>'Please pass report type',
        'ReportInsertError'=>'Report Not Saved',
        'ReportInsertSuccess'=>'Report SuccessFully Saved',
        'ReportAlready'=>'Report Already Exists',
        'PassBlockUserID'=>'Please pass block userID',
        'PassMuteUserID'=> 'Please pass mute userID',
        'BlockUserIDInsertError'=>'User Block Save Error',
        'BlockUserIDInsertSuccess'=>'User Block SuccessFully',
        'UserBlockOwnError'=>'User Not Block by own',
        'BlockIDError'=>'Block User ID Is Not User',
        'BlockAlreadyNot'=>'User is not blocked',
        'BlockAlready'=>'This User Already Block',
        'UnBlockSaveError'=>'User UnBlock Save Error',
        'UnBlockSaveSuccess'=>'User UnBlock SuccessFully',
        'MuteUserIDInsertError'=>'User Mute Save Error',
        'MuteUserIDInsertSuccess'=>'User Mute SuccessFully',
        'UserMuteOwnError'=>'User Not Mute by own',
        'MuteIDError'=>'Mute User ID Is Not User',
        'MuteAlreadyNot' => 'User is not muted',
        'MuteAlready'=>'This User Already Mute',
        'UnMuteSaveError'=>'User UnMute Save Error',
        'UnMuteSaveSuccess'=>'User UnMute SuccessFully',
        'SettingsSaveSuccess'=>'Settings SuccessFully Save',
        'SettingsSaveError'=>'Settings Not Save',
        'SettingUpdateSuccess'=>'Settings Update SuccessFully.!',
        'SettingUpdateError'=>'Setting Update Error.!',
        'passPostCommentID'=>'Please pass commentID',
        'passtype'=>'Please pass type',
        'sendpasswordsuccess'=>'New Password send on your email address.',
		'usernameRejected'=>'Invalid username please try again for other.' 
    );
    if(!empty($key)){
        $result = (isset($messageData[$key]))?$messageData[$key]:"";
    }
    return $result;
}

/**
* @param string $string
* @return string encrypted string
*/
function hashPassword($string) {
    return md5(MYSQL_KEY.trim($string));
}

function getKeyValue($array, $key) {
    if(isset($array[$key])) {
        return $array[$key];
    } else {
        return null;
    }
}

function getCurrency() {
    return '&#x20b9;';
}

function returnSuccess($mesg){
    return ['status'=>TRUE,'result'=>$mesg];
}

function returnFail($mesg){
    return ['status'=>FALSE,'result'=>$mesg];
}