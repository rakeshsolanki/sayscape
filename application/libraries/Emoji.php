<?php
class Emoji
{
	/**
	 * Encode emoji in text
	 * @param string $text text to encode
	 */
	public static function Encode($text) {
		return self::convertEmoji($text,"ENCODE");
	}

	/**
	 * Decode emoji in text
	 * @param string $text text to decode
	 */
	public static function Decode($text,$isSave=false) {
		$text = self::convertEmoji($text,"DECODE",$isSave);
		$text = self::replaceAccents($text);
		$text = str_replace('\"', '"', $text);
		$text = self::detectCustomEmoji($text);
		return str_replace("\\n", "<br>", $text);
	}

	private static function detectCustomEmoji($str) {
		$customImgUrl = base_url('assets/custom_emoji/');
		$regex = "/(cemoji+([0-9_]+))/";
		$str = preg_replace($regex, '<img class="wh25" src="'.$customImgUrl.'$1.png" />', $str);
		$str = str_replace('(', '', $str);
		$str = str_replace(')', '', $str);
		// $str = trim(preg_replace('/\s*\([^)]*\)/', '', $str));
		return($str);
	}

	private static function convertEmoji($text,$op,$isSave=false) {
		if($op=="ENCODE"){
			return preg_replace_callback('/([0-9|#][\x{20E3}])|[\x{00ae}|\x{00a9}|\x{203C}|\x{2047}|\x{2048}|\x{2049}|\x{3030}|\x{303D}|\x{2139}|\x{2122}|\x{3297}|\x{3299}][\x{FE00}-\x{FEFF}]?|[\x{2190}-\x{21FF}][\x{FE00}-\x{FEFF}]?|[\x{2300}-\x{23FF}][\x{FE00}-\x{FEFF}]?|[\x{2460}-\x{24FF}][\x{FE00}-\x{FEFF}]?|[\x{25A0}-\x{25FF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{FE00}-\x{FEFF}]?|[\x{2600}-\x{27BF}][\x{1F000}-\x{1FEFF}]?|[\x{2900}-\x{297F}][\x{FE00}-\x{FEFF}]?|[\x{2B00}-\x{2BF0}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F9FF}][\x{FE00}-\x{FEFF}]?|[\x{1F000}-\x{1F9FF}][\x{1F000}-\x{1FEFF}]?/u',array('self',"encodeEmoji"),$text);
		}else{
			if($isSave==false){
				$text = self::detectUrl($text);
				$text = self::convertHashtags($text);
				$text = self::convertMention($text);
			}
			$replaced = preg_replace("/\\\\u([0-9A-F]{1,4})/i", "&#x$1;", $text);
			$result = mb_convert_encoding($replaced, "UTF-16", "HTML-ENTITIES");
			$text = mb_convert_encoding($result, 'utf-8', 'utf-16');
			return preg_replace_callback('/(\\\u[0-9a-f]{4})+/',array('self',"decodeEmoji"),$text);
		}
	}

	private static function encodeEmoji($match) {
		return str_replace(array('[',']','"'),'',json_encode($match));
	}
	
	private static function decodeEmoji($text) {
		if(!$text) return '';
		$text = $text[0];
		$decode = json_decode($text,true);
		if($decode) return $decode;
		$text = '["' . $text . '"]';
		$decode = json_decode($text);
		if(count($decode) == 1){
		   return $decode[0];
		}
		return $text;
	}
	
	private static function convertHashtags($str) {
		$redirectUrl = base_url('hashtag-post/');
		$regex = "/#+([a-zA-Z0-9_]+)/";
		$str = preg_replace($regex, '<a href="'.$redirectUrl.'$1">$0</a>', $str);
		return($str);
	}

	private static function convertMention($str) {
		$redirectUrl = base_url('time-line/');
		$regex = "/@+([a-zA-Z0-9_]+)/";
		$str = preg_replace($regex, '<a href="'.$redirectUrl.'$1">$1</a>', $str);
		// if(preg_match($regex, $str, $text)) {
		// 	$text = preg_replace($regex, '<a href="'.$redirectUrl.$text[1].'">'.$text[1].'</a>', $text);
		// }
		return($str);
	}

	private static function detectUrl($text) {
		$reg_exUrl = "/(Http|Https|http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(\/\S*)?/";
		if(preg_match($reg_exUrl, $text, $url)) {
			if(strpos($url[0], "//tinyurl") !== false){
				$shorturl = $url[0];
			}else{
				$shorturl = self::get_tiny_url($url[0]);
			}
			$text = preg_replace($reg_exUrl, '<a target="_blank" href="'.$shorturl.'">'.$shorturl.'</a>', $text);
			$text .= self::generateLinkPreview($url[0]);
		}
		return($text);
	}

	private static function generateLinkPreview($link) {
		$result = "";
		
        $link    = preg_match('/^https?:\/\//i', $link) ? $link : 'http://'.$link;
		$url     = parse_url($link);
		$host = $url['host'];
		$url     = $url['scheme'].'://'.$url['host'];
		
        $img_url = $url;
        $link = @file_get_contents($link);
        $dom = new DOMDocument();
        @$dom->loadHTML($link);

        $link = array();
        $link['url'] = $url;
		$link['title'] = "";
		$link['description'] = "";
        $titles = $dom->getElementsByTagName('title');
        if ($titles){
            $title = $titles->item(0);
        }
        if (isset($title))
        {
            $link['title'] = $title->nodeValue;
        }
        $meta = $dom->getElementsByTagName('meta');
        if ($meta) {
            foreach ($meta as $elem){
                if ($elem->getAttribute('property') == 'og:url')
                {
                    $link['url'] = $elem->getAttribute('content');
                }

                if ($elem->getAttribute('name') == 'description')
                {
                    $link['description'] = $elem->getAttribute('content');
                }

                if ($elem->getAttribute('property') == 'og:image')
                {
                    $link['img'] = $elem->getAttribute('content');
                }
            }
        }
        if ( ! isset($link['img'])) {
            $imgs = $dom->getElementsByTagName('img');
            if ($imgs->length){
                $img = $imgs->item(rand(0, $imgs->length - 1))->getAttribute('src');
            }
            if (isset($img)){
                $link['img'] = preg_match("/http/", $img) ? $img : $img_url.$img;
            }
		} 
		
		if(!empty($link['img']) || !empty($link['title']) || !empty($link['detail'])){
			$result .= "<br><div class='linkPreviewDiv col-sm12'><a target='_blank' href='".$link['url']."'>";
				if(isset($link['img']) && $link['img']!=""){
					$result .= "<div class='linked-image align-left'><img src='".$link['img']."' alt='Preview image'></div>";
				}
				$result .= "<div class='detail'>";
					$result .= "<h6 class='ellips2'>".$link['title']."</h6>";
					$result .= "<p class='ellips3'>".$link['description']."</p>";
					$result .= "<div class='sitecolor'>".$host."</div>";
				$result .= "</div>";
			$result .= "</a></div>";
		}
        return $result;
	}
	
	private static function replaceAccents($str){
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í',
				'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü',
				'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë',
				'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û',
				'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ',
				'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę',
				'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ',
				'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ',
				'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń',
				'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ',
				'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š',
				'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů',
				'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž',
				'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ',
				'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ',
				'ǿ', '€', '™', '˜');
		$b = array('');
		return str_replace($a, $b, $str);
	}

	private static function get_tiny_url($url)  {  
		$ch = curl_init();  
		$timeout = 5;  
		curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);  
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);  
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);  
		$data = curl_exec($ch);  
		curl_close($ch);  
		return $data;  
	}
	
}
?>