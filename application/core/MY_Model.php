<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
    public function __construct() {
       // $this->db->query("SET time_zone='+5:30';");
    }
    public function getResharePost($columns = "*",$userID,$postType=NULL,$watchID = NULL) {
        $watchID=$watchID == NULL?$userID:$watchID;
        //Add Reshare Post by unioun
        $this->db->select($columns)
        ->from("tblpostshare tprs")
        ->join("tbluser urs","urs.userID = tprs.userID")
        ->join("tblpost p","p.postID = tprs.postID AND p.isActive = 1 and p.isDelete= 0")
        ->join("tbluser u","u.userID = p.userID")
        ->join("tblpostlike tpl","tpl.postID = p.postID and tpl.userID = $userID and tpl.isLiked = 1 and tpl.isActive = 1 and tpl.isDelete = 0","LEFT")
        ->join("tblpostlike tpl1","tpl1.postID = p.postID and tpl1.userID = $userID and tpl1.isLiked = 0 and tpl1.isActive = 1 and tpl1.isDelete = 0","LEFT")
        ->where("tprs.isActive = 1 and tprs.isDelete= 0")
        ->where("p.postID NOT IN (
            SELECT
                thp.postID
            FROM
                tblhidepost thp
            WHERE
                thp.userID = $watchID and thp.isActive = 1 AND thp.isDelete = 0
        )")
        ->where("(tprs.userID IN(SELECT followerID  FROM `tbluserfollower` WHERE `userID` = $watchID) OR tprs.userID = $watchID)")
        ->where("(p.isexpireOn = 0 OR ( p.isexpireOn = 1 and (CURDATE() <= DATE(p.expiryDateTime))))");
        //0->Normal Post , 2->Veteran Post	
        if($postType == 0 || $postType == 2)
        $this->db->where("p.postType",$postType);
        return $query3 = $this->db->get_compiled_select();
     }
}