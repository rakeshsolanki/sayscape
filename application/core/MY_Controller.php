<?php

class MY_Controller extends CI_Controller {
    
    function __construct() {
        parent::__construct();
    }
    public function store_logs($data = null) {
        $result = "";
        $result = $result."[".date('Y-m-d H:i:s')."]"."\n";
        $result = $result."uri_string: ".$this->uri->uri_string."\n";
        $result = $result."get: ".json_encode($_GET)."\n";
        $result = $result."post: ".json_encode($_POST)."\n";
        
        if($data != null) {
            $result = $result.json_encode($data)."\n";
        }
        
        $result = $result."\n";
        $log_file_name = "log_".date('Y-m-d').".txt";
        $log_file_path = APPPATH.'logs/'.$log_file_name;
        file_put_contents($log_file_path, $result, FILE_APPEND);
    }
    public function generateLog($response=""){
        $requestParameter = file_get_contents('php://input');
        $requestUrl = base_url(uri_string());
        //echo "<pre>"; print_r($requestParameter); die();
        
        $log  = "User: ".$_SERVER['REMOTE_ADDR'].' - '.date("F j, Y, g:i a").PHP_EOL;
        $log .= "URL: ".$requestUrl.PHP_EOL;
        $log .= "Parameter: ".$requestParameter.PHP_EOL;
        if(!empty($response)){
            //$log .= "Response: ".json_encode($response).PHP_EOL;
        }
        $log .= "--------------------------------------------------".PHP_EOL.PHP_EOL;
        file_put_contents('application/logs/log_'.date("d_M_Y").'.log', $log, FILE_APPEND);
    }

    public function escapeJsonString($value) {
        $result = str_replace('\\\\n', '\\n', $value);
        return $result;
    }
    
    public function response($response) {
        if(empty($response['data'])){
            $response['data'] = new stdClass();
        }
        if(empty($response['code'])){
            $response['code'] = 101;
        }
        if(empty($response['message'])){
            $response['message'] = "No Message found";
        }
        header('Content-Type:application/json');
        // echo json_encode($response);
        $response = json_encode($response);
        echo $this->escapeJsonString($response);

        $this->generateLog($response);
        exit();
    }
    
    public function render($data=""){
        $this->load->view('front/layout/header', $data);
        $this->load->view('front/layout/navbar', $data);
        if(!empty($data['view'])){
            if(is_array($data['view'])){
                foreach($data['view'] as $view){
                    $this->load->view($view);
                }
            }else{
                $this->load->view($data['view']);
            }
        }
        $this->load->view('front/layout/footer', $data);
    }

    public function renderPartial($data=""){
        if(!empty($data['view'])){
            if(is_array($data['view'])){
                foreach($data['view'] as $view){
                    $this->load->view($view, $data);
                }
            }else{
                $this->load->view($data['view'], $data);
            }
        }
    }
    
    //for get site setting value
    public function getSiteSetting($key=""){
        $result = "";
        if(!empty($key)){
            $query = $this->db->get_where(TBL_SITE_SETTING, array('isActive' => 1,'isDelete' => 0,'settingKey' => $key));
            $resultData = $query->row_array();
            if(!empty($resultData)){
                $result = $resultData['settingValue'];
            }
        }
        return $result;
    }
    
    //for check user login or not
    public function checkUserLogin(){
        $result = "";
        if ($this->session->has_userdata('is_user_login') && $this->session->has_userdata('userID')) {
            $result = $this->session->userdata('userID');
        }
        return $result;
    }
    
    //for generate unique acces accessToken for user
    public function generateToken($uniqval = '', $len = 30) {
        if ($uniqval == "" || $uniqval == null) {
            $uniqval = time();
            //echo $uniqval;
        }
        $hex = md5($uniqval . uniqid("", true));
        $pack = pack('H*', $hex);
        $uid = base64_encode($pack);
        $uid = str_replace("==", "", $uid);
        $uid = str_replace("+", "", $uid);
        $uid = str_replace("/", "", $uid);
        $uid = preg_replace("[^A-Z0-9]", "", strtoupper($uid));
        if ($len < 4)
        $len = 4;
        if ($len > 128)
        $len = 128;
        while (strlen($uid) < $len)
        $uid = $uid . $this->generateToken($uniqval, 20);
        return substr($uid, 0, $len);
    }
    public function rand_code($length = 4) {
        $chars = "0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }
    
    public function validStrLen($str, $min, $max){
        $len = strlen($str);
        
        if($len < $min){
            return "User Name is too short, minimum is $min characters ($max max).";
        }
        elseif($len > $max){
            return "User Name is too long, maximum is $max characters ($min min).";
        }
        return 'true';
    }

    function random_password() 
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password = array(); 
        $alpha_length = strlen($alphabet) - 1; 
        for ($i = 0; $i < 8; $i++) 
        {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        return implode($password); 
    }
    
}
