<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MY_Controller
{

    private $response = array(
        'code' => 101,
        'message' => 'No Message Found',
        'data' => []
    );
    private $userID = null;
    private $accessToken = null;


    public function __construct()
    {

        parent::__construct();

        $this->load->database();

        $this->load->model('UserModel', 'user');

        $this->load->model('LoginModel', 'login');

        $this->load->model('PostModel', 'post');

        $this->load->model('GroupModel', 'group');

        $this->load->model('MessageModel', 'message');

        $this->load->model('NotificationModel', 'notification');
        $this->load->library('emoji');

        $notCheckAuthMethod = array(

            "register_user",

            "loginWithSocialMedia",

            "user_login",

            "forgot_password",

            "create_group",

            "sendMessage",

            "editProfile",

            "verify_user",

            "test_notification"

        );


        if (!empty($_GET) && is_array($_GET)) {

            $_GET = array_map("trim", $_GET);

        }

        if (!empty($_POST) && is_array($_POST)) {

            // $_POST = array_map("trim", $_POST);

        }

        if (!in_array($this->router->method, $notCheckAuthMethod)) {

            $this->init();

        }

    }


    private function init()
    {

        $this->response['code'] = 101;

        $this->response['message'] = getMessage("MessageError");

        if (!$this->input->post('userID') || empty($this->input->post('userID'))) {

            $this->response['message'] = getMessage('passUserId');

            $this->response($this->response);

        }

        if (!$this->input->post('accessToken') || empty($this->input->post('accessToken'))) {

            $this->response['message'] = getMessage('passToken');

            $this->response($this->response);

        }


        $this->userID = $this->input->post('userID');

        $this->accessToken = $this->input->post('accessToken');


        $userInitDetails = $this->user->checkUserAuth($this->userID, $this->accessToken);


        $this->userName = $userInitDetails['name'];

    }

    public function test_notification()
    {

        $this->sendNotification(array(

            "type" => "profile",

            "title" => "SayScape",

            "desc" => ucfirst("shashank") . " has unfollowed you.",

            "sendNotificationUserID" => 40,

            "targetID" => 40,
            // "senderID"=>$this->userID,
            "notifyType"=>"unfollow",

        ));

    }

    //for Register new User


    public function register_user()
    {

        $this->store_logs();


        if (!$this->input->post('email') || empty($this->input->post('email'))) {

            $this->response['message'] = getMessage('passEmail');

            $this->response($this->response);

        }
        if (!$this->input->post('deviceType') || empty($this->input->post('deviceType'))) {

            $this->response['message'] = getMessage('passDeviceType');

            $this->response($this->response);
        }
        // if (!$this->input->post('deviceToken') || empty($this->input->post('deviceToken'))) {
        // $this->response['message'] = getMessage('passDeviceToken');
        // $this->response($this->response);
        // }

        $checkEmailExist = $this->CommonModel->get_row(TBL_USER, ['userID', 'isVeteran', 'isOffical'], ['email' => $this->input->post('email')]);

        if (!empty($checkEmailExist)) {

            $this->response['message'] = getMessage('emailExist');

            $this->response($this->response);

        }


        $this->accessToken = $this->generateToken();

        $otp = $this->rand_code(4);


        if (!preg_match("/^[a-zA-Z0-9_]+$/", $this->input->post('username'))) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage('usernameInvalid');

            $this->response($this->response);

        }

        //Validation of set minimum 3 maximum 16 :Hetal Patel

        $lenValidation = $this->validStrLen($this->input->post('username'), 3, 16);

        if ($lenValidation != 'true') {

            $this->response['code'] = 101;

            $this->response['message'] = $lenValidation;

            $this->response($this->response);

        }


        if ($this->CommonModel->checkExists(TBL_USER, ['username' => $this->input->post('username'), "isActive" => 1, "isDelete" => 0])) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage('usernameExist');

            $this->response($this->response);

        }


        if ($this->CommonModel->checkExists(TBL_REJECTED_USERNAME, ['rejectUsername' => $this->input->post('username'), "isActive" => 1, "isDelete" => 0])) {
            $this->response['code'] = 101;
            $this->response['message'] = getMessage('usernameRejected');
            $this->response($this->response);
        }
        if ($this->input->post('phone') && !empty($this->input->post('phone'))) {

            if ($this->CommonModel->checkExists(TBL_USER, ['phone' => $this->input->post('phone'), "isActive" => 1, "isDelete" => 0])) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage('phoneExist');

                $this->response($this->response);

            }

        }


        $arrUserInsert['name'] = $this->input->post('name');

        $arrUserInsert['username'] = $this->input->post('username');

        $arrUserInsert['email'] = $this->input->post('email');

        $arrUserInsert['password'] = md5($this->input->post('password'));

        $arrUserInsert['phone'] = $this->input->post('phone') ? $this->input->post('phone') : '';

        $arrUserInsert['location'] = $this->input->post('location') ? $this->input->post('location') : '';

        $arrUserInsert['dob'] = $this->input->post('dob') ? $this->input->post('dob') : '';

        $arrUserInsert['deviceType'] = $this->input->post('deviceType') ? $this->input->post('deviceType') : "";

        $arrUserInsert['deviceToken'] = ($this->input->post('deviceToken') != "") ? $this->input->post('deviceToken') : "";

        $arrUserInsert['profileImage'] = "defaultProfile.png";

        $arrUserInsert['otp'] = $otp;


        $this->userID = $this->CommonModel->insert(TBL_USER, $arrUserInsert);


        if (empty($this->userID)) {

            $this->response['message'] = getMessage('regFail');

            $this->response($this->response);

        }

        $arrTokenInsert['userID'] = $this->userID;

        $arrTokenInsert['accessToken'] = $this->accessToken;

        $arrTokenInsert['deviceToken'] = ($this->input->post('deviceToken') != "") ? $this->input->post('deviceToken') : "";


        $accesstoken_id = $this->CommonModel->insert(TBL_USER_ACCESS_TOKEN, $arrTokenInsert);


        /*Email Start*/


        /*Email Template start*/

        $html = file_get_contents(base_url() . "emailTemplate/welcome.php");

        $emailHeader = file_get_contents("emailTemplate/emailHeader.php");

        $emailFooter = file_get_contents("emailTemplate/emailFooter.php");


        $replaceOn = ["{{OTP}}", "{{base_url}}", "{{emailHeader}}", "{{emailFooter}}"];

        $replaceWith = [$otp, base_url(), $emailHeader, $emailFooter];

        $messageHTML = str_replace($replaceOn, $replaceWith, $html);

        /*Email Template end*/


        $subject = "Welcome to Say Scape Getting Started";

        $mail_end = $this->CommonModel->send_mail($arrUserInsert['email'], $subject, $messageHTML);

        /*End*/

        if (!$mail_end) {

            $this->response['message'] = "Email Not Sent";

            $this->response($this->response);

        }


        /*Bulksms  START*/


        /*END */


        /* Set Defult Notification On settings */


        $arrSettings['notificationStatus'] = 1;

        $arrSettings['privateMessageStatus'] = 1;

        $arrSettings['userID'] = $this->userID;

        $userExists = $this->CommonModel->get_row(TBL_SETTINGS, 'settingID', ['userID' => $this->userID, 'isActive' => 1, 'isDelete' => 0]);


        if (empty($userExists)) {
            $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);
        } else {
            $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userExists['settingID']]);
        }

        /*END*/


        $this->response['data']['userDetails']['accessToken'] = $this->accessToken;

        $this->response['data']['userDetails']['userID'] = $this->userID;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('regSuccess');

        $this->response($this->response);

    }


    public function loginWithSocialMedia()
    {

        $this->store_logs();


        if (!$this->input->post('email') || empty($this->input->post('email'))) {

            $this->response['message'] = getMessage('passEmail');

            $this->response($this->response);

        }

        if (!$this->input->post('name') || empty($this->input->post('name'))) {

            $this->response['message'] = getMessage('passName');

            $this->response($this->response);

        }

        if (!$this->input->post('deviceType') || empty($this->input->post('deviceType'))) {

            $this->response['message'] = getMessage('passDeviceType');

            $this->response($this->response);

        }

        // if (!$this->input->post('deviceToken') || empty($this->input->post('deviceToken'))) {
        // $this->response['message'] = getMessage('passDeviceToken');
        // $this->response($this->response);
        // }

        if (!$this->input->post('profileImage') || empty($this->input->post('profileImage'))) {

            $this->response['message'] = "Please Pass Profile Image";

            $this->response($this->response);

        }

        if (!$this->input->post('googleId') || empty($this->input->post('googleId'))) {

            $this->response['message'] = "Please Pass Google ID";

            $this->response($this->response);

        }

        $checkLogin = $this->CommonModel->get_row('tbluser', ['userID', 'username', 'isVerified', 'isVeteran', 'isOffical'], ['email' => $_POST['email']]);


        $deviceToken = $this->input->post('deviceToken') ? $this->input->post('deviceToken') : "";
        if (!empty($checkLogin) && $checkLogin['isVerified'] == 1) {

            $accsessTokenData = $this->CommonModel->get_all('tbluseraccesstoken', ['*'], array("userID" => $checkLogin['userID'], "deviceToken" => $deviceToken));

            if (!empty($accsessTokenData)) {

                $this->CommonModel->delete('tbluseraccesstoken', array('userAccessTokenID' => $accsessTokenData[0]['userAccessTokenID'], 'deviceToken' => $deviceToken));

            }

            $this->accessToken = $this->generateToken();


            $arrTokenInsert['userID'] = $checkLogin['userID'];

            $arrTokenInsert['accessToken'] = $this->accessToken;

            $arrTokenInsert['deviceToken'] = $deviceToken;


            $accesstoken_id = $this->CommonModel->insert('tbluseraccesstoken', $arrTokenInsert);


            $arrUserUpdate['googleId'] = $_POST['googleId'];

            $arrUserUpdate['profileImage'] = $_POST['profileImage'];

            $arrUserUpdate['loginType'] = 1;

            $arrUserUpdate['deviceToken'] = ($this->input->post('deviceToken') != "") ? $this->input->post('deviceToken') : "";

            $arrUserUpdate['deviceType'] = $this->input->post('deviceType');


            $this->CommonModel->update('tbluser', $arrUserUpdate, ['userID' => $checkLogin['userID']]);

            $userDetails = $this->CommonModel->get_row('tbluser', ['userID', 'username', 'name', 'email', 'phone', 'location', 'dob', 'loginType', 'profileImage', 'coverImage', 'website', 'about', 'isVeteran', 'isOffical'], ['userID' => $checkLogin['userID']]);

            $userDetails['profileImage'] = base_url() . "uploads/userprofile/" . $userDetails['profileImage'];

            $userDetails['coverImage'] = base_url() . "uploads/usercover/" . $userDetails['coverImage'];


            $this->response['code'] = 100;

            $this->response['message'] = getMessage('loginSuccess');

            $this->response['data']['userDetails'] = $userDetails;

            $this->response['data']['userDetails']['accessToken'] = $this->accessToken;

        } elseif (empty($checkLogin)) {


            $arrUserInsert['deviceType'] = $_POST['deviceType'];

            $arrUserInsert['deviceToken'] = $deviceToken;

            $arrUserInsert['email'] = $_POST['email'];

            $arrUserInsert['name'] = $_POST['name'];

            $arrUserInsert['username'] = $_POST['name'];

            $arrUserInsert['googleId'] = $_POST['googleId'];

            $arrUserInsert['profileImage'] = $_POST['profileImage'];

            $arrUserInsert['isVerified'] = 1;

            $arrUserInsert['loginType'] = 1;

            $user_id = $this->CommonModel->insert('tbluser', $arrUserInsert);

            $this->userID = $user_id;


            $this->accessToken = $this->generateToken();

            $arrTokenInsert['userID'] = $user_id;

            $arrTokenInsert['accessToken'] = $this->accessToken;

            $arrTokenInsert['deviceToken'] = $deviceToken;

            $arrTokenInsert['createdDateTime'] = date('Y-m-d H:i:s');

            $arrTokenInsert['updatedDateTime'] = date('Y-m-d H:i:s');


            $accesstoken_id = $this->CommonModel->insert('tbluseraccesstoken', $arrTokenInsert);

            $userDetails = $this->CommonModel->get_row('tbluser', ['userID', 'username', 'name', 'email', 'phone', 'location', 'dob', 'loginType', 'profileImage', 'coverImage', 'website', 'about', 'isVeteran', 'isOffical'], ['userID' => $user_id]);

            $userDetails['profileImage'] = base_url() . "uploads/userprofile/" . $userDetails['profileImage'];

            $userDetails['coverImage'] = base_url() . "uploads/usercover/" . $userDetails['coverImage'];


            /* Set Defult Notification On settings */


            $arrSettings['notificationStatus'] = 1;

            $arrSettings['privateMessageStatus'] = 1;

            $arrSettings['userID'] = $this->userID;

            $userExists = $this->CommonModel->get_row(TBL_SETTINGS, 'settingID', ['userID' => $this->userID, 'isActive' => 1, 'isDelete' => 0]);


            if (empty($userExists)) {

                $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);

            } else {

                $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userExists['settingID']]);

            }

            /*END*/


            $this->response['data']['userDetails'] = $userDetails;

            $this->response['code'] = 100;

            $this->response['message'] = getMessage('regSuccess');

            $this->response['data']['userDetails']['accessToken'] = $this->accessToken;

        } elseif ($checkLogin['isVerified'] != 1) {

            $this->response['message'] = getMessage('verifyAccount');

        }

        $this->response($this->response);

    }


    public function user_login()
    {

        $this->store_logs();


        if (!isset($_POST['deviceType']) || empty($_POST['deviceType'])) {

            $this->response['message'] = getMessage('passDeviceType');

            $this->response($this->response);

        }

        if (!isset($_POST['username']) || empty($_POST['username'])) {

            $this->response['message'] = getMessage('passUsername');

            $this->response($this->response);

        }

        if (!isset($_POST['password']) || empty($_POST['password'])) {

            $this->response['message'] = getMessage('passPassword');

            $this->response($this->response);

        }

        if (!isset($_POST['deviceToken']) || empty($_POST['deviceToken'])) {

            $this->response['message'] = getMessage('passDeviceToken');

            $this->response($this->response);

        }

        $username = $_POST['username'];

        $password = md5($_POST['password']);


        $result = $this->login->getUser($password, $username);

        if (!empty($result)) {

            $this->userID = $result[0]['userID'];

            $deviceToken = ($this->input->post('deviceToken') != "") ? $this->input->post('deviceToken') : "";
            $accsessTokenData = $this->CommonModel->get_all('tbluseraccesstoken', ['*'], array("userID" => $this->userID, "deviceToken" => $deviceToken));

            if (!empty($accsessTokenData)) {

                $this->CommonModel->delete('tbluseraccesstoken', array('userAccessTokenID' => $accsessTokenData[0]['userAccessTokenID'], 'deviceToken' => $deviceToken));

            }

            $this->accessToken = $this->generateToken();


            $arrTokenInsert['userID'] = $this->userID;

            $arrTokenInsert['accessToken'] = $this->accessToken;

            $arrTokenInsert['deviceToken'] = $deviceToken;


            $accesstoken_id = $this->CommonModel->insert('tbluseraccesstoken', $arrTokenInsert);


            $arrUserUpdate['deviceToken'] = $deviceToken;

            $arrUserUpdate['deviceType'] = $this->input->post('deviceType');


            $this->CommonModel->update('tbluser', $arrUserUpdate, ['userID' => $this->userID]);


            $userDetails = $this->CommonModel->get_row('tbluser', ['userID', 'username', 'name', 'email', 'phone', 'location', 'dob', 'loginType', 'profileImage', 'coverImage', 'website', 'about', 'isVeteran', 'allowVeteran', 'isOffical', 'isVerified'], ['userID' => $this->userID]);

            $userDetails['profileImage'] = base_url() . "uploads/userprofile/" . $userDetails['profileImage'];

            $userDetails['coverImage'] = base_url() . "uploads/usercover/" . $userDetails['coverImage'];


            $this->response['code'] = 100;

            $this->response['message'] = getMessage('loginSuccess');

            $this->response['data']['userDetails'] = $userDetails;

            $this->response['data']['userDetails']['accessToken'] = $this->accessToken;

        } elseif (empty($result)) {

            $this->response['message'] = getMessage('invalidLogin');

        }

        $this->response($this->response);


    }

    //for verify user

    public function verify_user()
    {

        if (!$this->input->post('userID') || empty($this->input->post('userID'))) {

            $this->response['message'] = getMessage('passUserId');

            $this->response($this->response);

        }

        if (!$this->input->post('accessToken') || empty($this->input->post('accessToken'))) {

            $this->response['message'] = getMessage('passToken');

            $this->response($this->response);

        }

        if (!$this->input->post('otp') || empty($this->input->post('otp'))) {

            $this->response['message'] = getMessage('passVerifyCode');

            $this->response($this->response);

        }


        $this->userID = $this->input->post('userID');

        $this->accessToken = $this->input->post('accessToken');


        $userData = $this->user->getUser(['tu.userID', 'tuat.accessToken', 'tu.otp'], ['tu.userID' => $this->userID, 'tuat.accessToken' => $this->accessToken]);


        if (empty($userData)) {

            $this->response['code'] = 102;

            $this->response['message'] = getMessage('tokenExpire');

            $this->response($this->response);

        }

        if ($userData['otp'] == $this->input->post('otp')) {

            $this->CommonModel->update(TBL_USER, ['isVerified' => 1], ['userID' => $this->userID]);


            $this->response['code'] = 100;

            $this->response['message'] = getMessage('userVerifySuccess');

            $this->response['data'] = $this->user->getUserResponse($this->userID);

        } else {

            $this->response['message'] = getMessage('codeNotMatch');

        }

        $this->response($this->response);

    }


    public function logout()
    {

        if (!$this->input->post('deviceToken') || empty($this->input->post('deviceToken'))) {

            $this->response['message'] = getMessage('passDeviceToken');

            $this->response($this->response);

        }


        $this->db->delete(TBL_USER_ACCESS_TOKEN, array('userID' => $this->userID, 'deviceToken' => $this->input->post('deviceToken'), 'accessToken' => $this->accessToken));

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('logoutSuccess');

        $this->response($this->response);


    }


    public function change_status_with_user()
    {

        if (!$this->input->post('followerID') || empty($this->input->post('followerID'))) {

            $this->response['message'] = getMessage('passfollowerID');

            $this->response($this->response);

        }


        if (!$this->input->post('status') || empty($this->input->post('status'))) {

            $this->response['message'] = getMessage('passstatus');

            $this->response($this->response);

        }


        $followerID = $this->input->post('followerID');


        $alreadyExistFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $this->userID, 'followerID' => $followerID]);


        if (empty($alreadyExistFollowUser)) {

            $arrInsert = array();

            $arrInsert['userID'] = $this->userID;

            $arrInsert['followerID'] = $followerID;

            $arrInsert['status'] = ($this->input->post('status')) ? $this->input->post('status') : 1;

            $arrInsert['createdBy'] = $this->userID;

            $userFollowerID = $this->CommonModel->insert(TBL_USER_FOLLOWER, $arrInsert);

        } else if (!empty($alreadyExistFollowUser)) {

            $arrUpdate = array();

            $arrUpdate['status'] = $this->input->post('status');

            $arrUpdate['createdBy'] = $this->userID;

            $userFollowerID = $this->CommonModel->update(TBL_USER_FOLLOWER, $arrUpdate, ['userFollowerID' => $alreadyExistFollowUser['userFollowerID']]);

        }


        $this->response['data']['userFollowingList'] = $this->user->getUserFollowingList(['tuf.followerID', 'tu.username', 'tu.name', 'tu.email', 'tu.phone'], ['tuf.userID' => $this->userID, 'status' => 1]);

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }


    public function followUnfollowUser()
    {

        $this->store_logs();

        if (!$this->input->post('followerID') || empty($this->input->post('followerID'))) {

            $this->response['message'] = getMessage('passfollowerID');

            $this->response($this->response);

        }


        if (($this->input->post('status') == "") || ($this->input->post('status') != "" && !in_array($this->input->post('status'), [0, 1, 4]))) {

            $this->response['message'] = getMessage('passstatus');

            $this->response($this->response);

        }


        $followerID = $this->input->post('followerID');

        $status = $this->input->post('status');


        if ($followerID == $this->userID) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("UserOwnFollowError");

            $this->response($this->response);

        }


        if (!$this->CommonModel->checkExists(TBL_USER, ['userID' => $followerID, "isActive" => 1, "isDelete" => 0])) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("FolloweIDError");

            $this->response($this->response);

        }


        if ($this->CommonModel->checkExists(TBL_BLOCKUSER, ['userID' => $this->userID, 'blockUserID' => $followerID, "isActive" => 1, "isDelete" => 0])) {

            $this->response['message'] = getMessage("userisBlock");

            $this->response($this->response);

        }


        $alreadyExistFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $this->userID, 'followerID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);

        /* 1 = Follow or 0 = UnFollow*/

        if ($status == 0) {

            if (empty($alreadyExistFollowUser) || (!empty($alreadyExistFollowUser['status']) && $alreadyExistFollowUser['status'] == 0)) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("NotFollowError");

            } else {

                $userFollowerID = $alreadyExistFollowUser['userFollowerID'];

                $this->CommonModel->delete(TBL_USER_FOLLOWER, ['userFollowerID' => $alreadyExistFollowUser['userFollowerID']]);

                // $arrUpdate=array();

                // $arrUpdate['status']=$this->input->post('status');

                // $arrUpdate['isActive'] = 0;

                // $userFollowerID=$this->CommonModel->update(TBL_USER_FOLLOWER,$arrUpdate,['userFollowerID'=>$alreadyExistFollowUser['userFollowerID']]);

                if (empty($userFollowerID)) {

                    $this->response['code'] = 101;

                    $this->response['message'] = getMessage("UnFollowError");

                } else {

                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("UnFollowSuccess");

                    // $this->sendNotification(array(

                    //     "type"=>"profile",

                    //     "title"=>"SayScape",

                    //     "desc"=>ucfirst($this->userName)." has unfollowed you.",

                    //     "sendNotificationUserID"=>$followerID,

                    //     "targetID"=>$this->userID

                    // ));

                }

            }

        } elseif ($status == 4) {

            $alreadyFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID', 'status'], ['userID' => $this->userID, 'followerID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);

            // echo "<pre>"; print_r($alreadyFollowUser); die();

            if (!empty($alreadyFollowUser) && isset($alreadyFollowUser['status']) && ($alreadyFollowUser['status'] == 1 || $alreadyFollowUser['status'] == 4)) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("FollowAlready");

            } else {

                $arrInsert = array();

                $arrInsert['userID'] = $this->userID;

                $arrInsert['followerID'] = $followerID;

                $arrInsert['status'] = $this->input->post('status');

                $arrInsert['createdBy'] = $this->userID;

                if ($this->CommonModel->insert(TBL_USER_FOLLOWER, $arrInsert) > 0) {

                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("followerSuccess");

                    $this->sendNotification(array(

                        "type" => "profile",

                        "title" => "SayScape",

                        "desc" => ucfirst($this->userName) . " has send you follow request",

                        "sendNotificationUserID" => $followerID,

                        "targetID" => $this->userID,
                        "senderID"=>$this->userID,
                        "notifyType"=>"followRequest"

                    ));

                } else {

                    $this->response['code'] = 101;

                    $this->response['message'] = getMessage("followerError");

                }

            }

        } else {

            if (empty($alreadyExistFollowUser)) {

                $arrInsert = array();

                $arrInsert['userID'] = $this->userID;

                $arrInsert['followerID'] = $followerID;

                $arrInsert['status'] = $this->input->post('status');

                $arrInsert['createdBy'] = $this->userID;

                if ($this->CommonModel->insert(TBL_USER_FOLLOWER, $arrInsert) > 0) {

                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("followerSuccess");

                    $this->sendNotification(array(

                        "type" => "profile",

                        "title" => "SayScape",

                        "desc" => ucfirst($this->userName) . " has started following you",

                        "sendNotificationUserID" => $followerID,

                        "targetID" => $this->userID,
                        "senderID"=>$this->userID,
                        "notifyType"=>"follow"

                    ));

                } else {

                    $this->response['code'] = 101;

                    $this->response['message'] = getMessage("followerError");

                }

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage('FollowAlready');

            }

        }

        $this->response($this->response);

    }


    public function getFollowRequestList()
    {
        $result = array();


        $getAllRequest = $this->user->getFollowRequestList(['tuf.userID', 'tuf.followerID', 'tu.username', 'tu.name', 'tu.email', 'tu.phone', 'tu.profileImage'], ['tuf.followerID' => $this->userID, 'status' => 4]);

        if (!empty($getAllRequest)) {

            // echo "<pre>"; print_r($getAllRequest); die();

            foreach ($getAllRequest as &$request) {


                $request['profileImage'] = base_url() . "upload/userprofile/" . $request['profileImage'];

                $result[] = $request;

            }

        }


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");

        $this->response['data']['followRequestList'] = $result;

        $this->response($this->response);

    }


    public function acceptDeclineFollowRequest()
    {

        $this->store_logs();

        if (!$this->input->post('followerID') || empty($this->input->post('followerID'))) {

            $this->response['message'] = getMessage('passfollowerID');

            $this->response($this->response);

        }

        if (($this->input->post('status') == "") || ($this->input->post('status') != "" && !in_array($this->input->post('status'), [0, 1]))) {

            $this->response['message'] = getMessage('passstatus');

            $this->response($this->response);

        }

        $followerID = $this->input->post('followerID');

        $status = $this->input->post('status');

        if ($followerID == $this->userID) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("UserOwnFollowError");

            $this->response($this->response);

        }

        if (!$this->CommonModel->checkExists(TBL_USER, ['userID' => $followerID, "isActive" => 1, "isDelete" => 0])) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("FolloweIDError");

            $this->response($this->response);

        }

        if ($this->CommonModel->checkExists(TBL_BLOCKUSER, ['userID' => $this->userID, 'blockUserID' => $followerID, "isActive" => 1, "isDelete" => 0])) {

            $this->response['message'] = getMessage("userisBlock");

            $this->response($this->response);

        }


        $alreadyFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['followerID' => $this->userID, 'userID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);

        if (empty($alreadyFollowUser)) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("NotFollowError");

        } else {

            if ($status == 1) {

                $arrUpdate = array();

                $arrUpdate['status'] = $this->input->post('status');

                // $arrUpdate['isActive'] = 1;

                $this->CommonModel->update(TBL_USER_FOLLOWER, $arrUpdate, ['userFollowerID' => $alreadyFollowUser['userFollowerID']]);


                $this->response['code'] = 100;

                $this->response['message'] = getMessage("reqAcceptSuccess");

                $this->sendNotification(array(

                    "type" => "profile",

                    "title" => "SayScape",

                    "desc" => ucfirst($this->userName) . " accept your request.",

                    "sendNotificationUserID" => $followerID,

                    "targetID" => $this->userID,
                    "senderID"=>$this->userID,
                    "notifyType"=>"acceptRequest"

                ));

            } else {

                $this->CommonModel->delete(TBL_USER_FOLLOWER, ['userFollowerID' => $alreadyFollowUser['userFollowerID']]);


                $this->response['code'] = 100;

                $this->response['message'] = getMessage("reqDeleteSuccess");

            }

        }

        $this->response($this->response);

    }


    // THIS API REMOVED POSTMAN => Get User Following List

    public function get_all_following_by_user()
    {

        $userdetails = $this->user->getUserFollowingList(['tuf.followerID', 'tu.username', 'tu.name', 'tu.email', 'tu.phone', 'tu.profileImage'], ['tuf.userID' => $this->userID, 'status' => 1]);

        foreach ($userdetails as &$value) {

            $value['profileImage'] = base_url() . "upload/userprofile/" . $value['profileImage'];

        }


        $this->response['data']['userFollowingList'] = $userdetails;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }


    public function createPost()
    {

        $this->store_logs();

        // echo "<pre>"; print_r($_POST['options']); die();

        if (isset($_POST['expiryDateTime']) || !empty($_POST['expiryDateTime']) || isset($_POST['isexpireOn']) || !empty($_POST['isexpireOn'])) {

            $arrPostInsert['expiryDateTime'] = $_POST['expiryDateTime'];

            $arrPostInsert['isexpireOn'] = $_POST['isexpireOn'];

            $arrPostInsert['isPermanent'] = 0;

        }

        // echo $_POST['text']; die();

        $arrPostInsert['userID'] = $this->userID;

        $arrPostInsert['text'] = (isset($_POST['text'])) ? $_POST['text'] : "";

        $arrPostInsert['gif'] = (isset($_POST['gif']) && $_POST['gif'] != "") ? trim($_POST['gif']) : null;

        $arrPostInsert['postType'] = $this->input->post('postType') ? $this->input->post('postType') : 0;

        $arrPostInsert['isPollPost'] = (isset($_POST['type']) && $_POST['type'] == 3) ? "1" : "0";
        
        $arrPostInsert['createdDateTime'] = date('Y-m-d H:i:s');
        $arrPostInsert['updatedDateTime'] = date('Y-m-d H:i:s');
        $last_post_id = $this->CommonModel->insert(TBL_POST, $arrPostInsert);
        
        if (isset($_POST['type']) && !empty($_POST['type']) && $_POST['type'] == 1) {

            //Modify By Hetal

            $image_data_count = $_REQUEST['image_data_count'];

            $arrMultipleImageInsert = array();

            if (!empty($image_data_count)) {

                for ($i = 0; $i < $image_data_count; $i++) {

                    $key = "image_data" . $i;

                    if (isset($_FILES[$key]["name"]) && $_FILES[$key]["name"] != "") {

                        if ($_FILES[$key]["error"] != 0) {

                            $this->response['code'] = 100;

                            $this->response['message'] = 'Error occurred while uploading file!';

                            $this->response['createdDateTime'] = date('Y-m-d H:i:s');

                            $this->response($this->response);

                        }


                        $config['upload_path'] = FCPATH . POST_IMAGE_PATH; // absolute dir path

                        $config['allowed_types'] = 'jpg|png|jpeg|gif';

                        $config['encrypt_name'] = true;

                        $config['remove_spaces'] = true;

                        $config['overwrite'] = false;

                        $config['file_name'] = microtime() . "." . pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);

                        $this->upload->initialize($config);


                        if (!$this->upload->do_upload($key)) {

                            $this->response['code'] = 100;

                            $this->response['message'] = $this->upload->display_errors();

                            $this->response['createdDateTime'] = date('Y-m-d H:i:s');

                            $this->response($this->response);

                        }

                        $arrInsert = array();

                        $arrInsert['userID'] = $this->userID;

                        $arrInsert['comID'] = $last_post_id;

                        $arrInsert['type'] = $_POST['type'];

                        $arrInsert['filename'] = $this->upload->data()["file_name"];


                        array_push($arrMultipleImageInsert, $arrInsert);

                    }

                }

                if (!empty($arrMultipleImageInsert)) {

                    $last_media_id = $this->post->insertdata($arrMultipleImageInsert);

                }

            }

        } elseif (isset($_POST['type']) && !empty($_POST['type']) && $_POST['type'] == 2) {
            file_put_contents('video.txt', json_encode($_FILES['videoname']));
            if (!empty($_FILES['videoname']['name'])) {
                $config['upload_path'] = './upload/postmedia/';
                $config['allowed_types'] = 'mp4|3gpp|3gp|mp3';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $arrMediaInsert['userID'] = $this->userID;
                $arrMediaInsert['comID'] = $last_post_id;
                $arrMediaInsert['type'] = $_POST['type'];
                if (!$this->upload->do_upload('videoname')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $fileData = array('upload_data' => $this->upload->data());
                    $arrMediaInsert['filename'] = $fileData["upload_data"]["file_name"];
                }
                if (!empty($_FILES['video_thumb']['name'])) {
                    $thumbconfig['upload_path'] = './upload/postmedia/';
                    $thumbconfig['allowed_types'] = 'jpg|png|jpeg';
                    $thumbconfig['encrypt_name'] = TRUE;
                    $thumbconfig['remove_spaces'] = TRUE;
                    $thumbconfig['overwrite'] = FALSE;
                    $this->load->library('upload', $thumbconfig);
                    $this->upload->initialize($thumbconfig);
                    if ($this->upload->do_upload('video_thumb')) {
                        $fileData = array('upload_data' => $this->upload->data());
                        $arrMediaInsert['video_thumb'] = $fileData["upload_data"]["file_name"];
                    }else{
                        $error = array('error' => $this->upload->display_errors());
                    }
                } 
                $last_media_id = $this->CommonModel->insert(TBL_MEDIA, $arrMediaInsert);
            }
        } elseif (isset($_POST['type']) && !empty($_POST['type']) && $_POST['type'] == 3) {

            if (!empty($_POST['options'])) {

                foreach ($_POST['options'] as $option) {

                    if (!empty($option)) {

                        $saveData['userID'] = $this->userID;

                        $saveData['postID'] = $last_post_id;

                        $saveData['text'] = $option;

                        $saveData['isActive'] = 1;

                        $this->CommonModel->insert(TBL_POST_POLL_OPTIONS, $saveData);

                    }

                }

            }

        }

        //SEnd notification mentions/tagging followers

        if ($this->input->post('text') != "") {

            preg_match_all('/(^|\s)(@\w+)/', $this->input->post('text'), $result);

            $mentionsUsers = end($result);

            if (!empty($mentionsUsers)) {

                foreach ($mentionsUsers as $value) {

                    $mentionUser = $this->CommonModel->get_row(TBL_USER, ["userID"], ["username" => ltrim($value, '@')]);

                    $this->sendNotification(array(

                        "type" => "post",

                        "title" => "SayScape",

                        "desc" => ucfirst($this->userName) . " mentioned you in a post.",

                        "targetID" => $last_post_id,

                        "mentionUserID" => $mentionUser['userID'],
                        "senderID"=>$this->userID,
                        "notifyType"=>"postMention"

                    ));

                }

            }

        }

        $getSubscribers = $this->CommonModel->get_all(TBL_POST_SUBSCRIBE, ['*'], ['followerID' => $this->userID, 'status' => 1]);

        if (!empty($getSubscribers)) {

            $blockUsers = array();

            $allblockList = $this->user->getUserBlockedList($this->userID);

            if (!empty($allblockList)) {

                foreach ($allblockList as $user) {

                    if (isset($user['userID']) && $user['userID'] != "") {

                        $blockUsers[] = $user['userID'];

                    }

                }

            }

            foreach ($getSubscribers as $subscribe) {


                $checkBlockUserData = $this->CommonModel->get_row('tblblockuser', ["userID", "blockUserID"], ["userID" => $subscribe['userID'], "blockUserID" => $this->userID]);

                if (empty($checkBlockUserData) && !in_array($subscribe['userID'], $blockUsers)) {

                    $this->sendNotification(array(

                        "type" => "post",

                        "title" => "SayScape",

                        "desc" => ucfirst($this->userName) . " add new post.",

                        "targetID" => $last_post_id,

                        "subscriberID" => $subscribe['userID'],

                        "postID" => $last_post_id,
                        "senderID"=>$this->userID,
                        "notifyType"=>"newPost"

                    ));

                }

            }

        }

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('PostSuccess');

        $this->response['createdDateTime'] = date('Y-m-d H:i:s');

        $this->response($this->response);

    }


    public function editPost()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }


        if (isset($_POST['expiryDateTime']) || !empty($_POST['expiryDateTime']) || isset($_POST['isexpireOn']) || !empty($_POST['isexpireOn'])) {

            $arrPostEdit['expiryDateTime'] = $_POST['expiryDateTime'];

            $arrPostEdit['isexpireOn'] = $_POST['isexpireOn'];

            $arrPostEdit['isPermanent'] = 0;

        }


        $arrPostEdit['text'] = $_POST['text'];

        $arrPostEdit['updatedDateTime'] = date('Y-m-d H:i:s');


        $update_post = $this->CommonModel->update(TBL_POST, $arrPostEdit, array('postID' => $_POST['postID']));


        /**Get Post Media to Hard Delete*/


        $mediaList = $this->CommonModel->get_all(TBL_MEDIA, ['filename', 'mediaID'], ['userID' => $this->userID, 'comID' => $this->input->post('postID'), 'mediaRef' => 0]);

        if (!empty($mediaList)) {

            $arrFilename = array();

            $arrFilename = array_column($mediaList, 'filename');

            foreach ($arrFilename as $valuefile) {

                $filename = POST_IMAGE_PATH . $valuefile;

                if (file_exists($filename)) {

                    unlink($filename);

                }

            }

            $this->CommonModel->delete(TBL_MEDIA, ['userID' => $this->userID, 'comID' => $this->input->post('postID'), 'mediaRef' => 0]);

        }


        if (isset($_POST['type']) && !empty($_POST['type']) && $_POST['type'] == 1) {

            //Modify By Hetal

            $image_data_count = $_REQUEST['image_data_count'];

            $arrMediaEdit = array();

            for ($i = 0; $i < $image_data_count; $i++) {

                $key = "image_data" . $i;

                if (isset($_FILES[$key]["name"]) && $_FILES[$key]["name"] != "") {

                    if ($_FILES[$key]["error"] != 0) {

                        $this->response['code'] = 100;

                        $this->response['message'] = "Error occurred while uploading file!";

                        $this->response['createdDateTime'] = date('Y-m-d H:i:s');

                        $this->response($this->response);

                    }


                    $config['upload_path'] = FCPATH . POST_IMAGE_PATH; // absolute dir path

                    $config['allowed_types'] = 'jpg|png|jpeg|gif';

                    $config['encrypt_name'] = true;

                    $config['remove_spaces'] = true;

                    $config['overwrite'] = false;

                    $config['file_name'] = microtime() . "." . pathinfo($_FILES[$key]['name'], PATHINFO_EXTENSION);

                    $this->upload->initialize($config);


                    if (!$this->upload->do_upload($key)) {

                        $this->response['code'] = 100;

                        $this->response['message'] = $this->upload->display_errors();

                        $this->response['createdDateTime'] = date('Y-m-d H:i:s');

                        $this->response($this->response);

                    }

                    $arrInsert = array();

                    $arrInsert['userID'] = $this->userID;

                    $arrInsert['comID'] = $_POST['postID'];

                    $arrInsert['type'] = $_POST['type'];

                    $arrInsert['filename'] = $this->upload->data()["file_name"];


                    array_push($arrMediaEdit, $arrInsert);


                }

            }

            if (!empty($arrMediaEdit))

                $last_media_id = $this->post->insertdata($arrMediaEdit);


        } elseif (isset($_POST['type']) && !empty($_POST['type']) && $_POST['type'] == 2) {

            if (!empty($_FILES['videoname']['name'])) {

                $config['upload_path'] = './upload/postmedia/';

                $config['allowed_types'] = 'mp4|3gp|mp3';

                $config['encrypt_name'] = TRUE;

                $config['remove_spaces'] = TRUE;

                $config['overwrite'] = FALSE;

                $config['max_size'] = '5000000';

                $this->load->library('upload', $config);

                $this->upload->initialize($config);


                $arrMediaEdit['userID'] = $this->userID;

                $arrMediaEdit['comID'] = $_POST['postID'];

                $arrMediaEdit['type'] = $_POST['type'];

                $arrMediaEdit['createdDateTime'] = date('Y-m-d H:i:s');

                $arrMediaEdit['updatedDateTime'] = date('Y-m-d H:i:s');


                if (!$this->upload->do_upload('videoname')) {


                    $error = array('error' => $this->upload->display_errors());


                } else {

                    $fileData = array('upload_data' => $this->upload->data());

                    $arrMediaEdit['filename'] = $fileData["upload_data"]["file_name"];


                }

                if (!empty($arrMediaEdit))

                    $last_media_id = $this->CommonModel->insert(TBL_MEDIA, $arrMediaEdit);

            }

        }

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('PostEditSuccess');

        $this->response($this->response);

    }


    public function deletePost()
    {
        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {
            $this->response['message'] = getMessage('passPostId');
            $this->response($this->response);
        }
        $arrPostUpdate['isDelete'] = 1;
        $arrPostUpdate['isActive'] = 0;
        $arrPostUpdate['updatedDateTime'] = date('Y-m-d H:i:s');
        $delete_post = $this->CommonModel->update(TBL_POST, $arrPostUpdate, array('postID' => $_POST['postID']));
        $delete_media = $this->CommonModel->update(TBL_MEDIA, $arrPostUpdate, array('comID' => $_POST['postID'], "mediaRef" => 0));
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('PostDeleteSuccess');
        // $this->response['data'] = $this->getAllPosts();
        $this->response($this->response);
    }


    public function deletePostMedia()
    {

        if (!$this->input->post('mediaID') || empty($this->input->post('mediaID'))) {

            $this->response['message'] = getMessage('passMediaId');

            $this->response($this->response);

        }

        $arrPostmediaDelete['isDelete'] = 1;

        $arrPostmediaDelete['updatedDateTime'] = date('Y-m-d H:i:s');


        $delete_media = $this->CommonModel->update(TBL_MEDIA, $arrPostmediaDelete, array('mediaID' => $_POST['mediaID'], "mediaRef" => 0));


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('MediaDeleteSuccess');

        $this->response($this->response);

    }


    public function likePost()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $arrLikeInsert['userID'] = $this->userID;

        $arrLikeInsert['postID'] = $this->input->post('postID');

        $arrLikeInsert['postShareID'] = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;

        $arrLikeInsert['isLiked'] = 1;


        $likeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $arrLikeInsert['userID'], 'postID' => $arrLikeInsert['postID'], "isLiked" => 1, "isDelete" => 0]);

        if (!empty($likeResult)) {

            if ($likeResult['isActive'] == 1) {

                $isActive = 0;

            }

            if ($likeResult['isActive'] == 0) {

                $isActive = 1;

            }

            if ($this->CommonModel->update(TBL_POST_LIKE, ['isActive' => $isActive], ['postLikeID' => $likeResult['postLikeID']]) > 0) {

                $this->getAllPosts();

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("PostLikeRemoveError");

            }

        } else {

            if ($this->CommonModel->insert(TBL_POST_LIKE, $arrLikeInsert) > 0) {


                $checkOwnPost = $this->CommonModel->get_row(TBL_POST, ['*'], ['userID' => $this->userID, 'postID' => $_POST['postID'], 'isActive' => 1, 'isDelete' => 0]);

                if (empty($checkOwnPost)) {

                    if ($arrLikeInsert['postShareID'] > 0) {

                        $this->sendNotification(array(

                            "type" => "post",

                            "title" => "SayScape",

                            "desc" => ucfirst($this->userName) . " has liked your reshare post.",

                            "targetID" => $arrLikeInsert['postID'],

                            "postShareID" => $arrLikeInsert['postShareID'],
                            "senderID"=>$this->userID,
                            "notifyType"=>"resharePostLike"

                        ));

                    } else {

                        $this->sendNotification(array(

                            "type" => "post",

                            "title" => "SayScape",

                            "desc" => ucfirst($this->userName) . " has liked your post.",

                            "targetID" => $arrLikeInsert['postID'],
                            "senderID"=>$this->userID,
                            "notifyType"=>"postLike"

                        ));
                    }
                }
                $this->getAllPosts();
            } else {
                $this->response['code'] = 101;
                $this->response['message'] = getMessage("PostLikeError");
            }
        }
        $this->response($this->response);
    }

    public function sharePost()
    {
        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {
            $this->response['message'] = getMessage('passPostId');
            $this->response($this->response);
        }


        $arrShare['userID'] = $this->userID;

        $arrShare['postID'] = $_POST['postID'];

        $arrShare['status'] = $_POST['status'];

        $arrShare['post'] = (isset($_POST['post']) && $_POST['post'] != "") ? $_POST['post'] : "";

        $arrShare['emoji'] = (isset($_POST['emoji']) && $_POST['emoji'] != "") ? $_POST['emoji'] : "";


        $shareResult = $this->CommonModel->get_all(TBL_POST_SHARE, ['postShareID'], ['userID' => $arrShare['userID'], 'postID' => $arrShare['postID'], "isActive" => 1, "isDelete" => 0]);
        if (!empty($shareResult)) {
            $this->response['code'] = 101;
            $this->response['message'] = getMessage("PostAlreadyShareError");
        } else {
            if ($this->CommonModel->insert(TBL_POST_SHARE, $arrShare) > 0) {
                $checkOwnPost = $this->CommonModel->get_row(TBL_POST, ['*'], ['userID' => $this->userID, 'postID' => $_POST['postID'], 'isActive' => 1, 'isDelete' => 0]);
                if (empty($checkOwnPost)) {
                    $this->sendNotification(array(
                        "type" => "post",
                        "title" => "SayScape",
                        "desc" => ucfirst($this->userName) . " has quoted your post.",
                        "targetID" => $this->input->post('postID'),
                        "senderID"=>$this->userID,
                        "notifyType"=>"quotePost"
                    ));
                }
                $this->response['code'] = 100;
                $this->response['message'] = getMessage('PostShareSuccess');
                // $this->getAllPosts();
            } else {
                $this->response['code'] = 101;
                $this->response['message'] = getMessage("PostShareError");
            }
        }
        $this->response($this->response);
    }

    public function addPostComment()
    {
        $this->store_logs();
        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {
            $this->response['message'] = getMessage('passPostId');
            $this->response($this->response);
        }
        $parentCommentID = "0";
        if ($this->input->post('parentCommentID') != "") {
            $parentCommentID = $this->input->post('parentCommentID');
        }
        $arrCommentInsert['userID'] = $this->userID;
        $arrCommentInsert['postID'] = $_POST['postID'];
        $arrCommentInsert['text'] = $_POST['text'];
        $tag = explode(" ", substr($_POST['text'], strpos($_POST['text'], '@')));
        $tagUsr = array_filter($tag, function ($value) {
            return (strpos($value, '@') === 0);
        });
        $arrCommentInsert['gif'] = (isset($_POST['gif'])) ? $_POST['gif'] : "";
        if (!empty($_FILES['commentImage']['name'])) {
            $config['upload_path'] = './upload/postmedia/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $config['encrypt_name'] = TRUE;

            $config['remove_spaces'] = TRUE;

            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('commentImage')) {

                $error = array('error' => $this->upload->display_errors());

            } else {

                $fileData = array('upload_data' => $this->upload->data());

                $arrCommentInsert['commentImage'] = $fileData["upload_data"]["file_name"];

            }

        }

        $arrCommentInsert['postShareID'] = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;

        $arrCommentInsert['parentCommentID'] = $parentCommentID;

        $arrCommentInsert['createdDateTime'] = date('Y-m-d H:i:s');

        $arrCommentInsert['updatedDateTime'] = date('Y-m-d H:i:s');


        $last_comment_id = $this->CommonModel->insert(TBL_POST_COMMENT, $arrCommentInsert);

        $comments = $this->post->getComments($_POST['postID'], '', $this->userID);


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('PostCommentSuccess');

        $this->response['data']['comments'] = $comments;


        $checkOwnPost = $this->CommonModel->get_row(TBL_POST, ['*'], ['userID' => $this->userID, 'postID' => $_POST['postID'], 'isActive' => 1, 'isDelete' => 0]);

        if (empty($checkOwnPost)) {

            if ($arrCommentInsert['postShareID'] > 0) {

                $this->sendNotification(array(

                    "type" => "post",

                    "title" => "SayScape",

                    "desc" => ucfirst($this->userName) . " has commented on your quoted post.",

                    "targetID" => $this->input->post('postID'),

                    "postShareID" => $arrCommentInsert['postShareID'],
                    "senderID"=>$this->userID,
                    "notifyType"=>"quotePostComment"

                ));

            } else {

                $this->sendNotification(array(

                    "type" => "post",

                    "title" => "SayScape",

                    "desc" => ucfirst($this->userName) . " has commented on your post.",

                    "targetID" => $this->input->post('postID'),
                    'parentCommentID' => $this->input->post('parentCommentID'),
                    'userID' => $this->input->post('userId'),
                    "senderID"=>$this->userID,
                    "notifyType"=>"postComment"
                ));
            }
        }
        if (!empty($tagUsr)) {
            foreach ($tagUsr as $user) {
                $this->sendNotification(array(
                    "type" => "post",
                    "title" => "SayScape",
                    "desc" => ucfirst($this->userName) . " has tag you on commented.",
                    "targetID" => $this->input->post('postID'),
                    'tagUser' => str_replace('@', "", $user),
                    'userID' => $this->input->post('userId'),
                    "senderID"=>$this->userID,
                    "notifyType"=>"commentTag"
                ));
            }
        }
        $this->response($this->response);
    }

    //Created By: Hetal Patel

    public function deletePostComment()
    {

        if (!$this->input->post('postCommentID') || empty($this->input->post('postCommentID'))) {

            $this->response['message'] = getMessage('passPostCommentID');

            $this->response($this->response);

        }


        if (!$this->CommonModel->checkExists(TBL_POST_COMMENT, ['postCommentID' => $this->input->post('postCommentID'), "isActive" => 1, "isDelete" => 0])) {

            $this->response['message'] = getMessage('notFoundComment');

            $this->response($this->response);

        }


        $last_comment_id = $this->CommonModel->set_delete(TBL_POST_COMMENT, ['postCommentID' => $this->input->post('postCommentID')]);

        $comments = $this->post->getComments($this->input->post('postID'));


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('PostCommentDeleteSuccess');

        $this->response['data']['comments'] = $comments;

        $this->response($this->response);

    }


    public function addPostBookmark()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $arrBookmark['userID'] = $this->userID;

        $arrBookmark['postID'] = $_POST['postID'];

        $arrBookmark['createdDateTime'] = date('Y-m-d H:i:s');

        $arrBookmark['updatedDateTime'] = date('Y-m-d H:i:s');


        $last_bookmark_id = $this->CommonModel->insert(TBL_POST_BOOKMARK, $arrBookmark);

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('PostBookmarkSuccess');

        $this->response($this->response);

    }


    public function dislikePost()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $arrDisLikeInsert['userID'] = $this->userID;

        $arrDisLikeInsert['postID'] = $_POST['postID'];

        $arrDisLikeInsert['postShareID'] = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;

        $arrDisLikeInsert['isLiked'] = 0;


        $dislikeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $arrDisLikeInsert['userID'], 'postID' => $arrDisLikeInsert['postID'], "isLiked" => 0, "isDelete" => 0]);


        if (!empty($dislikeResult)) {

            if ($dislikeResult['isActive'] == 1) {

                $isActive = 0;

            }

            if ($dislikeResult['isActive'] == 0) {

                $isActive = 1;

            }

            if ($this->CommonModel->update(TBL_POST_LIKE, ['isActive' => $isActive], ['postLikeID' => $dislikeResult['postLikeID']]) > 0) {

                $this->getAllPosts();

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("PostDislikeRemoveError");

            }

        } else {

            if ($this->CommonModel->insert(TBL_POST_LIKE, $arrDisLikeInsert) > 0) {


                $checkOwnPost = $this->CommonModel->get_row(TBL_POST, ['*'], ['userID' => $this->userID, 'postID' => $_POST['postID'], 'isActive' => 1, 'isDelete' => 0]);

                if (empty($checkOwnPost)) {

                    if ($arrDisLikeInsert['postShareID'] > 0) {

                        $this->sendNotification(array(

                            "type" => "post",

                            "title" => "SayScape",

                            "desc" => ucfirst($this->userName) . " has disliked your reshare post.",

                            "targetID" => $arrDisLikeInsert['postID'],

                            "postShareID" => $arrDisLikeInsert['postShareID'],
                            "senderID"=>$this->userID,
                            "notifyType"=>"resharePostDislike"

                        ));

                    } else {

                        $this->sendNotification(array(

                            "type" => "post",

                            "title" => "SayScape",

                            "desc" => ucfirst($this->userName) . " has disliked your post.",

                            "targetID" => $arrDisLikeInsert['postID'],
                            "senderID"=>$this->userID,
                            "notifyType"=>"postDislike"

                        ));

                    }

                }

                $this->getAllPosts();

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("PostDisLikeError");

            }

        }


        $this->response($this->response);

    }


    public function getAllPosts()
    {

        if ($this->input->post('search_keyword') != '' && $this->input->post('flag') == 2) {
            $usersDetails = $this->CommonModel->get_row(TBL_USER, ["isVeteran", "allowVeteran"], ["userID" => $this->userID]);
            // echo $usersDetails['isVeteran'];
            //3 for all post search normal also veteran
            $search_keyword = !empty($this->input->post('search_keyword')) ? $this->input->post('search_keyword') : NULL;

            $searchFrom = ($usersDetails['isVeteran'] == 0) ? $usersDetails['isVeteran'] : 3;

            $postsdata = $this->post->getSearchPosts($this->userID, $searchFrom, $search_keyword);

        } else if ($this->input->post('search_keyword') == '' && $this->input->post('flag') == 2) {

            $postsdata = [];

        } else {
            //Update Device Token If not updated in login screen

            if ($this->input->post('deviceToken')) {

                $accsessTokenData = $this->CommonModel->get_all('tbluseraccesstoken', ['*'], array("userID" => $this->userID, "deviceToken" => $this->input->post('deviceToken')));

                if (empty($accsessTokenData)) {

                    $this->accessToken = $this->generateToken();

                    $arrTokenInsert['userID'] = $this->userID;

                    $arrTokenInsert['accessToken'] = $this->accessToken;

                    $arrTokenInsert['deviceToken'] = ($this->input->post('deviceToken') != "") ? $this->input->post('deviceToken') : "";

                    $accesstoken_id = $this->CommonModel->insert('tbluseraccesstoken', $arrTokenInsert);

                }

                $arrUserUpdate['deviceToken'] = ($this->input->post('deviceToken') != "") ? $this->input->post('deviceToken') : "";

                $arrUserUpdate['deviceType'] = $this->input->post('deviceType');


                $this->CommonModel->update('tbluser', $arrUserUpdate, ['userID' => $this->userID]);

            }

            // $postsdata = $this->post->getPosts($this->userID,0);

            $this->response['data']['totalPages'] = $this->post->getPosts($this->userID, 0, TRUE);

            $pageNumber = $this->input->post('pageNumber') ? $this->input->post('pageNumber') : 'ALL';

            $postsdata = $this->post->getPosts($this->userID, 0, FALSE, $pageNumber);

        }

        // echo $this->db->last_query();die;

        if (!empty($postsdata)) {

            foreach ($postsdata as &$value) {
                $value = $this->post->getPostResponseData($value, $this->userID);
            }

        }


        $this->response['data']['allPosts'] = $postsdata;
        $this->response['data']['userDetails'] = $this->user->getUserdetails($this->userID);
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('getPostRecordsSuccess');
        $this->response($this->response);

    }


    public function getCommentsByPostID()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $postID = $this->input->post('postID');

        $comments = $this->post->getComments($postID, '', $this->userID);


        if (empty($comments)) {

            $this->response['message'] = getMessage('notFoundComments');

            $this->response($this->response);

        }


        $this->response['code'] = 100;

        $this->response['data']['comments'] = $comments;

        $this->response['message'] = getMessage('FoundComments');

        $this->response($this->response);

    }


    public function editProfile()
    {

        if (!$this->input->get_post('userID') || empty($this->input->get_post('userID'))) {

            $this->response['message'] = getMessage('passUserId');

            $this->response($this->response);

        }

        if (!$this->input->get_post('accessToken') || empty($this->input->get_post('accessToken'))) {

            $this->response['message'] = getMessage('passToken');

            $this->response($this->response);

        }


        $this->userID = $this->input->get_post('userID');

        $this->accessToken = $this->input->get_post('accessToken');


        $arrUserUpdate = array();


        $this->user->checkUserAuth($this->userID, $this->accessToken);


        if ($this->input->get_post('name') || !empty($this->input->get_post('name'))) {

            $arrUserUpdate['name'] = $this->input->get_post('name');

        }

        if ($this->input->get_post('username') || !empty($this->input->get_post('username'))) {

            if (!preg_match("/^[a-zA-Z0-9_]+$/", $this->input->get_post('username'))) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage('usernameInvalid');

                $this->response($this->response);

            }

            //Validation of set minimum 3 maximum 16 :Hetal Patel

            $lenValidation = $this->validStrLen($this->input->get_post('username'), 3, 16);

            if ($lenValidation != 'true') {

                $this->response['code'] = 101;

                $this->response['message'] = $lenValidation;

                $this->response($this->response);

            }


            if ($this->CommonModel->checkExists(TBL_USER, ['username' => $this->input->get_post('username'), "userID !=" => $this->userID, "isActive" => 1, "isDelete" => 0])) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage('usernameExist');

                $this->response($this->response);

            }

            if ($this->CommonModel->checkExists(TBL_REJECTED_USERNAME, ['rejectUsername' => $this->input->post('username'), "isActive" => 1, "isDelete" => 0])) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage('usernameRejected');

                $this->response($this->response);

            }

            $arrUserUpdate['username'] = $this->input->get_post('username');

        }

        if ($this->input->get_post('website') || !empty($this->input->get_post('website'))) {

            $arrUserUpdate['website'] = $this->input->get_post('website');

        }

        if ($this->input->get_post('phone') || !empty($this->input->get_post('phone'))) {

            $arrUserUpdate['phone'] = $this->input->get_post('phone');

        }

        if ($this->input->get_post('dob') || !empty($this->input->get_post('dob'))) {

            $arrUserUpdate['dob'] = $this->input->get_post('dob');

        }

        if ($this->input->get_post('location') || !empty($this->input->get_post('location'))) {

            $arrUserUpdate['location'] = $this->input->get_post('location');

        }

        if ($this->input->get_post('about') || !empty($this->input->get_post('about'))) {

            $arrUserUpdate['about'] = $this->input->get_post('about');

        }


        if (!empty($_FILES['user_profile_image']['name'])) {

            $config['upload_path'] = './upload/userprofile/';

            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $config['encrypt_name'] = TRUE;

            $config['remove_spaces'] = TRUE;

            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);

            $this->upload->initialize($config);


            if (!$this->upload->do_upload('user_profile_image')) {

                $error = array('error' => $this->upload->display_errors());


            } else {

                $fileData = array('upload_data' => $this->upload->data());

                $arrUserUpdate['profileImage'] = $fileData["upload_data"]["file_name"];


            }

        }

        if (!empty($_FILES['user_cover_image']['name'])) {

            $config['upload_path'] = './upload/usercover/';

            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $config['encrypt_name'] = TRUE;

            $config['remove_spaces'] = TRUE;

            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);

            $this->upload->initialize($config);


            if (!$this->upload->do_upload('user_cover_image')) {

                $error = array('error' => $this->upload->display_errors());

            } else {

                $fileData = array('upload_data' => $this->upload->data());

                $arrUserUpdate['coverImage'] = $fileData["upload_data"]["file_name"];


            }

        }


        if (empty($arrUserUpdate)) {

            $this->response['code'] = 101;

            $this->response['message'] = "Update Parameter In Invalid";

            $this->response($this->response);

        }


        if ($this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $this->userID]) > 0) {

            $this->response['code'] = 100;

            $this->response['message'] = getMessage('updateProfileSuccess');

        } else {

            $this->response['code'] = 101;

            $this->response['message'] = "Profile Update Error.!";

        }

        $this->response($this->response);

    }


    public function searchUser()
    {

        $search_keyword = $_POST['search_keyword'];

        $userdata = array();

        if ($search_keyword != '')
            $search_keyword=str_replace('@',"",$search_keyword);
            $userdata = $this->user->searchuser($this->userID, $search_keyword);

        // echo $this->db->last_query();die;

        if (empty($userdata)) {

            $this->response['message'] = getMessage('userNotFound');

            $this->response($this->response);

        }

        $result = array();

        foreach ($userdata as $user) {

            $user['privateProfileStatus'] = "0";

            $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $user['userID'], 'isActive' => 1, 'isDelete' => 0]);

            if (!empty($settingsData)) {

                $user['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";

            }

            //for check user follow me or not

            $user['isFollowMe'] = "0";

            $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $user['userID'], 'followerID' => $this->userID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);

            if (!empty($checkUserFollowData)) {

                $user['isFollowMe'] = "1";

            }

            $result[] = $user;

        }


        $this->response['code'] = 100;

        $this->response['data']['searchResult'] = $result;

        $this->response['message'] = getMessage('userFound');

        $this->response($this->response);

    }


    public function create_group()
    {

        if (!$this->input->get_post('userID') || empty($this->input->get_post('userID'))) {

            $this->response['message'] = getMessage('passUserId');

            $this->response($this->response);

        }

        if (!$this->input->get_post('accessToken') || empty($this->input->get_post('accessToken'))) {

            $this->response['message'] = getMessage('passToken');

            $this->response($this->response);

        }


        $this->userID = $this->input->get_post('userID');

        $this->accessToken = $this->input->get_post('accessToken');


        $this->user->checkUserAuth($this->userID, $this->accessToken);


        if (!isset($_REQUEST['type']) || !in_array($_REQUEST['type'], [0, 1])) {

            $this->response['message'] = "Please pass type";

            $this->response($this->response);

        }

        $type = $this->input->get_post('type');


        if ($type == 0) {

            if (!$this->input->get_post('groupName') || empty($this->input->get_post('groupName'))) {

                $this->response['message'] = "Please pass group name";

                $this->response($this->response);

            }

        }


        $groupName = $this->input->get_post('groupName');


        if (!in_array($type, [0, 1])) {

            $this->response['message'] = getMessage('passtype');

            $this->response($this->response);

        }


        $membersID = explode(",", $_REQUEST['membersID']);

        if (count($membersID) <= 0) {

            $this->response['message'] = "Please pass members";

            $this->response($this->response);

        }


        if ($type == 0) {

            $alreadyExistGroup = $this->CommonModel->get_row(TBL_GROUP, ['groupID'], ['userID' => $this->userID, 'groupName' => $groupName, 'type' => $type]);

            if (!empty($alreadyExistGroup)) {

                $this->response['message'] = getMessage('alreadyExistGroup');

                $this->response($this->response);

            }

        } else {

            $getAllRelatedGroupID = $this->CommonModel->get_all(TBL_GROUP, ['groupID'], ['userID' => $this->userID, "groupName" => $groupName, 'type' => $type]);

            foreach ($getAllRelatedGroupID as $groupID) {

                $getMembers = $this->CommonModel->get_all(TBL_USER_GROUP, ['userID'], ['groupID' => $groupID['groupID']]);

                $tempNewMember = array();

                foreach ($getMembers as $key => $value) {

                    array_push($tempNewMember, $value['userID']);

                }


                if (count(array_intersect($tempNewMember, $membersID)) == count($membersID)) {

                    $this->response['message'] = "Chat Group Already Exists";

                    $this->response($this->response);

                }

            }


        }


        $arrInsert = array();

        $arrInsert['userID'] = $this->userID;

        $arrInsert['groupName'] = $this->input->get_post('groupName');

        $arrInsert['type'] = $type;


        if (isset($_FILES['groupProfile']) && !empty($_FILES['groupProfile']['name'])) {

            $config['upload_path'] = './upload/group/'; // absolute dir path

            $config['allowed_types'] = 'jpg|png|jpeg|gif';

            $config['encrypt_name'] = FALSE;

            $config['remove_spaces'] = TRUE;

            $config['overwrite'] = FALSE;

            $config['file_name'] = microtime() . "." . pathinfo($_FILES['groupProfile']['name'], PATHINFO_EXTENSION);

            $this->upload->initialize($config);


            if (!$this->upload->do_upload('groupProfile')) {

                $this->response['message'] = $this->upload->display_errors();

                $this->response($this->response);

            }

            $arrInsert['groupProfile'] = $this->upload->data()["file_name"];

        }

        $groupID = $this->CommonModel->insert('tblgroup', $arrInsert);


        if ($this->input->get_post('membersID') && !empty($this->input->get_post('membersID')) && $this->input->get_post('membersID') != '0') {

            $arrUserGroupInsert = array();

            foreach ($membersID as $key => $value) {

                $arrUserGroupInsert[$key]['userID'] = $value;

                $arrUserGroupInsert[$key]['groupID'] = $groupID;

            }


            $last_user_group_id = $this->db->insert_batch(TBL_USER_GROUP, $arrUserGroupInsert);

        }

        // $groups = $this->group->getGroupList($this->userID, $type);


        $this->response['data']['groupID'] = $groupID;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('groupSuccess');

        $this->response($this->response);

    }


    public function addMembersToGroup()
    {

        if (!$this->input->post('membersID') || empty($this->input->post('membersID'))) {

            $this->response['message'] = getMessage('passmemberName');

            $this->response($this->response);

        }

        if (!$this->input->post('groupID') || empty($this->input->post('groupID'))) {

            $this->response['message'] = getMessage('passgroupId');

            $this->response($this->response);

        }


        if ($this->input->post('membersID') && !empty($this->input->post('membersID'))) {

            $membersID = explode(",", $_POST['membersID']);

            $flag = $this->input->post('flag') ? $this->input->post('flag') : 2;

            //Get already member

            $arrAlreadyMembers = $this->CommonModel->get_all(TBL_USER_GROUP, ['userID'], ['groupID' => $this->input->post('groupID')]);

            $arrAlreadyMemberUserIDs = array_column($arrAlreadyMembers, 'userID');

            $newMemberAdd = array();

            foreach ($membersID as $key => $value) {

                if (!in_array($value, $arrAlreadyMemberUserIDs)) {

                    //Check already added and remove than readd by isDeleted 0

                    $alreadyMember = $this->CommonModel->get_row_deleted(TBL_USER_GROUP, ['userGroupID'], ['groupID' => $this->input->post('groupID'), 'userID' => $value]);

                    if (empty($alreadyMember)) {

                        //If not in database then insert

                        $insert['userID'] = $value;

                        $insert['groupID'] = $this->input->post('groupID');

                        array_push($newMemberAdd, $insert);

                    } else {

                        $this->CommonModel->update(TBL_USER_GROUP, ['isDelete' => 0], ['userGroupID' => $alreadyMember['userGroupID']]);

                    }


                }

                //Remove the member that is not selected if flag not 1

                if ($flag == 2 && $flag != 1) {

                    foreach ($arrAlreadyMemberUserIDs as $detkey => $detvalue) {

                        if (in_array($value, $arrAlreadyMemberUserIDs)) {

                            unset($arrAlreadyMemberUserIDs[$detkey]);

                        }

                    }

                }

            }

            //Add New Members

            if (!empty($newMemberAdd))

                $last_user_group_id = $this->db->insert_batch(TBL_USER_GROUP, $newMemberAdd);


            //set delete other members

            if (!empty($arrAlreadyMemberUserIDs) && $flag == 2 && $flag != 1)

                $this->group->set_delete_group_member($this->input->post('groupID'), $arrAlreadyMemberUserIDs);


        }

        $this->response['data']['membersdetails'] = $this->group->getMembers($this->userID, $this->input->post('groupID'));

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('MembersAddedSuccess');

        $this->response($this->response);

    }

    public function removeMember()
    {

        if (!$this->input->post('userGroupID') || empty($this->input->post('userGroupID'))) {

            $this->response['message'] = getMessage('passusergroupID');

            $this->response($this->response);

        }


        $arrMemberDelete['isDelete'] = 1;

        $arrMemberDelete['updatedDateTime'] = date('Y-m-d H:i:s');


        $remove_member = $this->CommonModel->update('tblusergroup', $arrMemberDelete, array('userGroupID' => $_POST['userGroupID']));

        if (empty($remove_member)) {

            $this->response['message'] = getMessage('notRemoveMember');

            $this->response($this->response);

        }

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('MembersRemovedSuccess');

        $this->response($this->response);


    }


    public function getGroupList()
    {

        $type = $this->input->get_post('type') ? $this->input->get_post('type') : 0;

        $groups = $this->group->getGroupList($this->userID, $type);

        //echo $this->db->last_query();die;

        if (empty($groups)) {

            $this->response['message'] = getMessage('notFoundGroup');

            $this->response($this->response);

        }

        if ($this->input->get_post('isencrypt') && !empty($this->input->get_post('isencrypt'))) {
            if(!empty($groups)){
                foreach($groups as &$msg){
                    $msg['lastMessage'] = $this->message->decryptString($msg['lastMessage']);
                }
            }
        }
        
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('GroupFoundSuccess');
        $this->response['data']['groupdetails'] = $groups;
        $this->response($this->response);

    }

    public function getGroupMemberList()
    {

        if (!$this->input->post('groupID') || empty($this->input->post('groupID'))) {

            $this->response['message'] = getMessage('passgroupId');

            $this->response($this->response);

        }


        $groupID = $this->input->post('groupID');

        $group_members = $this->group->getMembers($this->userID, $groupID);


        if (empty($group_members)) {

            $this->response['message'] = getMessage('notFoundMembers');

            $this->response($this->response);

        }


        $this->response['code'] = 100;

        $this->response['data']['membersdetails'] = $group_members;

        $this->response['message'] = getMessage('MembersFoundSuccess');

        $this->response($this->response);

    }

    public function getGroupPostList()
    {

        if (!$this->input->post('groupID') || empty($this->input->post('groupID'))) {

            $this->response['message'] = getMessage('passgroupId');

            $this->response($this->response);

        }

        $groupID = $this->input->post('groupID');
        $postsdata = $this->group->getGroupPosts($this->userID, $groupID);
        $postsdata1['media_post_list'] = array();
        $postsdata1['text_post_list'] = array();
        if(!empty($postsdata)){
            foreach ($postsdata as &$value) {
                $media = array();
                $result = $this->CommonModel->get_all('tblmedia', ["mediaID", "filename", "type"], ["comID" => $value['postID'], "isActive" => 1, "isDelete" => 0, "mediaRef" => 0], 'createdDateTime');
                foreach ($result as $key => $res) {
                    $media[$key]['mediaID'] = $res['mediaID'];
                    $media[$key]['type'] = $res['type'];
                    $media[$key]['filename'] = base_url() . "upload/postmedia/" . $res['filename'];
                }
                if (isset($value['gif']) && $value['gif'] == null || is_null($value['gif'])) {
                    $value['gif'] = "";
                }
                $value['ResharePostContent'] = "";
                $value['ResharePostEmoji'] = "";
                $value['ReshareUserImage'] = "";
                $value['reshareUserData'] = null;
                if (isset($value['postShareID']) && $value['postShareID'] > 0) {
                    $getSharePost = $this->post->getUserSharePost($value['postShareID']);
                    if (!empty($getSharePost)) {
                        $value['isLiked'] = "0";
                        $value['isdisLiked'] = "0";
                        $likeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $value['postID'], 'postShareID' => $value['postShareID'], "isLiked" => 1, "isDelete" => 0]);
                        if (!empty($likeResult)) {
                            $value['isLiked'] = $likeResult['isActive'];
                        }
                        $dislikeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $value['postID'], 'postShareID' => $value['postShareID'], "isLiked" => 0, "isDelete" => 0]);
                        if (!empty($dislikeResult)) {
                            $value['isdisLiked'] = $dislikeResult['isActive'];
                        }
                        $value['ResharePostUserid'] = (isset($getSharePost['userID'])) ? $getSharePost['userID'] : "";
                        $value['ResharePostDate'] = (isset($getSharePost['createdDateTime'])) ? $getSharePost['createdDateTime'] : "";
                        $value['ResharePostContent'] = (isset($getSharePost['post'])) ? $getSharePost['post'] : "";
                        $value['ResharePostEmoji'] = (isset($getSharePost['emoji'])) ? $getSharePost['emoji'] : "";
                        $reshareUserdata = $this->user->getUserdetails($getSharePost['userID']);
                        if (!empty($reshareUserdata)) {
                            $value['ReshareUserImage'] = (isset($reshareUserdata['profileImage'])) ? $reshareUserdata['profileImage'] : "";
                            $shareuserData = array();
                            $shareuserData['isVeteran'] = (isset($reshareUserdata['isVeteran'])) ? $reshareUserdata['isVeteran'] : "";
                            $shareuserData['isOffical'] = (isset($reshareUserdata['isOffical'])) ? $reshareUserdata['isOffical'] : "";
                            $shareuserData['isResponder'] = (isset($reshareUserdata['isResponder'])) ? $reshareUserdata['isResponder'] : "";
                            $value['reshareUserData'] = $shareuserData;
                        }
                    }
                }
                $pollPostOptions = array();
                $isPollPost = (isset($value['isPollPost'])) ? $value['isPollPost'] : "";
                $getSinglePostData = $this->CommonModel->get_row(TBL_POST, ['*'], ['postID' => $value['postID'], 'isActive' => 1, 'isDelete' => 0]);
                $value['isPollPost'] = (isset($isPollPost) && $isPollPost != "") ? $isPollPost : (isset($getSinglePostData['isPollPost'])) ? $getSinglePostData['isPollPost'] : "0";
                if (isset($value['isPollPost']) && trim($value['isPollPost']) != "" && $value['isPollPost'] == null || is_null($value['isPollPost'])) {
                    $value['isPollPost'] = "";
                }
                if (isset($value['isPollPost']) && $value['isPollPost'] == 1) {
                    $value['hasPollPostAnswer'] = $this->post->checkPollPostHasUserAnswer($this->userID, $value['postID']);
                    $pollOptions = $this->CommonModel->get_all(TBL_POST_POLL_OPTIONS, ['*'], ["postID" => $value['postID'], "isActive" => 1, "isDelete" => 0]);
                    if (!empty($pollOptions)) {
                        foreach ($pollOptions as $opt) {
                            $avgAnswer = $this->post->getPollOptionAvgAns($value['postID'], $opt['optionID']);
                            $res = array();
                            $res['optionID'] = $opt['optionID'];
                            $res['option'] = $opt['text'];
                            $res['avgAnswer'] = $avgAnswer;
                            $pollPostOptions[] = $res;
                        }
                    }
                }
                $value['pollPostOptions'] = (!empty($pollPostOptions)) ? $pollPostOptions : array();
                $totalPollAnswer = $this->CommonModel->get_all(TBL_POST_POLL_ANSWER, ['*'], ["postID" => $value['postID'], "isActive" => 1, "isDelete" => 0]);
                $value['totalPollAnswer'] = (!empty($totalPollAnswer)) ? count($totalPollAnswer) . "" : "0";
    
                $totalComments = $this->post->totalComments($value['postID'], $this->userID,$value['postShareID']);
                $totalShares = $this->post->totalShares($value['postID'], $this->userID,$value['postShareID']);
                $totalLikes = $this->post->totalLikes($value['postID'], $this->userID,$value['postShareID']);
                $totalDisLikes = $this->post->totalDisLikes($value['postID'], $this->userID,$value['postShareID']);
                
                $value['media'] = $media;
                $value['commentCount'] = $totalComments;
                $value['reShareCount'] = $totalShares;
                $value['likesCount'] = $totalLikes;
                $value['disLikeCount'] = $totalDisLikes;
    
                if (count($media) == 0) {
                    array_push($postsdata1['text_post_list'], $value);
                } else {
                    array_push($postsdata1['media_post_list'], $value);
                }
                unset($media);
            }
        }
        $this->response['data'] = $postsdata1;
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('getGroupPostRecordsSuccess');
        $this->response($this->response);

    }

    public function deleteGroup()
    {

        if (!$this->input->post('groupID') || empty($this->input->post('groupID'))) {

            $this->response['message'] = getMessage('passgroupId');

            $this->response($this->response);

        }


        $arrGroupDelete['isDelete'] = 1;

        $arrGroupDelete['updatedDateTime'] = date('Y-m-d H:i:s');


        $delete_group = $this->CommonModel->update(TBL_GROUP, $arrGroupDelete, array('groupID' => $this->input->post('groupID')));


        if (empty($delete_group)) {

            $this->response['message'] = getMessage('notDeleteGroup');

            $this->response($this->response);

        }


        $groups = $this->group->getGroupList($this->userID);


        $this->response['code'] = 100;

        $this->response['data']['groupdetails'] = $groups;

        $this->response['message'] = getMessage('GroupDeleteSuccess');

        $this->response($this->response);

    }

    public function getGroupPostMedia()
    {

        if (!$this->input->post('groupID') || empty($this->input->post('groupID'))) {

            $this->response['message'] = getMessage('passgroupId');

            $this->response($this->response);

        }


        $groupID = $this->input->post('groupID');


        $mediadata = $this->group->getPostMedia($this->userID, $groupID);

        foreach ($mediadata as &$value) {

            $value['filename'] = base_url() . "upload/postmedia/" . $value['filename'];

        }


        if (empty($mediadata)) {

            $this->response['message'] = getMessage('notFoundMedia');

            $this->response($this->response);

        }


        $this->response['data']['allMedia'] = $mediadata;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getMediaRecordsSuccess');

        $this->response($this->response);

    }

    public function resendOtp()
    {

        $userEmail = $this->CommonModel->get_row(TBL_USER, ["email"], ["userID" => $this->userID]);


        if (empty($userEmail['email'])) {

            $this->response['message'] = "User not exists";

            $this->response($this->response);

        }


        $otp = $this->rand_code(4);


        $arrUserUpdate['otp'] = $otp;


        if ($this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $this->userID]) > 0) {


            /*Email Start*/


            /*Email Template start*/

            $html = file_get_contents(base_url() . "emailTemplate/welcome.php");

            $emailHeader = file_get_contents("emailTemplate/emailHeader.php");

            $emailFooter = file_get_contents("emailTemplate/emailFooter.php");


            $replaceOn = ["{{OTP}}", "{{base_url}}", "{{emailHeader}}", "{{emailFooter}}"];

            $replaceWith = [$otp, base_url(), $emailHeader, $emailFooter];

            $messageHTML = str_replace($replaceOn, $replaceWith, $html);

            /*Email Template end*/


            $subject = "Welcome to Say Scape Getting Started";

            $mail_end = $this->CommonModel->send_mail($userEmail['email'], $subject, $messageHTML);

            /*End*/

            if (!$mail_end) {

                $this->response['message'] = "Email Not Sent";

                $this->response($this->response);

            }


            $this->response['code'] = 100;

            $this->response['message'] = getMessage('resendOTPsuccess');


        } else {

            $this->response['message'] = "OTP NOT UPDATE";

        }


        $this->response($this->response);

    }


    public function forgot_password()
    {

        $userEmail = $this->CommonModel->get_row(TBL_USER, ["userID", "email"], ["email" => $this->input->post('email')]);


        if (empty($userEmail['email'])) {

            $this->response['message'] = "User not exists";

            $this->response($this->response);

        }


        $new_password = $this->random_password();


        $arrUserUpdate['password'] = md5($new_password);


        if ($this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $userEmail['userID']]) > 0) {


            /*Email Start*/


            /*Email Template start*/

            $html = file_get_contents(base_url() . "emailTemplate/forgotPassword.php");

            $emailHeader = file_get_contents("emailTemplate/emailHeader.php");

            $emailFooter = file_get_contents("emailTemplate/emailFooter.php");


            $replaceOn = ["{{password}}", "{{base_url}}", "{{emailHeader}}", "{{emailFooter}}"];

            $replaceWith = [$new_password, base_url(), $emailHeader, $emailFooter];

            $messageHTML = str_replace($replaceOn, $replaceWith, $html);

            /*Email Template end*/


            $subject = "Say Scape Login";

            $mail_end = $this->CommonModel->send_mail($userEmail['email'], $subject, $messageHTML);

            /*End*/

            if (!$mail_end) {

                $this->response['message'] = "Email Not Sent";

                $this->response($this->response);

            }


            $this->response['code'] = 100;

            $this->response['message'] = getMessage('sendpasswordsuccess');


        } else {

            $this->response['message'] = "OTP NOT UPDATE";

        }


        $this->response($this->response);

    }


    public function getUserPosts()
    {

        $postsdata = $this->user->getPostByUser($this->userID);

        foreach ($postsdata as &$value) {

            $value = $this->post->getPostResponseData($value, $this->userID);

        }

        if (empty($postsdata)) {

            $this->response['message'] = getMessage('NoData');

            $this->response($this->response);

        }


        $this->response['data']['allPosts'] = $postsdata;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }


    public function getUserPostByMedia()
    {

        $postsdata = $this->user->getMediaPostByUser($this->userID);

        foreach ($postsdata as &$value) {

            $media = array();

            $result = $this->CommonModel->get_all('tblmedia', ["mediaID", "filename", "type"], ["comID" => $value['postID'], "isActive" => 1, "isDelete" => 0, "mediaRef" => 0]);

            foreach ($result as $key => $res) {

                $media[$key]['mediaID'] = $res['mediaID'];

                $media[$key]['type'] = $res['type'];

                $media[$key]['filename'] = base_url() . "upload/postmedia/" . $res['filename'];
            }
            $totalComments = $this->post->totalComments($value['postID'], $this->userID,$value['postShareID']);
            $totalShares = $this->post->totalShares($value['postID'], $this->userID,$value['postShareID']);
            $totalLikes = $this->post->totalLikes($value['postID'], $this->userID,$value['postShareID']);
            $totalDisLikes = $this->post->totalDisLikes($value['postID'], $this->userID,$value['postShareID']);
            $value['media'] = $media;
            $value['commentCount'] = $totalComments;
            $value['reShareCount'] = $totalShares;
            $value['likesCount'] = $totalLikes;
            $value['disLikeCount'] = $totalDisLikes;
            unset($media);
        }

        if (empty($postsdata)) {

            $this->response['message'] = getMessage('NoData');
            $this->response($this->response);
        }


        $this->response['data']['allPosts'] = $postsdata;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');
        $this->response($this->response);
    }


    public function getUserPostByComment()
    {

        $postsdata = $this->user->getCommentPostByUser($this->userID);

        foreach ($postsdata as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }
        if (empty($postsdata)) {
            $this->response['message'] = getMessage('NoData');
            $this->response($this->response);
        }
        $this->response['data']['allPosts'] = $postsdata;
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('getRecordSuccess');
        $this->response($this->response);
    }

    public function getUserPostByLikes()
    {

        $postsdata = $this->user->getLikedPostByUser($this->userID);
        foreach ($postsdata as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }

        if (empty($postsdata)) {
            $this->response['message'] = getMessage('NoData');
            $this->response($this->response);
        }
        $this->response['data']['allPosts'] = $postsdata;
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('getRecordSuccess');
        $this->response($this->response);
    }


    public function getUserProfile()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);
        }

        $watchID = $this->input->post('wid');


        $userdata['userDetails'] = $this->user->getUserdetails($watchID, $this->userID);

        $userdata['userDetails']['totalFollowing'] = count($this->user->getFollowingList($this->userID, $watchID));

        $userdata['userDetails']['totalFollowers'] = count($this->user->getFollowersList($this->userID, $watchID));


        $userdata['post_list'] = $this->user->getPostByUser($this->userID, $watchID);
        $userdata['userDetails']['media_post_list'] = array();
        $userdata['userDetails']['text_post_list'] = array();

        foreach ($userdata['post_list'] as &$value) {
            // $value = $this->post->getPostResponseData($value, $this->userID);
            $value = $this->post->getPostResponseData($value, $this->userID,'','otherType');
            if(isset($value['media_post_list']) && !empty($value['media_post_list'])){
                $userdata['userDetails']['media_post_list'][] = $value['media_post_list'];
            }
            if(isset($value['text_post_list']) && !empty($value['text_post_list'])){
                $userdata['userDetails']['text_post_list'][] = $value['text_post_list'];
            }
        }
        unset($userdata['post_list']);
        $userdata['userDetails']['comments_list'] = $this->user->getCommentPostByUser($watchID);

        foreach ($userdata['userDetails']['comments_list'] as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }

        $userdata['userDetails']['like_post_list'] = $this->user->getLikedPostByUser($watchID);

        foreach ($userdata['userDetails']['like_post_list'] as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }

        $userdata['userDetails']['group_list'] = $this->group->getGroupList($watchID);

        //for check user subscribe or not
        $userdata['userDetails']['postSubscribeStatus'] = "0";
        $checkPostSubscribeStatus = $this->CommonModel->get_row(TBL_POST_SUBSCRIBE, ['*'], ['userID' => $this->userID, 'followerID' => $watchID, 'isActive' => 1, 'isDelete' => 0]);
        if (!empty($checkPostSubscribeStatus)) {
            $userdata['userDetails']['postSubscribeStatus'] = $checkPostSubscribeStatus['status'] . "";
        }

        $userdata['userDetails']['privateProfileStatus'] = "0";
        $userdata['userDetails']['hideBirthday'] = "0";
        $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $watchID, 'isActive' => 1, 'isDelete' => 0]);
        if (!empty($settingsData)) {
            $userdata['userDetails']['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";
            $userdata['userDetails']['hideBirthday'] = (isset($settingsData['hideBirthday'])) ? $settingsData['hideBirthday'] . "" : "0";
        }
        //$userdata['comments_list'] = $this->user->getCommentPostByUser($this->userID);
        if (empty($userdata)) {
            $this->response['message'] = getMessage('NoData');
            $this->response($this->response);
        }

        $this->response['data'] = $userdata;
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('getRecordSuccess');
        $this->response($this->response);
    }

    public function sendMessage()
    {
        $this->store_logs();
        if (!$this->input->get_post('userID') || empty($this->input->get_post('userID'))) {
            $this->response['message'] = getMessage('passUserId');
            $this->response($this->response);
        }
        if (!$this->input->get_post('accessToken') || empty($this->input->get_post('accessToken'))) {
            $this->response['message'] = getMessage('passToken');
            $this->response($this->response);
        }

        $this->userID = $this->input->get_post('userID');
        $this->accessToken = $this->input->get_post('accessToken');

        $userInitDetails = $this->user->checkUserAuth($this->userID, $this->accessToken);

        $this->userName = $userInitDetails['name'];
        if (!$this->input->get_post('toUserID') || empty($this->input->get_post('toUserID'))) {
            $this->response['message'] = getMessage('passToUserId');
            $this->response($this->response);
        }

        if (!isset($_FILES['media']["name"]) || empty($_FILES['media']["name"])) {
            if (!$this->input->get_post('text') || empty($this->input->get_post('text'))) {
                $this->response['message'] = getMessage('notFoundText');
                $this->response($this->response);
            }
        }

        $toUserID = $this->input->get_post('toUserID');
        $message = $this->input->get_post('text');
        if ($this->input->get_post('isencrypt') && !empty($this->input->get_post('isencrypt'))) {
            $message = $this->message->encryptString($message);
        }
        //Changes By fromUserID = groupID : Hetal Patel
        $messageInsert['fromUserID'] = $this->userID;
        $messageInsert['type'] = $this->input->get_post('type') ? $this->input->get_post('type') : 0;
        $messageInsert['toUserID'] = $toUserID;
        $messageInsert['text'] = htmlspecialchars($message);
        $messageInsert['gif'] = (isset($_POST['gif'])) ? $_POST['gif'] : "";
        $messageInsert['createdDateTime'] = date('Y-m-d H:i:s');
        $messageInsert['updatedDateTime'] = date('Y-m-d H:i:s');
        if ($messageInsert['toUserID'] == $messageInsert['fromUserID'] && $messageInsert['type'] == 0) {
            $this->response['code'] = 101;
            $this->response['message'] = "User not send message by own";
            $this->response($this->response);
        }

        // if(!$this->CommonModel->checkExists(TBL_USER_FOLLOWER,['userID'=>$messageInsert['fromUserID'],'followerID'=>$messageInsert['toUserID'],"isActive"=>1,"isDelete"=>0]) && $messageInsert['type'] != 1 ){
        //     $this->response['message'] = "You are not follow this user";
        //     $this->response($this->response);
        // }

        if ($this->CommonModel->checkExists(TBL_BLOCKUSER, ['userID' => $messageInsert['fromUserID'], 'blockUserID' => $messageInsert['toUserID'], "isActive" => 1, "isDelete" => 0]) && $messageInsert['type'] != 1) {
            $this->response['message'] = getMessage("userisBlock");
            $this->response($this->response);
        }

        $messageInsertResult = $this->message->insertUserMessage($messageInsert);
        if ($messageInsertResult === FALSE) {
            $this->response['message'] = getMessage('MessageError');
        } else {
            $messageID = $messageInsertResult;

            $this->response['code'] = 100;
            $this->response['message'] = "Message Successfully send.!";

            if ((isset($_FILES['media']["name"]) && !empty($_FILES['media']["name"]))) {
                $config['upload_path'] = './upload/messagemedia/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|mp4';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('media')) {
                    $this->response['code'] = 101;
                    $this->response['message'] = "Message Media Upload Error.!";
                } else {
                    $fileData = array('upload_data' => $this->upload->data());
                    $messageMediaInsert['comID'] = $messageID;
                    $messageMediaInsert['userID'] = $this->userID;
                    $messageMediaInsert['type'] = $this->input->get_post('media_type');
                    $messageMediaInsert['mediaRef'] = 1;
                    $messageMediaInsert['filename'] = $fileData["upload_data"]["file_name"];

                    if ($this->message->insertMedia($messageMediaInsert)) {
                        $this->response['code'] = 100;
                        $this->response['message'] = "Message With Media Successfully send.!";
                        $this->sendNotification(array(
                            "type" => "chat",
                            "title" => "SayScape",
                            "desc" => ucfirst($this->userName) . " has sent you a message.",
                            "mesgType" => $this->input->get_post('type'),
                            "targetID" => $toUserID,
                            "senderID"=>$this->userID,
                            "notifyType"=>"newMessage"
                        ));
                    } else {
                        $this->response['code'] = 101;
                        $this->response['message'] = "Message Media Insert Error..!";
                    }
                }
            } else {
                $this->sendNotification(array(
                    "type" => "chat",
                    "title" => "SayScape",
                    "desc" => ucfirst($this->userName) . " has sent you a message.",
                    "mesgType" => $this->input->get_post('type'),
                    "targetID" => $toUserID,
                    "senderID"=>$this->userID,
                    "notifyType"=>"newMessage"
                ));
            }
        }

        $this->response($this->response);

    }

    public function getUserMessages()
    {
        if (!$this->input->post('toUserID') || empty($this->input->post('toUserID'))) {
            $this->response['message'] = getMessage('passToUserId');
            $this->response($this->response);
        }

        $toUserID = $this->input->post('toUserID');
        $type = $this->input->post('type');

        $this->response['code'] = 100;
        $this->response['message'] = 'Get Data Successfully.!';
        
         
        if ($this->input->get_post('isencrypt') && !empty($this->input->get_post('isencrypt'))) {
            $messageData = $this->message->getUserMessages($this->userID, $toUserID, $type);
            if(!empty($messageData)){
                foreach($messageData as &$msg){
                    if(isset($msg['media_type']) && !empty($msg['media_type'])){
                        $msgText = $msg['text'];
                    }else{
                        $msgText = $this->message->decryptString($msg['text']);
                    }
                    $msg['text'] = $msgText;
                }
            }
            $this->response['data']['message_chat_list'] = $messageData;
        }else{
            $this->response['data']['message_chat_list'] = $this->message->getUserMessages($this->userID, $toUserID, $type);
        }
        $this->response['data']['userDetails'] = $this->user->getUserdetails($toUserID, $this->userID);
        //for read message
        /*$arrUpdate = array();
        $arrUpdate['isRead'] = 1;
        $this->CommonModel->update('tblmessage',$arrUpdate,['toUserID' => $this->userID, 'isRead'=>0, 'isActive'=>1, 'isDelete'=>0]);*/
        $this->response($this->response);

    }

    public function getRecentChatUserList()
    {
        $this->response['code'] = 100;
        $this->response['message'] = 'Get Data Successfully.!';
        $chatUserList = array(); 
        $user_list = $this->message->getRecentChatList($this->userID);
        if(!empty($user_list)){
            foreach($user_list as $user){
                $toUserID = $user['toUserID'];
                $checkClearData = $this->CommonModel->get_row(TBL_MESSAGE_CLEAR, ['*'], ['isActive'=>1,'userID' => $this->userID, 'toUserID'=>$toUserID]);
                // echo "<pre>"; print_r($checkClearData);
                if(!empty($checkClearData) && ($checkClearData['messageID'] >= $user['messageID'])){
                    unset($user);
                }else{
                    if(isset($user['toUserID']) && $user['toUserID']!=""){
                        if ($this->input->get_post('isencrypt') && !empty($this->input->get_post('isencrypt'))) {
                            $user['text'] = $this->message->decryptString($user['text']);
                        }
                        $chatUserList[] = $user;    
                    }
                }
            }
        }
        $this->response['data']['user_list'] = $chatUserList;
        $arrUpdate = array();
        $arrUpdate['isRead'] = 1;
        $this->CommonModel->update('tblmessage', $arrUpdate, ['toUserID' => $this->userID, 'isRead' => 0, 'isActive' => 1, 'isDelete' => 0]);
        $this->response($this->response);
    }

    public function passwordChange()
    {
        if (!$this->input->post('oldPassword') || empty($this->input->post('oldPassword'))) {
            $this->response['message'] = getMessage('passToken');
            $this->response($this->response);
        }
        if (!$this->input->post('newPassword') || empty($this->input->post('newPassword'))) {
            $this->response['message'] = getMessage('passToken');
            $this->response($this->response);
        }

        $oldPassword = $this->input->post('oldPassword');
        $newPassword = $this->input->post('newPassword');

        if ($this->user->changePassword($oldPassword, $newPassword, $this->userID)) {
            $this->response['code'] = 100;
            $this->response['message'] = 'Password Update SuccessFully';
        } else {
            $this->response['code'] = 101;
            $this->response['message'] = "Old Password Not Match";
        }

        $this->response($this->response);
    }

    public function getVeteranPost()
    {
        //        $veteranData = $this->post->getVeteranPost($this->userID);
        $veteranData = $this->post->getPosts($this->userID, 2);
        if (!empty($veteranData)) {
            foreach ($veteranData as &$value) {
                $value = $this->post->getPostResponseData($value, $this->userID);
            }
        }

        if ($veteranData === false) {
            $this->response['message'] = "Not Allow";
        } else {
            $this->response['code'] = 100;
            $this->response['message'] = "Get Data SuccessFully";
            $this->response['data']['allPosts'] = $veteranData;
        }

        $this->response($this->response);
    }

    public function addUserBookmarkPost()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $addBookmark = array();


        $addBookmark['userID'] = $this->userID;

        $addBookmark['postID'] = $this->input->post('postID');

        $addBookmark['accessToken'] = $this->accessToken;


        unset($addBookmark['accessToken']);


        if ($this->CommonModel->checkExists(TBL_POST_BOOKMARK, ['userID' => $addBookmark['userID'], 'postID' => $addBookmark['postID'], "isActive" => 1, "isDelete" => 0])) {

            $this->response['message'] = "THIS BOOKMARK ALREADY EXISTS";

        } else {

            if ($this->post->insertBookmark($addBookmark)) {

                $this->response['code'] = 100;

                $this->response['message'] = "BOOKMARK SAVED";

            } else {

                $this->response['message'] = "BOOKMARK NOT SAVED";

            }

        }


        $this->response($this->response);

    }

    public function getUserBookmarkPost()
    {
        $postList = $this->post->getUserBookmarkPost($this->userID);
        if (!empty($postList)) {
            foreach ($postList as &$value) {
                $value = $this->post->getPostResponseData($value, $this->userID);
            }
        }

        $this->response['code'] = 100;

        $this->response['message'] = "Get Data SuccessFully";

        $this->response['data']['allPosts'] = $postList;


        $this->response($this->response);

    }

    public function removeUserBookmarkPost()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }


        $postID = $this->input->post('postID');


        if ($this->post->updateBookmark($this->userID, $postID)) {

            $this->response['code'] = 100;

            $this->response['message'] = "BOOKMARK REMOVED";

        } else {

            $this->response['message'] = "BOOKMARK NOT REMOVED";

        }


        $this->response($this->response);

    }

    public function getTrendingPostList()
    {

        /**
         * 0 - Current 24 Hour
         * 1 - Current Week
         * 2 - Current Month
         * 3 - Current Year
         */


        if (!isset($_POST['time']) || !in_array($this->input->post('time'), ['0', '1', '2', '3'])) {

            $this->response['message'] = "Please pass time";

            $this->response($this->response);

        }

        $time = $this->input->post('time');
        $this->response['data']['totalPages'] = $this->post->getTrendingPost($this->userID, $time, TRUE);
        $pageNumber = $this->input->post('pageNumber') ? $this->input->post('pageNumber') : 'ALL';


        $postList = $this->post->getTrendingPost($this->userID, $time, FALSE, $pageNumber);
        foreach ($postList as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }

        $this->response['code'] = 100;

        $this->response['message'] = "Get Data SuccessFully";

        $this->response['data']['allPosts'] = $postList;


        $this->response($this->response);

    }

    public function getPostLikeList()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $postID = $this->input->post("postID");


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");


        $this->response['data']['userList'] = $this->post->getPostLikeUserList($this->userID, $postID);


        $this->response($this->response);

    }

    public function getPostDisLikeList()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $postID = $this->input->post("postID");


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");


        $this->response['data']['userList'] = $this->post->getPostDisLikeUserList($this->userID, $postID);


        $this->response($this->response);

    }

    public function getFollowersList()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);

        }

        $wid = $this->input->post('wid');


        $userdata = $this->user->getFollowersList($this->userID, $wid);

        // echo "<pre>"; print_r($userdata); die();

        // $this->response['data']['userFollowersList'] = $this->user->getFollowersList($this->userID,$wid);

        if (empty($userdata)) {

            $this->response['message'] = getMessage('userNotFound');

            $this->response($this->response);

        }


        $result = array();

        foreach ($userdata as $user) {

            $user['privateProfileStatus'] = "0";

            $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $user['followerID'], 'isActive' => 1, 'isDelete' => 0]);

            if (!empty($settingsData)) {

                $user['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";

            }

            //for check user follow me or not

            $user['isFollowMe'] = "0";

            $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $user['followerID'], 'followerID' => $this->userID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);

            if (!empty($checkUserFollowData)) {

                $user['isFollowMe'] = "1";

            }

            $result[] = $user;

        }


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");

        $this->response['data']['userFollowersList'] = $result;

        $this->response($this->response);

    }

    public function getFollowingList()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);

        }

        $wid = $this->input->post('wid');

        $userdata = $this->user->getFollowingList($this->userID, $wid);

        // echo "<pre>"; print_r($userdata); die();

        // $this->response['data']['userFollowingList'] = $this->user->getFollowingList($this->userID,$wid);

        if (empty($userdata)) {

            $this->response['message'] = getMessage('userNotFound');

            $this->response($this->response);

        }


        $result = array();

        foreach ($userdata as $user) {

            $user['privateProfileStatus'] = "0";

            $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $user['followerID'], 'isActive' => 1, 'isDelete' => 0]);

            if (!empty($settingsData)) {

                $user['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";

            }

            //for check user follow me or not

            $user['isFollowMe'] = "0";

            $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $user['followerID'], 'followerID' => $this->userID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);

            if (!empty($checkUserFollowData)) {

                $user['isFollowMe'] = "1";

            }

            $result[] = $user;

        }


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");

        $this->response['data']['userFollowingList'] = $result;

        $this->response($this->response);

    }

    public function getPostDetail()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        $postShareID = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;

        $postID = $this->input->post('postID');


        $postData = $this->post->getParticularPost($this->userID, $postID, $postShareID);
        foreach ($postData as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }

        $this->response['code'] = 100;
        $this->response['message'] = getMessage("getRecordSuccess");
        $this->response['data']['allPosts'] = $postData;
        $this->response($this->response);

    }

    public function saveReport()
    {

        if (!$this->input->post('reportID') || empty($this->input->post('reportID'))) {

            $this->response['message'] = getMessage('PassReportID');

            $this->response($this->response);

        }

        if (($this->input->post('reportType') && !empty($this->input->post('reportType'))) && !in_array($this->input->post('reportType'), [0, 1, 2])) {

            $this->response['message'] = getMessage('passReportType');

            $this->response($this->response);

        }

        $arrReportInsert['userID'] = $this->userID;

        $arrReportInsert['reportType'] = $this->input->post("reportType");

        $arrReportInsert['comID'] = $this->input->post('reportID');


        if (!$this->CommonModel->checkExists(TBL_REPORT, ['userID' => $arrReportInsert['userID'], "reportType" => $arrReportInsert['reportType'], "comID" => $arrReportInsert['comID'], "isActive" => 1, "isDelete" => 0])) {

            $reportID = $this->CommonModel->insert(TBL_REPORT, $arrReportInsert);


            if ($reportID > 0) {

                $this->response['code'] = 100;

                $this->response['message'] = getMessage("ReportInsertSuccess");

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("ReportInsertError");

            }

        } else {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("ReportAlready");

        }


        $this->response($this->response);

    }

    public function blockUnBLockUser()
    {

        if (!$this->input->post('blockUserID') || empty($this->input->post('blockUserID'))) {

            $this->response['message'] = getMessage("PassBlockUserID");

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = getMessage("passstatus");

            $this->response($this->response);

        }


        $status = $this->input->post("status");

        $arrBlockInsert['userID'] = $this->userID;

        $arrBlockInsert['blockUserID'] = $this->input->post('blockUserID');


        if ($arrBlockInsert['blockUserID'] == $arrBlockInsert['userID']) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("UserBlockOwnError");

            $this->response($this->response);

        }


        if (!$this->CommonModel->checkExists(TBL_USER, ['userID' => $arrBlockInsert['blockUserID'], "isActive" => 1, "isDelete" => 0])) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("BlockIDError");

            $this->response($this->response);

        }


        $alreadyExistsBlockUser = $this->CommonModel->get_row(TBL_BLOCKUSER, ['blockID'], ['userID' => $this->userID, 'blockUserID' => $arrBlockInsert['blockUserID'], 'isActive' => 1, 'isDelete' => 0]);


        if ($status == 0) {

            if (empty($alreadyExistsBlockUser)) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("BlockAlreadyNot");

            } else {

                $userFollowerID = $this->CommonModel->update(TBL_BLOCKUSER, ['isActive' => 0], ['blockID' => $alreadyExistsBlockUser['blockID']]);

                if (empty($userFollowerID)) {

                    $this->response['code'] = 101;

                    $this->response['message'] = getMessage("UnBlockSaveError");

                } else {

                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("UnBlockSaveSuccess");

                }

            }

        } else {

            if (empty($alreadyExistsBlockUser)) {

                if ($this->CommonModel->insert(TBL_BLOCKUSER, $arrBlockInsert) > 0) {

                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("BlockUserIDInsertSuccess");

                } else {

                    $this->response['code'] = 101;

                    $this->response['message'] = getMessage("BlockUserIDInsertError");

                }

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage('BlockAlready');

            }

        }


        $this->response($this->response);

    }

    public function getUserBlockList()
    {

        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");

        $this->response['data']['userList'] = $this->user->getUserBlockedList($this->userID);


        $this->response($this->response);

    }

    public function saveSettings()
    {

        if (!isset($_POST['notificationStatus']) || !in_array($this->input->post('notificationStatus'), [0, 1])) {

            $this->response['message'] = "Please Pass Notification Status";

            $this->response($this->response);

        }


        if (!isset($_POST['privateMessageStatus']) || !in_array($this->input->post('privateMessageStatus'), [0, 1])) {

            $this->response['message'] = "Please Pass Private Message Status";

            $this->response($this->response);

        }


        $arrSettings['notificationStatus'] = $this->input->post('notificationStatus');

        $arrSettings['privateNotification'] = $this->input->post('privateNotification');
        $arrSettings['privateMessageStatus'] = $this->input->post('privateMessageStatus');

        $arrSettings['blacklistKeyword'] = ($this->input->post('blacklistKeyword')) ? $this->input->post('blacklistKeyword') : '';

        $arrSettings['privateProfileStatus'] = ($this->input->post('privateProfileStatus') != "") ? $this->input->post('privateProfileStatus') : "";

        $arrSettings['hideBirthday'] = ($this->input->post('hideBirthday') != "") ? $this->input->post('hideBirthday') : "";

        $arrSettings['userID'] = $this->userID;


        $userExists = $this->CommonModel->get_row(TBL_SETTINGS, 'settingID', ['userID' => $this->userID, 'isActive' => 1, 'isDelete' => 0]);


        if (empty($userExists)) {

            if ($this->CommonModel->insert(TBL_SETTINGS, $arrSettings) > 0) {

                $this->response['code'] = 100;

                $this->response['message'] = getMessage("SettingsSaveSuccess");

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("SettingsSaveError");

            }

        } else {

            if ($this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userExists['settingID']]) > 0) {

                $this->response['code'] = 100;

                $this->response['message'] = getMessage("SettingUpdateSuccess");

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("SettingsSaveError");

            }

        }


        $this->response($this->response);

    }

    public function getSettings()
    {

        $getSettingsRow = ['notificationStatus' => "0", 'privateMessageStatus' => "0", 'blacklistKeyword' => '', 'privateProfileStatus' => '0', 'hideBirthday' => '0'];

        $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['notificationStatus', 'privateMessageStatus', 'blacklistKeyword', 'privateProfileStatus', 'hideBirthday', 'privateNotification'], ['userID' => $this->userID, 'isActive' => 1, 'isDelete' => 0]);


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");

        $this->response['data']['settings'] = empty($settingsData) ? $getSettingsRow : $settingsData;


        $this->response($this->response);

    }

    public function hideUnHidePost()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage("passPostId");

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = getMessage("passstatus");

            $this->response($this->response);

        }


        $status = $this->input->post("status");


        $arrHideUnHideInsert['userID'] = $this->userID;

        $arrHideUnHideInsert['postID'] = $this->input->post('postID');


        if (!$this->CommonModel->checkExists(TBL_POST, ['postID' => $arrHideUnHideInsert['postID'], "isActive" => 1, "isDelete" => 0])) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("notFoundPosts");

            $this->response($this->response);

        }


        $alreadyHidePost = $this->CommonModel->get_row(TBL_HIDEPOST, ['hidePostID'], ['userID' => $this->userID, 'postID' => $arrHideUnHideInsert['postID'], 'isActive' => 1, 'isDelete' => 0]);


        if ($status == 0) {

            if (empty($alreadyHidePost)) {

                $this->response['code'] = 101;

                $this->response['message'] = "This post already unhide.";

            } else {

                if ($this->CommonModel->update(TBL_HIDEPOST, ['isActive' => 0], ['hidePostID' => $alreadyHidePost['hidePostID']]) > 0) {

                    // $this->response['code'] = 100;

                    // $this->response['message'] = "Post UnHide SuccessFully.!";

                    $this->getAllPosts();

                } else {

                    $this->response['code'] = 101;

                    $this->response['message'] = "Post UnHide Error.";

                }

            }

        } else {

            if (empty($alreadyHidePost)) {

                if ($this->CommonModel->insert(TBL_HIDEPOST, $arrHideUnHideInsert) > 0) {

                    // $this->response['code'] = 100;

                    // $this->response['message'] = "Post Hide SuccessFully.!";

                    $this->getAllPosts();

                } else {

                    $this->response['code'] = 101;

                    $this->response['message'] = "Post Hide Error.!";

                }

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = "This post is already hide.";

            }

        }


        $this->response($this->response);

    }

    public function getHidePostList()
    {

        $postHideList = $this->post->getHidePost($this->userID);


        foreach ($postHideList as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }


        $this->reponse['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");

        $this->response['data'] = $postHideList;


        $this->response($this->response);

    }

    public function getPostReshareList()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage("passPostId");

            $this->response($this->response);

        }


        $postID = $this->input->post('postID');


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");

        $this->response['data']['userList'] = $this->post->getPostReshareUserList($this->userID, $postID);


        $this->response($this->response);

    }

    public function getUserIDByUsername()
    {

        if (!$this->input->post('username') || empty($this->input->post('username'))) {

            $this->response['message'] = "Please Pass Username";

            $this->response($this->response);

        }


        $username = $this->input->post('username');


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $userID = $this->user->getUser(['tu.userID'], ['username' => $username]);

        $this->response['data'] = empty($userID) ? "No User Data Found" : $userID;


        $this->response($this->response);

    }

    public function getAllPostByHashTag()
    {

        if (!$this->input->post('hashtag') || empty($this->input->post('hashtag'))) {

            $this->response['message'] = "Please pass hashtag";

            $this->response($this->response);

        }


        $hashtag = $this->input->post('hashtag');


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');


        $usersDetails = $this->CommonModel->get_row(TBL_USER, ["isVeteran"], ["userID" => $this->userID]);

        // echo $usersDetails['isVeteran'];

        //3 for all post search normal also veteran

        $searchFrom = ($usersDetails['isVeteran'] == 0) ? $usersDetails['isVeteran'] : 3;

        $postsdata = $this->post->getSearchPosts($this->userID, $searchFrom, $hashtag);


        foreach ($postsdata as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }


        $this->response['data']['allPosts'] = $postsdata;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getPostRecordsSuccess');

        $this->response($this->response);

    }

    public function setPostPinToTop()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage("passPostId");

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = getMessage('passstatus');

            $this->response($this->response);

        }


        $postID = $this->input->post('postID');

        $status = $this->input->post('status');


        if (!$this->CommonModel->checkExists(TBL_POST, ['postID' => $postID, "isActive" => 1, "isDelete" => 0])) {

            $this->response['message'] = "Post Not Exists";

            $this->response($this->response);

        }


        $this->CommonModel->update(TBL_POST, ['isTop' => 0], ['userID' => $this->userID, 'isActive' => 1, 'isDelete' => 0]);


        if ($status) {

            if ($this->CommonModel->update(TBL_POST, ['isTop' => 1], ['userID' => $this->userID, 'postID' => $postID, 'isActive' => 1, 'isDelete' => 0, 'postType' => 0]) > 0) {

                $this->response['code'] = 100;

                $this->response['message'] = "Post SuccessFully Pin.!";

            } else {

                $this->response['message'] = "Post Pin Error.!";

            }

        } else {

            $this->response['code'] = 100;

            $this->response['message'] = "Post Successfully Unpin.!";

        }


        $this->response($this->response);

    }

    //Created By: Hetal Patel

    public function deleteUser()
    {

        $last_comment_id = $this->CommonModel->set_delete(TBL_USER, ['userID' => $this->input->post('userID')]);


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('UserDeleteSuccess');

        $this->response($this->response);

    }


    private function sendNotification($notificationData)
    {
        $notificationIns = array();

        $userNotificationData = array();

        $userData = array();

        $userID = 0;

        $postShareID = 0;


        if (strtoupper($notificationData['type']) == "POST") {

            $notificationIns['type'] = 0;

            //Share post user data

            if (isset($notificationData['postShareID']) && $notificationData['postShareID'] > 0) {

                $userID = $this->CommonModel->get_row(TBL_POST_SHARE, ["userID"], ["postShareID" => $notificationData['postShareID']]);

                $postShareID = $notificationData['postShareID'];

            } //mention Users in post send notification

            else if (isset($notificationData['mentionUserID']) && $notificationData['mentionUserID'] > 0) {

                $userID = [$notificationData['mentionUserID']];

            } else if (isset($notificationData['subscriberID']) && $notificationData['subscriberID'] > 0) {

                $userID = [$notificationData['subscriberID']];

                $postShareID = $notificationData['postID'];

            } else if (isset($notificationData['postCommentID']) && $notificationData['postCommentID'] > 0) {
                $uID = $this->CommonModel->get_row(TBL_POST_COMMENT, ["userID"], ["postCommentID" => $notificationData['postCommentID']]);
//                $res = $this->isFollowing($uID['userID'], $notificationData['userID']);
//                if ($res)
                $userID = ($uID['userID'] == $notificationData['userID']) ? 0 : $uID;
            } else if (isset($notificationData['parentCommentID']) && $notificationData['parentCommentID'] > 0) {
                $uID = $this->CommonModel->get_row(TBL_POST_COMMENT, ["userID"], ["postCommentID" => $notificationData['parentCommentID']]);
                $userID = ($uID['userID'] == $notificationData['userID']) ? 0 : $uID;
            } else {
                $userID = $this->CommonModel->get_row(TBL_POST, ["userID"], ["postID" => $notificationData['targetID']]);

            }

            if (isset($notificationData['tagUser']) && !empty($notificationData['tagUser'])) {
                $uID = $this->CommonModel->get_row(TBL_USER, ["userID"], ["username" => $notificationData['tagUser']]);
                $userID = ($uID['userID'] == $notificationData['userID']) ? 0 : $uID;
            }
        } else if (strtoupper($notificationData['type']) == "PROFILE" || strtoupper($notificationData['type']) == "notification") {

            $notificationIns['type'] = 1;

            $userID = [$notificationData['sendNotificationUserID']];

        } else if (strtoupper($notificationData['type']) == "CHAT") {

            if ($notificationData['mesgType'] == 0) {

                $notificationIns['type'] = 2;

                $userID = [$notificationData['targetID']];

            } else {

                $notificationIns['type'] = 3;

                $userID = $this->CommonModel->get_all(TBL_USER_GROUP, ['userID'], ["groupID" => $notificationData['targetID']]);

            }

        }

        // else if(strtoupper($notificationData['type']) == "notification"){

        //     $notificationIns['type'] = 4;

        // }


        $notificationIns['title'] = $notificationData['title'];

        $notificationIns['text'] = $notificationData['desc'];

        $notificationIns['image'] = isset($notificationData['image']) ? $notificationData['image'] : "defaultNotificationImage.png";

        $notificationIns['targetID'] = $notificationData['targetID'];


        // INSERT AFTER NOTIFICATION MASTER SEND

        if (strtoupper($notificationData['type']) != "CHAT") {

            $notificationID = $this->CommonModel->insert(TBL_NOTIFICATION, $notificationIns);

        } else {

            $notificationID = 1;

        }


        if (!empty($notificationData['tagUser'])) {
        if (!empty($userID)) {

            foreach ($userID as $user) {

                if (strtoupper($notificationData['type']) == "CHAT" && $notificationData['mesgType'] == 1) {

                    $user = $user['userID'];

                    }
                    //Check notification Setting
                    $userExists = $this->CommonModel->get_row(TBL_SETTINGS, 'settingID,notificationStatus', ['userID' => $user, 'isActive' => 1, 'isDelete' => 0]);
                    $notificationStatus = $userExists['notificationStatus'] ? $userExists['notificationStatus'] : "1";
                    //echo $notificationStatus;die;
                    $userData = $this->CommonModel->get_row(TBL_USER, ["deviceToken", "deviceType"], ["userID" => $user]);
                    if (strtoupper($notificationData['type']) != "CHAT" && $notificationStatus == 1) {
                        $this->CommonModel->insert(TBL_USER_NOTIFICATION, ["n_id" => $notificationID, "userID" => $user]);
                }

                    if ($userData['deviceType'] == 1 && strlen($userData['deviceToken']) == 152 && $notificationStatus == 1) {
                        return $this->CommonModel->sendAndroidNotification(array($userData['deviceToken']), $notificationIns['text'], 100, $notificationIns['title'], $notificationID, $notificationIns['type'], $notificationIns['targetID'], $postShareID);
                    } else if ($userData['deviceType'] == 2 && strlen($userData['deviceToken']) == 64 && $notificationStatus == 1) {
                        return $this->CommonModel->sendAppleNotification(array($userData['deviceToken']), $notificationIns['text'], 100, $notificationIns['type'], $notificationIns['title'], $notificationID, $notificationIns['targetID'], $postShareID);
                    }
                }
            }
        }
        if (!empty($userID)) {
            foreach ($userID as $user) {
                if (strtoupper($notificationData['type']) == "CHAT" && $notificationData['mesgType'] == 1) {
                    $user = $user['userID'];
                }
                //Check notification Setting

                $userExists = $this->CommonModel->get_row(TBL_SETTINGS, 'settingID,notificationStatus', ['userID' => $user, 'isActive' => 1, 'isDelete' => 0]);

                $notificationStatus = $userExists['notificationStatus'] ? $userExists['notificationStatus'] : "1";

                //echo $notificationStatus;die;

                $userData = $this->CommonModel->get_row(TBL_USER, ["deviceToken", "deviceType"], ["userID" => $user]);

                if (strtoupper($notificationData['type']) != "CHAT" && $notificationStatus == 1) {

                    $this->CommonModel->insert(TBL_USER_NOTIFICATION, ["n_id" => $notificationID, "userID" => $user]);

                }

                if ($userData['deviceType'] == 1 && strlen($userData['deviceToken']) == 152 && $notificationStatus == 1) {

                    return $this->CommonModel->sendAndroidNotification(array($userData['deviceToken']), $notificationIns['text'], 100, $notificationIns['title'], $notificationID, $notificationIns['type'], $notificationIns['targetID'], $postShareID);

                } else if ($userData['deviceType'] == 2 && strlen($userData['deviceToken']) == 64 && $notificationStatus == 1) {

                    return $this->CommonModel->sendAppleNotification(array($userData['deviceToken']), $notificationIns['text'], 100, $notificationIns['type'], $notificationIns['title'], $notificationID, $notificationIns['targetID'], $postShareID);
                }

            }
         }
     }

    public function isFollowing($userId, $followerId)
    {
        $result = $this->CommonModel->isfollowing(TBL_USER_FOLLOWER, ['userID' => $userId, 'followerID' => $followerId, 'status' => '1']);
        if (!empty($result)) {
            return true;
        }
        return false;
    }


    public function getNotificationList()
    {
        $this->response['code'] = 100;
        $this->response['message'] = "Get Record Successfully";
        // $this->response['data']['notificationList'] = $this->notification->getNotificationList($this->userID);
        $notificationList = $this->notification->getNotificationList($this->userID);
        if(!empty($notificationList)){
            foreach($notificationList as &$notification){
                if($notification['image']=='defaultNotificationImage.png'){
                    $notificationImg = base_url().'upload/notification/'.$notification['image'];
                }else{
                    if(strpos($notification['image'], 'https://') !== false){
                        $notificationImg = $notification['image'];
                    }else{
                        $notificationImg = base_url().'upload/userprofile/'.$notification['image'];
                    }
                }
                $notifyText = isset($notification['text']) ? $this->emoji->Decode($notification['text']):'';
                $notifyTime = (isset($notification['createdDateTime'])) ? $notification['createdDateTime'] : "";
                $senderudata = $this->user->getUserNamerow($notification['senderID']);
                $senderName = (isset($senderudata['username']))?$senderudata['username']:"";
                $extraUserData = array();
                if(isset($notification['senderID']) && $notification['senderID']!=""){
                    $extraUserData[] = array(
                        'userID' => $notification['senderID'],
                        'username' => $senderName,
                        'profileImage' => $notificationImg, 
                    );
                }
                if(isset($notification['totalCount']) && $notification['totalCount']>1){
                    $filterData = array(
                        'userID'=>$notification['userID'],
                        'otherID'=>$notification['otherID'],
                        'notifyType'=>$notification['notifyType']
                    );
                    $getRelatedData = $this->notification->getRelatedNotificationList($filterData);
                    if(!empty($getRelatedData)){
                        // echo "<pre>"; print_r($getRelatedData); die();
                        $totalusercount = (count($getRelatedData) - 1);
                        $notifyText = (isset($getRelatedData[0]['text']))?$this->emoji->Decode($getRelatedData[0]['text']):$notifyText;
                        $notifyText = str_replace("has","and ".$totalusercount." others",$notifyText);
                        if(strpos($notificationImg, 'https://') !== false){
                            $notificationImg = (isset($getRelatedData[0]['image']))?$getRelatedData[0]['image']:$notificationImg;
                        }else{
                            $notificationImg = (isset($getRelatedData[0]['image']))?base_url().'upload/userprofile/'.$getRelatedData[0]['image']:$notificationImg;
                        }
                        $notifyTime = (isset($getRelatedData[0]['createdDateTime']))?$getRelatedData[0]['createdDateTime']:$notifyTime;
                        foreach($getRelatedData as $key=>$rel){
                            $anotherUdata = $this->CommonModel->get_row(TBL_USER, ["userID", "username", "name","profileImage"], ["userID" => $rel['senderID']]);
                            if($rel['senderID']!=$getRelatedData[0]['senderID']){
                                $extraUserData[] = array(
                                    'userID' => $rel['senderID'],
                                    'username' => $rel['username'],
                                    'profileImage' => (isset($anotherUdata['profileImage']))?base_url().'upload/userprofile/'.$anotherUdata['profileImage']:"",
                                    // 'profileLink' => (isset($anotherUdata['username']))?base_url('time-line/'.$anotherUdata['username']):"javascript:void(0)",
                                );
                            }
                            if($key > 4){ break; }
                        }
                    }
                }
                $notification['text'] = $notifyText;
                $notification['createdDateTime'] = $notifyTime;
                $notification['image'] = $notificationImg;
                $notification['extraUserData'] = $extraUserData;
            }
        }
        $this->response['data']['notificationList'] = $notificationList;

        /*$blockUsers = array();
        $allblockList = $this->user->getUserBlockedList($this->userID);
        if(!empty($allblockList)){
            foreach($allblockList as $user){
                if(isset($user['userID']) && $user['userID']!=""){
                    $blockUsers[] = $user['userID'];
                }
            }
        }
        $userID2 = "106";
        $checkBlockUserData = $this->CommonModel->get_row('tblblockuser',['userID','blockUserID'],["userID"=> $userID2, "blockUserID"=>$this->userID]);
        if(empty($checkBlockUserData) && !in_array($userID2,$blockUsers)){
            echo "not block";
        }else{
            echo "block";
        }
        echo "<pre>"; print_r($blockUsers); die();*/
        //for read notification
        $arrUpdate = array();
        $arrUpdate['isRead'] = 1;
        $this->CommonModel->update('tblusernotification', $arrUpdate, ['userID' => $this->userID, 'isRead' => 0, 'isActive' => 1, 'isDelete' => 0]);
        $this->response($this->response);
    }


    public function removeMemberFromGroupChat()
    {

        if (!$this->input->post('groupID') || empty($this->input->post('groupID'))) {

            $this->response['message'] = getMessage('passgroupId');

            $this->response($this->response);

        }


        $remove_member = $this->CommonModel->set_delete('tblusergroup', array('userID' => $this->input->post('userID'), 'groupID' => $this->input->post('groupID')));


        if (empty($remove_member)) {

            $this->response['message'] = getMessage('notRemoveMember');

            $this->response($this->response);

        }

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('MembersRemovedSuccess');

        $this->response($this->response);

    }

    public function getTredingHashTag()
    {

        $responseData = array();

        // $usersDetails = $this->CommonModel->get_row(TBL_USER,["isVeteran"],["userID" => $this->userID]);

        $postsdata = $this->post->getPosts($this->userID, 0);

        if (!empty($postsdata)) {

            $hashtagArray = array();

            foreach ($postsdata as &$value) {

                preg_match_all('/#(\w+)/', $value['text'], $hashtagMatches);

                // echo "<pre>"; print_r($hashtagMatches);

                if (!empty($hashtagMatches[0])) {

                    foreach ($hashtagMatches[0] as $match) {

                        $hashtagArray[] = $match;

                    }

                }

            }

            // echo "<pre>"; print_r($hashtagArray); die();

            $newHashtagsArray = array_count_values($hashtagArray);

            arsort($newHashtagsArray);

            $hashtags = array_keys($newHashtagsArray);

            if (!empty($hashtags)) {

                foreach ($hashtags as $value) {

                    $responseData[]['hashtag'] = $value;

                }

            }

        }

        if (empty($responseData)) {

            $this->response['message'] = getMessage('notFoundPosts');

            $this->response($this->response);

        }


        $this->response['data'] = $responseData;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getPostRecordsSuccess');

        $this->response($this->response);

    }


    public function subscribeUserPost()
    {

        if (!$this->input->post('followerID') || empty($this->input->post('followerID'))) {

            $this->response['message'] = getMessage('passfollowerID');

            $this->response($this->response);

        }


        if (($this->input->post('status') && !empty($this->input->post('status'))) && !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = getMessage('passstatus');

            $this->response($this->response);

        }


        $followerID = $this->input->post('followerID');

        $status = $this->input->post('status');


        if ($followerID == $this->userID) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("UserOwnSubscribeError");

            $this->response($this->response);

        }


        if (!$this->CommonModel->checkExists(TBL_USER, ['userID' => $followerID, "isActive" => 1, "isDelete" => 0])) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("FolloweIDError");

            $this->response($this->response);

        }


        $alreadyExistFollowUser = $this->CommonModel->get_row(TBL_POST_SUBSCRIBE, ['postNotifyID'], ['userID' => $this->userID, 'followerID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);


        /* 1 = subscribe or 0 = unsubscribe*/

        if ($status == 0) {

            if (empty($alreadyExistFollowUser)) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("NotSubscribeError");

            } else {

                $arrUpdate = array();

                $arrUpdate['status'] = $this->input->post('status');

                $postNotifyID = $this->CommonModel->update(TBL_POST_SUBSCRIBE, $arrUpdate, ['postNotifyID' => $alreadyExistFollowUser['postNotifyID']]);

                if (empty($postNotifyID)) {

                    $this->response['code'] = 101;

                    $this->response['message'] = getMessage("UnsubscribeError");

                } else {

                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("UnsubscribeSuccess");

                }

            }

        } else {

            if (isset($alreadyExistFollowUser['status']) && $alreadyExistFollowUser['status'] == 1) {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage('subscribeAlready');

            } else {

                if ($this->CommonModel->checkExists(TBL_BLOCKUSER, ['userID' => $this->userID, 'blockUserID' => $followerID, "isActive" => 1, "isDelete" => 0])) {

                    $this->response['message'] = getMessage("userisBlock");

                    $this->response($this->response);

                }

                if (!empty($alreadyExistFollowUser)) {

                    $arrUpdate = array();

                    $arrUpdate['status'] = $this->input->post('status');

                    $postNotifyID = $this->CommonModel->update(TBL_POST_SUBSCRIBE, $arrUpdate, ['postNotifyID' => $alreadyExistFollowUser['postNotifyID']]);


                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("subscribeSuccess");

                } else {

                    $arrInsert = array();

                    $arrInsert['userID'] = $this->userID;

                    $arrInsert['followerID'] = $followerID;

                    $arrInsert['status'] = $this->input->post('status');

                    if ($this->CommonModel->insert(TBL_POST_SUBSCRIBE, $arrInsert) > 0) {

                        $this->response['code'] = 100;

                        $this->response['message'] = getMessage("subscribeSuccess");

                    } else {

                        $this->response['code'] = 101;

                        $this->response['message'] = getMessage("subscribeError");

                    }

                }

            }

        }

        $this->response($this->response);

    }

    //for save poll post answer

    public function savePollAnswer()
    {

        $this->store_logs();

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        if (!$this->input->post('optionID') || empty($this->input->post('optionID'))) {

            $this->response['message'] = getMessage('passOptionID');

            $this->response($this->response);

        }


        $postID = (isset($_POST['postID'])) ? $_POST['postID'] : "";

        $checkAnswerExist = $this->CommonModel->get_row(TBL_POST_POLL_ANSWER, ['*'], ['userID' => $this->userID, 'postID' => $postID, 'isDelete' => 0]);

        if (!empty($checkAnswerExist)) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage('pollAnsExist');

            $this->response($this->response);

        }

        // echo $_POST['text']; die();

        $saveData['userID'] = $this->userID;

        $saveData['postID'] = $postID;

        $saveData['userID'] = $this->userID;

        $saveData['optionID'] = (isset($_POST['optionID'])) ? $_POST['optionID'] : "";

        $saveData['answer'] = (isset($_POST['answer'])) ? $_POST['answer'] : "";


        $answerID = $this->CommonModel->insert(TBL_POST_POLL_ANSWER, $saveData);


        $responseData = array();

        $pollPostOptions = array();

        $pollOptions = $this->CommonModel->get_all(TBL_POST_POLL_OPTIONS, ['*'], ["postID" => $postID, "isActive" => 1, "isDelete" => 0]);

        if (!empty($pollOptions)) {

            foreach ($pollOptions as $opt) {

                $avgAnswer = $this->post->getPollOptionAvgAns($postID, $opt['optionID']);

                $res = array();

                $res['optionID'] = $opt['optionID'];

                $res['option'] = $opt['text'];

                $res['avgAnswer'] = $avgAnswer;

                $pollPostOptions[] = $res;

            }

        }

        $responseData['pollPostOptions'] = (!empty($pollPostOptions)) ? $pollPostOptions : null;


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('pollAnsSuccess');

        $this->response['data'] = $responseData;

        $this->response($this->response);

    }


    public function muteUnmuteUser()
    {

        if (!$this->input->post('muteUserID') || empty($this->input->post('muteUserID'))) {

            $this->response['message'] = getMessage("PassMuteUserID");

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = getMessage("passstatus");

            $this->response($this->response);

        }


        $status = $this->input->post("status");

        $arrMuteInsert['userID'] = $this->userID;
        $arrMuteInsert['muteUserID'] = $this->input->post('muteUserID');


        if ($arrMuteInsert['muteUserID'] == $arrMuteInsert['userID']) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("UserMuteOwnError");

            $this->response($this->response);

        }


        if (!$this->CommonModel->checkExists(TBL_USER, ['userID' => $arrMuteInsert['muteUserID'], "isActive" => 1, "isDelete" => 0])) {

            $this->response['code'] = 101;

            $this->response['message'] = getMessage("MuteIDError");

            $this->response($this->response);

        }


        $alreadyExistsMuteUser = $this->CommonModel->getMuteUser(TBL_MUTEUSER, ['muteID', 'isActive'], ['userID' => $this->userID, 'muteUserID' => $arrMuteInsert['muteUserID'], 'isDelete' => 0]);
        if (!empty($alreadyExistsMuteUser)) {
            $status = ($alreadyExistsMuteUser['isActive'] == '1') ? "0" : "1";
            $userFollowerID = $this->CommonModel->update(TBL_MUTEUSER, ['isActive' => $status], ['muteID' => $alreadyExistsMuteUser['muteID']]);

                if (empty($userFollowerID)) {

                    $this->response['code'] = 101;

                $this->response['message'] = $status == '0' ? getMessage("UnMuteSaveError") : getMessage("MuteUserIDInsertError");

                } else {

                    $this->response['code'] = 100;

                $this->response['message'] = $status == '0' ? getMessage("UnMuteSaveSuccess") : getMessage("MuteUserIDInsertSuccess");

            }

        } else {

            if ($status !== "0") {
                if ($this->CommonModel->insert(TBL_MUTEUSER, $arrMuteInsert) > 0) {

                    $this->response['code'] = 100;

                    $this->response['message'] = getMessage("MuteUserIDInsertSuccess");

                } else {

                    $this->response['code'] = 101;

                    $this->response['message'] = getMessage("MuteUserIDInsertError");

                }

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("MuteAlreadyNot");

            }

        }


        $this->response($this->response);

    }


    public function likePostComment()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        if (!$this->input->post('postCommentID') || empty($this->input->post('postCommentID'))) {

            $this->response['message'] = getMessage('please pass post comment ID');

            $this->response($this->response);

        }

        $arrLikeInsert['userID'] = $this->userID;

        $arrLikeInsert['postID'] = $this->input->post('postID');

        $arrLikeInsert['postCommentID'] = $this->input->post('postCommentID');

        $arrLikeInsert['postShareID'] = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;

        $arrLikeInsert['isLiked'] = 1;


        $likeResult = $this->CommonModel->get_row_com(TBL_POST_COMMENT_LIKE, ['postcommentLikeID', 'isLiked', 'isActive'], ['userID' => $arrLikeInsert['userID'], 'postID' => $arrLikeInsert['postID'], 'postCommentID' => $arrLikeInsert['postCommentID'], "isLiked" => 1, "isDelete" => 0]);

        if (!empty($likeResult)) {

            if ($likeResult['isActive'] == 1) {

                $isActive = 0;

            }

            if ($likeResult['isActive'] == 0) {

                $isActive = 1;

            }

            if ($this->CommonModel->update(TBL_POST_COMMENT_LIKE, ['isActive' => $isActive], ['postcommentLikeID' => $likeResult['postcommentLikeID']]) > 0) {

                $this->getCommentsByPostID();

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("PostLikeRemoveError");

            }

        } else {

            if ($postcommentLikeID = $this->CommonModel->insert(TBL_POST_COMMENT_LIKE, $arrLikeInsert) > 0) {

                if ($arrLikeInsert['postShareID'] > 0) {
                    $this->sendNotification(array(

                        "type" => "post",

                        "title" => "SayScape",

                        "desc" => ucfirst($this->userName) . " has liked your quoted comment.",

                        "targetID" => $arrLikeInsert['postID'],

                        "postShareID" => $arrLikeInsert['postShareID'],
                        "senderID"=>$this->userID,
                        "notifyType"=>"quoteCommentLike"

                    ));
                } else {
                    $this->sendNotification(array(
                        "type" => "post",
                        "title" => "SayScape",
                        "desc" => ucfirst($this->userName) . " has liked your comment.",
                        "targetID" => $arrLikeInsert['postID'],
                        "postCommentID" => $arrLikeInsert['postCommentID'],
                        "userID" => $arrLikeInsert['userID'],
                        "senderID"=>$this->userID,
                        "notifyType"=>"likePostComment"
                    ));
                }

                $this->getCommentsByPostID();

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("PostLikeError");

            }

        }

        // $comments = $this->post->getComments($this->input->post('postID'));

        // $this->response['data']['comments'] = $comments;

        $this->response($this->response);

    }


    public function dislikePostComment()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        if (!$this->input->post('postCommentID') || empty($this->input->post('postCommentID'))) {

            $this->response['message'] = getMessage('please pass post comment ID');

            $this->response($this->response);

        }

        $arrDisLikeInsert['userID'] = $this->userID;

        $arrDisLikeInsert['postID'] = $_POST['postID'];

        $arrDisLikeInsert['postCommentID'] = $this->input->post('postCommentID');

        $arrDisLikeInsert['postShareID'] = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;

        $arrDisLikeInsert['isLiked'] = 0;


        $dislikeResult = $this->CommonModel->get_row_com(TBL_POST_COMMENT_LIKE, ['postcommentLikeID', 'isLiked', 'isActive'], ['userID' => $arrDisLikeInsert['userID'], 'postID' => $arrDisLikeInsert['postID'], 'postCommentID' => $arrDisLikeInsert['postCommentID'], "isLiked" => 0, "isDelete" => 0]);


        if (!empty($dislikeResult)) {

            if ($dislikeResult['isActive'] == 1) {

                $isActive = 0;

            }

            if ($dislikeResult['isActive'] == 0) {

                $isActive = 1;

            }

            if ($this->CommonModel->update(TBL_POST_COMMENT_LIKE, ['isActive' => $isActive], ['postcommentLikeID' => $dislikeResult['postcommentLikeID']]) > 0) {

                $this->getCommentsByPostID();

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("PostDislikeRemoveError");

            }

        } else {

            if ($this->CommonModel->insert(TBL_POST_COMMENT_LIKE, $arrDisLikeInsert) > 0) {

                if ($arrDisLikeInsert['postShareID'] > 0) {

                    $this->sendNotification(array(

                        "type" => "post",

                        "title" => "SayScape",

                        "desc" => ucfirst($this->userName) . " has disliked your quoted post comment.",

                        "targetID" => $arrDisLikeInsert['postID'],

                        "postShareID" => $arrDisLikeInsert['postShareID'],
                        "senderID"=>$this->userID,
                        "notifyType"=>"quoteCommentDislike"

                    ));

                } else {

                    $this->sendNotification(array(

                        "type" => "post",

                        "title" => "SayScape",

                        "desc" => ucfirst($this->userName) . " has disliked your post comment.",

                        "targetID" => $arrDisLikeInsert['postID'],
                        "postCommentID" => $arrDisLikeInsert['postCommentID'],
                        "userID" => $arrDisLikeInsert['userID'],
                        "senderID"=>$this->userID,
                        "notifyType"=>"dislikeComment"

                    ));

                }

                $this->getCommentsByPostID();

            } else {

                $this->response['code'] = 101;

                $this->response['message'] = getMessage("PostDisLikeError");

            }

        }

        $this->response($this->response);

    }

    public function getPostCommentLikeList()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        if (!$this->input->post('postCommentID') || empty($this->input->post('postCommentID'))) {

            $this->response['message'] = "please pass postCommentID";

            $this->response($this->response);

        }

        $postID = $this->input->post("postID");

        $postCommentID = $this->input->post("postCommentID");


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");


        $this->response['data']['userList'] = $this->post->getPostCommentLikeUserList($this->userID, $postID, $postCommentID);

        

        // print_r($this->db->last_query());die();

        $this->response($this->response);

    }

    public function getPostCommentDisLikeList()
    {

        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {

            $this->response['message'] = getMessage('passPostId');

            $this->response($this->response);

        }

        if (!$this->input->post('postCommentID') || empty($this->input->post('postCommentID'))) {

            $this->response['message'] = getMessage('please pass post comment ID');

            $this->response($this->response);

        }

        $postID = $this->input->post("postID");

        $postCommentID = $this->input->post("postCommentID");


        $this->response['code'] = 100;

        $this->response['message'] = getMessage("getRecordSuccess");


        $this->response['data']['userList'] = $this->post->getPostCommentDisLikeUserList($this->userID, $postID, $postCommentID);


        $this->response($this->response);

    }


    public function removeAllPosts()
    {

        if (!isset($_POST['password']) || empty($_POST['password'])) {

            $this->response['message'] = getMessage('passPassword');

            $this->response($this->response);

        }

        $checkUserPassword = $this->CommonModel->get_row(TBL_USER, ['*'], ['userID' => $this->userID, 'password' => md5($_POST['password'])]);

        if (!empty($checkUserPassword)) {


            $arrPostUpdate['isDelete'] = 1;

            $arrPostUpdate['isActive'] = 0;

            $arrPostUpdate['updatedDateTime'] = date('Y-m-d H:i:s');


            $this->CommonModel->update(TBL_POST, $arrPostUpdate, array('userID' => $this->userID));

            $this->CommonModel->update(TBL_POST_SHARE, $arrPostUpdate, array('userID' => $this->userID));

            $this->CommonModel->update(TBL_MEDIA, $arrPostUpdate, array('userID' => $this->userID, "mediaRef" => 0));


            $this->response['code'] = 100;

            $this->response['message'] = getMessage('PostDeleteSuccess');

            $this->response['data'] = $this->getAllPosts();

        } else {

            $this->response['message'] = "Your enter Password is incorrect!";

        }

        $this->response($this->response);

    }


    public function getUserProfileDetail()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);

        }


        $watchID = $this->input->post('wid');


        $userdata['userDetails'] = $this->user->getUserdetails($watchID, $this->userID);

        $userdata['userDetails']['totalFollowing'] = count($this->user->getFollowingList($this->userID, $watchID));

        $userdata['userDetails']['totalFollowers'] = count($this->user->getFollowersList($this->userID, $watchID));


        $userdata['userDetails']['group_list'] = $this->group->getGroupList($watchID);


        //for check user subscribe or not

        $userdata['userDetails']['postSubscribeStatus'] = "0";

        $checkPostSubscribeStatus = $this->CommonModel->get_row(TBL_POST_SUBSCRIBE, ['*'], ['userID' => $this->userID, 'followerID' => $watchID, 'isActive' => 1, 'isDelete' => 0]);

        if (!empty($checkPostSubscribeStatus)) {

            $userdata['userDetails']['postSubscribeStatus'] = $checkPostSubscribeStatus['status'] . "";

        }


        $userdata['userDetails']['privateProfileStatus'] = "0";

        $userdata['userDetails']['hideBirthday'] = "0";

        $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $watchID, 'isActive' => 1, 'isDelete' => 0]);

        if (!empty($settingsData)) {

            $userdata['userDetails']['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";

            $userdata['userDetails']['hideBirthday'] = (isset($settingsData['hideBirthday'])) ? $settingsData['hideBirthday'] . "" : "0";

        }

        //$userdata['comments_list'] = $this->user->getCommentPostByUser($this->userID);

        if (empty($userdata)) {

            $this->response['message'] = getMessage('NoData');

            $this->response($this->response);

        }

        $this->response['data'] = $userdata;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }


    public function getProfileMediaPostList()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);

        }


        $watchID = $this->input->post('wid');

        $userdata = array();

        $userdata['media_post_list'] = array();
        $post_list = $this->user->getPostByUser($this->userID, $watchID);

        // echo "<pre>"; print_r($post_list); die();

        if (!empty($post_list)) {

            foreach ($post_list as &$value) {
                $value = $this->post->getPostResponseData($value, $this->userID,'','otherType');
                if(isset($value['media_post_list']) && !empty($value['media_post_list'])){
                    $userdata['media_post_list'][] = $value['media_post_list'];
                }
            }

        }


        $limit = ONE_PAGE_LIMIT;

        $pageNumber = $this->input->post('pageNumber') ? $this->input->post('pageNumber') : 0;

        if ($pageNumber == 1) {

            $offset = 0;

        } else {

            $offset = ($pageNumber * $limit) - $limit;

        }

        $allResultList = $userdata['media_post_list'];

        $Pages = count($allResultList) / $limit;

        $totalPage = ceil($Pages);

        $userdata['media_post_list'] = array_slice($allResultList, $offset, $limit);


        if (empty($userdata)) {

            $this->response['message'] = getMessage('NoData');

            $this->response($this->response);

        }

        $this->response['data']['userDetails'] = $userdata;

        $this->response['data']['totalPages'] = $totalPage;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }


    public function getProfileTextPostList()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);

        }


        $watchID = $this->input->post('wid');

        $userdata = array();

        $post_list = $this->user->getPostByUser($this->userID, $watchID);
        // echo "<pre>"; print_r($post_list); die();
        $userdata['text_post_list'] = array();

        foreach ($post_list as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID,'','otherType');
            if(isset($value['text_post_list']) && !empty($value['text_post_list'])){
                $userdata['text_post_list'][] = $value['text_post_list'];
            }
        }
        $limit = ONE_PAGE_LIMIT;

        $pageNumber = $this->input->post('pageNumber') ? $this->input->post('pageNumber') : 0;

        if ($pageNumber == 1) {

            $offset = 0;

        } else {

            $offset = ($pageNumber * $limit) - $limit;

        }

        $allResultList = $userdata['text_post_list'];

        $Pages = count($allResultList) / $limit;

        $totalPage = ceil($Pages);

        $userdata['text_post_list'] = array_slice($allResultList, $offset, $limit);


        if (empty($userdata)) {

            $this->response['message'] = getMessage('NoData');

            $this->response($this->response);

        }

        $this->response['data']['userDetails'] = $userdata;

        $this->response['data']['totalPages'] = $totalPage;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }


    public function getProfileCommentPostList()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);

        }


        $watchID = $this->input->post('wid');

        $userdata = array();

        $pageNumber = $this->input->post('pageNumber') ? $this->input->post('pageNumber') : "ALL";

        $userdata['comments_list'] = $this->user->getCommentPostByUser($watchID, FALSE, $pageNumber);

        foreach ($userdata['comments_list'] as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }

        $limit = ONE_PAGE_LIMIT;

        $pageNumber = $this->input->post('pageNumber') ? $this->input->post('pageNumber') : 0;

        if ($pageNumber == 1) {

            $offset = 0;

        } else {

            $offset = ($pageNumber * $limit) - $limit;

        }

        $allResultList = $userdata['comments_list'];

        $Pages = count($allResultList) / $limit;

        $totalPage = ceil($Pages);

        $userdata['comments_list'] = array_slice($allResultList, $offset, $limit);


        if (empty($userdata)) {

            $this->response['message'] = getMessage('NoData');

            $this->response($this->response);

        }

        $this->response['data']['userDetails'] = $userdata;

        $this->response['data']['totalPages'] = $totalPage;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }


    public function getProfileLikePostList()
    {

        if (!$this->input->post('wid') || empty($this->input->post('wid'))) {

            $this->response['message'] = getMessage('PassWatchID');

            $this->response($this->response);

        }


        $watchID = $this->input->post('wid');

        $userdata = array();


        $userdata['like_post_list'] = $this->user->getLikedPostByUser($watchID);

        foreach ($userdata['like_post_list'] as &$value) {
            $value = $this->post->getPostResponseData($value, $this->userID);
        }


        $limit = ONE_PAGE_LIMIT;

        $pageNumber = $this->input->post('pageNumber') ? $this->input->post('pageNumber') : 0;

        if ($pageNumber == 1) {

            $offset = 0;

        } else {

            $offset = ($pageNumber * $limit) - $limit;

        }

        $allResultList = $userdata['like_post_list'];

        $Pages = count($allResultList) / $limit;

        $totalPage = ceil($Pages);

        $userdata['like_post_list'] = array_slice($allResultList, $offset, $limit);


        if (empty($userdata)) {

            $this->response['message'] = getMessage('NoData');

            $this->response($this->response);

        }

        $this->response['data']['userDetails'] = $userdata;

        $this->response['data']['totalPages'] = $totalPage;

        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response($this->response);

    }

    //for get count of unread content
    public function getUnreadCounts()
    {

        $unreadMessageCount = 0;

        $unreadNotificationCount = 0;

        $followReqCount = 0;

        $getUnreadMessageData = $this->CommonModel->get_all('tblmessage', ['*'], ['toUserID' => $this->userID, 'isRead' => 0, 'isActive' => 1, 'isDelete' => 0]);

        if (!empty($getUnreadMessageData)) {

            $unreadMessageCount = count($getUnreadMessageData);

        }

        $getUnreadNotifications = $this->CommonModel->get_all('tblusernotification', ['*'], ['userID' => $this->userID, 'isRead' => 0, 'isActive' => 1, 'isDelete' => 0]);

        if (!empty($getUnreadNotifications)) {

            $unreadNotificationCount = count($getUnreadNotifications);

        }

        $getFollowReqData = $this->CommonModel->get_all('tbluserfollower', ['*'], ['followerID' => $this->userID, 'status' => 4, 'isActive' => 1, 'isDelete' => 0]);

        if (!empty($getFollowReqData)) {

            $followReqCount = count($getFollowReqData);

        }

        $responseData['messageCount'] = (isset($unreadMessageCount)) ? $unreadMessageCount . "" : "0";

        $responseData['notificationCount'] = (isset($unreadNotificationCount)) ? $unreadNotificationCount . "" : "0";

        $responseData['followReqCount'] = (isset($followReqCount)) ? $followReqCount . "" : "0";


        $this->response['code'] = 100;

        $this->response['message'] = getMessage('getRecordSuccess');

        $this->response['data'] = $responseData;

        $this->response($this->response);

    }
    
    // for Unquote post
    public function unquotepost(){
        if (!$this->input->post('postID') || empty($this->input->post('postID'))) {
            $this->response['message'] = getMessage('passPostId');
            $this->response($this->response);
        }
        $postShareData = $this->CommonModel->get_row(TBL_POST_SHARE, ['postShareID','postID','userID'], ['isActive'=>1,'postID' => $_POST['postID'], 'userID'=>$this->userID]);
        if(empty($postShareData)){
            $this->response['message'] = getMessage('notFoundPosts');
            $this->response($this->response);
        }

        $arrPostUpdate['isDelete'] = 1;
        $arrPostUpdate['isActive'] = 0;
        $arrPostUpdate['updatedDateTime'] = date('Y-m-d H:i:s');
        $delete_post = $this->CommonModel->update(TBL_POST_SHARE, $arrPostUpdate, array('postID' => $_POST['postID'], 'userID'=>$this->userID));
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('PostEditSuccess');
        $this->response($this->response);
    }


    // for clear chat history
    public function deleteChatHistory(){
        if (!$this->input->post('toUserID') || empty($this->input->post('toUserID'))) {
            $this->response['message'] = getMessage('passToUserId');
            $this->response($this->response);
        }
        $toUserID = $this->input->post('toUserID');
        $type = (isset($_POST['type']))?$this->input->post('type'):0;
        $lastMsgData = $this->message->getUserLastMessage($this->userID,$toUserID,$type);
        
        if(empty($lastMsgData)){
            $this->response['message'] = getMessage('NoData');
            $this->response($this->response);
        }
        // echo "<pre>"; print_r($lastMsgData); die();
        $lastMsgId = $lastMsgData['messageID'];

        $saveData['userID'] = $this->userID;
        $saveData['toUserID'] = $toUserID;
        $saveData['messageID'] = $lastMsgId;
        $saveData['updatedDateTime'] = date('Y-m-d H:i:s');
        $checkRecordExist = $this->CommonModel->get_row(TBL_MESSAGE_CLEAR, ['*'], ['isActive'=>1,'userID' => $this->userID, 'toUserID'=>$toUserID]);
        if(!empty($checkRecordExist)){
            $this->CommonModel->update(TBL_MESSAGE_CLEAR, $saveData, array('userID' => $this->userID, 'clearID'=>$checkRecordExist['clearID']));
        }else{
            $saveData['createdDateTime'] = date('Y-m-d H:i:s');
            $this->CommonModel->insert(TBL_MESSAGE_CLEAR, $saveData);
        }
        $this->response['code'] = 100;
        $this->response['message'] = getMessage('DeleteSuccess');
        $this->response($this->response);
    }


}