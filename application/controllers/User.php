<?php


class User extends MY_Controller
{
    //private $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PostModel', 'post');
        $this->load->model('LoginModel', 'login');
        $this->load->model('UserModel', 'user');
        $this->load->model('GroupModel', 'group');
        $this->load->model('CommonModel');
        $this->load->model('MessageModel', 'message');
        $this->load->model('NotificationModel', 'notification');
        $this->load->library('emoji');
    }
    //User Login
    public function login(){
        if ($this->session->userdata('userID')) {
            redirect(base_url());
        }
        if ($this->input->post('verify')) {
            $userID = $this->input->post('userID');
            $otp = $this->input->post('otp');
            $user_session_data = $this->CommonModel->get_row('tbluser', '*', ['userID'=>$userID,'otp'=>$otp]);
            if (!empty($user_session_data)) {
                $updateData = array();
                $updateData['lastLogin'] = date('Y-m-d H:i:s');
                $updateData['onlineTime'] = time();
                $updateData['isVerified'] = 1;
                $this->CommonModel->update(TBL_USER, $updateData, ["userID" => $userID]);
                
                $user_session_data['plusAccount'] = $this->user->checkUserPlusAccount($userID);
                $this->session->set_userdata($user_session_data);
                //array_push($multiuserID, $userID);
                $multiuserID[] = $userID;
                $this->session->set_userdata('multiuserID',$multiuserID);
                redirect(base_url());
            }else{
                $data['userID'] = $userID;
                $this->session->set_flashdata('error_msg', 'invalid Otp.');
            }
        }

        if ($this->input->post('login')) {
            $user_data['email'] = $this->input->post('email');
            $user_data['password'] = md5($this->input->post('password'));
            $checkEmail = $this->CommonModel->get_row('tbluser', '*', $user_data);
            // echo "<pre>"; print_r($checkEmail); die();
            if(empty($checkEmail)){
                $username_data['username'] = $this->input->post('email');
                $username_data['password'] = md5($this->input->post('password'));
                $user_session_data = $this->CommonModel->get_row('tbluser', '*', $username_data);
            }else{
                $user_session_data = $checkEmail;
            }
            
            if (!empty($user_session_data)) {
                    $userID = $user_session_data['userID'];
                if(!empty($user_session_data['isVerified'])){
                    $updateData = array();
                    $updateData['lastLogin'] = date('Y-m-d H:i:s');
                    $updateData['onlineTime'] = time();
                    $this->CommonModel->update(TBL_USER, $updateData, ["userID" => $userID]);

                    $user_session_data['plusAccount'] = $this->user->checkUserPlusAccount($userID);
                    $this->session->set_userdata($user_session_data);
                    $multiuserID[] = $userID;
                    $this->session->set_userdata('multiuserID',$multiuserID);
                    redirect(base_url());
                }else{
                    $data['userID'] = $userID;
                }
            } else {
                $this->session->set_flashdata('error_msg', 'invalid Username & Password.');
            }
        }
        $data['title'] = "Login";
        $data['view'] = 'front/layout/login';
        $this->renderPartial($data);
    }

    //User register
    public function register(){
        if ($this->session->userdata('userID')) {
            redirect(base_url());
        }
        if ($this->input->post('register')) {
            if (!preg_match("/^[a-zA-Z0-9_]+$/", $this->input->post('username'))) {
                $this->session->set_flashdata('success_msg', getMessage('usernameInvalid'));
                redirect(base_url('register'));
            }
            //Validation of set minimum 3 maximum 16 :Hetal Patel
            $lenValidation = $this->validStrLen($this->input->post('username'), 3, 16);
            if ($lenValidation != 'true') {
                $this->session->set_flashdata('success_msg', $lenValidation);
                redirect(base_url('register'));
            }

            if ($this->CommonModel->checkExists(TBL_USER, ['username' => $this->input->post('username'), "isActive" => 1, "isDelete" => 0])) {
                $this->session->set_flashdata('success_msg', getMessage('usernameExist'));
                redirect(base_url('register'));
            }
            $otp = $this->rand_code(4);
            $userData = array(
                'name' => $this->input->post('name'),
                'deviceType' => 3,
                'otp' => $otp,
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password'))
            );
            if (!empty($_FILES['picture']['name'])) {
                $config['upload_path'] = './upload/userprofile/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('picture')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $fileData = array('upload_data' => $this->upload->data());
                    $userData['profileImage'] = $fileData["upload_data"]["file_name"];
                }
            }
            $result = $this->login->insert('tbluser', $userData);

            /*Email Template start*/

            $html = file_get_contents(base_url() . "emailTemplate/welcome.php");

            $emailHeader = file_get_contents("emailTemplate/emailHeader.php");

            $emailFooter = file_get_contents("emailTemplate/emailFooter.php");

            $replaceOn = ["{{OTP}}", "{{base_url}}", "{{emailHeader}}", "{{emailFooter}}"];

            $replaceWith = [$otp, base_url(), $emailHeader, $emailFooter];

            $messageHTML = str_replace($replaceOn, $replaceWith, $html);

            /*Email Template end*/
            $subject = "Welcome to Say Scape Getting Started";
            $mail_end = $this->CommonModel->send_mail($userData['email'], $subject, $messageHTML);

            $this->session->set_flashdata('success_msg', 'Register Successfuly.');
            redirect(base_url('login'));
        }
        $data['title'] = "register";
        $data['view'] = 'front/register';
        $this->renderPartial($data);
    }

    //User Logout
    public function logout(){
        $this->session->sess_destroy();
        // $this->session->unset_userdata('userID');
        redirect(site_url('login'));
    }

    //User Edit Profile
    public function editProfile(){
        $this->data['title'] = "Edit Profile";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        if ($this->input->post('btnSubmitProfile')) {
            // echo "<pre>"; print_r($_POST); die();
            $about = ($this->input->post('about'))?$this->input->post('about'):"";
            if($about!=""){
                $about = $this->emoji->Encode(trim($this->input->post('about')));
                $about = nl2br($about);
                $about = trim($about);
            }
            $arrUserUpdate = array(
                'username' => $this->input->post('username'),
                'name' => ($this->input->post('name'))?$this->emoji->Encode($this->input->post('name')):"",
                'email' => $this->input->post('email'),
                'location' => $this->input->post('location'),
                'dob' => $this->input->post('dob'),
                'website' => ($this->input->post('website'))?$this->input->post('website'):"",
                'phone' => ($this->input->post('phone'))?$this->input->post('phone'):"",
                'about' => $about
            );
            if (!empty($_FILES['profileImage']['name'])) {
                $config['upload_path'] = './upload/userprofile/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('profileImage')) {
                    $fileData = array('upload_data' => $this->upload->data());
                    $arrUserUpdate['profileImage'] = $fileData["upload_data"]["file_name"];
                }
            }
            if (!empty($_FILES['coverImage']['name'])) {
                $config['upload_path'] = './upload/usercover/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('coverImage')) {
                    $fileData = array('upload_data' => $this->upload->data());
                    $arrUserUpdate['coverImage'] = $fileData["upload_data"]["file_name"];
                }
            }
            $this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $userID]);
            redirect(base_url('edit-profile'));
        }
        if ($this->input->post('btnSubmitSetting')) {
            
            $arrSettings['notificationStatus'] = ($this->input->post('notificationStatus'))?$this->input->post('notificationStatus'):0;
            $arrSettings['privateMessageStatus'] = ($this->input->post('privateMessageStatus'))?$this->input->post('privateMessageStatus'):0;
            $arrSettings['blacklistKeyword'] = ($this->input->post('blacklistKeyword')) ? $this->input->post('blacklistKeyword'):"";
            $arrSettings['privateProfileStatus'] = ($this->input->post('privateProfileStatus')) ? $this->input->post('privateProfileStatus'):0;
            $arrSettings['hideBirthday'] = ($this->input->post('hideBirthday') != "") ? $this->input->post('hideBirthday'):0;
            $arrSettings['isDarkmode'] = ($this->input->post('isDarkmode') != "") ? $this->input->post('isDarkmode'):0;
            $arrSettings['userID'] = $userID;
            $arrSettings['updatedDateTime'] = date('Y-m-d H:i:s');
            $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
            if(empty($userSetting)){
                $arrSettings['createdDateTime'] = date('Y-m-d H:i:s');
                $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);
            }else{
                $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userSetting['settingID']]);
            }
        }
        if ($this->input->post('btnthemechange')) {
            $arrSettings['themeColor'] = ($this->input->post('themecolor') != "") ? $this->input->post('themecolor'):'';
            $arrSettings['userID'] = $userID;
            $arrSettings['updatedDateTime'] = date('Y-m-d H:i:s');
            $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
            if(empty($userSetting)){
                $arrSettings['createdDateTime'] = date('Y-m-d H:i:s');
                $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);
            }else{
                $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userSetting['settingID']]);
            }
        }
        $data['userDetails'] = $this->user->getUserdetails($userID, $userID);
        $data['aditionalImage'] = $this->CommonModel->get_all(TBL_ADITIONAL_IMG, [], ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        $data['userSetting'] = $userSetting;
        $data['blockuserlist'] = $this->CommonModel->get_all(TBL_BLOCKUSER, [], ['userID' => $userID]);
        $data['totalPostCount'] = count($this->CommonModel->get_all(TBL_POST, [], ['userID' => $userID]));
        //echo "<pre>"; print_r($data['blockuserlist']); die();
        $data['view'] = 'front/edit_profile';
        $this->render($data);
    }

    //Change Password
    public function changePassword(){
        $this->data['title'] = "Change Password";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        if ($this->input->post('change')) {
            $newPassword = md5($this->input->post('newpass'));
            $arrUserUpdate['password'] = $newPassword;
            $this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $userID]);
            redirect(base_url('login'));
        }
        $data['userDetails'] = $this->user->getUserdetails($userID, $userID);
        $data['view'] = 'front/change_password';
        $this->render($data);
    }

    // User About
    public function user_about(){
        $this->data['title'] = "User About";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $data['userDetails'] = $this->user->getUserdetails($userID, $userID);
        $this->load->view('web/about', $data);
    }

    //Notification List
    public function notification(){
        $this->data['title'] = "Notification";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $result = $this->CommonModel->update(TBL_USER_NOTIFICATION, ['isRead' => 1], ['isRead' => 0 , 'userID' => $userID]);
        $data['notificationList'] = $this->notification->getNotificationList($userID);
        // echo '<pre>';print_r($data['notificationList']);die;
        $data['view'] = 'front/notification';
        $this->render($data);
    }

    public function clearNotification(){
        $userID = $this->session->userdata('userID');
        $this->CommonModel->update(TBL_USER_NOTIFICATION, ['isRead' => 1], ['isRead' => 0 , 'userID' => $userID]);
        return true;
    }

    //User Group Member
    public function groupMembers($groupID = ""){
        $this->data['title'] = "Group Members";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $groupDetails = $this->CommonModel->get_row('tblgroup', '*', array('groupID'=>$groupID));
        $data['groupDetails'] = $groupDetails;
        $data['remove'] = ($groupDetails['userID'] == $userID) ? true :'0';
        $data['groupmember'] = $this->group->getMembers($userID, $groupID);
        //echo '<pre>';print_r($data['groupDetails']);die;
        $data['view'] = 'front/group_member';
        $this->render($data);
    }

    // Message Page
    public function message_box($otherUserID=''){
        $this->data['title'] = "Message";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $data['followersName'] = $this->message->getRecentChatList($userID);
       
        if ($otherUserID !='') {
            // $data['checkClearData'] = $this->CommonModel->get_row(TBL_MESSAGE_CLEAR, ['*'], ['isActive'=>1,'userID' => $userID, 'toUserID'=>$otherUserID]);
            $data['otherUserDetails'] = $this->user->getUserdetails($otherUserID, $otherUserID);
            $totalrecord = count($this->message->getUserMessages($userID, $otherUserID,0));
            if($totalrecord != 0){
                $limit = 10;
                $offset =0;
                if($totalrecord > $limit){
                    $offset = $totalrecord - $limit;
                }
                $messageData = $this->message->getUserMessages($userID, $otherUserID,0,$limit,$offset);
                //$qq = $this->db->last_query();
                if(!empty($messageData)){
                    foreach($messageData as &$value){
                        // echo "<pre>"; print_r($value); echo "</pre>";
                        if(isset($value['media_type']) && !empty($value['media_type'])){
                            $msgText = $value['text'];
                        }else{
                            $msgText = $this->message->decryptString($value['text']);
                        }
                        $value['text'] = ($msgText)?$this->emoji->Decode($msgText):"";
                        $value['fromUserPic'] = $this->CommonModel->getUserProfilePic($value['userID']);
                    }
                }
                //echo "<pre>"; print_r($messageData); die();
                $data['messageData'] = $messageData;
            }
            $this->CommonModel->update('tblmessage', array('isRead'=>1), ['toUserID' => $userID, 'fromUserID' => $otherUserID, 'isRead' => 0, 'isActive' => 1, 'isDelete' => 0]);
        }
        $data['view'] = 'front/message_box';
        $this->render($data);
    }

    public function pin_this_post($postID){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        if (!$this->CommonModel->checkExists(TBL_POST, ['postID' => $postID, "isActive" => 1, "isDelete" => 0])) {
            // $this->response['message'] = "Post Not Exists";
            redirect(base_url());
        }
        $result = $this->CommonModel->update(TBL_POST, ['isTop' => 1], ['userID' => $userID, 'postID' => $postID, 'isActive' => 1, 'isDelete' => 0, 'postType' => 0]);
        redirect(base_url());
    }

    public function unpin_this_post($postID){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        if (!$this->CommonModel->checkExists(TBL_POST, ['postID' => $postID, "isActive" => 1, "isDelete" => 0])) {
            redirect(base_url());
        }
        $result = $this->CommonModel->update(TBL_POST, ['isTop' => 0], ['userID' => $userID, 'postID' => $postID, 'isActive' => 1, 'isDelete' => 0]);
        redirect(base_url());
    }

    public function delete_this_post(){
        $postID = (isset($_POST['postID']))?$this->input->post('postID'):"";
        $userID = $this->session->userdata('userID');
        $arrPostUpdate['isDelete'] = 1;
        $arrPostUpdate['isActive'] = 0;
        $arrPostUpdate['updatedDateTime'] = date('Y-m-d H:i:s');
        $delete_post = $this->CommonModel->update(TBL_POST, $arrPostUpdate, array('postID' => $postID));
        $delete_media = $this->CommonModel->update(TBL_MEDIA, $arrPostUpdate, array('comID' => $postID, "mediaRef" => 0));
        if(!empty($delete_post) && !empty($delete_media)){
            echo true;
        }else{
            echo false;
        }
    }

    public function hide_this_post(){
        $userID = $this->session->userdata('userID');
        $postID = (isset($_POST['postID']))?$this->input->post('postID'):"";

        $arrHideUnHideInsert['updatedDateTime'] = date('Y-m-d H:i:s');
        $arrHideUnHideInsert['userID'] = $userID;
        $arrHideUnHideInsert['postID'] = $this->input->post('postID');
        $hide_post = $this->CommonModel->insert(TBL_HIDEPOST, $arrHideUnHideInsert);
        if(!empty($hide_post)){
            echo true;
        }else{
            echo false;
        }
    }

    public function saveReport(){
        $userID = $this->session->userdata('userID');
        $arrReportInsert['userID'] = $userID;
        $reportType = $this->input->post("reporttype");
        $arrReportInsert['reportType'] = $reportType;
        $arrReportInsert['comID'] = $this->input->post('postID');

       $alreadyReported = $this->CommonModel->checkExists(TBL_REPORT, ['userID' => $arrReportInsert['userID'], "reportType" => $arrReportInsert['reportType'], "comID" => $arrReportInsert['comID'], "isActive" => 1, "isDelete" => 0]);
        if(!empty($alreadyReported)){
            echo '1';
        }else {
            $reportID = $this->CommonModel->insert(TBL_REPORT, $arrReportInsert);
            if ($reportType) {
                echo 'hh';
            }
        }
    }
    
    // for update user live status
    public function updateUserLiveStatus(){
        $userID = $this->session->userdata('userID');
        if(!empty($userID)){
            $userOnlineStatus = $this->user->userOnlineStatus($userID);
            if($userOnlineStatus==1){
                $updateData = array();
                $updateData['onlineTime'] = time();
                $this->CommonModel->update(TBL_USER, $updateData, ["userID" => $userID]);
            }
            echo $userOnlineStatus;
        }
    }

    // for message Section
    public function userMsgSave(){
        // echo "<pre>"; print_r($_POST); die();
        $arrInsert = array();
        $toUserID = $this->input->POST('toID');
        $userID = $this->session->userdata('userID');
        if (isset($_POST['gif']) && $_POST['gif']!="") {
            $arrInsert['gif'] = $this->input->POST('gif');
        }
        if (isset($_POST['message']) && $_POST['message']!="") {
            $msg = $this->emoji->Encode($this->input->POST('message'));
            $msg = $this->message->encryptString($msg);
            $arrInsert['text'] = $msg;
        }
        if ($this->input->post('message') || $this->input->post('gif')) {
            //$arrInsert['isRead'] = 1;
            $arrInsert['fromUserID'] = $userID;
            $arrInsert['toUserID'] = $toUserID;
            $arrInsert['createdDateTime'] = date('Y-m-d H:i:s');
            $arrInsert['updatedDateTime'] = date('Y-m-d H:i:s');
            $last_comment_id = $this->CommonModel->insert('tblmessage', $arrInsert);
        }
    }

    public function userMsgList(){
        $toUserID = $this->input->POST('toID');
        $userID = $this->session->userdata('userID');
        //$toUserID = 107;
        //$userDetails = $this->user->getUserdetails($toUserID);
        $messageData = $this->message->getUserMessages($userID, $toUserID, 0);
        $response = array();
        if(!empty($messageData)){
            foreach($messageData as $msg){
                if(isset($msg['media_type']) && !empty($msg['media_type'])){
                    $msgText = $msg['text'];
                }else{
                    $msgText = $this->message->decryptString($msg['text']);
                }
                $msg['text'] = ($msgText)?$this->emoji->Decode($msgText):"";
                $response[] = $msg;
            }
        }
        // echo "<pre>"; print_r($response); die();
        echo json_encode($response);
    }

    public function groupMsgSave() {
        $userID = $this->session->userdata('userID');
        $arrCommentInsert = array();
        $toUserID = $this->input->POST('toID');
        if (isset($_POST['gif']) && $_POST['gif']!="") {
            $arrCommentInsert['gif'] = $this->input->POST('gif');
        }
        if (isset($_POST['message']) && $_POST['message']!="") {
            $msg = $this->emoji->Encode($this->input->POST('message'));
            $msg = $this->message->encryptString($msg);
            $arrCommentInsert['text'] = $msg;
        }
        if ($this->input->post('message') || $this->input->post('gif')) {
            $arrCommentInsert['fromUserID'] = $userID;
            $arrCommentInsert['type'] = 1;
            //$arrCommentInsert['isRead'] = 1;
            $arrCommentInsert['toUserID'] = $toUserID;
            $last_comment_id = $this->CommonModel->insert('tblmessage', $arrCommentInsert);
            $result = $this->message->getRecentChatList($userID);
        }
    }

    public function chatImageSend(){
        $arrInsert['text'] = "";
        $arrInsert['type'] = 0;
        $arrInsert['fromUserID'] = $this->session->userdata('userID');
        $arrInsert['toUserID'] = $this->input->post('toID');
        $last_message_id = $this->CommonModel->insert('tblmessage', $arrInsert);
        //echo "<pre>"; print_r($last_message_id); die();
        $userID = $this->session->userdata('userID');
        if (!empty($_FILES['file']['name'])) {
            $config['upload_path'] = './upload/messagemedia/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = FALSE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $fileData = array('upload_data' => $this->upload->data());
                $arrMediaInsert['filename'] = $fileData["upload_data"]["file_name"];
                $arrMediaInsert['comID'] = $last_message_id;
                $arrMediaInsert['userID'] = $userID;
                $arrMediaInsert['type'] = 1;
                $arrMediaInsert['mediaRef'] = 1;
                $this->CommonModel->insert('tblmedia', $arrMediaInsert);
                $result = $this->message->getRecentChatList($userID);
            }
        }
    }

    public function chatVideoSend(){
        //echo "<pre>"; print_r($_FILES); die();
        $arrInsert['text'] = "";
        $arrInsert['type'] = 0;
        $arrInsert['fromUserID'] = $this->session->userdata('userID');
        $arrInsert['toUserID'] = $this->input->post('toID');
        $last_message_id = $this->CommonModel->insert('tblmessage', $arrInsert);
        //echo "<pre>"; print_r($last_message_id); die();

        $userID = $this->session->userdata('userID');

        if (!empty($_FILES['video']['name'])) {
            if ($_FILES['video']['error'] == 0) {
                $temp_file = $_FILES['video']['tmp_name'];
                $img_name = "video" . mt_rand(100, 999999) . time();
                $path = $_FILES['video']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                $arrMediaInsert['filename'] = $img_name . "." . $ext;
                $url = 'upload/messagemedia/' . $arrMediaInsert['filename'];
                move_uploaded_file($temp_file, $url);
                $arrMediaInsert['comID'] = $last_message_id;
                $arrMediaInsert['userID'] = $userID;
                $arrMediaInsert['type'] = 2;
                $arrMediaInsert['mediaRef'] = 1;
                $this->CommonModel->insert('tblmedia', $arrMediaInsert);
            }
        }
    }

    public function groupImageSend(){
        $arrInsert['text'] = "";
        $arrInsert['type'] = 1;
        $arrInsert['fromUserID'] = $this->session->userdata('userID');
        $arrInsert['toUserID'] = $this->input->post('toID');
        $last_message_id = $this->CommonModel->insert('tblmessage', $arrInsert);
        //echo "<pre>"; print_r($last_message_id); die();

        $userID = $this->session->userdata('userID');
        if (!empty($_FILES['file']['name'])) {
            $config['upload_path'] = './upload/messagemedia/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['encrypt_name'] = TRUE;
            $config['remove_spaces'] = TRUE;
            $config['overwrite'] = FALSE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);


            if (!$this->upload->do_upload('file')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $fileData = array('upload_data' => $this->upload->data());
                $arrMediaInsert['filename'] = $fileData["upload_data"]["file_name"];
                $arrMediaInsert['comID'] = $last_message_id;
                $arrMediaInsert['userID'] = $userID;
                $arrMediaInsert['type'] = 1;
                $arrMediaInsert['mediaRef'] = 1;
                $this->CommonModel->insert('tblmedia', $arrMediaInsert);
                $result = $this->message->getRecentChatList($userID);

            }
        }
    }

    public function groupVideoSend(){
        //echo "<pre>"; print_r($_FILES); die();
        $arrInsert['text'] = "";
        $arrInsert['type'] = 1;
        $arrInsert['fromUserID'] = $this->session->userdata('userID');
        $arrInsert['toUserID'] = $this->input->post('toID');
        $last_message_id = $this->CommonModel->insert('tblmessage', $arrInsert);
        //echo "<pre>"; print_r($last_message_id); die();

        $userID = $this->session->userdata('userID');

            if (!empty($_FILES['video']['name'])) {
                if ($_FILES['video']['error'] == 0) {
                    $temp_file = $_FILES['video']['tmp_name'];
                    $img_name = "video" . mt_rand(100, 999999) . time();
                    $path = $_FILES['video']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $arrMediaInsert['filename'] = $img_name . "." . $ext;
                    $url = 'upload/messagemedia/' . $arrMediaInsert['filename'];
                    move_uploaded_file($temp_file, $url);
                    $arrMediaInsert['comID'] = $last_message_id;
                    $arrMediaInsert['userID'] = $userID;
                    $arrMediaInsert['type'] = 2;
                    $arrMediaInsert['mediaRef'] = 1;
                    $this->CommonModel->insert('tblmedia', $arrMediaInsert);
                }
                //$result = $this->message->getRecentChatList($userID);
            }
    }

    public function msgimage(){
        //echo "<pre>"; print_r($_FILES); die();
        if (!empty($_FILES['sortpic']['name'])) {
            $config['upload_path'] = './upload/messagemedia/';

            $config['allowed_types'] = 'gif|jpg|png|jpeg';

            $config['encrypt_name'] = TRUE;

            $config['remove_spaces'] = TRUE;

            $config['overwrite'] = FALSE;

            $this->load->library('upload', $config);

            $this->upload->initialize($config);

            //echo "<pre>"; print_r($config); die();

            if (!$this->upload->do_upload('sortpic')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $fileData = array('upload_data' => $this->upload->data());
                $arrMediaInsert['filename'] = $fileData["upload_data"]["file_name"];
                // $arrMediaInsert['comID'] = $last_comment_id;
                // $arrMediaInsert['userID'] = $userID;
                $arrMediaInsert['type'] = 1;
                $arrMediaInsert['mediaRef'] = 1;
                $this->CommonModel->insert('tblmedia', $arrMediaInsert);
                //$result = $this->message->getRecentChatList($userID);
            }
        }

    }

    public function groupMsgList(){
        $toUserID = $this->input->POST('toID');
        $userID = $this->session->userdata('userID');
        //$toUserID = 107;
        $messageData = $this->message->getUserMessages($userID, $toUserID, 1);
        if(!empty($messageData)){
            foreach($messageData as &$value){
                $value['fromUserPic'] = $this->CommonModel->getUserProfilePic($value['userID']);
                if(isset($value['media_type']) && !empty($value['media_type'])){
                    $msgText = $value['text'];
                }else{
                    $msgText = $this->message->decryptString($value['text']);
                }
                $value['text'] = ($msgText)?$this->emoji->Decode($msgText):"";
            }
        }
       // echo "<pre>"; print_r($messageData); die();
        echo json_encode($messageData);
    }
    

    public function group_message($groupID=''){
        error_reporting(0);
        $this->data['title'] = "Group Message";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');

        $otherGroupID = $this->uri->segment(2);
        if ($otherGroupID) {
            $data['groupDetail'] = $this->group->groupName($otherGroupID);
            $data['groupPostList'] = $this->group->getGroupPosts($userID, $otherGroupID);
            //echo "<pre>"; print_r($data['groupDetail']); die();
        }
        $data['groupList'] = $this->group->getGroupList($userID, $type = 0);
        $data['userDetails'] = $this->user->getUserdetails($userID, $userID);
        // echo "<pre>"; print_r($data['groupList']); die();
        if($groupID != ''){
            $totalrecord = count($this->message->getUserMessages($userID, $groupID,1));
            $limit = 10;
            $page = ceil($totalrecord/$limit);
            $offset =0;
            if($totalrecord > $limit){
                $offset = ($totalrecord - $limit);
            }
           
            $messageData = $this->message->getUserMessages($userID, $groupID,1,$limit,$offset);
            
            if(!empty($messageData)){
                foreach($messageData as &$value){
                    // $value['text'] = $this->emoji->Decode($value['text']);
                    $value['fromUserPic'] = $this->CommonModel->getUserProfilePic($value['userID']);
                    if(isset($value['media_type']) && !empty($value['media_type'])){
                        $msgText = $value['text'];
                    }else{
                        $msgText = $this->message->decryptString($value['text']);
                    }
                    $value['text'] = ($msgText)?$this->emoji->Decode($msgText):"";
                }
            }
            $data['messageData'] = $messageData;
        }
       
        $data['watchUserID'] = $userID;
        $data['view'] = 'front/group_message';
        $this->render($data);
    }

    public function getOldMessage(){
        $toUserID = $this->input->POST('toID');
        $postpage = $this->input->POST('page');
        $type = $this->input->POST('type');
        $userID = $this->session->userdata('userID');

        $allResultList = count($this->message->getUserMessages($userID, $toUserID,$type));

        $limit = 10;
        $Pages = $allResultList/$limit;
        $totalPage = ceil($Pages);
        $Page = $totalPage + 1;
        if($postpage <= $totalPage) {
            $page_number = (isset($postpage) && $postpage != '') ? $Page - $postpage : $Page - 1;
            if (isset($postpage) && $postpage == $totalPage) {
                $limit = $allResultList - ($limit * ($totalPage - 1));
                $offset = 0;
            } else {
                if (isset($postpage) && $postpage != $totalPage) {
                    $offset = $allResultList - ($limit * $postpage);
                } else {
                    $offset = 0;
                }
            }
            $messageData = $this->message->getUserMessages($userID, $toUserID, $type, $limit, $offset);
            //echo "<pre>"; print_r($messageData); die();

            if (!empty($messageData)) {
                foreach ($messageData as &$value) {
                    if(isset($value['media_type']) && !empty($value['media_type'])){
                        $msgText = $value['text'];
                    }else{
                        $msgText = $this->message->decryptString($value['text']);
                    }
                    $value['text'] = ($msgText)?$this->emoji->Decode($msgText):"";
                    $value['fromUserPic'] = $this->CommonModel->getUserProfilePic($value['userID']);
                }
                $data['messagesdata'] = $messageData;
                $this->load->view('front/msg_conversation', $data);
            } else {
                echo false;
            }
        }else{
            echo false;
        }
    }

    public function lastMessage()
    {
        $toUserID = $this->input->POST('toID');
        $type = $this->input->POST('type');
        $lastmsgid = $this->input->POST('lastmsgid');
        $userID = $this->session->userdata('userID');
        //$toUserID = 107;
        $messageData = $this->message->getUserNewMessages($userID, $toUserID, $type,'','','',$lastmsgid);
        if(!empty($messageData)){
            foreach($messageData as &$value){
                // $value['text'] = $this->message->decryptString($value['text']);
                if(isset($value['media_type']) && !empty($value['media_type'])){
                    $msgText = $value['text'];
                }else{
                    $msgText = $this->message->decryptString($value['text']);
                }
                $value['text'] = ($msgText)?$this->emoji->Decode($msgText):"";
                $value['fromUserPic'] = $this->CommonModel->getUserProfilePic($value['userID']);
            }
            $data['messagesdata']= $messageData;
            //echo '<pre>';print_r($messageData);
            $this->load->view('front/msg_conversation',$data);
        }else{
            echo false;
        }
    }
    
    public function groups(){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $data['grouplist'] = $this->group->getGroupList($userID);
        $data['followingList'] = $this->user->getFollowingList($userID, $userID);
        $data['view'] = 'front/groups';
        $this->render($data);
    }
    
    public function group_details($groupID){
        $this->data['title'] = "Group Details";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $data['followingList'] = $this->user->getFollowingList($userID, $userID);
        if($this->input->post('btnSaveMembers')!=""){
            // echo '<pre>'; print_r($_POST);die;
            $memberIds = $this->input->post('memberID');
            if(!empty($memberIds)){
                $savegroupMember = array(
                    'groupID' => $groupID,
                    'createdDateTime' => date('Y-m-d H:i:s'),
                    'updatedDateTime' => date('Y-m-d H:i:s')
                );
                foreach($memberIds as $id){
                    $checkMemberExist = $this->CommonModel->get_row(TBL_USER_GROUP, ['userID'], ['groupID' => $groupID,'userID' => $id,'isActive' =>1,'isDelete' =>0]);
                    if(empty($checkMemberExist)){
                        $savegroupMember['userID'] = $id;
                        $this->CommonModel->insert('tblusergroup', $savegroupMember);
                    }
                }
            }
        }
        
        $groupDetails = $this->CommonModel->get_row('tblgroup', '*', array('groupID'=>$groupID));
        $data['groupDetails'] = $groupDetails;
        $data['remove'] = ($groupDetails['userID'] == $userID) ? true :'0';
        $data['groupmember'] = $this->group->getMembers($userID, $groupID);
        // echo "<pre>"; print_r($data['groupmember']); die();
        $data['groupDetails']['groupProfile'] = base_url()."upload/group/".$data['groupDetails']['groupProfile'];

        $pageNumber = ($this->input->post('page')) ? $this->input->post('page'):'1';

        $data['media_post_list'] = array();
        $data['text_post_list'] = array();
        // $postList = $this->post->getPosts($userID, 0, FALSE, $pageNumber);
        $postList = $this->group->getGroupPosts($userID, $groupID);
        if (!empty($postList)) {
            foreach ($postList as &$value) {
                $media = array();
                $result = $this->CommonModel->get_all('tblmedia', ["mediaID", "filename", "type"], ["comID" => $value['postID'], "isActive" => 1, "isDelete" => 0, "mediaRef" => 0], 'createdDateTime');
                foreach ($result as $key => $res) {
                    $media[$key]['mediaID'] = $res['mediaID'];
                    $media[$key]['type'] = $res['type'];
                    $media[$key]['filename'] = base_url() . "upload/postmedia/" . $res['filename'];
                }

                if (isset($value['gif']) && $value['gif'] == null || is_null($value['gif'])) {
                    $value['gif'] = "";
                }

                $value['ResharePostContent'] = "";
                $value['ResharePostEmoji'] = "";
                $value['ReshareUserImage'] = "";
                if (isset($value['postShareID']) && $value['postShareID'] > 0) {
                    $getSharePost = $this->post->getUserSharePost($value['postShareID']);
                    if (!empty($getSharePost)) {
                        $value['isLiked'] = "0";
                        $value['isdisLiked'] = "0";
                        $likeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $value['postID'], 'postShareID' => $value['postShareID'], "isLiked" => 1, "isDelete" => 0]);
                        if (!empty($likeResult)) {
                            $value['isLiked'] = $likeResult['isActive'];
                        }
                        $dislikeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $value['postID'], 'postShareID' => $value['postShareID'], "isLiked" => 0, "isDelete" => 0]);
                        if (!empty($dislikeResult)) {
                            $value['isdisLiked'] = $dislikeResult['isActive'];
                        }
                        $value['ResharePostUserid'] = (isset($getSharePost['userID'])) ? $getSharePost['userID'] : "";
                        $value['ResharePostDate'] = (isset($getSharePost['createdDateTime'])) ? $getSharePost['createdDateTime'] : "";
                        $value['ResharePostContent'] = (isset($getSharePost['post'])) ? $getSharePost['post'] : "";
                        $value['ResharePostEmoji'] = (isset($getSharePost['emoji'])) ? $getSharePost['emoji'] : "";
                        $reshareUserdata = $this->user->getUserdetails($getSharePost['userID']);
                        if (!empty($reshareUserdata)) {
                            $value['ReshareUserImage'] = (isset($reshareUserdata['profileImage'])) ? $reshareUserdata['profileImage'] : "";
                        }
                    }
                }

                $pollPostOptions = array();
                $isPollPost = (isset($value['isPollPost'])) ? $value['isPollPost'] : "";
                $getSinglePostData = $this->CommonModel->get_row(TBL_POST, ['*'], ['postID' => $value['postID'], 'isActive' => 1, 'isDelete' => 0]);
                $value['isPollPost'] = (isset($isPollPost) && $isPollPost != "") ? $isPollPost : (isset($getSinglePostData['isPollPost'])) ? $getSinglePostData['isPollPost'] : "0";
                if (isset($value['isPollPost']) && trim($value['isPollPost']) != "" && $value['isPollPost'] == null || is_null($value['isPollPost'])) {
                    $value['isPollPost'] = "";
                }

                if (isset($value['isPollPost']) && $value['isPollPost'] == 1) {
                    $value['hasPollPostAnswer'] = $this->post->checkPollPostHasUserAnswer($userID, $value['postID']);
                    $pollOptions = $this->CommonModel->get_all(TBL_POST_POLL_OPTIONS, ['*'], ["postID" => $value['postID'], "isActive" => 1, "isDelete" => 0]);
                    if (!empty($pollOptions)) {
                        foreach ($pollOptions as $opt) {
                            $avgAnswer = $this->post->getPollOptionAvgAns($value['postID'], $opt['optionID']);
                            $res = array();
                            $res['optionID'] = $opt['optionID'];
                            $res['option'] = $opt['text'];
                            $res['avgAnswer'] = $avgAnswer;
                            $pollPostOptions[] = $res;
                        }
                    }
                }
                $value['pollPostOptions'] = (!empty($pollPostOptions)) ? $pollPostOptions : array();
                $totalPollAnswer = $this->CommonModel->get_all(TBL_POST_POLL_ANSWER, ['*'], ["postID" => $value['postID'], "isActive" => 1, "isDelete" => 0]);
                $value['totalPollAnswer'] = (!empty($totalPollAnswer)) ? count($totalPollAnswer) . "" : "0";

                $totalComments = $this->post->totalComments($value['postID'], $userID);
                $totalShares = $this->post->totalShares($value['postID'], $userID);
                $totalLikes = $this->post->totalLikes($value['postID'], $userID);
                $totalDisLikes = $this->post->totalDisLikes($value['postID'], $userID);

                $value['media'] = $media;
                $value['commentCount'] = $totalComments;
                $value['reShareCount'] = $totalShares;
                $value['likesCount'] = $totalLikes;
                $value['disLikeCount'] = $totalDisLikes;

                if (count($media) == 0) {
                    array_push($data['text_post_list'], $value);
                } else {
                    array_push($data['media_post_list'], $value);
                }

                unset($media);
            }
        }
        //unset($postList);
        //echo '<pre>';print_r($postsdata['media_post_list']);die;
        $data['view'] = 'front/group_details';
        $this->render($data);
    }

    public function updateUserOnlineStatus(){
        $result = array('status'=>0);
        $userID = $this->session->userdata('userID');
        $status = $this->input->post('status');
        if($status!=""){
            $updateData = array();
            $updateData['onlineStatus'] = $status;
            $this->CommonModel->update(TBL_USER, $updateData, ["userID" => $userID]);
            $result['status'] = $status;
            $result['statusLabel'] = ($status==1)?'f-online':'f-offline';
        }
        echo json_encode($result); exit();
    }

    public function unBlockUser(){
        $blockUserID = $this->input->post('userid');
        $userID = $this->session->userdata('userID');
        $alreadyExistsMuteUser = $this->CommonModel->get_row(TBL_BLOCKUSER, ['blockID'], ['userID' => $userID, 'blockUserID' => $blockUserID]);
        if(!empty($alreadyExistsMuteUser)){
            $usermuteID = $this->CommonModel->update(TBL_BLOCKUSER, ['isActive' => 0], ['blockID' => $alreadyExistsMuteUser['blockID']]);
        }
        if($usermuteID != ''){
            echo true;
        }else{
            echo false;
        }
    }

    // remove all post in one click
    public function removeAllPosts()
    {
        $blockUserID = $this->input->post('userid');
        $userID = $this->session->userdata('userID');
        if($userID == $blockUserID){
            $arrPostUpdate['isDelete'] = 1;
            $arrPostUpdate['isActive'] = 0;
            $arrPostUpdate['updatedDateTime'] = date('Y-m-d H:i:s');
            $res1 = $this->CommonModel->update(TBL_POST, $arrPostUpdate, array('userID' => $userID));
            $res2 = $this->CommonModel->update(TBL_POST_SHARE, $arrPostUpdate, array('userID' => $userID));
            $res3 = $this->CommonModel->update(TBL_MEDIA, $arrPostUpdate, array('userID' => $userID, "mediaRef" => 0));
            if($res1!='' && $res2!='' && $res3!=''){
                echo true;
            }else{
                echo false;
            }
        }else{
            echo false;
        }
    }

    //for user post subscribe
    public function subscribeUserPost(){
        /* 1 = subscribe or 0 = unsubscribe*/
        
        $userID = $this->session->userdata('userID');
        $followerID = $this->input->post('followerID');
        $status = $this->input->post('status');

        $subscribuser = $this->user->getUserNamerow($followerID);
        $alreadyExistaubscibeUser = $this->CommonModel->get_row(TBL_POST_SUBSCRIBE, ['postNotifyID'], ['userID' => $userID, 'followerID' => $followerID,'isActive'=>1,'isDelete'=>0]);
        if(!empty($alreadyExistaubscibeUser)){
            $arrUpdate = array();
            $arrUpdate['status'] = $status;
            $postNotifyID = $this->CommonModel->update(TBL_POST_SUBSCRIBE, $arrUpdate, ['postNotifyID' => $alreadyExistaubscibeUser['postNotifyID']]);
        }else{
            $arrInsert = array();
            $arrInsert['status'] = $status;
            $arrInsert['userID'] = $userID;
            $arrInsert['followerID'] = $followerID;
            $postNotifyID = $this->CommonModel->insert(TBL_POST_SUBSCRIBE, $arrInsert);
        }

        // if(isset($postNotifyID) && $postNotifyID != ''){
            redirect('time-line/'.$subscribuser['username']);
        // }


    }
    // for clear chat history
    public function deletegroupChatHistory($toUserID,$type){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        if(!empty($toUserID) && $type){

        }
        // $type = (isset($_POST['type']))?$this->input->post('type'):0;
        $lastMsgData = $this->message->getUserLastMessage($userID,$toUserID,$type);
        if(empty($lastMsgData)){
            if($type != 1){
                redirect('message-box');
            }else{
                redirect('group-message');
            }
        }
        // echo "<pre>"; print_r($lastMsgData); die();
        $lastMsgId = $lastMsgData['messageID'];
        $saveData['userID'] = $userID;
        $saveData['toUserID'] = $toUserID;
        $saveData['messageID'] = $lastMsgId;
        $saveData['updatedDateTime'] = date('Y-m-d H:i:s');
        $checkRecordExist = $this->CommonModel->get_row(TBL_MESSAGE_CLEAR, ['*'], ['isActive'=>1,'userID' => $userID, 'toUserID'=>$toUserID]);
        if(!empty($checkRecordExist)){
            $this->CommonModel->update(TBL_MESSAGE_CLEAR, $saveData, array('userID' => $userID, 'clearID'=>$checkRecordExist['clearID']));
        }else{
            $saveData['createdDateTime'] = date('Y-m-d H:i:s');
            $this->CommonModel->insert(TBL_MESSAGE_CLEAR, $saveData);
        }
        if($type != 1){
            redirect('message-box');
        }else{
            redirect('group-message');
        }
    }
    
    public function following_list($watchUserID =''){
        $data['title'] = "Following users";
        $data['sectopnTitle'] = "Following Users";
        $data['type'] = "Following";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        if($watchUserID ==''){
            $watchUserID = $userID;
        }
        $followingList = $this->user->getFollowingList($userID, $watchUserID);
        if(!empty($followingList)){
            foreach ($followingList as &$followinguser) {
                $user['privateProfileStatus'] = "0";
                $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $followinguser['followerID'], 'isActive' => 1, 'isDelete' => 0]);
                if (!empty($settingsData)) {
                    $followinguser['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";
                }   
                //for check user follow me or not
                $followinguser['isFollowMe'] = "0";
                $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $followinguser['followerID'], 'followerID' => $watchUserID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);
                if (!empty($checkUserFollowData)) {
                    $followinguser['isFollowMe'] = "1";
                }

            }
        }
        $topfive = $this->CommonModel->get_row(TBL_TOPFIVE_FOLLLOWERS, ['followUserID'], ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        
        $data['topfiveArray'] = json_decode($topfive['followUserID'],true);
        
        $data['followinguser'] = $followingList;
        $data['followCount'] = count($followingList);
        $data['view'] = 'front/following_list';
        $this->render($data);
    }

    public function followers_list($watchUserID =''){
        $data['title'] = "Followers users";
        $data['sectopnTitle'] = "Followers Users";
        $data['type'] = "Followers";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        if($watchUserID ==''){
            $watchUserID = $userID;
        }
        //followers data
        $followingList = $this->user->getFollowersList($userID, $watchUserID);
        if(!empty($followingList)){
            foreach ($followingList as &$followeruser) {
                $user['privateProfileStatus'] = "0";
                $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $followeruser['followerID'], 'isActive' => 1, 'isDelete' => 0]);
                if (!empty($settingsData)) {
                    $followeruser['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";
                }
                //for check user follow me or not
                $followeruser['isFollowMe'] = "0";
                $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $followeruser['followerID'], 'followerID' => $watchUserID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);
                if (!empty($checkUserFollowData)) {
                    $followeruser['isFollowMe'] = "1";
                }
            }
        }
        
        $data['followCount'] = count($followingList);
        $data['followinguser'] = $followingList;
        $data['view'] = 'front/following_list';
        $this->render($data);
    }

    public function updateThemeSetting(){
        $result = array('status'=>0);
        $userID = $this->session->userdata('userID');
        $isDarkmode = ($this->input->post('isDarkmode') != "") ? $this->input->post('isDarkmode'):0;
        if($isDarkmode!=""){
            $arrSettings['isDarkmode'] = $isDarkmode;
            $arrSettings['userID'] = $userID;
            $arrSettings['updatedDateTime'] = date('Y-m-d H:i:s');
            $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
            if(empty($userSetting)){
                $arrSettings['createdDateTime'] = date('Y-m-d H:i:s');
                $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);
            }else{
                $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userSetting['settingID']]);
            }
            $result['status'] = 1;
        }
        echo json_encode($result); exit();
    }
    
    public function user_image(){

        $data['title'] = "Images";
        $data['view'] = 'front/images_list';
        $this->render($data);
    }

    public function addTopFiveFollower(){
        $userID = $this->session->userdata('userID');
        $follower = $this->input->post('followers');
        $checkdata = $this->CommonModel->get_row(TBL_TOPFIVE_FOLLLOWERS, '*', ['userID'=>$userID]);
        $data['userID'] = $userID;
        $data['followUserID'] = json_encode($follower);
        $data['updatedDateTime'] = date('Y-m-d H:i:s');
        if(!empty($checkdata)){
            $this->CommonModel->update(TBL_TOPFIVE_FOLLLOWERS, $data, ["userID" => $userID]);
        }else{
            $data['createdDateTime'] = date('Y-m-d H:i:s');
            $this->CommonModel->insert(TBL_TOPFIVE_FOLLLOWERS, $data);
        } 
    }

    public function uploadAditionalimg() {
        //echo '<pre>'; print_r('ok');die;
        $userID = $this->session->userdata('userID');
        $aditionalID = $this->input->post('aditionalID');
        
        $data = array(
            'userID' =>$userID,
            'createdDateTime' => date('Y-m-d H:i:s'),
            'updatedDateTime' => date('Y-m-d H:i:s'),
        );
        if (isset($_FILES['picture']) && $_FILES['picture']['error'] == 0) {
            $temp_file = $_FILES['picture']['tmp_name'];
            $img_name = "tray_img" . mt_rand(10000, 999999999) . time();
            $path = $_FILES['picture']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $data['filename'] = $img_name . "." . $ext;
            $fileurl = ADITIONAL_IMG . $data['filename'];
            move_uploaded_file($temp_file, $fileurl);
            if(isset($aditionalID) && $aditionalID !=''){
                $checkdata = $this->CommonModel->get_row(TBL_ADITIONAL_IMG, '*', ['aditionalID'=>$aditionalID]);
                if(!empty($checkdata)){
                    unlink(ADITIONAL_IMG.$checkdata['filename']);
                }
                $this->CommonModel->update(TBL_ADITIONAL_IMG, $data, ["aditionalID" => $aditionalID,'userID'=>$userID]);
            }else{
                $aditionalID = $this->CommonModel->insert(TBL_ADITIONAL_IMG, $data);
            }
            echo $aditionalID; exit;
        }

    }
    
    public function postHighlighted(){
        $userID = $this->session->userdata('userID');
        $arrSettings['highlightPost'] = $this->input->post('highlightPost');;
        $arrSettings['userID'] = $userID;
        $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        if(empty($userSetting)){
            $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);
        }else{
            $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userSetting['settingID']]);
        }
    }

    public function postHighlightStatus(){
        $userID = $this->session->userdata('userID');
        $status = $this->input->post('status');
        $arrSettings['highlightStatus'] = isset($status) ? $status :'0';
        $arrSettings['userID'] = $userID;
        $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        if(empty($userSetting)){
            $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);
        }else{
            $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userSetting['settingID']]);
        }
    }

    public function profileFrame(){
        $userID = $this->session->userdata('userID');
        $customFrame = $this->input->post('customFrame');
        $arrSettings['customFrame'] = isset($customFrame) ? $customFrame :'';
        $arrSettings['userID'] = $userID;
        $userSetting = $this->CommonModel->get_row(TBL_SETTINGS, '*', ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        if(empty($userSetting)){
            $this->CommonModel->insert(TBL_SETTINGS, $arrSettings);
        }else{
            $this->CommonModel->update(TBL_SETTINGS, $arrSettings, ['settingID' => $userSetting['settingID']]);
        }
    }

    //User Edit Profile
    public function plusAccount(){
        $data['title'] = "Plus Account";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $plusAccount = $this->user->checkUserPlusAccount($userID);
        if(empty($plusAccount)){
            redirect('upgrade-account');
        }
        $data['plusAccount'] = $plusAccount;
        if ($this->input->post('btnSubmitProfile')) {
            // echo "<pre>"; print_r($_POST); die();
            $arrUserUpdate = array(
                'username' => $this->input->post('username'),
                'name' => ($this->input->post('name'))?$this->emoji->Encode($this->input->post('name')):"",
                'email' => $this->input->post('email'),
                'location' => $this->input->post('location'),
                'dob' => $this->input->post('dob'),
                'website' => ($this->input->post('website'))?$this->input->post('website'):"",
                'phone' => ($this->input->post('phone'))?$this->input->post('phone'):"",
                'about' => $about
            );
            if (!empty($_FILES['profileImage']['name'])) {
                $config['upload_path'] = './upload/userprofile/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('profileImage')) {
                    $fileData = array('upload_data' => $this->upload->data());
                    $arrUserUpdate['profileImage'] = $fileData["upload_data"]["file_name"];
                }
            }
            $this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $userID]);
            redirect(base_url('edit-profile'));
        }
        $data['userDetails'] = $this->user->getUserdetails($userID, $userID);
        $data['view'] = 'front/payment_gateway';
        $this->render($data);
    }

    // for upgrade to plus account
    public function upgradeAccount(){
        $data['title'] = "Upgrade Account";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $data['plusAccount'] = $this->user->checkUserPlusAccount($userID);
        $data['packageData'] = $this->CommonModel->get_all(TBL_PACKAGE, '*', ['isDelete' => 0, 'isActive' => 1]);
        $data['view'] = 'front/join_plus_account';
        $this->render($data);
    }

    public function paymentProcess(){
        $data['title'] = "Make Payment";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        if (!isset($_GET['plan']) || empty($_GET['plan'])) {
            redirect('upgrade-account');
        }
        $plan = $this->input->get('plan');
        $packageData = $this->CommonModel->get_row(TBL_PACKAGE, '*', ['packageID'=>$plan, 'isDelete' => 0, 'isActive' => 1]);
        if(empty($packageData)){
            redirect('upgrade-account');
        }
        $userID = $this->session->userdata('userID');
        $today = date("Y-m-d");
        
        if ($this->input->post('btnSubmitPayment')) {
            // echo "<pre>"; print_r($_POST); die();
            $packageAmount = (isset($packageData['amount']))?$packageData['amount']:0;
            $duration = (isset($packageData['duration']))?'+1 '.$packageData['duration']:'+1 day';
            $expiry = date('Y-m-d', strtotime($duration, strtotime($today)));
            
            // stripe payment gateway
            $transactionID = 'SAY'.rand(10000000,99999999);
            
            $saveTransaction = array(
                'userID' => $userID,
                'transactionID' => $transactionID,
                'transAmount' => $packageAmount,
                'expiry' => $expiry,
                'cardName' => (isset($_POST['first-name']) && isset($_POST['last-name']))?$this->input->post('first-name')." ".$this->input->post('last-name'):"",
                'address' => (isset($_POST['streetaddress']))?$this->input->post('streetaddress'):"",
                'city' => (isset($_POST['city']))?$this->input->post('city'):"",
                'zipcode' => (isset($_POST['zipcode']))?$this->input->post('zipcode'):"",
                'email' => (isset($_POST['email']))?$this->input->post('email'):"",
                'createdDateTime' => date('Y-m-d H:i:s'),
                'updatedDateTime' => date('Y-m-d H:i:s'),
            );
            $transID = $this->CommonModel->insert(TBL_USER_TRANSACTION, $saveTransaction);

            $user_session_data['plusAccount'] = $this->user->checkUserPlusAccount($userID);
            $this->session->set_userdata($user_session_data);
            redirect(base_url('payment-success/'.$transID));
        }
        $data['view'] = 'front/payment_gateway';
        $this->render($data);
    }

    // for payment success page
    public function paymentSuccess($transID){
        $data['title'] = "Plus Account";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $transData = $this->CommonModel->get_row(TBL_USER_TRANSACTION, '*', ['transID'=>$transID, 'isActive' => 1]);
        if(empty($transData)){
            redirect(base_url());
        }
        $data['view'] = 'front/payment_success';
        $this->render($data);
    }

    public function plusAccountDetails(){
        $data['title'] = "Plus Account Details";
        $data['view'] = 'front/plus_account_details';
        $this->render($data);
    }


    public function forgot_password()
    {
        $userEmail = $this->CommonModel->get_row(TBL_USER, ["userID", "email"], ["email" => $this->input->post('email')]);
        if (empty($userEmail['email'])) {
            $this->session->set_flashdata('error_msg', 'User not exists.');
            redirect(base_url('login'));
        }
        $new_password = $this->random_password();
        $arrUserUpdate['password'] = md5($new_password);
        if ($this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $userEmail['userID']]) > 0) {
            /*Email Start*/
            /*Email Template start*/
            $html = file_get_contents(base_url() . "emailTemplate/forgotPassword.php");
            $emailHeader = file_get_contents("emailTemplate/emailHeader.php");
            $emailFooter = file_get_contents("emailTemplate/emailFooter.php");
            $replaceOn = ["{{password}}", "{{base_url}}", "{{emailHeader}}", "{{emailFooter}}"];
            $replaceWith = [$new_password, base_url(), $emailHeader, $emailFooter];
            $messageHTML = str_replace($replaceOn, $replaceWith, $html);
            /*Email Template end*/

            $subject = "Say Scape Login";
            $mail_end = $this->CommonModel->send_mail($userEmail['email'], $subject, $messageHTML);
            /*End*/
            if (!$mail_end) {
                $this->session->set_flashdata('error_msg', 'Email Not Sent.');
                redirect(base_url('login'));
            }
            $this->session->set_flashdata('success_msg', getMessage('sendpasswordsuccess'));
            redirect(base_url('login'));

        } else {
            $this->session->set_flashdata('error_msg', 'Not Update userData.');
            redirect(base_url('login'));
        }
    }

    // for multi account login
    public function multipleLogin(){
        $multiuserID = $this->session->userdata('multiuserID');
        // echo '<pre>';print_r($multiuserID);die;
        $data['multiuserID'] = $multiuserID;
        $data['view'] = 'front/layout/multiple_login';
        $this->render($data);
    }

    public function checkLogin(){
        $user_data['email'] = $this->input->post('email');
        $user_data['password'] = md5($this->input->post('password'));
        $checkEmail = $this->CommonModel->get_row('tbluser', '*', $user_data);
        //echo '<pre>';print_r($checkEmail);die;
        if(empty($checkEmail)){
            $username_data['username'] = $this->input->post('email');
            $username_data['password'] = md5($this->input->post('password'));
            $user_session_data = $this->CommonModel->get_row('tbluser', '*', $username_data);
        }else{
            $user_session_data = $checkEmail;
        }
        if (!empty($user_session_data)) {
            $userID = $user_session_data['userID'];
            if(!empty($user_session_data['isVerified'])){
                $updateData = array();
                $updateData['lastLogin'] = date('Y-m-d H:i:s');
                $updateData['onlineTime'] = time();
                $this->CommonModel->update(TBL_USER, $updateData, ["userID" => $userID]);
                
                /*if(!empty($this->session->userdata('multiuserID'))){
                    $multiuserID = $this->session->userdata('multiuserID');
                }else{  $multiuserID = array(); }
                array_push($multiuserID, $userID);
                $multiuserID = array_unique($multiuserID);
                $this->session->set_userdata('multiuserID',$multiuserID);*/
                echo $userID;
            }else{
                //$data['userID'] = $userID;
                echo '102';
            }
        } else {
            //$this->session->set_flashdata('error_msg', 'invalid Username & Password.');
            echo '103';
        }
        
    }

    public function withoutlogin($userID){
        $user_data['userID'] = $userID;
        $user_session_data = $this->CommonModel->get_row('tbluser', '*', $user_data);
        //echo '<pre>';print_r($checkEmail);die;
        if (!empty($user_session_data)) {
            $userID = $user_session_data['userID'];
            $updateData = array();
            $updateData['lastLogin'] = date('Y-m-d H:i:s');
            $updateData['onlineTime'] = time();
            $this->CommonModel->update(TBL_USER, $updateData, ["userID" => $userID]);
            $this->session->set_userdata($user_session_data);
            redirect(base_url());
        } else {
            redirect(base_url());
        }
    }

    public function multiuserView(){
        $data['multiuserID'] = $this->input->post('multiuserID');
        $this->load->view('front/layout/multiuser',$data);
    }

    //user alredy login
    public function alredyLogin(){
        if ($this->input->post('loginUserID')) {
            $userID = $this->input->post('loginUserID');
            $user_data['userID'] = $userID;
            $user_session_data = $this->CommonModel->get_row('tbluser', '*', $user_data);
            //echo '<pre>';print_r($checkEmail);die;
            if (!empty($user_session_data)) {
                $userID = $user_session_data['userID'];
                $updateData = array();
                $updateData['lastLogin'] = date('Y-m-d H:i:s');
                $updateData['onlineTime'] = time();
                $this->CommonModel->update(TBL_USER, $updateData, ["userID" => $userID]);
                $this->session->set_userdata($user_session_data);
                echo 101;
            } else {
               echo false;
            }
        }
    }

    


}
?>