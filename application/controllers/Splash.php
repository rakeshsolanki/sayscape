<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Splash extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('SplashModel');
    }

    public function index() {
        
        $arrResponse = array();

        /*referral code and message*/
        $arrResponse['referralCode']='';
        $arrResponse['shareMessage']='';
        $arrResponse['sos_number']=SOS_NUMBER;
        if(isset($_REQUEST['userID']) && $_REQUEST['userID'] != '')
        {
            $checkMobileExist = $this->CommonModel->get_row(TBL_USERS, ['userID','referralCode'], ['userID' => $_POST['userID']]);
            if(!empty($checkMobileExist))
            {
                $arrResponse['referralCode']=$checkMobileExist['referralCode'];
                $arrResponse['shareMessage']=str_replace("{{Refferral Code}}",$checkMobileExist['referralCode'],REFERRAL_MESSAGE);;
            }
        }
        
        /*Colume you need to select */
        $arrSelect=array();
        $arrSelect=array('makeID','makeName');

        $arrResponse['arrMakeRecord'] = $this->CommonModel->get_all(TBL_MAKE,$arrSelect,[],'makeID');
        foreach ($arrResponse['arrMakeRecord'] as &$row) {
            $arrWhere=array();
            $arrWhere['makeID'] = $row['makeID'];
             /*Colume you need to select */
            $arrSelect = array();
            $arrSelect = array('modelID', 'makeID', 'modelName');

            $row['arrModelRecord'] = $this->CommonModel->get_all(TBL_MODEL, $arrSelect, $arrWhere, 'modelID');
        }

        /*Colume you need to select */
        $arrSelect=array();
        $arrSelect=array('cityID','cityName');

        $arrResponse['arrCityRecord'] = $this->CommonModel->get_all(TBL_CITY,$arrSelect,[],'cityID');

        foreach ($arrResponse['arrCityRecord'] as &$row) {
            $arrWhere=array();
            $arrWhere['cityID'] = $row['cityID'];
             /*Colume you need to select */
            $arrSelect = array();
            $arrSelect = array('areaID', 'areaName', 'cityID');

            $row['arrAreaRecord'] = $this->CommonModel->get_all(TBL_AREA, $arrSelect, $arrWhere, 'areaID');
        }



         /*Colume you need to select */
         $arrSelect=array();
         $arrSelect=array('branchID','cityID','branchName');
 
         $arrResponse['arrBranchRecord'] = $this->CommonModel->get_all(TBL_BRANCH,$arrSelect,[],'branchID');


         /*Colume you need to select */
         $arrSelect=array();
         $arrSelect=array('insuranceCompanyID','companyName');
 
         $arrResponse['arrInsuranceCompanyRecord'] = $this->CommonModel->get_all(TBL_INSURANCE_COMPANY_MASTER,$arrSelect,[],'insuranceCompanyID');

         $arrResponse['about_us_text']="Lorem ipsum about us text";

         $arrResponse['code'] = 100;
         $arrResponse['success'] = '1';
         $arrResponse['message'] = 'Successful';
         $arrResponse['message_key'] = 'splash';
       
        $this->output->set_content_type('application/json')
               ->set_output(json_encode($arrResponse));

    }

}
