<?php
defined('BASEPATH') or exit('No direct script access allowed');

    class Dashboard extends MY_Controller
    {
        public function __construct() {
            parent::__construct();
    
            if(!$this->session->userdata('admin_loggedIN')) {
                redirect(site_url('admin/login'));
            }
    
        }

        public function index(){
            $data['title'] = SITE_NAME. ' | Admin Dashboard';
            $this->template->load('admin_layout', 'admin/dashboard', $data);
        }
        
    }
    
?>