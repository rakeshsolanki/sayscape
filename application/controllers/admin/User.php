<?php

defined('BASEPATH') or exit('No direct script access allowed');


class User extends MY_Controller

{

    protected $response = array(

        'code' => 101,

        'message' => 'No Data Found',

        'data' => []

    );


    public function __construct()
    {

        parent::__construct();


        if (!$this->session->userdata('admin_loggedIN')) {

            redirect(site_url('admin/login'));

        }


        $this->load->model('UserModel', 'user');


    }


    public function index()
    {

        $data['title'] = SITE_NAME . ' | Admin Dashboard';

        $this->template->load('admin_layout', 'admin/user', $data);

    }

    public function getUserList()

    {

        // if (!$this->input->get('userType') || empty($this->input->get('userType'))) {

        //     $this->response['message'] = "Pass User Type";

        //     $this->response($this->response);

        // }


        if (!isset($_GET['status']) || !in_array($this->input->get('status'), [0, 1])) {

            $this->response['message'] = "Please pass status";

            $this->response($this->response);

        }


        $status = $this->input->get('status');


        $this->response['code'] = 100;

        $this->response['message'] = "Get User List SuccessFully..!";

        $this->response['data'] = $this->user->getAllUser(['userID', 'username', 'name', 'email', 'phone', 'isVerified', 'createdDateTime', 'isVeteran', 'allowVeteran', 'isOffical', 'isResponder'], ['isActive' => $status, 'isDelete' => 0]);


        $this->response($this->response);

    }

    public function rejectedUsername(){
        $data['title'] = SITE_NAME. ' | Rejected Username';
        $this->template->load('admin_layout', 'admin/rejectedUsername', $data);
    }
    public function ajaxRejectedUsernameList()
    {

        $this->response['code'] = 100;
        $this->response['message'] = "Get Data SuccessFully";
        $this->response['data'] =  $this->CommonModel->get_all(TBL_REJECTED_USERNAME,['rejectUsernameID','rejectUsername','createdDateTime','updatedDateTime']);;
        
        $this->response($this->response);
    }

    public function getUserReportList()
    {
        $this->response['code'] = 100;

        $this->response['message'] = "Get User Report List SuccessFully..!";

        $this->response['data'] = $this->user->getAllRportUser(['u.userID', 'ru.username', 'u.username', 'u.name', 'u.email', 'u.phone', 'u.isVerified', 'u.createdDateTime', 'u.isVeteran', 'u.isOffical'], ['r.reportType' => 1]);


        $this->response($this->response);

    }


    public function ajaxVeteran()
    {

        if (!$this->input->post('userID') || empty($this->input->post('userID'))) {

            $this->response['message'] = "Please pass userID";

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = "Please pass status";

            $this->response($this->response);

        }


        $userID = $this->input->post('userID');

        $status = $this->input->post('status');


        if ($this->CommonModel->update(TBL_USER, ['isVeteran' => $status], ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]) > 0) {

            $this->response['code'] = 100;

            $this->response['message'] = "isVeteran SuccessFully Update";

        } else {

            $this->response['code'] = 101;

            $this->response['message'] = "isVeteran Update Problem.!";

        }


        $this->response($this->response);

    }

    public function ajaxAllowVeteran()
    {

        if (!$this->input->post('userID') || empty($this->input->post('userID'))) {
            $this->response['message'] = "Please pass userID";
            $this->response($this->response);
        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {
            $this->response['message'] = "Please pass status";
            $this->response($this->response);
        }

        $userID = $this->input->post('userID');
        $status = $this->input->post('status');

        if ($this->CommonModel->update(TBL_USER, ['allowVeteran' => $status], ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]) > 0) {
            $this->response['code'] = 100;
            $this->response['message'] = "isVeteran SuccessFully Update";
        } else {
            $this->response['code'] = 101;
            $this->response['message'] = "isVeteran Update Problem.!";
        }
        $this->response($this->response);
    }


    public function ajaxOfficial()
    {

        if (!$this->input->post('userID') || empty($this->input->post('userID'))) {

            $this->response['message'] = "Please pass userID";

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = "Please pass status";

            $this->response($this->response);

        }


        $userID = $this->input->post('userID');

        $status = $this->input->post('status');


        if ($this->CommonModel->update(TBL_USER, ['isOffical' => $status], ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]) > 0) {

            $this->response['code'] = 100;

            $this->response['message'] = "isOfficail SuccessFully Update";

        } else {

            $this->response['code'] = 101;

            $this->response['message'] = "isOfficail Update Problem.!";

        }


        $this->response($this->response);

    }


    public function ajaxResponder()
    {

        if (!$this->input->post('userID') || empty($this->input->post('userID'))) {

            $this->response['message'] = "Please pass userID";

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = "Please pass status";

            $this->response($this->response);

        }


        $userID = $this->input->post('userID');

        $status = $this->input->post('status');


        if ($this->CommonModel->update(TBL_USER, ['isResponder' => $status], ['userID' => $userID, 'isActive' => 1, 'isDelete' => 0]) > 0) {

            $this->response['code'] = 100;

            $this->response['message'] = "isResponder SuccessFully Update";

        } else {

            $this->response['code'] = 101;

            $this->response['message'] = "isResponder Update Problem.!";

        }

        $this->response($this->response);

    }


    public function ajaxBlock()
    {

        if (!$this->input->post('userID') || empty($this->input->post('userID'))) {

            $this->response['message'] = "Please pass userID";

            $this->response($this->response);

        }

        if (!isset($_POST['status']) || !in_array($this->input->post('status'), [0, 1])) {

            $this->response['message'] = "Please pass status";

            $this->response($this->response);

        }


        $userID = $this->input->post('userID');

        $status = $this->input->post('status');


        if ($this->CommonModel->update(TBL_USER, ['isActive' => $status], ['userID' => $userID, 'isDelete' => 0]) > 0) {

            $this->response['code'] = 100;

            $this->response['message'] = "isActive SuccessFully Update";

        } else {

            $this->response['code'] = 101;

            $this->response['message'] = "isActive Update Problem.!";

        }


        $this->response($this->response);

    }


    public function edit($userID = "")
    {

        $data['title'] = SITE_NAME . ' | Admin Users';

        // $data['userdata'] = $this->user->getUser(['tu.*'],['tu.userID'=>$userID,'tu.isDelete'=>0]);

        $this->db->select('tu.*')
            ->from(TBL_USER . ' as tu')
            ->join(TBL_USER_ACCESS_TOKEN . ' as tuat', 'tuat.userID = tu.userID', 'LEFT')
            ->where('tu.isDelete', 0)
            ->where('tu.userID', $userID)
            ->limit(1);

        $data['userdata'] = $this->db->get()->row_array();


        if ($this->input->post('btnSubmit') != "") {

            $arrUserUpdate['username'] = ($this->input->post('username') != "") ? $this->input->post('username') : "";

            $arrUserUpdate['email'] = ($this->input->post('email') != "") ? $this->input->post('email') : "";

            $arrUserUpdate['name'] = ($this->input->post('name') != "") ? $this->input->post('name') : "";

            $arrUserUpdate['phone'] = ($this->input->post('phone') != "") ? $this->input->post('phone') : "";

            $arrUserUpdate['location'] = ($this->input->post('location') != "") ? $this->input->post('location') : "";

            $arrUserUpdate['isActive'] = ($this->input->post('isActive') != "") ? $this->input->post('isActive') : "1";


            if ($this->CommonModel->update(TBL_USER, $arrUserUpdate, ["userID" => $userID]) > 0) {

                redirect(site_url('admin/user'));

            }

        }

        $this->template->load('admin_layout', 'admin/user_edit', $data);

    }

    public function ajaxDelete()
    {
        if(!$this->input->post('userID') || empty($this->input->post('userID'))){
            $this->response['message'] = "Please pass userID";
            $this->response($this->response);
        }
        if(!isset($_POST['status']) || !in_array($this->input->post('status'),[0,1]) ){
            $this->response['message'] = "Please pass status";
            $this->response($this->response);
        }

        $userID = $this->input->post('userID');
        $status = $this->input->post('status');

        if($this->CommonModel->update(TBL_USER,['isDelete'=>$status],['userID'=>$userID]) > 0){
            $this->response['code'] = 100;
            $this->response['message'] = "isDelete SuccessFully Update";
        }else{
            $this->response['code'] = 101;
            $this->response['message'] = "isDelete Update Problem.!";
        }

        $this->response($this->response);
    }

    public function ajaxClear()
    {
        if(!$this->input->post('reportID') || empty($this->input->post('reportID'))){
            $this->response['message'] = "Please pass reportID";
            $this->response($this->response);
        }
        if(!isset($_POST['status']) || !in_array($this->input->post('status'),[0,1]) ){
            $this->response['message'] = "Please pass status";
            $this->response($this->response);
        }

        $reportID = $this->input->post('reportID');
        $status = $this->input->post('status');

        if($this->CommonModel->update(TBL_REPORT,['isDelete'=>$status],['reportID'=>$reportID]) > 0){
            $this->response['code'] = 100;
            $this->response['message'] = "Report SuccessFully Update";
        }else{
            $this->response['code'] = 101;
            $this->response['message'] = "Report Update Problem.!";
        }

        $this->response($this->response);
    }

    public function saveRejectedUsername()
    {
        if(!$this->input->post('rejectUsername') || empty($this->input->post('rejectUsername'))){
            $this->response['message'] = "Please rejectUsername";
            $this->response($this->response);
        }

        $arrInsert['rejectUsername'] = $this->input->post('rejectUsername');
        
        $rejectUsernameID = $this->CommonModel->insert(TBL_REJECTED_USERNAME,$arrInsert);
        $this->response['code'] = 100;
        $this->response['message'] = 'Username Added Successfully';
        
        $this->response($this->response);
    }
    
    public function deleteRejectedUsername()
    {
        if(!$this->input->post('rejectUsernameID') || empty($this->input->post('rejectUsernameID'))){
            $this->response['message'] = "Please pass rejectUsernameID";
            $this->response($this->response);
        }

        $rejectUsernameID = $this->input->post('rejectUsernameID');

        if($this->CommonModel->update(TBL_REJECTED_USERNAME,['isActive'=>0,'isDelete'=>1],['rejectUsernameID'=>$rejectUsernameID]) > 0){
            echo $this->db->last_query();die;
            $this->response['code'] = 100;
            $this->response['message'] = "Rejected Username SuccessFully. Removed.!";
        }else{
            $this->response['code'] = 101;
            $this->response['message'] = "Rejected Username Removed Error.!";
        }

        $this->response($this->response);
    }
    
}