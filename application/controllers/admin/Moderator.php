<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Moderator extends MY_Controller
    {
        private $userID = null;
        public function __construct() {
            parent::__construct();
    
            if(!$this->session->userdata('admin_loggedIN')) {
                redirect(site_url('admin/login'));
            }
            $this->userID = $this->session->userdata['admin_loggedIN']['adminID'];

            //$this->load->model('PostModel', 'post');
    
        }

        public function index(){
            $data['title'] = SITE_NAME. ' | Manage Moderator';
            $this->template->load('admin_layout', 'admin/moderator', $data);
        }

        public function getModeratorList()
        {
            $this->response['data']  = $this->CommonModel->get_all(TBL_MODERATOR_USER,["userID","name","email","password"]);
            $this->response($this->response);
        }
        
        public function add() {
            $data['title'] = SITE_NAME.' | Moderator | Add';
            $data['admin_row']='';
           $this->template->load('admin_layout', 'admin/moderator_add', $data);
        }
        /**
    * Check unique email
    *
    * @param string $email
    * @return boolean
    */
    public function check_unique_email($email) {
        $user_data = $this->CommonModel->get_row(TBL_MODERATOR_USER, 'userID', ['email' => $email]);
        if(empty($user_data)) {
            return true;
        } else {
            $this->form_validation->set_message('check_unique_email', 'This Email already exists. Please try another.');
            return FALSE;
        }
        
    }
    
    public function save_moderator() {
        try {
            // pr($_POST);pr($_FILES);
            if(!$this->input->post()) {
                throw new Exception("No input data!");
            }
            $this->form_validation->set_rules('email', 'Email Address', 'required|max_length[100]|valid_email|callback_check_unique_email|xss_clean');
            
            if($this->form_validation->run() == false) {
                throw new Exception(validation_errors());
            }
            $insertData['name'] = $this->input->post('name');
            $insertData['email'] = $this->input->post('email');
            $insertData['password'] = md5($this->input->post('password'));
            // pr($newspaper_data);
            $this->CommonModel->insert(TBL_MODERATOR_USER, $insertData);
            $response_array = [
                'code' => 100,
                'status' => 'success',
                'message' => "User has been added."
            ];
        } catch (Exception $e) {
            $response_array = [
                'code' => 101,
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
        $this->output->set_content_type('application/json')
        ->set_output(json_encode($response_array));
    }
    public function remove()
    {
        if(!$this->input->post('userID') || empty($this->input->post('userID'))){
            $this->response['message'] = "Please pass userID";
            $this->response($this->response);
        }

        $userID = $this->input->post('userID');

        if($this->CommonModel->update(TBL_MODERATOR_USER,['isActive'=>0,'isDelete'=>1],['userID'=>$userID]) > 0){
          $this->response['code'] = 100;
            $this->response['message'] = "User SuccessFully. Removed.!";
        }else{
            $this->response['code'] = 101;
            $this->response['message'] = "User Removed Error.!";
        }

        $this->response($this->response);
    }
    }
    
?>