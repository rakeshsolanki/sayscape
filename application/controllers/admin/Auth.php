<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Auth extends MY_Controller
    {
        public function __construct() {
            parent::__construct();
        }

        public function redirectLogin(){
            redirect(site_url('admin/login'));
        }

        public function index() {

            if($this->session->userdata('admin_loggedIN')) {
                redirect(site_url('admin/dashboard'));
            }

            $data['title'] = SITE_NAME. ' | Admin login';
            $this->template->load('admin_login_layout', 'admin/login', $data);
        }

        public function do_login() {
            try {
                if(!$this->input->post()) {
                    throw new Exception("Invalid parameters!");
                }
    
                $this->form_validation->set_rules('emailAddress', 'Email Address', 'required|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'required');
                if(!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }
    
                $user_data['email'] = $this->input->post('emailAddress');
                $user_data['password'] = md5($this->input->post('password'));
    
                $user_session_data = $this->CommonModel->get_row(TBL_ADMIN_USER, '*', $user_data);
                if(!$user_session_data) {
                    throw new Exception("Invalid Email or Password!");
                }
    
                $this->session->set_userdata('admin_loggedIN', $user_session_data);
    
                $response_array = [
                    'status' => 'success',
                    'message' => "User has logged in successfully!"
                ];
            } catch (Exception $e) {
                $response_array = [
                    'status' => 'error',
                    'message' => $e->getMessage()
                ];
            }
            $this->response($response_array);
        }

        public function logout() {
            $this->session->unset_userdata('admin_loggedIN');
            redirect(site_url('admin'));
        }
    }
    
?>