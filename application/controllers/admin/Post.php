<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Post extends MY_Controller
    {
        protected $response = array(
            'code'=>101,
            'message'=>'No Data Found',
            'data'=>[]
        );

        public function __construct() {
            parent::__construct();
    
            if(!$this->session->userdata('admin_loggedIN')) {
                redirect(site_url('admin/login'));
            }

            $this->load->model('PostModel', 'post');
    
        }

        public function index(){
            $data['title'] = SITE_NAME. ' | Post List';
            $this->template->load('admin_layout', 'admin/post', $data);
        }
        
        public function getPostList()
        {

            $this->response['code'] = 100;
            $this->response['message'] = "Get Post List SuccessFully..!";
            $this->response['data'] = $this->post->getAllPosts(0);
           // echo $this->db->last_query();die;
            if(!empty($this->response['data'])){
                foreach($this->response['data'] as &$value)
                {
                   // $media= array();
                    $result = $this->CommonModel->get_all('tblmedia',["mediaID","filename","type"],["comID" => $value['postID'] ,"isActive" => 1, "isDelete" => 0,"mediaRef"=>0]);
                    $value['totalMedia'] = count($result);
                    foreach ($result as $key => $res) {
                        $media[$key]['mediaID'] =  $res['mediaID'];
                        $media[$key]['type'] =  $res['type'];
                        $media[$key]['filename'] =  base_url(). "upload/postmedia/" . $res['filename'];
                    }
                    
                    $totalComments = $this->post->totalComments($value['postID']);
                    $totalShares = $this->post->totalShares($value['postID']);
                    $totalLikes = $this->post->totalLikes($value['postID']);
                    $totalDisLikes = $this->post->totalDisLikes($value['postID']);
                    
                    $value['media'] = $media;
                    $value['commentCount'] = $totalComments;
                    $value['reShareCount'] = $totalShares;
                    $value['likesCount'] = $totalLikes;
                    $value['disLikeCount'] = $totalDisLikes;
                    unset($media);
                }
            }
            
            $this->response($this->response);
        }

        public function getPostMediaListByPostID()
        {

            if(!$this->input->post('postID') || empty($this->input->post('postID'))){
                $this->response['message'] = "Please pass postID";
                $this->response($this->response);
            }
            $postID = $this->input->post('postID');
            $media = $this->CommonModel->get_all('tblmedia',["mediaID","IF(filename != '',CONCAT('".base_url()."upload/postmedia/',filename),'') as filename","type"],["comID" => $postID ,"isActive" => 1, "isDelete" => 0,"mediaRef"=>0]);
            $this->response['media']=$media;
            $this->response['code'] = 100;
            $this->response['message'] = "Media List";
            $this->response($this->response);
        }
        public function getReportPostList()
        {

            $this->response['code'] = 100;
            $this->response['message'] = "Get Report Post List SuccessFully..!";
            $this->response['data'] = $this->post->getAllReportPosts(0);
           // echo $this->db->last_query();die;
            if(!empty($this->response['data'])){
                foreach($this->response['data'] as &$value)
                {
                    $media= array();
                    $result = $this->CommonModel->get_all('tblmedia',["mediaID","filename","type"],["comID" => $value['postID'] ,"isActive" => 1, "isDelete" => 0,"mediaRef"=>0]);
                    foreach ($result as $key => $res) {
                        $media[$key]['mediaID'] =  $res['mediaID'];
                        $media[$key]['type'] =  $res['type'];
                        $media[$key]['filename'] =  base_url(). "upload/postmedia/" . $res['filename'];
                    }
                    
                    $totalComments = $this->post->totalComments($value['postID']);
                    $totalShares = $this->post->totalShares($value['postID']);
                    $totalLikes = $this->post->totalLikes($value['postID']);
                    $totalDisLikes = $this->post->totalDisLikes($value['postID']);
                    
                    $value['media'] = $media;
                    $value['commentCount'] = $totalComments;
                    $value['reShareCount'] = $totalShares;
                    $value['likesCount'] = $totalLikes;
                    $value['disLikeCount'] = $totalDisLikes;
                    unset($media);
                }
            }
            
            $this->response($this->response);
        }
        public function removePost()
        {
            if(!$this->input->post('postID') || empty($this->input->post('postID'))){
                $this->response['message'] = "Please pass postID";
                $this->response($this->response);
            }

            $postID = $this->input->post('postID');
    
            if($this->CommonModel->update(TBL_POST,['isActive'=>0,'isDelete'=>1],['postID'=>$postID]) > 0){
                $this->response['code'] = 100;
                $this->response['message'] = "Post SuccessFully. Removed.!";
            }else{
                $this->response['code'] = 101;
                $this->response['message'] = "Post Removed Error.!";
            }
    
            $this->response($this->response);
        }
        public function ajaxClearReportPost()
        {
            if(!$this->input->post('reportID') || empty($this->input->post('reportID'))){
                $this->response['message'] = "Please pass reportID";
                $this->response($this->response);
            }
            if(!isset($_POST['status']) || !in_array($this->input->post('status'),[0,1]) ){
                $this->response['message'] = "Please pass status";
                $this->response($this->response);
            }

            $reportID = $this->input->post('reportID');
            $status = $this->input->post('status');
    
            if($this->CommonModel->update(TBL_REPORT,['isDelete'=>$status],['reportID'=>$reportID]) > 0){
                $this->response['code'] = 100;
                $this->response['message'] = "Report SuccessFully Update";
            }else{
                $this->response['code'] = 101;
                $this->response['message'] = "Report Update Problem.!";
            }
    
            $this->response($this->response);
        }

    }
    
?>