<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Veteran extends MY_Controller
    {
        private $adminID = null;
        public function __construct() {
            parent::__construct();
    
            if(!$this->session->userdata('admin_loggedIN')) {
                redirect(site_url('admin/login'));
            }

            $this->adminID = $this->session->userdata['admin_loggedIN']['adminID'];

            $this->load->model('PostModel', 'post');
    
        }

        public function index(){
            $data['title'] = SITE_NAME. ' | Admin Dashboard';
            $this->template->load('admin_layout', 'admin/veteran', $data);
        }

        public function getVeteranList()
        {
            $veteranData = $this->post->getAdminVeteranPost($this->adminID);

            foreach($veteranData as &$value)
            {
                $media = array();
                $result = $this->CommonModel->get_all('tblmedia',["mediaID","filename","type"],["comID" => $value['postID'] ,"isActive" => 1, "isDelete" => 0,"mediaRef"=>0]);
                foreach ($result as $key => $res) {
                    $media[$key]['mediaID'] =  $res['mediaID'];
                    $media[$key]['type'] =  $res['type'];
                    $media[$key]['filename'] =  base_url(). "upload/postmedia/" . $res['filename'];
                }
    
                $value['media'] = $media;
                
                $totalComments = $this->post->totalComments($value['postID']);
                $totalShares = $this->post->totalShares($value['postID']);
                $totalLikes = $this->post->totalLikes($value['postID']);
                $totalDisLikes = $this->post->totalDisLikes($value['postID']);
    
                $value['commentCount'] = $totalComments;
                $value['reShareCount'] = $totalShares;
                $value['likesCount'] = $totalLikes;
                $value['disLikeCount'] = $totalDisLikes;
    
                unset($media);
            }
    
    
            if($veteranData === false){
                $this->response['message'] = "Not Allow";
            }else{
                $this->response['code'] = 100;
                $this->response['message'] = "Get Data SuccessFully";
                $this->response['data'] = $veteranData;
            }
            
            $this->response($this->response);
        }
        
        public function removeVeteranPost()
        {
            if(!$this->input->post('postID') || empty($this->input->post('postID'))){
                $this->response['message'] = "Please pass postID";
                $this->response($this->response);
            }

            $postID = $this->input->post('postID');
    
            if($this->CommonModel->update(TBL_POST,['isActive'=>0,'isDelete'=>1],['postID'=>$postID,'userID'=>$this->adminID]) > 0){
              echo $this->db->last_query();die;
                $this->response['code'] = 100;
                $this->response['message'] = "VeteranPost SuccessFully. Removed.!";
            }else{
                $this->response['code'] = 101;
                $this->response['message'] = "VeteranPost Removed Error.!";
            }
    
            $this->response($this->response);
        }

        public function addVeteranPost()
        {
            if(!$this->input->post('veteranPostContent') || empty($this->input->post('veteranPostContent'))){
                $this->response['message'] = "Please pass text";
                $this->response($this->response);
            }

            $arrPostInsert['userID'] = $this->adminID;
            $arrPostInsert['postType'] = 2;
            $arrPostInsert['text'] = $this->input->post('veteranPostContent');
          
            $last_post_id = $this->CommonModel->insert(TBL_POST,$arrPostInsert);
            if(isset($_POST['type']) && !empty($_POST['type']) && $_POST['type'] == 1)
            {
                $arrMediaInsert = array();

                $filesCount = count($_FILES['veteranPostImage']['name']);

                $files = $_FILES;

                for($i = 0; $i < $filesCount; $i++)
                {
                        $_FILES['veteranPostImage']['name']     = $files['veteranPostImage']['name'][$i];
                        $_FILES['veteranPostImage']['type']     = $files['veteranPostImage']['type'][$i];
                        $_FILES['veteranPostImage']['tmp_name'] = $files['veteranPostImage']['tmp_name'][$i];
                        $_FILES['veteranPostImage']['error']     = $files['veteranPostImage']['error'][$i];
                        $_FILES['veteranPostImage']['size']     = $files['veteranPostImage']['size'][$i];
                        
                        // File upload configuration
                        $config['upload_path'] = './upload/postmedia/';
                        $config['allowed_types'] = 'jpg|gif|png|jpeg|JPG|PNG';
                        $config['encrypt_name'] = TRUE;
                        $config['remove_spaces'] = TRUE;
                        $config['overwrite'] = FALSE;
                        
                        // Load and initialize upload library
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);

                        $arrMediaInsert[$i]['userID'] = $this->adminID;
                        $arrMediaInsert[$i]['comID'] = $last_post_id;
                        $arrMediaInsert[$i]['type'] = $_POST['type'];
                        
                        // Upload file to server
                        if($this->upload->do_upload('veteranPostImage')){
                            // Uploaded file data
                            $fileData = $this->upload->data();
                            $arrMediaInsert[$i]['filename'] = $fileData['file_name'];
                        }else{
                            print_r(array('error' => $this->upload->display_errors()));
                        }
                }

                //for( $i=0 ; $i < $total ; $i++ ) {  
                // foreach($_FILES as $key => $value){
                //     if($value['name'] == "veteranPostImage"){
                //        // print_r($key);
                //         //echo "<br/>";
                //         //$key = "veteranPostImage" . $i;
                //         if(isset($_FILES["veteranPostImage"]["name"]) && $_FILES["veteranPostImage"]["name"] != "")
                //         {
                //             $arrMediaInsert[$key]['userID'] = $this->adminID;
                //             $arrMediaInsert[$key]['comID'] = $last_post_id;
                //             $arrMediaInsert[$key]['type'] = $_POST['type'];
                            
                //             if (!$this->upload->do_upload($key)) {  
                //                 echo array('error' => $this->upload->display_errors());
                                
                //             } else {    
                //                 $arrMediaInsert[$key]['filename'] = $this->upload->data()["file_name"];
                //             }  
                //         }
                //     } 
                // }
                $last_media_id = $this->post->insertdata($arrMediaInsert);
            }
            elseif(isset($_POST['type']) && !empty($_POST['type']) && $_POST['type'] == 2)
            {
                if(!empty($_FILES['veteranPostVideo']['name']))
                {
                    $config['upload_path'] = './upload/postmedia/';
                    $config['allowed_types'] = 'mp4|3gpp|3gp|mp3';
                    $config['encrypt_name'] = TRUE;
                    $config['remove_spaces'] = TRUE;
                    $config['overwrite'] = FALSE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    $arrMediaInsert['userID'] = $this->adminID;
                    $arrMediaInsert['comID'] = $last_post_id;
                    $arrMediaInsert['type'] = $_POST['type'];

                    if (!$this->upload->do_upload('veteranPostVideo')) {
                        
                        $error = array('error' => $this->upload->display_errors());
                        
                    } else {
                        $fileData = array('upload_data' => $this->upload->data());
                        $arrMediaInsert['filename'] = $fileData["upload_data"]["file_name"];                        
                    }
                    $last_media_id = $this->CommonModel->insert(TBL_MEDIA,$arrMediaInsert);
                }
            }
            
            $this->response['code'] = 100;
            $this->response['message'] = getMessage('PostSuccess');
            
            $this->response($this->response);
        }
        
        public function getdeleteVeteranPostList()
        {

            $this->response['code'] = 100;
            $this->response['message'] = "Get Delete Vetaran Post List SuccessFully..!";
            $this->response['data'] = $this->post->getAllDeleteVeteranPost();
            // echo $this->db->last_query();die;
            if(!empty($this->response['data'])){
                foreach($this->response['data'] as &$value)
                {
                    $media= array();
                    $result = $this->CommonModel->get_all('tblmedia',["mediaID","filename","type"],["comID" => $value['postID'] ,"isActive" => 1, "isDelete" => 0,"mediaRef"=>0]);
                    foreach ($result as $key => $res) {
                        $media[$key]['mediaID'] =  $res['mediaID'];
                        $media[$key]['type'] =  $res['type'];
                        $media[$key]['filename'] =  base_url(). "upload/postmedia/" . $res['filename'];
                    }
                    
                    $totalComments = $this->post->totalComments($value['postID']);
                    $totalShares = $this->post->totalShares($value['postID']);
                    $totalLikes = $this->post->totalLikes($value['postID']);
                    $totalDisLikes = $this->post->totalDisLikes($value['postID']);
                    
                    $value['media'] = $media;
                    $value['commentCount'] = $totalComments;
                    $value['reShareCount'] = $totalShares;
                    $value['likesCount'] = $totalLikes;
                    $value['disLikeCount'] = $totalDisLikes;
                    unset($media);
                }
            }
            
            $this->response($this->response);
        }

        public function getReportedVeteranPostList()
        {

            $this->response['code'] = 100;
            $this->response['message'] = "Get Delete Vetaran Post List SuccessFully..!";
            $this->response['data'] = $this->post->getAllReportPosts(2);
            // echo $this->db->last_query();die;
            if(!empty($this->response['data'])){
                foreach($this->response['data'] as &$value)
                {
                    $media= array();
                    $result = $this->CommonModel->get_all('tblmedia',["mediaID","filename","type"],["comID" => $value['postID'] ,"isActive" => 1, "isDelete" => 0,"mediaRef"=>0]);
                    foreach ($result as $key => $res) {
                        $media[$key]['mediaID'] =  $res['mediaID'];
                        $media[$key]['type'] =  $res['type'];
                        $media[$key]['filename'] =  base_url(). "upload/postmedia/" . $res['filename'];
                    }
                    
                    $totalComments = $this->post->totalComments($value['postID']);
                    $totalShares = $this->post->totalShares($value['postID']);
                    $totalLikes = $this->post->totalLikes($value['postID']);
                    $totalDisLikes = $this->post->totalDisLikes($value['postID']);
                    
                    $value['media'] = $media;
                    $value['commentCount'] = $totalComments;
                    $value['reShareCount'] = $totalShares;
                    $value['likesCount'] = $totalLikes;
                    $value['disLikeCount'] = $totalDisLikes;
                    unset($media);
                }
            }
            
            $this->response($this->response);
        }

    }
    
?>