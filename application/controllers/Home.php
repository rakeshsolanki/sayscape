<?php

class Home extends MY_Controller
{
    //private $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('PostModel', 'post');
        $this->load->model('LoginModel', 'login');
        $this->load->model('UserModel', 'user');
        $this->load->model('GroupModel', 'group');
        $this->load->model('CommonModel');
        $this->load->model('MessageModel', 'message');
        $this->load->model('NotificationModel', 'notification');
        $this->load->library('emoji');
    }

    //Home Page
    public function index() {
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $data['userDetails'] = $this->user->getUserdetails($userID, $userID);
        $pageNumber = ($this->input->post('page')) ? $this->input->post('page'):'1';
        $postList = $this->post->getPosts($userID, 0, FALSE, $pageNumber, 3);
        // $postList = $this->post->getPosts($userID, 0);
        if (!empty($postList)) {
            foreach ($postList as &$value) {
                $value = $this->post->getPostResponseData($value, $userID);
            }
        }
        $data['postList'] = $postList;
        $data['watchUserID'] = $userID;
        $data['view'] = 'front/home';
        $this->render($data);
    }

    public function loadMorePost(){
        $userID = $this->session->userdata('userID');
        $usersDetails = $this->CommonModel->get_row(TBL_USER, ["isVeteran"], ["userID" => $userID]);
        

        $page = (isset($_POST['pageno']))?$this->input->post('pageno'):"";
        $type = (isset($_POST['type']))?$this->input->post('type'):"";
        $otherUserID = (isset($_POST['otherUserID']))?$this->input->post('otherUserID'):$userID;
        $postsdata = array();
        if($type=="veteran"){
            $postsdata = $this->post->getPosts($otherUserID, 2, false,$page,3);
        }
        elseif($type=="responder"){
            $postsdata = $this->post->getPosts($otherUserID, 4, false,$page,3);
        }
        elseif($type=="posts"){
            $postsdata = $this->user->getPostByUser($userID, $otherUserID,$page,3);
        }elseif($type=="comments"){
            $postsdata = $this->user->getCommentPostByUser($otherUserID,$page,3);
        }elseif($type=="media"){
            $postsdata = $this->user->getMediaPostByUser($otherUserID,$page,3);
        }elseif($type=="likes"){
            $postsdata = $this->user->getLikedPostByUser($otherUserID,$page,3);
        }elseif($type=="bookmark"){
            $postsdata = $this->post->getUserBookmarkPost($otherUserID,$page,3);
        }elseif($type=="trending"){
            $other = (isset($_POST['other']))?$this->input->post('other'):1;
            $postsdata = $this->post->getTrendingPost($otherUserID, $other, FALSE, $page, 3);
        }elseif($type=="hashtagTrending"){
            $hashtag = (isset($_POST['other']))?$this->input->post('other'):"";
            $searchFrom = ($usersDetails['isVeteran'] == 0) ? $usersDetails['isVeteran'] : 3;
            $postsdata = $this->post->getSearchPosts($userID, $searchFrom, $hashtag, $page, 3);
        }elseif($type=="searchPosts"){
            $keyword = (isset($_POST['other']))?$this->input->post('other'):"";
            $searchFrom = ($usersDetails['isVeteran'] == 0) ? $usersDetails['isVeteran'] : 3;
            $postsdata = $this->post->getSearchPosts($userID, $searchFrom, $keyword, $page, 3);
        }else{
            $postsdata = $this->post->getPosts($otherUserID, 0, false,$page,3);
        }
        if (!empty($postsdata)) {
            foreach ($postsdata as &$value) {
                $value = $this->post->getPostResponseData($value, $otherUserID);
            }
        }
        $data['watchUserID'] = $otherUserID;
        $data['postList'] = $postsdata;
        $data['count'] = $page;
        $data['view'] = 'front/loadPostList';
        $this->renderPartial($data);
    }

    // Veteran Feed
    public function veteran_feed(){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $this->data['title'] = "Veteran Feed";
        $userID = $this->session->userdata('userID');
        $userDetails = $this->user->getUserdetails($userID, $userID);
        $data['userDetails'] = $userDetails;
        if(isset($userDetails['isVeteran']) && $userDetails['isVeteran']==1){
            // $postsdata = $this->post->getPosts($userID, 2);
            $postsdata = $this->post->getPosts($userID, 2, false, 1, 3);
            if (!empty($postsdata)) {
                foreach ($postsdata as &$value) {
                    $value = $this->post->getPostResponseData($value, $userID);
                }
            }
            $data['postList'] = $postsdata;
        }
        $data['view'] = 'front/veteran_feed';
        $this->render($data);
        //$this->load->view('web/veteran_feed', $data);
    }

    public function idMeVerify(){
        echo "<pre>"; print_r($_POST);
        die();
    }

    //Another user timeline
    public function timeline($otherUsername="") {
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        // $otherUserID = $this->uri->segment(2);
        $otherUserID = $userID;
        if ($otherUsername!="") {
            $otherUserID = $this->user->getUserId($otherUsername);
            if($otherUserID==""){
                redirect(base_url());
            }
        }
        $data['otherUsername'] = $otherUsername;
        $data['otherUserID'] = $otherUserID;
        $data['userDetails'] = $this->user->getUserdetails($otherUserID, $userID);
        $data['watchUserID'] = $otherUserID;
        $pageNumber = ($this->input->post('page')) ? $this->input->post('page'):1;
        $type = $this->input->get_post('type');
        if($type=='about'){
            $data['pageType'] = 'about';
            $data['settingsData'] = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $otherUserID, 'isActive' => 1, 'isDelete' => 0]);
        }elseif($type=='comments'){
            $postList = $this->user->getCommentPostByUser($otherUserID,$pageNumber,3);
            if(!empty($postList)){
                foreach ($postList as &$value) {
                    $value = $this->post->getPostResponseData($value, $userID);
                }
            }
            $data['postList'] = $postList;
        }elseif($type=='likes'){
            $postList = $this->user->getLikedPostByUser($otherUserID,$pageNumber,3);
            if(!empty($postList)){
                foreach ($postList as &$value) {
                    $value = $this->post->getPostResponseData($value, $otherUserID);
                }
            }
            $data['postList'] = $postList;
        }elseif($type=='media'){
            $postList = $this->user->getMediaPostByUser($otherUserID,$pageNumber,3);
            if(!empty($postList)){
                foreach ($postList as &$value) {
                    $value = $this->post->getPostResponseData($value, $otherUserID);
                }
            }
            $data['postList'] = $postList;
        }else{
            $type = 'posts';
            $postList = $this->user->getPostByUser($userID, $otherUserID,$pageNumber,3);
            if(!empty($postList)){
                foreach ($postList as &$value) {
                    $value = $this->post->getPostResponseData($value, $userID);
                }
            }
            $data['postList'] = $postList;
        }
        $data['pageType'] = $type;
        $data['title'] = "Time-Line";
        $data['view'] = 'front/timeline';
        $this->render($data);
    }

    // for search post and user
    public function search(){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $usersDetails = $this->user->getUserdetails($userID, $userID);
        $pageNumber = 1;
        $keyword = !empty($this->input->get_post('keyword')) ? $this->input->get_post('keyword') : NULL;
        $searchFrom = ($usersDetails['isVeteran'] == 0) ? $usersDetails['isVeteran'] : 3;
        $postList = $this->post->getSearchPosts($userID, $searchFrom, $keyword, $pageNumber, 3);
        if (!empty($postList)) { 
            foreach ($postList as &$value) {
                $value = $this->post->getPostResponseData($value, $userID);
            }
        }
        $data['postList'] = $postList;
        $data['watchUserID'] = $userID;
        $data['view'] = 'front/search_result';
        $this->render($data);
    }

    public function searchUser(){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');
        $usersDetails = $this->user->getUserdetails($userID, $userID);
        $pageNumber = 1;
        $keyword = !empty($this->input->get_post('keyword')) ? $this->input->get_post('keyword') : NULL;
        $keyword = str_replace('@',"",$keyword);
        $userList = $this->user->searchuser($userID, $keyword);
        
        if (!empty($userList)) {
            foreach ($userList as &$user) {
                $user['privateProfileStatus'] = "0";
                $settingsData = $this->CommonModel->get_row(TBL_SETTINGS, ['*'], ['userID' => $user['userID'], 'isActive' => 1, 'isDelete' => 0]);
                if (!empty($settingsData)) {
                    $user['privateProfileStatus'] = (isset($settingsData['privateProfileStatus'])) ? $settingsData['privateProfileStatus'] . "" : "0";
                }
                $user['isFollowMe'] = "0";
                $checkUserFollowData = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $user['userID'], 'followerID' => $userID, 'status' => 1, 'isActive' => 1, 'isDelete' => 0]);
                if (!empty($checkUserFollowData)) {
                    $user['isFollowMe'] = "1";
                }
                // $result[] = $user;
            }
        }
        //echo "<pre>"; print_r($userList); die();
        $data['userList'] = $userList;
        $data['view'] = 'front/search_result';
        $this->render($data);
    }

    //User Register
    public function register()
    {
        if ($this->input->post('register')) {
            $userData = array(
                'name' => $this->input->post('name'),
                'deviceType' => 3,
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password'))
            );
            if (!empty($_FILES['picture']['name'])) {
                $config['upload_path'] = './upload/userprofile/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('picture')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $fileData = array('upload_data' => $this->upload->data());
                    $userData['profileImage'] = $fileData["upload_data"]["file_name"];
                }
            }
            $this->login->addData('tbluser', $userData);
            redirect(base_url('login'));
        }
        $this->data['title'] = "Login / Register";
        $this->load->view('web/login');
    }

    // Friends List
    public function friends_request(){
        $userID = $this->session->userdata('userID');
        $this->data['title'] = "Timeline Friends";
        if (!$userID) {
            redirect('login');
        }
        $data['requestList'] = $this->user->getFollowRequestList(['tuf.userID', 'tuf.followerID', 'tu.username', 'tu.name', 'tu.email', 'tu.phone', 'tu.profileImage'], ['tuf.followerID' => $userID, 'status' => 4]);
        $data['view'] = 'front/friends_request';
        $this->render($data);
    }

    public function timeline_friends()
    {
        $this->data['title'] = "Timeline Friends";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');

        $data['totalFollowing'] = count($this->user->getFollowingList($userID, $userID));
        $data['totalFollowers'] = count($this->user->getFollowersList($userID, $userID));
        $data['userDetails'] = $this->user->getUserdetails($userID, $userID);
        $data['notificationList'] = $this->notification->getNotificationList($userID);
        $data['followingName'] = $this->user->getFollowingList($userID, $userID);
        $data['followersName'] = $this->user->getFollowersList($userID, $userID);
        $data['grouplist'] = $this->group->getGroupList($userID);
        $data['requestList'] = $this->user->getFollowRequestList(['tuf.userID', 'tuf.followerID', 'tu.username', 'tu.name', 'tu.email', 'tu.phone', 'tu.profileImage'], ['tuf.followerID' => $userID, 'status' => 4]);
        $this->load->view('web/timeline-friends', $data);
    }

    //Hashtag Trending  Name
    public function hashtag_trending() {
        $this->data['title'] = "Hashtag Trending";
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $userID = $this->session->userdata('userID');

        $postsdata = $this->post->getPosts($userID, 0);
        if (!empty($postsdata)) {
            $hashtagArray = array();
            foreach ($postsdata as &$value) {
                preg_match_all('/#(\w+)/', $value['text'], $hashtagMatches);
                if (!empty($hashtagMatches[0])) {
                    foreach ($hashtagMatches[0] as $match) {
                        $hashtagArray[] = $match;
                    }
                }
            }
            // echo "<pre>"; print_r($hashtagArray); die();
            $newHashtagsArray = array_count_values($hashtagArray);
            arsort($newHashtagsArray);
            $hashtags = array_keys($newHashtagsArray);
            $data['hashtagTrendingList'] = $hashtags;
            //$this->load->view('web/hashtag_trending',$data);
        }
        $data['view'] = 'front/hashtag_trending';
        $this->render($data);
    }

    //Trending Post
    public function trending($time='0') {
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $pageNumber = 1;
        $this->data['title'] = "Trending Post";
        $userID = $this->session->userdata('userID');
        if ($time) {
            // echo "time"; die();
            $postList = $this->post->getTrendingPost($userID, $time, FALSE,$pageNumber,3);
            foreach ($postList as &$value) {
                $value = $this->post->getPostResponseData($value, $userID);
            }
            $data['postList'] = $postList;
        } else {
            // echo "not"; die();
            $postList = $this->post->getTrendingPost($userID, 0, FALSE,$pageNumber,3);
            foreach ($postList as &$value) {
                $value = $this->post->getPostResponseData($value, $userID);
            }
            $data['postList'] = $postList;
        }
        $data['view'] = 'front/trending';
        $this->render($data);
    }

    // get hashtag all post
    public function hashtagPost($hashtag="") {
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $this->data['title'] = "Hashtag Trending Post";
        $userID = $this->session->userdata('userID');
        $pageNumber = 1;
        $usersDetails = $this->CommonModel->get_row(TBL_USER, ["isVeteran"], ["userID" => $userID]);
        $searchFrom = ($usersDetails['isVeteran'] == 0) ? $usersDetails['isVeteran'] : 3;
        // $hashtag = $this->input->post('hashtag');
        $postList = $this->post->getSearchPosts($userID, $searchFrom, $hashtag,$pageNumber,3);
        foreach ($postList as &$value) {
            $value = $this->post->getPostResponseData($value, $userID);
        }
        $data['hashtag'] =$hashtag;
        $data['watchUserID'] =$userID;
        $data['postList'] = $postList;
        $data['view'] = 'front/hashtag_trending_post';
        $this->render($data);
    }

    // Bookmark
    public function bookmark(){
        //error_reporting(0);
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $this->data['title'] = "Bookmark";
        $userID = $this->session->userdata('userID');
        $postList = $this->post->getUserBookmarkPost($userID,1 ,3);
        if (!empty($postList)) {
            foreach ($postList as &$value) {
                $value = $this->post->getPostResponseData($value, $userID);
            }
        }
        //$post = array();
        $data['postList'] = $postList;
        $data['view'] = 'front/bookmark';
        $this->render($data);
    }
    
    // User Group Delete
    public function deleteGroup(){
        $arrGroupDelete['isDelete'] = 1;
        $arrGroupDelete['updatedDateTime'] = date('Y-m-d H:i:s');
        $delete_group = $this->CommonModel->update(TBL_GROUP, $arrGroupDelete, array('groupID' => $this->input->post('delete_id')));
    }

    // Delete Group Member
    public function deleteGroupMember(){
        $userGroupID = $this->input->post('remove_group_member');
        $arrMemberDelete['isDelete'] = 1;
        $arrMemberDelete['updatedDateTime'] = date('Y-m-d H:i:s');
        $remove_member = $this->CommonModel->update('tblusergroup', $arrMemberDelete, array('userGroupID' => $userGroupID));
    }

    // User Confirm Request
    public function confirmRequest(){
        $userID = $this->session->userdata('userID');
        $followerID = $this->input->post('confirm_data');

        $alreadyFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['followerID' => $userID, 'userID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);
        //echo "<pre>"; print_r($alreadyFollowUser); die();
        $arrGroupUpdate['status'] = 1;
        $arrGroupUpdate['updatedDateTime'] = date('Y-m-d H:i:s');
        $delete_group = $this->CommonModel->update(TBL_USER_FOLLOWER, $arrGroupUpdate, array('userFollowerID' => $alreadyFollowUser['userFollowerID']));
    }

    // User Delete Request
    public function deleteRequest()
    {
        $userID = $this->session->userdata('userID');
        $followerID = $this->input->post('delete_data');
        $alreadyFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['followerID' => $userID, 'userID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);
        //echo "<pre>"; print_r($alreadyFollowUser); die();
        $arrGroupUpdate['status'] = 0;
        $arrGroupUpdate['updatedDateTime'] = date('Y-m-d H:i:s');
        $delete_group = $this->CommonModel->update(TBL_USER_FOLLOWER, $arrGroupUpdate, array('userFollowerID' => $alreadyFollowUser['userFollowerID']));
        echo $delete_group;
    }

    // User Follow 
    public function follow(){
        $userID = $this->session->userdata('userID');
        $followerID = $this->input->post('follow');

        $alreadyFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['followerID' => $followerID, 'userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        //echo "<pre>"; print_r($alreadyFollowUser); die();
        
        $arrInsert['userID'] = $userID;
        $arrInsert['followerID'] = $followerID;
        $arrInsert['status'] = 1;
        $arrInsert['updatedDateTime'] = date('Y-m-d H:i:s');
        $arrInsert['isActive '] = 1;
        $arrInsert['createdBy'] = $userID;
        $result = $this->CommonModel->insert(TBL_USER_FOLLOWER, $arrInsert);
        if (!empty($result)) {
            echo true;
        } else {
            echo false;
        }
    }

    //  User Unfollow
    public function unfollow(){
        $userID = $this->session->userdata('userID');
        $followerID = $this->input->post('unfollow');

        $alreadyFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['followerID' => $followerID, 'userID' => $userID, 'isActive' => 1, 'isDelete' => 0]);
        //echo "<pre>"; print_r($alreadyFollowUser); die();
        $arrInsert['userID'] = $userID;
        $arrInsert['followerID'] = $followerID;
        $arrInsert['status'] = 0;
        $arrInsert['updatedDateTime'] = date('Y-m-d H:i:s');
        $arrInsert['isActive'] = 1;
        $arrInsert['createdBy'] = $userID;
        $this->CommonModel->delete(TBL_USER_FOLLOWER, ['userFollowerID' => $alreadyFollowUser['userFollowerID']]);
        if (!empty($result)) {
            echo true;
        } else {
            echo false;
        }
    }

    // Create Post
    public function createPost(){
        // echo "<pre>"; print_r($_FILES);
        // echo "<pre>"; print_r($_POST); die();
        $userID = $this->session->userdata('userID');
        $userDetails = $this->user->getUserdetails($userID, $userID);
        $savePostData = array();
        if (isset($_POST['gif']) && $_POST['gif']!="") {
            $savePostData['gif'] = $this->input->post('gif');
        }
        $redirectUrl = base_url();
        if (isset($_POST['postType']) && $_POST['postType']==2) {
            $redirectUrl = base_url('veteran-feed');
            $savePostData['postType'] = $this->input->post('postType'); 
        }
        if (isset($_POST['postType']) && $_POST['postType']==4) {
            $redirectUrl = base_url('responder-feed');
            $savePostData['postType'] = $this->input->post('postType'); 
        }
        if (!empty($this->input->post('expiryDateTime'))) {
            $savePostData['isexpireOn'] = 1;
            $savePostData['expiryDateTime'] = $this->input->post('expiryDateTime');
        }
        //post Text
        if ($this->input->post('text')) {
            $savePostData['text'] = $this->emoji->Encode($this->input->post('text'));
        }
        $isMedia = 0;
        if ((!empty($_FILES['video']) && $_FILES['video']['error']==0) || (!empty($_FILES['picture']) && $_FILES['picture']['error'][0]==0)) {
            $isMedia = 1;
        }
        // echo "<pre>"; print_r($savePostData);
        // echo "<pre>"; print_r($_FILES);
        // echo "<pre>"; print_r($_POST); die();
        if(!empty($savePostData) || $isMedia==1){
            $savePostData['userID'] = $userID;
            $savePostData['createdDateTime'] = date('Y-m-d H:i:s');
            $savePostData['updatedDateTime'] = date('Y-m-d H:i:s');
            $last_post_id = $this->CommonModel->insert(TBL_POST, $savePostData);
        }

        if(isset($last_post_id) && $last_post_id!=""){
            // for send notificatio to mantion user
            if ($this->input->post('text') != "") {
                preg_match_all('/(^|\s)(@\w+)/', $this->input->post('text'), $result);
                $mentionsUsers = end($result);
                if (!empty($mentionsUsers)) {
                    foreach ($mentionsUsers as $value) {
                        $mentionUser = $this->CommonModel->get_row(TBL_USER, ["userID"], ["username" => ltrim($value, '@')]);
                        $this->sendNotification(array(
                            "type" => "post",
                            "title" => "SayScape",
                            "desc" => ucfirst($userDetails['username']) . " mentioned you in a post.",
                            "targetID" => $last_post_id,
                            "mentionUserID" => $mentionUser['userID'],
                            "senderID"=>$userID,
                            "notifyType"=>"postMention"
                        ));
                    }
                }
            }
            // for send notificatio to subscriber user
            $getSubscribers = $this->CommonModel->get_all(TBL_POST_SUBSCRIBE, ['*'], ['followerID' => $userID, 'status' => 1]);
            if (!empty($getSubscribers)) {
                $blockUsers = array();
                $allblockList = $this->user->getUserBlockedList($userID);
                if (!empty($allblockList)) {
                    foreach ($allblockList as $user) {
                        if (isset($user['userID']) && $user['userID'] != "") {
                            $blockUsers[] = $user['userID'];
                        }
                    }
                }
                if(!empty($getSubscribers)){
                    foreach ($getSubscribers as $subscribe) {
                        $checkBlockUserData = $this->CommonModel->get_row('tblblockuser', ["userID", "blockUserID"], ["userID" => $subscribe['userID'], "blockUserID" => $userID]);
                        if (empty($checkBlockUserData) && !in_array($subscribe['userID'], $blockUsers)) {
                            $this->sendNotification(array(
                                "type" => "post",
                                "title" => "SayScape",
                                "desc" => ucfirst($userDetails['username']) . " add new post.",
                                "targetID" => $last_post_id,
                                "subscriberID" => $subscribe['userID'],
                                "postID" => $last_post_id,
                                "senderID"=>$userID,
                                "notifyType"=>"newPost"
                            ));
                        }
                    }
                }
            }

            //echo "<pre>"; print_r($_FILES); die();
            //post text/Images
            if (!empty($_FILES['picture']['name'])) {
                $imgData = array(
                    'userID' => $userID,
                    'comID' => $last_post_id,
                    'type' => 1,
                    'isActive' => 1,
                    'createdDateTime' => date('Y-m-d H:i:s'),
                    'updatedDateTime' => date('Y-m-d H:i:s')
                );
                if (!empty($_FILES['picture']['name'])) {
                    foreach ($_FILES['picture']['name'] as $key => $imageData) {
                        if ($_FILES['picture']['error'][$key] == 0) {
                            $temp_file = $_FILES['picture']['tmp_name'][$key];
                            $img_name = "img" . mt_rand(100, 999999) . time();
                            $path = $_FILES['picture']['name'][$key];
                            $ext = pathinfo($path, PATHINFO_EXTENSION);
                            $imgData['filename'] = $img_name . "." . $ext;
                            $url = POST_IMAGE_PATH . $imgData['filename'];
                            move_uploaded_file($temp_file, $url);
                        }
                        if(!empty($imgData['filename']) && $imgData['filename']!=""){
                            $this->CommonModel->insert(TBL_MEDIA, $imgData);
                        }
                    }
                }
            }
            //post text/Video
            if (!empty($_FILES['video']['name'])) {
                $videoData = array(
                    'userID' => $userID,
                    'comID' => $last_post_id,
                    'type' => 2,
                    'isActive' => 1,
                    'createdDateTime' => date('Y-m-d H:i:s'),
                    'updatedDateTime' => date('Y-m-d H:i:s')
                );
                if ($_FILES['video']['error'] == 0) {
                    $temp_file = $_FILES['video']['tmp_name'];
                    $img_name = "video" . mt_rand(100, 999999) . time();
                    $path = $_FILES['video']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $videoData['filename'] = $img_name . "." . $ext;
                    $url = POST_IMAGE_PATH . $videoData['filename'];
                    move_uploaded_file($temp_file, $url);

                    if($this->input->post('video_thumb') != ""){
                        $videoimage = $this->input->post("video_thumb");
                        $videoimage = str_replace('data:image/png;base64,', '', $videoimage);
	                    $videoimage = str_replace(' ', '+', $videoimage);
                        $videoimage = base64_decode($videoimage);
                        $video_thumb_name = 'thumb'.md5(uniqid(rand(), true)). '.' . 'png';
                        $path = POST_IMAGE_PATH;
                        file_put_contents($path . $video_thumb_name, $videoimage);
                        $videoData['video_thumb'] = $video_thumb_name;
                    }
                    if(isset($videoData['filename']) && $videoData['filename']!=""){
                        $this->CommonModel->insert(TBL_MEDIA, $videoData);
                    }
                }
            }
        }
        // redirect($redirectUrl);
    }

    // User Question Poll
    public function savePollPost(){
        // echo "<pre>"; print_r($_POST); die();
        $userID = $this->session->userdata('userID');
        if ($this->input->post('question')) {
            $arrPollInsert['userID'] = $userID;
            $arrPollInsert['text'] = $this->input->post('question');
            $arrPollInsert['isPollPost'] = 1;
            $arrPollInsert['createdDateTime'] = date('Y-m-d H:i:s');
            $arrPollInsert['updatedDateTime'] = date('Y-m-d H:i:s');
            $last_post_id = $this->CommonModel->insert(TBL_POST, $arrPollInsert);
            
            $option = $this->input->post('option');
            if (!empty($option)) {
                foreach ($option as $opt) {
                    $arrOptionInsert['userID'] = $userID;
                    $arrOptionInsert['postID'] = $last_post_id;
                    $arrOptionInsert['text'] = $opt;
                    $arrOptionInsert['createdDateTime'] = date('Y-m-d H:i:s');
                    $arrOptionInsert['updatedDateTime'] = date('Y-m-d H:i:s');
                    $this->CommonModel->insert(TBL_POST_POLL_OPTIONS, $arrOptionInsert);
                }
            }
            // redirect(base_url());
        }
    }

    public function edit_post_data(){
        // $currentUrl = $this->uri->segment(2);
        $userID = $this->session->userdata('userID');
        $postID = $this->input->post('postID');
        $posttype = $this->input->post('posttype');
        $postData = $this->post->getParticularPost($userID, $postID);
        foreach ($postData as &$value) {
            $value = $this->post->getPostResponseData($value, $userID);
        }
        // echo '<pre>';print_r($postData);die;
        $data['postData'] = (isset($postData[0]))?$postData[0]:"";
        $data['view'] = 'front/edit_post_model';
        // $this->load->view('front/edit_post_model', $data);
        $this->renderPartial($data);
    }
    
    public function editPost(){
        // echo "<pre>"; print_r($_FILES); die();
        // echo "<pre>"; print_r($_POST); die();
        $userID = $this->session->userdata('userID');
        $postID = $this->input->post('postID');
        if (isset($_POST['expiryDateTime']) || !empty($_POST['expiryDateTime']) || isset($_POST['isexpireOn']) || !empty($_POST['isexpireOn'])) {
            $arrPostEdit['expiryDateTime'] = $_POST['expiryDateTime'];
            $arrPostEdit['isexpireOn'] = $_POST['isexpireOn'];
            $arrPostEdit['isPermanent'] = 0;
        }
        $arrPostEdit['text'] = $this->emoji->Encode($this->input->post('text'));
        $arrPostEdit['updatedDateTime'] = date('Y-m-d H:i:s');
        $update_post = $this->CommonModel->update(TBL_POST, $arrPostEdit, array('postID' => $_POST['postID']));
        /**Get Post Media to Hard Delete*/
        $mediaList = $this->CommonModel->get_all(TBL_MEDIA, ['filename', 'mediaID'], ['userID' => $userID, 'comID' => $this->input->post('postID'), 'mediaRef' => 0]);
        if (!empty($mediaList)) {
            $arrFilename = array();
            $arrFilename = array_column($mediaList, 'filename');
            foreach ($arrFilename as $valuefile) {
                $filename = POST_IMAGE_PATH . $valuefile;
                if (file_exists($filename)) {
                    unlink($filename);
                }
            }
            $this->CommonModel->delete(TBL_MEDIA, ['userID' => $userID, 'comID' => $this->input->post('postID'), 'mediaRef' => 0]);
        }

        $postType = 0;
        $postType = (isset($_POST['image']))?1:$postType;
        $postType = (isset($_POST['video']))?2:$postType;
        $arrMediaEdit = array();
        if (isset($postType) && !empty($postType) && $postType == 1) {
            //Modify By Hetal
            foreach ($_FILES['picture']['name'] as $key => $image) {
                if ($_FILES['picture']['error'][$key] == 0) {
                    $temp_file = $_FILES['picture']['tmp_name'][$key];
                    $path = $_FILES['picture']['name'][$key];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $imgUrl = microtime() . "." . $ext;
                    $fileurl = FCPATH . POST_IMAGE_PATH . $imgUrl;
                    move_uploaded_file($temp_file, $fileurl);

                    $arrInsert = array();
                    $arrInsert['userID'] = $userID;
                    $arrInsert['comID'] = $_POST['postID'];
                    $arrInsert['type'] = $postType;
                    $arrInsert['filename'] = $imgUrl;
                    array_push($arrMediaEdit, $arrInsert);
                }
            }

            if (!empty($arrMediaEdit)){
                $last_media_id = $this->post->insertdata($arrMediaEdit);
            }
        } elseif (isset($postType) && !empty($postType) && $postType == 2) {
            if (!empty($_FILES['video']['name'])) {
                $config['upload_path'] = './upload/postmedia/';
                $config['allowed_types'] = 'mp4|3gp|mp3';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $config['max_size'] = '5000000';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $arrMediaEdit['userID'] = $userID;
                $arrMediaEdit['comID'] = $_POST['postID'];
                $arrMediaEdit['type'] = $postType;
                $arrMediaEdit['createdDateTime'] = date('Y-m-d H:i:s');
                $arrMediaEdit['updatedDateTime'] = date('Y-m-d H:i:s');
                if (!$this->upload->do_upload('video')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $fileData = array('upload_data' => $this->upload->data());
                    $arrMediaEdit['filename'] = $fileData["upload_data"]["file_name"];
                }
                if (!empty($arrMediaEdit)){
                    $last_media_id = $this->CommonModel->insert(TBL_MEDIA, $arrMediaEdit);
                }  
            }
        }
        // redirect(base_url());
    }

    // for save user comment
    public function savePostComment(){
        $response = array('status'=>0,'error'=>'');
        // echo "<pre>"; print_r($_FILES);
        // echo "<pre>"; print_r($_POST); die();
        $userID = $this->session->userdata('userID');
        $userDetails = $this->user->getUserdetails($userID, $userID);
        if ($this->input->post('comment')) {
            $postID = $this->input->post('postID');
            $commentData['userID'] = $userID;
            $commentData['postID'] = $postID;
            $commentData['createdDateTime'] = date('Y-m-d H:i:s');
            $commentData['updatedDateTime'] = date('Y-m-d H:i:s');
            if ($this->input->post('commentID')) {
                $commentData['parentCommentID'] = $this->input->post('commentID');
            }
            if ($this->input->post('postShareID')) {
                $commentData['postShareID'] = $this->input->post('postShareID');
            }
            $commentData['text'] = $this->emoji->Encode($this->input->post('comment'));
            if (!empty($_FILES['commentImage']['name'])) {
                $config['upload_path'] = './upload/postmedia/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['encrypt_name'] = TRUE;
                $config['remove_spaces'] = TRUE;
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('commentImage')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $fileData = array('upload_data' => $this->upload->data());
                    $commentData['commentImage'] = $fileData["upload_data"]["file_name"];
                }
            } 
            //echo "<pre>"; print_r($commentData); die();
            $last_comment_id = $this->CommonModel->insert(TBL_POST_COMMENT, $commentData);
            $this->sendNotification(array(
                "type" => "post",
                "title" => "SayScape",
                "desc" => ucfirst($userDetails['username']) . " has comment on your post",
                "targetID" => $postID,
                "senderID"=>$userID,
                "notifyType"=>"postComment"
            ));
            $response['status'] = 1;
            $response['totalCommentCount'] = $this->post->totalComments($postID, $userID);
        }
        echo json_encode($response);
    }

    // Create User Group
    public function createGroup()
    {
        $userID = $this->session->userdata('userID');
        if ($this->input->post('submit')) {
            $arrInsert = array();
            $arrInsert['userID'] = $userID;
            $arrInsert['groupName'] = $this->input->post('name');
            $arrInsert['createdDateTime'] = date('Y-m-d H:i:s');
            $arrInsert['updatedDateTime'] = date('Y-m-d H:i:s');

            if (!empty($_FILES['picture']['name'])) {
                if ($_FILES['picture']['error'] == 0) {
                    $temp_file = $_FILES['picture']['tmp_name'];
                    $img_name = "picture" . mt_rand(100, 999999) . time();
                    $path = $_FILES['picture']['name'];
                    $ext = pathinfo($path, PATHINFO_EXTENSION);
                    $arrInsert['groupProfile'] = $img_name . "." . $ext;
                    $url = GROUP_PROFILE_UPLOAD_PATH . $arrInsert['groupProfile'];
                    move_uploaded_file($temp_file, $url);
                }
            }
            $groupID = $this->CommonModel->insert('tblgroup', $arrInsert);


            $groupMember = array();
            $groupMember = array(
                'groupID' => $groupID,
                'createdDateTime' => date('Y-m-d H:i:s'),
                'updatedDateTime' => date('Y-m-d H:i:s')
            );
            $memberID = $this->input->post('memberID');
            if (!empty($memberID)) {
                foreach ($memberID as $member) {
                    $groupMember['userID'] = $member;
                    $this->CommonModel->insert('tblusergroup', $groupMember);
                }
            }
            redirect(base_url('groups'));
        }

    }
    
    // Like Post
    public function likePost() {
        // echo "<pre>"; print_r($_POST); die();
        $response = array('status'=>0,'error'=>'');
        
        $userID = $this->session->userdata('userID');
        $userData = $this->CommonModel->get_row(TBL_USER, ["userID", "username", "name"], ["userID" => $userID]);
        $postID = $this->input->post('postID');
        // $otherUserID = (isset($_POST['userid']))?$this->input->post('userid'):$userID;
        $postShareID = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;
        if($postID!=""){
            $saveLikeData['userID'] = $userID;
            $saveLikeData['postID'] = $postID;
            $saveLikeData['postShareID'] = $postShareID;
            $saveLikeData['isLiked'] = 1;
            $likeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $postID, "isLiked" => 1, "isDelete" => 0]);
            if (!empty($likeResult)) {
                if ($likeResult['isActive'] == 1) {
                    $isActive = 0;
                }
                if ($likeResult['isActive'] == 0) {
                    $isActive = 1;
                }
                if ($this->CommonModel->update(TBL_POST_LIKE, ['isActive' => $isActive], ['postLikeID' => $likeResult['postLikeID']]) > 0) {
                    $response['status'] = 1;
                    $response['totalLikeCount'] = $this->post->totalLikes($postID, $userID);
                    $response['likestatus'] = $isActive;
                } else {
                    $response['error'] = getMessage("PostLikeRemoveError");
                }
            } else {
                if ($this->CommonModel->insert(TBL_POST_LIKE, $saveLikeData) > 0) {
                    $checkOwnPost = $this->CommonModel->get_row(TBL_POST, ['*'], ['userID' => $userID, 'postID' => $_POST['postID'], 'isActive' => 1, 'isDelete' => 0]);
                    if (empty($checkOwnPost)) {
                        if ($saveLikeData['postShareID'] > 0) {
                            $this->sendNotification(array(
                                "type" => "post",
                                "title" => "SayScape",
                                "desc" => ucfirst($userData['username']) . " has liked your reshare post.",
                                "targetID" => $saveLikeData['postID'],
                                "postShareID" => $saveLikeData['postShareID'],
                                "senderID"=>$userID,
                                "notifyType"=>"resharePostLike"
                            ));
                        } else {
                            $this->sendNotification(array(
                                "type" => "post",
                                "title" => "SayScape",
                                "desc" => ucfirst($userData['username']) . " has liked your post.",
                                "targetID" => $saveLikeData['postID'],
                                "senderID"=>$userID,
                                "notifyType"=>"postLike"
                            ));
                        }
                    }
                    $response['status'] = 1;
                    $response['totalLikeCount'] = $this->post->totalLikes($postID, $userID);
                    $response['likestatus'] = 1;
                } else {
                    $response['error'] = getMessage("PostLikeError");
                }
            }
        }
        
        echo json_encode($response);
    }

    // DisLike Post
    public function disLikePost() {
        // echo "<pre>"; print_r($_POST); die();
        $response = array('status'=>0,'error'=>'');
        
        $userID = $this->session->userdata('userID');
        $userData = $this->CommonModel->get_row(TBL_USER, ["userID", "username", "name"], ["userID" => $userID]);
        $postID = $this->input->post('postID');
        // $otherUserID = (isset($_POST['userid']))?$this->input->post('userid'):$userID;
        $postShareID = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;
        if($postID!=""){
            $saveLikeData['userID'] = $userID;
            $saveLikeData['postID'] = $postID;
            $saveLikeData['postShareID'] = $postShareID;
            $saveLikeData['isLiked'] = 0;
            $likeResult = $this->CommonModel->get_row_com(TBL_POST_LIKE, ['postLikeID', 'isLiked', 'isActive'], ['userID' => $userID, 'postID' => $postID, "isLiked" => 0, "isDelete" => 0]);
            if (!empty($likeResult)) {
                if ($likeResult['isActive'] == 1) {
                    $isActive = 0;
                }
                if ($likeResult['isActive'] == 0) {
                    $isActive = 1;
                }
                if ($this->CommonModel->update(TBL_POST_LIKE, ['isActive' => $isActive], ['postLikeID' => $likeResult['postLikeID']]) > 0) {
                    $response['status'] = 1;
                    $response['totalLikeCount'] = $this->post->totalLikes($postID, $userID);
                    $response['dislikestatus'] = $isActive;
                } else {
                    $response['error'] = getMessage("PostLikeRemoveError");
                }
            } else {
                if ($this->CommonModel->insert(TBL_POST_LIKE, $saveLikeData) > 0) {
                    $checkOwnPost = $this->CommonModel->get_row(TBL_POST, ['*'], ['userID' => $userID, 'postID' => $_POST['postID'], 'isActive' => 1, 'isDelete' => 0]);
                    if (empty($checkOwnPost)) {
                        if ($saveLikeData['postShareID'] > 0) {
                            $this->sendNotification(array(
                                "type" => "post",
                                "title" => "SayScape",
                                "desc" => ucfirst($userData['username']) . " has disliked your reshare post.",
                                "targetID" => $saveLikeData['postID'],
                                "postShareID" => $saveLikeData['postShareID'],
                                "senderID"=>$userID,
                                "notifyType"=>"resharePostDislike"
                            ));
                        } else {
                            $this->sendNotification(array(
                                "type" => "post",
                                "title" => "SayScape",
                                "desc" => ucfirst($userData['username']) . " has disliked your post.",
                                "targetID" => $saveLikeData['postID'],
                                "senderID"=>$userID,
                                "notifyType"=>"postDislike"
                            ));
                        }
                    }
                    $response['status'] = 1;
                    $response['totalLikeCount'] = $this->post->totalLikes($postID, $userID);
                    $response['dislikestatus'] = 1;
                } else {
                    $response['error'] = getMessage("PostLikeError");
                }
            }
        }
        echo json_encode($response);
    }

    // like Post Comment
    public function likePostComment(){
        // echo "<pre>"; print_r($_POST); die();
        $response = array('status'=>0,'error'=>'');
        
        $userID = $this->session->userdata('userID');
        $userData = $this->CommonModel->get_row(TBL_USER, ["userID", "username", "name"], ["userID" => $userID]);
        $postID = $this->input->post('postID');
        $postShareID = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;
        $postCommentID = $this->input->post('postCommentID');
        
        $commentLikeData['userID'] = $userID;
        $commentLikeData['postID'] = $postID;
        $commentLikeData['postCommentID'] = $postCommentID;
        $commentLikeData['postShareID'] = $postShareID;
        $commentLikeData['isLiked'] = 1;
        $likeResult = $this->CommonModel->get_row_com(TBL_POST_COMMENT_LIKE, ['postcommentLikeID', 'isLiked', 'isActive'], ['userID' => $commentLikeData['userID'], 'postID' => $commentLikeData['postID'], 'postCommentID' => $commentLikeData['postCommentID'], "isLiked" => 1, "isDelete" => 0]);
        if (!empty($likeResult)) {
            if ($likeResult['isActive'] == 1) {
                $isActive = 0;
            }
            if ($likeResult['isActive'] == 0) {
                $isActive = 1;
            }
            if ($this->CommonModel->update(TBL_POST_COMMENT_LIKE, ['isActive' => $isActive], ['postcommentLikeID' => $likeResult['postcommentLikeID']]) > 0) {
                $response['status'] = 1;
                $response['totalLikeCount'] = $this->post->totalPostCommentLikes($postID, $postCommentID, $userID);
                $response['likestatus'] = $isActive;
            } else {
                $response['error'] = getMessage("PostLikeRemoveError");
            }
        } else {
            if ($postcommentLikeID = $this->CommonModel->insert(TBL_POST_COMMENT_LIKE, $commentLikeData)) {
                if ($commentLikeData['postShareID'] > 0) {
                    $this->sendNotification(array(
                        "type" => "post",
                        "title" => "SayScape",
                        "desc" => ucfirst($userData['username']) . " has liked your reshare comment.",
                        "targetID" => $commentLikeData['postID'],
                        "postShareID" => $commentLikeData['postShareID'],
                        "senderID"=>$userID,
                        "notifyType"=>"likeResharePostComment"
                    ));
                } else {
                    $this->sendNotification(array(
                        "type" => "post",
                        "title" => "SayScape",
                        "desc" => ucfirst($userData['username']) . " has liked your comment.",
                        "targetID" => $commentLikeData['postID'],
                        "postCommentID" => $commentLikeData['postCommentID'],
                        "userID" => $commentLikeData['userID'],
                        "senderID"=>$userID,
                        "notifyType"=>"likePostComment"
                    ));
                }
                $response['status'] = 1;
                $response['totalLikeCount'] = $this->post->totalPostCommentLikes($postID, $postCommentID, $userID);
                $response['likestatus'] = 1;
            } else {
                $response['error'] = getMessage("PostLikeError");
            }
        }
        echo json_encode($response);
    }

    // dislike Post Comment
    public function dislikePostComment(){
        // echo "<pre>"; print_r($_POST); die();
        $response = array('status'=>0,'error'=>'');
        
        $userID = $this->session->userdata('userID');
        $userData = $this->CommonModel->get_row(TBL_USER, ["userID", "username", "name"], ["userID" => $userID]);
        $postID = $this->input->post('postID');
        $postShareID = $this->input->post('postShareID') ? $this->input->post('postShareID') : 0;
        $postCommentID = $this->input->post('postCommentID');
        
        $commentLikeData['userID'] = $userID;
        $commentLikeData['postID'] = $postID;
        $commentLikeData['postCommentID'] = $postCommentID;
        $commentLikeData['postShareID'] = $postShareID;
        $commentLikeData['isLiked'] = 0;
        $likeResult = $this->CommonModel->get_row_com(TBL_POST_COMMENT_LIKE, ['postcommentLikeID', 'isLiked', 'isActive'], ['userID' => $commentLikeData['userID'], 'postID' => $commentLikeData['postID'], 'postCommentID' => $commentLikeData['postCommentID'], "isLiked" => 0, "isDelete" => 0]);
        if (!empty($likeResult)) {
            if ($likeResult['isActive'] == 1) {
                $isActive = 0;
            }
            if ($likeResult['isActive'] == 0) {
                $isActive = 1;
            }
            if ($this->CommonModel->update(TBL_POST_COMMENT_LIKE, ['isActive' => $isActive], ['postcommentLikeID' => $likeResult['postcommentLikeID']]) > 0) {
                $response['status'] = 1;
                $response['totalDislikeCount'] = $this->post->totalPostCommentLikes($postID, $postCommentID, $userID,0);
                $response['dislikestatus'] = $isActive;
            } else {
                $response['error'] = getMessage("PostLikeRemoveError");
            }
        } else {
            if ($postcommentLikeID = $this->CommonModel->insert(TBL_POST_COMMENT_LIKE, $commentLikeData)) {
                if ($commentLikeData['postShareID'] > 0) {
                    $this->sendNotification(array(
                        "type" => "post",
                        "title" => "SayScape",
                        "desc" => ucfirst($userData['username']) . " has disliked your reshare comment.",
                        "targetID" => $commentLikeData['postID'],
                        "postShareID" => $commentLikeData['postShareID'],
                        "senderID"=>$userID,
                        "notifyType"=>"reshareDislikeComment"
                    ));
                } else {
                    $this->sendNotification(array(
                        "type" => "post",
                        "title" => "SayScape",
                        "desc" => ucfirst($userData['username']) . " has disliked your comment.",
                        "targetID" => $commentLikeData['postID'],
                        "postCommentID" => $commentLikeData['postCommentID'],
                        "userID" => $commentLikeData['userID'],
                        "senderID"=>$userID,
                        "notifyType"=>"dislikeComment"
                    ));
                }
                $response['status'] = 1;
                $response['totalDislikeCount'] = $this->post->totalPostCommentLikes($postID, $postCommentID, $userID,0);
                $response['dislikestatus'] = 1;
            } else {
                $response['error'] = getMessage("PostLikeError");
            }
        }
        echo json_encode($response);
    }

    // User Post Share
    public function sharePost(){
        $userID = $this->session->userdata('userID');
        if ($this->input->post('postID')) {
            $arrInsert['userID'] = $userID;
            $arrInsert['postID'] = $this->input->post('postID');
            if($this->input->post('text')){
                $arrInsert['post'] = $this->emoji->Encode($this->input->post('text'));
            }
            if($this->input->post('emoji')){
                $arrInsert['emoji'] = $this->input->post('emoji');
            }
            $arrInsert['status'] = 1;
            $arrInsert['createdDateTime'] = date('Y-m-d H:i:s');
            $arrInsert['updatedDateTime'] = date('Y-m-d H:i:s');
            $postShareID = $this->CommonModel->insert(TBL_POST_SHARE, $arrInsert);
            // redirect(base_url());
        }
    }

    public function userData(){
        $userID = $this->input->post('userID');
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $userData = $this->user->getUser(array('userID'=>$userID, 'email'=>$email));
        if(!empty($userData)){
            $response = array();
            foreach($userData as $row){
                $response['userID'] = $row['userID'];
                $response['accessToken'] = $row['accessToken'];
                $response['email'] = $row['email'];
                $response['name'] = $row['name'];
                $response['address'] = $row['address'];
                $response['desc'] = $row['desc'];
                $response['createat'] = $row['createat'];
                $response['updateat'] = $row['updateat'];
            }
        }

        $userList = $this->user->getUser(array('userID'=>$userID));
        if(!empty($userList)){
            $response = array();
            foreach($userList as $row){
                $response['userID'] = $row['userID'];
                $response['accessToken'] = $row['accessToken'];
                $response['email'] = $row['email'];
                $response['name'] = $row['name'];
                $response['address'] = $row['address'];
                $response['desc'] = $row['desc'];
                $response['createat'] = $row['createat'];
                $response['updateat'] = $row['updateat'];
            } 
        }

        $userCreate = $this->user->getUser(array('userID'=>$userID,'email'=>$email));
        if(!empty($userCreate)){
            $response = array();
            foreach($userCreate as $row){
                $response['userID'] = $row['userID'];
                $response['accessToken'] = $row['accessToken'];
                $response['email'] = $row['email'];
                $response['name'] = $row['name'];
                $response['address'] = $row['address'];
                $response['createat'] = $row['createat'];
                $response['updateat'] = $row['updateat'];
            }
        }
        $userResponse = $this->user->getUser(array('userID'=>$userID, 'email'=>$email));
    }

    private function sendNotification($notificationData){
        $notificationIns = array();
        $userNotificationData = array();
        $userData = array();
        $userID = 0;
        $postShareID = 0;
        $notifyType = isset($notificationData['notifyType']) ? $notificationData['notifyType'] : "";
        $senderID = isset($notificationData['senderID']) ? $notificationData['senderID'] : "";
        $senderDetails = $this->CommonModel->get_row(TBL_USER, ["*"], ["userID" => $senderID]);
        if(!empty($senderDetails)){
            $notificationData['image'] = isset($senderDetails['profileImage']) ? $senderDetails['profileImage'] : "defaultNotificationImage.png";
        }

        if (strtoupper($notificationData['type']) == "POST") {
            $notificationIns['type'] = 0;
            //Share post user data
            if (isset($notificationData['postShareID']) && $notificationData['postShareID'] > 0) {
                $userID = $this->CommonModel->get_row(TBL_POST_SHARE, ["userID"], ["postShareID" => $notificationData['postShareID']]);
                $postShareID = $notificationData['postShareID'];
            } //mention Users in post send notification
            else if (isset($notificationData['mentionUserID']) && $notificationData['mentionUserID'] > 0) {
                $userID = [$notificationData['mentionUserID']];
            } else if (isset($notificationData['subscriberID']) && $notificationData['subscriberID'] > 0) {
                $userID = [$notificationData['subscriberID']];
                $postShareID = $notificationData['postID'];
            } else if (isset($notificationData['postCommentID']) && $notificationData['postCommentID'] > 0) {
                $uID = $this->CommonModel->get_row(TBL_POST_COMMENT, ["userID"], ["postCommentID" => $notificationData['postCommentID']]);
                $userID = ($uID['userID'] == $notificationData['userID']) ? 0 : $uID;
            } else {
                $userID = $this->CommonModel->get_row(TBL_POST, ["userID"], ["postID" => $notificationData['targetID']]);
            }
        } else if (strtoupper($notificationData['type']) == "PROFILE" || strtoupper($notificationData['type']) == "notification") {
            $notificationIns['type'] = 1;
            $userID = [$notificationData['sendNotificationUserID']];
        } else if (strtoupper($notificationData['type']) == "CHAT") {

            if ($notificationData['mesgType'] == 0) {

                $notificationIns['type'] = 2;

                $userID = [$notificationData['targetID']];

            } else {

                $notificationIns['type'] = 3;

                $userID = $this->CommonModel->get_all(TBL_USER_GROUP, ['userID'], ["groupID" => $notificationData['targetID']]);

            }

        }
        // else if(strtoupper($notificationData['type']) == "notification"){
        //     $notificationIns['type'] = 4;
        // }
        $notificationIns['title'] = $notificationData['title'];
        $notificationIns['text'] = $notificationData['desc'];
        $notificationIns['image'] = isset($notificationData['image']) ? $notificationData['image'] : "defaultNotificationImage.png";
        $notificationIns['targetID'] = $notificationData['targetID'];
        // INSERT AFTER NOTIFICATION MASTER SEND
        if (strtoupper($notificationData['type']) != "CHAT") {
            $notificationID = $this->CommonModel->insert(TBL_NOTIFICATION, $notificationIns);
        } else {
            $notificationID = 1;
        }

        if (!empty($userID)) {
            foreach ($userID as $user) {
                if (strtoupper($notificationData['type']) == "CHAT" && $notificationData['mesgType'] == 1) {
                    $user = $user['userID'];
                }
                //Check notification Setting
                $userExists = $this->CommonModel->get_row(TBL_SETTINGS, 'settingID,notificationStatus', ['userID' => $user, 'isActive' => 1, 'isDelete' => 0]);
                $notificationStatus = $userExists['notificationStatus'] ? $userExists['notificationStatus'] : "1";
                //echo $notificationStatus;die;
                $userData = $this->CommonModel->get_row(TBL_USER, ["deviceToken", "deviceType"], ["userID" => $user]);
                if (strtoupper($notificationData['type']) != "CHAT" && $notificationStatus == 1) {
                    $saveUserNotify = array(
                        'n_id' => $notificationID,
                        'userID' => $user,
                        'senderID' => $senderID,
                        'notifyType' => $notifyType,
                        'otherID' => (isset($notificationData['targetID']))?$notificationData['targetID']:"",
                    );
                    $this->CommonModel->insert(TBL_USER_NOTIFICATION, $saveUserNotify);
                }
                if ($userData['deviceType'] == 1 && strlen($userData['deviceToken']) == 152 && $notificationStatus == 1) {
                    $this->CommonModel->sendAndroidNotification(array($userData['deviceToken']), $notificationIns['text'], 100, $notificationIns['title'], $notificationID, $notificationIns['type'], $notificationIns['targetID'], $postShareID);
                } else if ($userData['deviceType'] == 2 && strlen($userData['deviceToken']) == 64 && $notificationStatus == 1) {
                    $this->CommonModel->sendAppleNotification(array($userData['deviceToken']), $notificationIns['text'], 100, $notificationIns['type'], $notificationIns['title'], $notificationID, $notificationIns['targetID'], $postShareID);
                }
            }
        }
    }
    
    // for follow Unfollow User
    public function followUnfollowUser()
    {
        // echo "<pre>"; print_r($_POST); die();
        $result = array('status'=>0,'followStatus'=>'','error'=>'');
        $userID = $this->session->userdata('userID');
        $userDetails = $this->user->getUserdetails($userID, $userID);
        $followerID = $this->input->post('followerID');
        $status = $this->input->post('status');
        if ($followerID == $userID) {
            $result['error'] = getMessage("UserOwnFollowError");
        } elseif (!$this->CommonModel->checkExists(TBL_USER, ['userID' => $followerID, "isActive" => 1, "isDelete" => 0])) {
            $result['error'] = getMessage("FollowAlready");
        } elseif ($this->CommonModel->checkExists(TBL_BLOCKUSER, ['userID' => $userID, 'blockUserID' => $followerID, "isActive" => 1, "isDelete" => 0])) {
            $result['error'] = getMessage("userisBlock");
        } else{
            $alreadyExistFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID'], ['userID' => $userID, 'followerID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);
            /* 1 = Follow or 0 = UnFollow*/
            if ($status == 0) {
                if (empty($alreadyExistFollowUser) || (!empty($alreadyExistFollowUser['status']) && $alreadyExistFollowUser['status'] == 0)) {
                    $result['error'] = getMessage("NotFollowError");
                } else {
                    $userFollowerID = $alreadyExistFollowUser['userFollowerID'];
                    $this->CommonModel->delete(TBL_USER_FOLLOWER, ['userFollowerID' => $alreadyExistFollowUser['userFollowerID']]);
                    if (!empty($userFollowerID)) {
                        $result = array('status'=>1,'followStatus'=>0);
                    }
                }
            } elseif ($status == 4) {
                $alreadyFollowUser = $this->CommonModel->get_row(TBL_USER_FOLLOWER, ['userFollowerID', 'status'], ['userID' => $userID, 'followerID' => $followerID, 'isActive' => 1, 'isDelete' => 0]);
                if (!empty($alreadyFollowUser) && isset($alreadyFollowUser['status']) && ($alreadyFollowUser['status'] == 1 || $alreadyFollowUser['status'] == 4)) {
                    $result['error'] = getMessage("FollowAlready");
                } else {
                    $arrInsert = array();
                    $arrInsert['userID'] = $userID;
                    $arrInsert['followerID'] = $followerID;
                    $arrInsert['status'] = $this->input->post('status');
                    $arrInsert['createdBy'] = $userID;
                    if ($this->CommonModel->insert(TBL_USER_FOLLOWER, $arrInsert) > 0) {
                        $this->sendNotification(array(
                            "type" => "profile",
                            "title" => "SayScape",
                            "desc" => ucfirst($userDetails['username']) . " has send you follow request",
                            "sendNotificationUserID" => $followerID,
                            "targetID" => $userID,
                            "senderID"=>$userID,
                            "notifyType"=>"followRequest"
                        ));
                        // followerSuccess
                        $result = array('status'=>1,'followStatus'=>4);
                    }
                }
            } else {
                if (empty($alreadyExistFollowUser)) {
                    $arrInsert = array();
                    $arrInsert['userID'] = $userID;
                    $arrInsert['followerID'] = $followerID;
                    $arrInsert['status'] = $this->input->post('status');
                    $arrInsert['createdBy'] = $userID;
                    if ($this->CommonModel->insert(TBL_USER_FOLLOWER, $arrInsert) > 0) {
                        $this->sendNotification(array(
                            "type" => "profile",
                            "title" => "SayScape",
                            "desc" => ucfirst($userDetails['username']) . " has started following you",
                            "sendNotificationUserID" => $followerID,
                            "targetID" => $userID,
                            "senderID"=>$userID,
                            "notifyType"=>"follow"
                        ));
                        $result = array('status'=>1,'followStatus'=>1);
                    }
                }
            }
        }
        echo json_encode($result); exit();
    }

    // for save poll answer
    public function savePollAnswer(){
        $result = array('status'=>'0');
        
        $userID = $this->session->userdata('userID');
        $postID = $this->input->post('postID');
        $optionID = $this->input->post('optionID');
        if ($postID && $optionID) {
            $checkAnswerExist = $this->CommonModel->get_row(TBL_POST_POLL_ANSWER, ['*'], ['userID' => $userID, 'postID' => $postID, 'isDelete' => 0]);
            if (!empty($checkAnswerExist)) {
                $this->response['code'] = 101;
                $this->response['message'] = getMessage('pollAnsExist');
                $this->response($this->response);
                $result['error'] = getMessage('pollAnsExist');
            }else{
                $saveData['userID'] = $userID;
                $saveData['postID'] = $postID;
                $saveData['optionID'] = $optionID;
                $saveData['answer'] = (isset($_POST['answer'])) ? $_POST['answer'] : "";
                $answerID = $this->CommonModel->insert(TBL_POST_POLL_ANSWER, $saveData);
                
                $responseData = array();
                $pollPostOptions = array();
                $pollOptions = $this->CommonModel->get_all(TBL_POST_POLL_OPTIONS, ['*'], ["postID" => $postID, "isActive" => 1, "isDelete" => 0]);
                if (!empty($pollOptions)) {
                    foreach ($pollOptions as $opt) {
                        $avgAnswer = $this->post->getPollOptionAvgAns($postID, $opt['optionID']);
                        $res = array();
                        $res['optionID'] = $opt['optionID'];
                        $res['option'] = $opt['text'];
                        $res['avgAnswer'] = $avgAnswer;
                        $pollPostOptions[] = $res;
                    }
                }
                $pollPostOptions = (!empty($pollPostOptions)) ? $pollPostOptions : array();
                $content = "";
                if(!empty($pollPostOptions)){
                    foreach ($pollPostOptions as $poll) {
                        $totalPollAnswer = count($this->CommonModel->get_all(TBL_POST_POLL_ANSWER, ['*'], ["postID" => $postID,"optionID" => $poll['optionID'], "isActive" => 1, "isDelete" => 0]));
                        $totalPollAnswer = (!empty($totalPollAnswer))?'('.$totalPollAnswer.')':"";

                        $avgAnswer = (!empty($poll['avgAnswer']))?$poll['avgAnswer']:0;
                        $content .= '<label class="nomar">'.$poll['option'].'</label><div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="'.$avgAnswer.'"
                            aria-valuemin="0" aria-valuemax="100" style="width:'.$avgAnswer.'%"> '.$avgAnswer.'% '.$totalPollAnswer.' </div>
                        </div>';
                    }
                }
                $result['status'] = 1;
                $result['content'] = $content;
            }
        }
        echo json_encode($result); exit();
    }

    // for load post comment section
    public function loadPostComment(){
        // echo "<pre>"; print_r($_POST); die();
        $userID = $this->session->userdata('userID');
        $postID = $this->input->post('postID');
        $otherUserID = (isset($_POST['userid']))?$this->input->post('userid'):$userID;
        $postShareID = (isset($_POST['postShareID']))?$this->input->post('postShareID'):'';
        $comments = $this->post->getComments($postID, '', $userID, $postShareID);
        
        $data['comments'] = $comments;
        $data['postID'] = $postID;
        $data['postShareID'] = $postShareID;
        $data['view'] = 'front/post_comment_view';
        $this->load->view('front/post_comment_view',$data);
        // $this->renderPartial($data);
    }

    //for post details
    public function post_details($postID){
        $userID = $this->session->userdata('userID');
        // $postList = $this->post->getPostRow($userID, 0);
        $postData = $this->post->getParticularPostBYPostID($userID, $postID);
        $value = $this->post->getPostResponseData($postData, $userID);
        // echo "<pre>"; print_r($value); die();s
        $data['postList'] = $value;

        $data['view'] = 'front/post_details';
        $this->render($data);
    }

    public function muteUnmuteUser(){
        $muteUserID = $this->input->post('muteUserID');
        $status = $this->input->post("status");
        $userID = $this->session->userdata('userID');
        $muteuser = $this->user->getUserNamerow($muteUserID);
        // $arrBlockInsert['blockUserID'] = $this->input->post('blockUserID');

        $alreadyExistsMuteUser = $this->CommonModel->get_row(TBL_MUTEUSER, ['muteID'], ['userID' => $userID, 'muteUserID' => $muteUserID]);
        if(!empty($alreadyExistsMuteUser)){
            $usermuteID = $this->CommonModel->update(TBL_MUTEUSER, ['isActive' => $status], ['muteID' => $alreadyExistsMuteUser['muteID']]);
        }else{
            $usermuteID = $this->CommonModel->insert(TBL_MUTEUSER, array('userID'=> $userID,'muteUserID'=>$muteUserID));
        }
        //echo $usermuteID ;die;
        if($usermuteID != ''){
            redirect('time-line/'.$muteuser['username']);
        }
    }

    public function blockUnBLockUser(){
        $blockUserID = $this->input->post('blockUserID');
        $status = $this->input->post("status");
        $userID = $this->session->userdata('userID');
        $muteuser = $this->user->getUserNamerow($blockUserID);
        // $arrBlockInsert['blockUserID'] = $this->input->post('blockUserID');

        $alreadyExistsMuteUser = $this->CommonModel->get_row(TBL_BLOCKUSER, ['blockID'], ['userID' => $userID, 'blockUserID' => $blockUserID]);
        if(!empty($alreadyExistsMuteUser)){
            $usermuteID = $this->CommonModel->update(TBL_BLOCKUSER, ['isActive' => $status], ['blockID' => $alreadyExistsMuteUser['blockID']]);
        }else{
            $usermuteID = $this->CommonModel->insert(TBL_BLOCKUSER, array('userID'=> $userID,'blockUserID'=>$blockUserID));
        }
        //echo '<pre>'; print_r( $alreadyExistsMuteUser); ;die;
        if($usermuteID != ''){
            redirect('time-line/'.$muteuser['username']);
        }
    }

    public function loadPostCountUsers(){
        // echo "<pre>"; print_r($_POST); die();
        $userID = $this->session->userdata('userID');
        $postID = $this->input->post('postID');
        $type = $this->input->post('type');
        if($type=='like'){
            $userList = $this->post->getPostLikeUserList($userID, $postID);
        }elseif($type=='dislike'){
            $userList = $this->post->getPostDisLikeUserList($userID, $postID);
        }elseif($type=='reShare'){
            $userList = $this->post->getPostReshareUserList($userID, $postID);
        }
        $data['userList'] = $userList;
        $data['view'] = 'front/post_count_userlist';
        $this->renderPartial($data);
    }

    public function privacy_policy(){
        $data['view'] = 'front/privacy_policy';
        $this->render($data);
    }

    public function unquotePost(){
        $postID = (isset($_POST['postID']))?$this->input->post('postID'):"";
        $userID = $this->session->userdata('userID');
        $postShareData = $this->CommonModel->get_row(TBL_POST_SHARE, ['postShareID','postID','userID'], ['isActive'=>1,'postID' => $postID, 'userID'=>$userID]);
        if(empty($postShareData)){
            $this->response['message'] = getMessage('notFoundPosts');
            $this->response($this->response);
        }
        $arrPostUpdate['isDelete'] = 1;
        $arrPostUpdate['isActive'] = 0;
        $arrPostUpdate['updatedDateTime'] = date('Y-m-d H:i:s');
        $delete_post = $this->CommonModel->update(TBL_POST_SHARE, $arrPostUpdate, array('postID' => $postID, 'userID'=>$userID));
        echo true;
    }
    
    // for Responder Feed
    public function responder_feed(){
        if (!$this->session->userdata('userID')) {
            redirect('login');
        }
        $this->data['title'] = "Responder Feed";
        $userID = $this->session->userdata('userID');
        $userDetails = $this->user->getUserdetails($userID, $userID);
        $data['userDetails'] = $userDetails;
        if(isset($userDetails['isResponder']) && $userDetails['isResponder']==1){
            $postsdata = $this->post->getPosts($userID, 4, false, 1, 3);
            if (!empty($postsdata)) {
                foreach ($postsdata as &$value) {
                    $value = $this->post->getPostResponseData($value, $userID);
                }
            }
            $data['postList'] = $postsdata;
        }
        $data['view'] = 'front/responder_feed';
        $this->render($data);
    }

    public function testEncryptMessage(){
        $secretyKey = 'BlVssQKxzAHFAUNZbqvwS+yKw/m';
        $encryption = new \MrShan0\CryptoLib\CryptoLib();
        
        // $string = 'Test Message';
        // $encrypt = $encryption->encryptPlainTextWithRandomIV($string, $secretyKey);
        // echo '<br>encrypt: ' . $encrypt . PHP_EOL;
        $string = "RJBNls6jgFMBR/lOa79aweL4mgdSPYW5GL24nEn3pfI=";
        $plainText = $encryption->decryptCipherTextWithRandomIV($string, $secretyKey);
        echo 'Decrypted: ' . $plainText . PHP_EOL;
    }
    
}
?>