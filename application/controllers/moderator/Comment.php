<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Comment extends MY_Controller
{
    protected $response = array(
        'code' => 101,
        'message' => 'No Data Found',
        'data' => []
    );

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('moderator_loggedIN')) {
            redirect(site_url('moderator/login'));
        }
        $this->load->model('CommentModel', 'comment');
    }

    public function index()
    {
        $data['title'] = SITE_NAME . ' | Comment List';
        $this->template->load('moderator_layout', 'moderator/comment', $data);
    }

    public function getReportCommentList()
    {
        $this->response['code'] = 100;
        $this->response['message'] = "Get Report Comment List SuccessFully..!";
        $this->response['data'] = $this->comment->getAllReportComments(0);
        $this->response($this->response);
    }

    public function removeComment()
    {
        if(!$this->input->post('postCommentID') || empty($this->input->post('postCommentID'))){
            $this->response['message'] = "Please pass postCommentID";
            $this->response($this->response);
        }
        $postCommentID = $this->input->post('postCommentID');
        if($this->CommonModel->update(TBL_POST_COMMENT,['isActive'=>0,'isDelete'=>1],['postCommentID'=>$postCommentID]) > 0){
            $this->response['code'] = 100;
            $this->response['message'] = "Comment SuccessFully. Removed.!";
        }else{
            $this->response['code'] = 101;
            $this->response['message'] = "Comment Removed Error.!";
        }
        $this->response($this->response);
    }
}