<?php
defined('BASEPATH') or exit('No direct script access allowed');

    class Dashboard extends MY_Controller
    {
        public function __construct() {
            parent::__construct();
    
            if(!$this->session->userdata('moderator_loggedIN')) {
                redirect(site_url('moderator/login'));
            }
    
        }

        public function index(){
            $data['title'] = SITE_NAME. ' | Moderator Dashboard';
            $this->template->load('moderator_layout', 'moderator/dashboard', $data);
        }
        
    }
    
?>