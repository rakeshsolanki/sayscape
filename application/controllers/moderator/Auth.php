<?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Auth extends MY_Controller
    {
        public function __construct() {
            parent::__construct();
        }

        public function redirectLogin(){
            redirect(site_url('moderator/login'));
        }

        public function index() {

            if($this->session->userdata('moderator_loggedIN')) {
                redirect(site_url('moderator/dashboard'));
            }

            $data['title'] = SITE_NAME. ' | Moderator login';
            $this->template->load('moderator_login_layout', 'moderator/login', $data);
        }

        public function do_login() {
            try {
                if(!$this->input->post()) {
                    throw new Exception("Invalid parameters!");
                }

                $this->form_validation->set_rules('emailAddress', 'Email Address', 'required|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'required');
                if(!$this->form_validation->run()) {
                    throw new Exception(validation_errors());
                }
    
                $user_data['email'] = $this->input->post('emailAddress');
                $user_data['password'] = md5($this->input->post('password'));
    
                $user_session_data = $this->CommonModel->get_row(TBL_MODERATOR_USER, '*', $user_data);
                if(!$user_session_data) {
                    throw new Exception("Invalid Email or Password!");
                }
                $this->session->set_userdata('moderator_loggedIN', $user_session_data);
                $response_array = [
                    'status' => 'success',
                    'message' => "User has logged in successfully!"
                ];
            } catch (Exception $e) {
                $response_array = [
                    'status' => 'error',
                    'message' => $e->getMessage()
                ];
            }
            $this->response($response_array);
        }

        public function logout() {
            $this->session->unset_userdata('moderator_loggedIN');
            redirect(site_url('moderator'));
        }
    }
    
?>