$("#loginForm").validate({
    rules: {
        emailAddress: {
            required: true,
            email: true,
            maxlength: 100
        },
        password: {
            required: true,
            maxlength: 100
        },
    },
    submitHandler: function(form) {
        $('#formMessage').removeClass('alert alert-danger alert-success').html("");
        $('#btn-login').attr('disabled', 'disabled');
        var login_url = site_url + 'admin/auth/do_login';
        $.ajax({
            url: login_url,
            type: form.method,
            data: $(form).serialize(),
            success: function(response) {
                if(response.status == 'success') {
                    $('#formMessage').addClass('alert alert-success').html(response.message);
                    window.location = site_url + 'admin/';
                } else if (response.status == 'error') {
                    $('#formMessage').addClass('alert alert-danger').html(response.message);
                } else {
                    $('#formMessage').addClass('alert alert-danger').html(SERVER_ERROR_MSG);
                }
                $('#btn-login').attr('disabled', false);
            }
        });
    }
});
