var userList_table = null, userBlockList_table = null, userReportList_table = null;
var DatatableDataSources = function () {

    //
    // Setup module components
    //

    // Basic Datatable examples
    var _componentDatatableDataSources = function () {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Switchery
        var _componentSwitchery = function () {
            if (typeof Switchery == 'undefined') {
                console.warn('Warning - switchery.min.js is not loaded.');
                return;
            }
            // Initialize
            var elems = Array.prototype.slice.call(document.querySelectorAll('.form-control-switchery'));
            elems.forEach(function (html) {
                var switchery = new Switchery(html);
            });
        };

        // Setting datatable defaults
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Search:</span> _INPUT_',
                searchPlaceholder: 'Search',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                }
            }
        });

        // AJAX sourced data
        userList_table = $('#userList_table').DataTable({
            fixedHeader: true,
            ajax: {
                "type": "GET",
                "url": base_url + "admin/user/getUserList?status=1",
                "dataSrc": function (json) {
                    $("#ActiveDTCount").text(json.data.length);
                    return json.data;
                }
            },
            columns: [
                {data: "userID"},
                {data: "username", title: "Username"},
                {data: "name", title: "name"},
                {data: "email", title: "Email"},
                {data: "phone", title: "Phone"},
                {
                    data: "isVerified",
                    title: "Is Verified",
                    render: function (data, type, row) {
                        var content = '';
                        if (data == 1) {
                            content = '<label class="badge badge-success" title="User Verified">Verifed</label>';
                        } else {
                            content = '<label class="badge badge-danger" title="User Not Verified">Not Verified</label>';
                        }
                        return content;
                    }
                },
                {
                    data: "isVeteran",
                    title: "Is Veteran",
                    targets: [0],
                    orderable: false,
                    render: function (data, type, row) {
                        var content = "<div class='form-check form-check-switchery mb-0'>" +
                            "<label class='form-check-label'>" +
                            "<input type='checkbox' class='form-control-switchery' onClick='veteranToggle(" + row.userID + ",this)' " + (data == 1 ? 'checked' : '') + "/>" +
                            "</label>" +
                            "</div>";
                        //    if(data == 1) {
                        //        content = '<label class="badge badge-success" title="User Verified">Yes</label>';
                        //    } else {
                        //        content = '<label class="badge badge-danger" title="User Not Verified">No</label>';
                        //    }
                        return content;
                    }
                },
                {
                    data: "allowVeteran",
                    title: "Allow Veteran",
                    targets: [0],
                    orderable: false,
                    render: function (data, type, row) {
                        var content = "<div class='form-check form-check-switchery mb-0'>" +
                            "<label class='form-check-label'>" +
                            "<input type='checkbox' class='form-control-switchery' onClick='veteranAllowToggle(" + row.userID + ",this)' " + (data == 1 ? 'checked' : '') + "/>" +
                            "</label>" +
                            "</div>";
                        //    if(data == 1) {
                        //        content = '<label class="badge badge-success" title="User Verified">Yes</label>';
                        //    } else {
                        //        content = '<label class="badge badge-danger" title="User Not Verified">No</label>';
                        //    }
                        return content;
                    }
                },
                {
                    data: "isOffical",
                    title: "Is Official",
                    targets: [0],
                    orderable: false,
                    render: function (data, type, row) {
                        var content = "<div class='form-check form-check-switchery mb-0'>" +
                            "<label class='form-check-label'>" +
                            "<input type='checkbox' class='form-control-switchery' onClick='officialToggle(" + row.userID + ",this)' " + (data == 1 ? 'checked' : '') + "/>" +
                            "</label>" +
                            "</div>";
                        return content;
                    }
                },
                {
                    data: "isResponder",
                    title: "Is Responder",
                    targets: [0],
                    orderable: false,
                    render: function (data, type, row) {
                        var content = "<div class='form-check form-check-switchery mb-0'>" +
                            "<label class='form-check-label'>" +
                            "<input type='checkbox' class='form-control-switchery' onClick='responderToggle(" + row.userID + ",this)' " + (data == 1 ? 'checked' : '') + "/>" +
                            "</label>" +
                            "</div>";
                        return content;
                    }
                },
                {
                    data: "createdDateTime",
                    title: "Created Date Time"
                },
                {
                    title: "Actions",
                    render: function (data, type, row) {
                        //return "<a href='javascript:void(0)' onClick='blockUnBlockUser("+row.userID+",0)' class='badge badge-danger'> BLOCK</a>";
                        return "<a href='" + base_url + "admin/user/edit/" + row.userID + "' class='badge badge-info' title='edit Profile'><i class='icon-pencil7'></i> </a> &nbsp;" +
                            "<a href='javascript:void(0)' onClick='blockUnBlockUser(" + row.userID + ",0)' class='badge badge-danger'> BLOCK</a>";
                    }
                }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
            ],
            rowId: function (a) {
                return a.userID;
            },
            order: [[6, 'DESC']],
            drawCallback: function (settings) {
                _componentSwitchery();
            }
        });

        userBlockList_table = $('#userBlockList_table').DataTable({
            ajax: {
                "type": "GET",
                "url": base_url + "admin/user/getUserList?status=0",
                "dataSrc": function (json) {
                    $("#BlockDTCount").text('0');
                    $("#BlockDTCount").text(json.data.length);
                    return json.data;
                }
            },
            columns: [
                {data: "userID"},
                {data: "username", title: "Username"},
                {data: "name", title: "name"},
                {data: "email", title: "Email"},
                {data: "phone", title: "Phone"},
                {
                    data: "isVerified",
                    title: "Is Verified",
                    render: function (data, type, row) {
                        var content = '';
                        if (data == 1) {
                            content = '<label class="badge badge-success" title="User Verified">Verifed</label>';
                        } else {
                            content = '<label class="badge badge-danger" title="User Not Verified">Not Verified</label>';
                        }
                        return content;
                    }
                },
                {
                    data: "isVeteran",
                    title: "Is Veteran",
                    render: function (data, type, row) {
                        var content = "<div class='form-check form-check-switchery mb-0'>" +
                            "<label class='form-check-label'>" +
                            "<input type='checkbox' class='form-control-switchery' readonly  " + (data == 1 ? 'checked' : '') + "/>" +
                            "</label>" +
                            "</div>";
                        //    if(data == 1) {
                        //        content = '<label class="badge badge-success" title="User Verified">Yes</label>';
                        //    } else {
                        //        content = '<label class="badge badge-danger" title="User Not Verified">No</label>';
                        //    }
                        return content;
                    }
                },
                {
                    data: "isOffical",
                    title: "Is Official",
                    render: function (data, type, row) {
                        var content = "<div class='form-check form-check-switchery mb-0'>" +
                            "<label class='form-check-label'>" +
                            "<input type='checkbox' class='form-control-switchery' readonly " + (data == 1 ? 'checked' : '') + "/>" +
                            "</label>" +
                            "</div>";
                        return content;
                    }
                },
                {
                    data: "isResponder",
                    title: "Is Responder",
                    targets: [0],
                    orderable: false,
                    render: function (data, type, row) {
                        var content = "<div class='form-check form-check-switchery mb-0'>" +
                            "<label class='form-check-label'>" +
                            "<input type='checkbox' class='form-control-switchery' onClick='responderToggle(" + row.userID + ",this)' " + (data == 1 ? 'checked' : '') + "/>" +
                            "</label>" +
                            "</div>";
                        return content;
                    }
                },
                {data: "createdDateTime", title: "Created Date Time"},
                {
                    title: "Actions",
                    render: function (data, type, row) {
                        //return "<a href='javascript:void(0)' onClick='blockUnBlockUser("+row.userID+",1)' class='badge badge-danger'>UNBLOCK</a>";
                        return "<a href='" + base_url + "admin/user/edit/" + row.userID + "' class='badge badge-info' title='edit Profile'><i class='icon-pencil7'></i> </a> &nbsp;" +
                            "<a href='javascript:void(0)' onClick='blockUnBlockUser(" + row.userID + ",0)' class='badge badge-danger'> BLOCK</a>";
                    }
                }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
            ],
            rowId: function (a) {
                return a.userID;
            },
            order: [[6, 'DESC']],
            drawCallback: function (settings) {
                _componentSwitchery();
            }
        });

        userReportList_table = $('#userReportList_table').DataTable({
            ajax: {
                "type": "GET",
                "url": base_url + "admin/user/getUserReportList",
                "dataSrc": function (json) {
                    $("#ReportUserDTCount").text('0');
                    $("#ReportUserDTCount").text(json.data.length);
                    return json.data;
                }
            },
            columns: [
                {data: "userID"},
                {data: "username", title: "Username"},
                {data: "name", title: "name"},
                {data: "email", title: "Email"},
                {data: "phone", title: "Phone"},
                {
                    data: "isVerified",
                    title: "Is Verified",
                    render: function (data, type, row) {
                        var content = '';
                        if (data == 1) {
                            content = '<label class="badge badge-success" title="User Verified">Verifed</label>';
                        } else {
                            content = '<label class="badge badge-danger" title="User Not Verified">Not Verified</label>';
                        }
                        return content;
                    }
                },
                {data: "createdDateTime", title: "Created Date Time"},
                {
                    title: "Actions",
                    render: function (data, type, row) {
                        //return "<a href='javascript:void(0)' onClick='blockUnBlockUser("+row.userID+",0)' class='badge badge-danger'>BLOCK</a>";
                        return "<a href='" + base_url + "admin/user/edit/" + row.userID + "' class='badge badge-info' title='edit Profile'><i class='icon-pencil7'></i> </a> &nbsp;" +
                            "<a href='javascript:void(0)' onClick='blockUnBlockUser(" + row.userID + ",0)' class='badge badge-danger'>BLOCK</a>";
                    }
                }
            ],
            columnDefs: [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
            ],
            rowId: function (a) {
                return a.userID;
            },
            order: [[6, 'DESC']],
            drawCallback: function (settings) {
                _componentSwitchery();
            }
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentDatatableDataSources();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    DatatableDataSources.init();
    // $("#userList_table").on("click", "tr", function(event){
    //     console.log(userList_table.row( this ).id());
    // });
    // setInterval(function(){
    //     userList_table.ajax.reload(null,false);
    // },30000);

    // if (window.Notification && Notification.permission !== "granted") {
    //     Notification.requestPermission(function (status) {
    //       if (Notification.permission !== status) {
    //         Notification.permission = status;
    //       }
    //     });
    // }

    // if(window.Notification && Notification.permission === "granted"){
    //     new Notification('To do list', { body: "Hello world how are you i am fine", icon: "https://lf2020.club/app/assets/images/logo-icon.png" });
    // }

});

function veteranToggle(userID, athis) {
    var checkUncheck = $(athis).is(":checked");
    $.ajax({
        url: base_url + "admin/user/ajaxVeteran",
        type: "POST",
        data: {"userID": userID, "status": (checkUncheck === true ? 1 : 0)},
        success: function (data) {
            if (data.code === 101) {
                console.log(data.message);
            }
        },
        error: function (data) {
            console.log(data);
        },
    });
}

function veteranAllowToggle(userID, athis) {
    var checkUncheck = $(athis).is(":checked");
    $.ajax({
        url: base_url + "admin/user/ajaxAllowVeteran",
        type: "POST",
        data: {"userID": userID, "status": (checkUncheck === true ? 1 : 0)},
        success: function (data) {
            if (data.code === 101) {
                console.log(data.message);
            }
        },
        error: function (data) {
            console.log(data);
        },
    });
}

function officialToggle(userID, athis) {
    var checkUncheck = $(athis).is(":checked");

    $.ajax({
        url: base_url + "admin/user/ajaxOfficial",
        type: "POST",
        data: {"userID": userID, "status": (checkUncheck === true ? 1 : 0)},
        success: function (data) {
            if (data.code === 101) {
                alert(data.message);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function responderToggle(userID, athis) {
    var checkUncheck = $(athis).is(":checked");

    $.ajax({
        url: base_url + "admin/user/ajaxResponder",
        type: "POST",
        data: {"userID": userID, "status": (checkUncheck === true ? 1 : 0)},
        success: function (data) {
            if (data.code === 101) {
                alert(data.message);
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}

function blockUnBlockUser(userID, status) {
    $.ajax({
        url: base_url + "admin/user/ajaxBlock",
        type: "POST",
        data: {"userID": userID, "status": status},
        success: function (data) {
            if (data.code === 101) {
                console.log(data.message);
            } else {
                if (userList_table.ajax.reload(null, false)) {
                    if (userReportList_table.ajax.reload(null, false)) {
                        userBlockList_table.ajax.reload(null, false);
                    }
                }
            }
        },
        error: function (data) {
            console.log(data);
        }
    });
}