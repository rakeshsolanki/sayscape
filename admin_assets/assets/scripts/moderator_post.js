var postList_table = null,reportPostList_table = null;
var DatatableDataSources = function() {

    //
    // Setup module components
    //

    // Basic Datatable examples
    var _componentDatatableDataSources = function() {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Switchery
        var _componentSwitchery = function() {
            if (typeof Switchery == 'undefined') {
                console.warn('Warning - switchery.min.js is not loaded.');
                return;
            }    
            // Initialize
            var elems = Array.prototype.slice.call(document.querySelectorAll('.form-control-switchery'));
            elems.forEach(function(html) {
                var switchery = new Switchery(html);
            });
        };

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Search:</span> _INPUT_',
                searchPlaceholder: 'Search',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });
        
        // AJAX sourced data
        postList_table = $('#postList_table').DataTable({
            fixedHeader: true,
             ajax: {
                 "type":"GET",
                 "url":base_url + "moderator/post/getPostList",
                 "dataSrc":function (json){
                     $("#ActiveDTCount").text(json.data.length);
                     return json.data;
                 }
             },
             columns:[
                 {data:"postID"},
                 {
                    data:"profileImage",
                    title:"Profile Image",
                    render: function (data, type, row) {
                       var content = '';
                           content = '<img src="'+data+'" width="100px" height="100px">';
                       return content;
                   }
                },
                 {data:"username",title:"User Name"},
                 {data:"name",title:"Name"},
                 {data:"text",title:"Text"},
                 {data:"likesCount",title:"Total Like"},
                 {data:"disLikeCount",title:"Total DisLike"},
                 {data:"commentCount",title:"Total Comment"},
                 {data:"reShareCount",title:"Total Reshare"},
                 {data:"createdDateTime",title:"Created Date Time"},
                 {
                    title:"Actions",
                    render:function(data,type,row){
                        return " &lrm; <a href='javascript:void(0)' onClick='removePost(this)' class='badge badge-danger'>REMOVE</a>";
                            // " &lrm; <a href='javascript:void(0)' onClick='detailPostShow(this)' class='badge badge-success'>DETAIL</a>";
                    }
                 }
             ],
             columnDefs:[
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
             ],
             rowId: function(a) {
                return a.userID;
             },
             order: [],
             drawCallback: function(settings){
                _componentSwitchery();
             }
        });

          // AJAX sourced data
          reportPostList_table = $('#reportPostList_table').DataTable({
            fixedHeader: true,
             ajax: {
                 "type":"GET",
                 "url":base_url + "moderator/post/getReportPostList",
                 "dataSrc":function (json){
                     $("#ReportDTCount").text('0');
                    $("#ReportDTCount").text(json.data.length);
                     return json.data;
                 }
             },
             columns:[
                 {data:"postID"},
                 {
                    data:"profileImage",
                    title:"Profile Image",
                    render: function (data, type, row) {
                       var content = '';
                           content = '<img src="'+data+'" width="100px" height="100px">';
                       return content;
                   }
                },
                 {data:"username",title:"User Name"},
                 {data:"name",title:"Name"},
                 {data:"text",title:"Text"},
                 {data:"likesCount",title:"Total Like"},
                 {data:"disLikeCount",title:"Total DisLike"},
                 {data:"commentCount",title:"Total Comment"},
                 {data:"reShareCount",title:"Total Reshare"},
                 {data:"reportby",title:"Report By"},
                 {data:"createdDateTime",title:"Created Date Time"},
                 {
                    title:"Actions",
                    render:function(data,type,row){
                        return " &lrm; <a href='javascript:void(0)' onClick='removePost(this)' class='badge badge-danger'>REMOVE</a>";
                            // " &lrm; <a href='javascript:void(0)' onClick='detailPostShow(this)' class='badge badge-success'>DETAIL</a>";
                    }
                 }
             ],
             columnDefs:[
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
             ],
             rowId: function(a) {
                return a.userID;
             },
             order: [],
             drawCallback: function(settings){
                _componentSwitchery();
             }
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentDatatableDataSources();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    DatatableDataSources.init();
});

function removePost(athis){
    var tableID = $(athis).closest('table').attr('id');
    if (tableID == "postList_table")
    var postID = postList_table.row($(athis).closest('tr')).data().postID;
    if (tableID == "reportPostList_table")
    var postID = reportPostList_table.row($(athis).closest('tr')).data().postID;
    swal({
        title: 'Are you sure?',
        text: 'You want to remove this post.?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, remove it!',
    }).then(function(isConfirm) {
        if(isConfirm.value === true){
            $.ajax({
                url:base_url+"moderator/post/removePost",
                type:"POST",
                data:{"postID":postID},
                success: function(data){
                    if(data.code === 101){
                        swal(data.message, 'error');
                    }else{
                        if(postList_table.ajax.reload(null,false)){
                            reportPostList_table.ajax.reload(null,false);
                        } 
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });     
        }
    });
}