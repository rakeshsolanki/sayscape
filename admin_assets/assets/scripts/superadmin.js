var superAdminTable = null;
var DatatableDataSources = function() {

    //
    // Setup module components
    //

    // Bootbox extension
    var _componentModalBootbox = function() {
        if (typeof bootbox == 'undefined') {
            console.warn('Warning - bootbox.min.js is not loaded.');
            return;
        }
    };

    // Sweet Alerts
    var _componentSweetAlert = function() {
        if (typeof swal == 'undefined') {
            console.warn('Warning - sweet_alert.min.js is not loaded.');
            return;
        }

        // Defaults
        var setCustomDefaults = function() {
            swal.setDefaults({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });
        }
        setCustomDefaults();
    };

    // Basic Datatable examples
    var _componentDatatableDataSources = function() {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Search:</span> _INPUT_',
                searchPlaceholder: 'Search',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });
        
        // AJAX sourced data
        superAdminTable = $('#superAdminTable').DataTable({
            fixedHeader: true,
             ajax: {
                 "type":"GET",
                 "url":base_url + "admin/superadmin/getSuperAdminList",
             },
             columns:[
                 {data:"name",title:"Name"},
                 {data:"email",title:"Email"},
                 {
                    data:"adminID",
                    title:"Actions",
                    render:function(data,type,row){
                        return " &lrm; <a href='javascript:void(0)' onClick='removeSuperAdmin(this)' class='badge badge-danger'>REMOVE</a>";
                 }
                 }
             ],
             order: [[ 1, 'DESC' ]],
        });

    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentDatatableDataSources();
            _componentSweetAlert();
            _componentModalBootbox();
        }
    }
}();


// Initialize module
// ------------------------------
// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    DatatableDataSources.init();
    // $("#superAdminTable").on("click", "tr", function(event){
    //     bootbox.alert({
    //         title: 'Post Detail',
    //         message:
    //             "<a href='javascript:void(0)' class='media mt-0 pb-2 mxChat1' style='border-bottom:1px solid #f2f2f2;'>"+
    //                 "<div class='mr-3'>"+
    //                     "<img src='http://localhost/lf2020/assets/images/defaultProfile.png' class='rounded-circle' width='40' height='40' alt='AdminProfile'>"+
    //                 "</div>"+
    //                 "<div class='media-body'><div class='media-title font-weight-semibold'>"+localStorage.username+"</div>"+
    //                     "<span class='text-muted' style='text-overflow:ellipsis'>"+localStorage.email+"</span>"+
    //                 "</div>"+
    //             "</a>", 
    //     });
    //     console.log(superAdminTable.row( this ).id());
    // });
});
function detailPostShow(athis){
    console.log(superAdminTable.row($(athis).closest('tr')).data());
}
$(document).on('click', '.view-detail', function (e) {
    var tableID = $(this).closest('table').attr('id');
    if (tableID == "superAdminTable")
        var RowData = superAdminTable.row($(this).closest('tr')).data();

    var RowDataKeys = Object.keys(RowData);
    $("#" + tableID + "Modal").find('.DataBody').empty();
    //console.log($("#"+tableID).parents('body').closest("#"+tableID+"Modal").find('.DataBody'));return false;
    $.each(RowDataKeys, function (index, row) {
        $("#" + tableID + "Modal").find('.DataBody').append('<tr><th style="width:20%;">' + insertSpaces(row) + '</th><td style="width:80%;">' + RowData[row] + '</td></tr>');
    });
    $("#" + tableID + "Modal").modal();
});


$('#add_superadmin_form').validate({
    rules: {
        email: {
            email: true,
        },
        password: {
            required: true,
            minlength: 6,
        },
        confirm_password: {
            required: true,
            equalTo: "#password"
        },
    },
    submitHandler: function (form) {
        var url = base_url + 'admin/superadmin/save_superadmin';
        $.ajax({
            url: url,
            type: form.method,
            data: $(form).serialize(),
            success: function (response) {
                response_data = response;
                if (response_data.code == 100) {
                    swal("Success!", response.message, "success").then((value) => {
                        window.location.href = base_url + 'admin/superadmin';
                    });
                } else if (response_data.code == 101) {
                    $('#FormMessage').html('<div class="alert alert-danger">' + response.message) + '</div>';
                } else {
                    $('#FormMessage').html('<div class="alert alert-danger">Some error occurred at server side.</div>');
                }
            }
        });
    }
});

function removeSuperAdmin(athis){
    var adminID = superAdminTable.row($(athis).closest('tr')).data().adminID;
    swal({
        title: 'Are you sure?',
        text: 'You want to remove this admin.?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, remove it!',
    }).then(function(isConfirm) {
        if(isConfirm.value === true){
            $.ajax({
                url:base_url+"admin/superadmin/remove",
                type:"POST",
                data:{"adminID":adminID},
                success: function(data){
                    superAdminTable.ajax.reload(null,false);
                },
                error: function(data){
                    console.log(data);
                }
            });     
        }
    });
}