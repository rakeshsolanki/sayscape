var veteranPost_table = null;
var DatatableDataSources = function() {

    //
    // Setup module components
    //

    // Bootbox extension
    var _componentModalBootbox = function() {
        if (typeof bootbox == 'undefined') {
            console.warn('Warning - bootbox.min.js is not loaded.');
            return;
        }
    };

    // Sweet Alerts
    var _componentSweetAlert = function() {
        if (typeof swal == 'undefined') {
            console.warn('Warning - sweet_alert.min.js is not loaded.');
            return;
        }

        // Defaults
        var setCustomDefaults = function() {
            swal.setDefaults({
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-primary',
                cancelButtonClass: 'btn btn-light'
            });
        }
        setCustomDefaults();
    };

    // Basic Datatable examples
    var _componentDatatableDataSources = function() {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Search:</span> _INPUT_',
                searchPlaceholder: 'Search',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });
        
        // AJAX sourced data
        veteranPost_table = $('#veteranPost_table').DataTable({
            fixedHeader: true,
             ajax: {
                 "type":"GET",
                 "url":base_url + "admin/veteran/getVeteranList",
                 "dataSrc":function (json){
                     $("#ActiveDTCount").text(json.data.length);
                     return json.data;
                 }
             },
             columns:[
                 {data:"text",title:"Content"},
                 {data:"createdDateTime",title:"Created Date Time"},
                 {data:"updatedDateTime",title:"Last Change"},
                 {
                    title:"Actions",
                    render:function(data,type,row){
                        return " &lrm; <a href='javascript:void(0)' onClick='removeVeteranPost(this)' class='badge badge-danger'>REMOVE</a>";
                            // " &lrm; <a href='javascript:void(0)' onClick='detailPostShow(this)' class='badge badge-success'>DETAIL</a>";
                    }
                 }
             ],
             order: [[ 1, 'DESC' ]],
        });

    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentDatatableDataSources();
            _componentSweetAlert();
            _componentModalBootbox();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    DatatableDataSources.init();
    // $("#veteranPost_table").on("click", "tr", function(event){
    //     bootbox.alert({
    //         title: 'Post Detail',
    //         message:
    //             "<a href='javascript:void(0)' class='media mt-0 pb-2 mxChat1' style='border-bottom:1px solid #f2f2f2;'>"+
    //                 "<div class='mr-3'>"+
    //                     "<img src='http://localhost/lf2020/assets/images/defaultProfile.png' class='rounded-circle' width='40' height='40' alt='AdminProfile'>"+
    //                 "</div>"+
    //                 "<div class='media-body'><div class='media-title font-weight-semibold'>"+localStorage.username+"</div>"+
    //                     "<span class='text-muted' style='text-overflow:ellipsis'>"+localStorage.email+"</span>"+
    //                 "</div>"+
    //             "</a>", 
    //     });
    //     console.log(veteranPost_table.row( this ).id());
    // });
});
function detailPostShow(athis){
    console.log(veteranPost_table.row($(athis).closest('tr')).data());
}
function removeVeteranPost(athis){
    var postID = veteranPost_table.row($(athis).closest('tr')).data().postID;
    swal({
        title: 'Are you sure?',
        text: 'You want to remove this post.?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, remove it!',
    }).then(function(isConfirm) {
        if(isConfirm.value === true){
            $.ajax({
                url:base_url+"admin/veteran/removeVeteranPost",
                type:"POST",
                data:{"postID":postID},
                success: function(data){
                    if(data.code === 101){
                        console.log(data.message);
                    }else{
                        veteranPost_table.ajax.reload(null,false);
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });     
        }
    });
}
document.getElementById('veteranPostImage').addEventListener('change', function(e) {
    var list = document.getElementById('filelist');
    list.innerHTML = '';
    for (var i = 0; i < this.files.length; i++) {
        list.innerHTML += (i + 1) + '. ' + this.files[i].name + '\n';
    }
    if (list.innerHTML == '') list.style.display = 'none';
    else list.style.display = 'block';
});
function submitVeteranPost(event){
    event.preventDefault();
    $.blockUI({ 
	    message: '<i class="icon-spinner4 spinner"></i>',
	    overlayCSS: {
	        backgroundColor: '#1b2024',
	        opacity: 0.8,
	        cursor: 'wait'
	    },
	    css: {
	        border: 0,
	        color: '#fff',
	        padding: 0,
	        backgroundColor: 'transparent'
	    }
	});    
    var form = $('#veteranForm')[0];
    var sendData = new FormData(form);

    sendData.append("type",$(".nav-pills .nav-item").find("a.active").attr("data-id"));

    for(var pair of sendData.entries()) {
        if($(".nav-pills .nav-item").find("a.active").attr("data-id") == 1){
            if(pair[0] == "veteranPostVideo"){
                sendData.delete(pair[0]);
            }
        }else{
            if(pair[0] == "veteranPostImage[]"){
                sendData.delete(pair[0]);
            }
        }
    }

    $.ajax({
        url:base_url+"admin/veteran/addVeteranPost",
        type:'POST',
        data:sendData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function(data){
            if(data.code == 101){
                console.log(data);
            }else{
                veteranPost_table.ajax.reload(null, false);
                document.getElementById('filelist').innerHTML = "";
                document.getElementById('filelist').style.display = "none";
                $('#veteranForm').trigger("reset");
            }
        },error: function(e){
            console.log(e);
        },complete: function(e){
            $.unblockUI();
        }
    });
    return false;
}