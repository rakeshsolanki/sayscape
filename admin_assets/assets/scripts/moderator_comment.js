var commentList_table = null,reportCommentList_table = null;
var DatatableDataSources = function() {

    //
    // Setup module components
    //

    // Basic Datatable examples
    var _componentDatatableDataSources = function() {
        if (!$().DataTable) {
            console.warn('Warning - datatables.min.js is not loaded.');
            return;
        }

        // Switchery
        var _componentSwitchery = function() {
            if (typeof Switchery == 'undefined') {
                console.warn('Warning - switchery.min.js is not loaded.');
                return;
            }
            // Initialize
            var elems = Array.prototype.slice.call(document.querySelectorAll('.form-control-switchery'));
            elems.forEach(function(html) {
                var switchery = new Switchery(html);
            });
        };

        // Setting datatable defaults
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
            language: {
                search: '<span>Search:</span> _INPUT_',
                searchPlaceholder: 'Search',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });

        // AJAX sourced data
        commentList_table = $('#commentList_table').DataTable({
            fixedHeader: true,
            ajax: {
                "type":"GET",
                "url":base_url + "moderator/comment/getCommentList",
                "dataSrc":function (json){
                    $("#ActiveDTCount").text(json.data.length);
                    return json.data;
                }
            },
            columns:[
                {data:"postCommentID"},
                {data:"text",title:"Comment"},
                {data:"username",title:"Comment By"},
                {data:"createdDateTime",title:"Created Date Time"},
                {
                    title:"Actions",
                    render:function(data,type,row){
                        return " &lrm; <a href='javascript:void(0)' onClick='removeComment(this)' class='badge badge-danger'>REMOVE</a>";
                        // " &lrm; <a href='javascript:void(0)' onClick='detailPostShow(this)' class='badge badge-success'>DETAIL</a>";
                    }
                }
            ],
            columnDefs:[
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
            ],
            rowId: function(a) {
                return a.userID;
            },
            order: [],
            drawCallback: function(settings){
                _componentSwitchery();
            }
        });

        // AJAX sourced data
        reportCommentList_table = $('#reportCommentList_table').DataTable({
            fixedHeader: true,
            ajax: {
                "type":"GET",
                "url":base_url + "moderator/comment/getReportCommentList",
                "dataSrc":function (json){
                    $("#ReportDTCount").text('0');
                    $("#ReportDTCount").text(json.data.length);
                    return json.data;
                }
            },
            columns:[
                {data:"reportID"},
                {data:"text",title:"Comment"},
                {data:"reason",title:"Reason"},
                {data:"reportby",title:"Report By"},
                {data:"createdDateTime",title:"Created Date Time"},
                {
                    title:"Actions",
                    render:function(data,type,row){
                        return " &lrm; <a href='javascript:void(0)' onClick='removeComment(this)' class='badge badge-danger'>REMOVE</a>";
                        // " &lrm; <a href='javascript:void(0)' onClick='detailPostShow(this)' class='badge badge-success'>DETAIL</a>";
                    }
                }
            ],
            columnDefs:[
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": false
                },
            ],
            rowId: function(a) {
                return a.userID;
            },
            order: [],
            drawCallback: function(settings){
                _componentSwitchery();
            }
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentDatatableDataSources();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    DatatableDataSources.init();
});

function removeComment(athis){
    var tableID = $(athis).closest('table').attr('id');
    if (tableID == "commentList_table")
        var postCommentID = commentList_table.row($(athis).closest('tr')).data().postCommentID;
    if (tableID == "reportCommentList_table")
        var postCommentID = reportCommentList_table.row($(athis).closest('tr')).data().postCommentID;
    swal({
        title: 'Are you sure?',
        text: 'You want to remove this comment.?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, remove it!',
    }).then(function(isConfirm) {
        if(isConfirm.value === true){
            $.ajax({
                url:base_url+"moderator/comment/removeComment",
                type:"POST",
                data:{"postCommentID":postCommentID},
                success: function(data){
                    if(data.code === 101){
                        swal(data.message, 'error');
                    }else{
                        if(commentList_table.ajax.reload(null,false)){
                            reportCommentList_table.ajax.reload(null,false);
                        }
                    }
                },
                error: function(data){
                    console.log(data);
                }
            });
        }
    });
}